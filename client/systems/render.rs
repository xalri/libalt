use glium::*;

use esc::storage::*;
use esc::render::*;
use esc::geom::*;
use esc::util::time::*;
use esc::util::hprof;

use model::storage::*;
use altx::*;

pub struct RenderSystem {
   storage: *mut Storage,
   view_bounds: Bounds,
   debug: DebugDraw,
   queue: DrawQueue,
   prev_frame: u64,
   pub follow: Ptr<Transform>,
   pub debug_mode: bool,
}

impl RenderSystem {
   pub fn new(storage: *mut Storage, res: &AtlasSource) -> RenderSystem {
      RenderSystem {
         storage,
         view_bounds: bounds((0.0, 1280.0), (0.0, 720.0)),
         debug: DebugDraw::new(&**res),
         queue: DrawQueue::new(),
         prev_frame: precise_time_ns(),
         follow: Ptr::null(),
         debug_mode: true,
      }
   }

   pub fn draw(&mut self, frame: &mut (impl Surface + 'static), t: f32, params: DrawParameters, res: &AtlasSource) {
      let storage = unsafe{ &mut *self.storage };

      let elapsed = ns_to_ms(precise_time_ns() - self.prev_frame) as f32;
      self.prev_frame = precise_time_ns();

      frame.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
      let mut params: DrawParameters<'static> = unsafe{ ::std::mem::transmute(params) };
      params.blend = Blend::alpha_blending();

      let ui_view = Mat3::view(self.view_bounds);

      let pos = if !self.follow.is_null() {
         self.follow.interpolate(t).position
      } else {
         vec2(0.0, 0.0)
      };

      let world_view = Mat3::flip_y() * ui_view * Mat3::translate(-pos + self.view_bounds.center());

      let _frame: *mut _ = frame;
      let _res: *const _ = res;

      for mut s in storage.sprites.ref_iter() {
         s.update_anim(elapsed);
         let params = params.clone();
         let depth = s.depth.to_u32();
         self.queue.add(depth, move || s.draw(unsafe { &mut *_frame }, unsafe { &*_res }, world_view, params, t));
      }
      self.queue.run();

      if self.debug_mode {
         for body in storage.bodies.iter() {
            self.debug.draw_body(frame, body, world_view, params.clone(), t);
         }

         let mut s = String::new();
         hprof::profiler().print_timing(&mut s);
         self.debug.draw_text(frame, &s, ui_view * Mat3::scale_uniform(0.9), params.clone());
      }
   }
}
