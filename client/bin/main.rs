#![feature(rust_2018_preview)]
#![feature(integer_atomics)]

extern crate glium;
extern crate esc;
extern crate model;
extern crate altx;
extern crate client;
extern crate proto;
extern crate simple_logger;
extern crate log;
extern crate failure;

use std::net::*;
use std::time::*;
use std::sync::Arc;
use std::sync::atomic::*;
use std::io;
use std::thread;
use log::*;
use glium::*;
use failure::*;

use esc::resources::*;
use esc::geom::*;
use esc::storage::*;
use esc::error::*;
use esc::window::*;
use esc::input::*;
use esc::util::time::*;
use esc::util::hprof;
use esc::net::socket::{*, select};

use altx::*;
use model::components::*;
use model::team::*;
use model::events::*;
use client::systems::*;
use client::input::*;
use client::state::*;
use client::join::*;
use proto::Version;
use proto::*;

pub enum GameState {
   Joining(JoinServer),
   Playing(Playing),
}

pub struct Playing {
   plane: Ptr<Plane>,
   render: RenderSystem,
   state: ClientState,
}

impl Playing {
   pub fn new(res: &AtlasSource, window: &mut Window, mut state: ClientState) -> Playing {
      // Spawn a plane
      let init = ComponentInit::Loopy(LoopyInit {
         id: state.reserve(),
         common: PlaneInit {
            id: state.reserve(),
            team: Team::Yellow,
            position: vec2(500.0, 1000.0),
            orientation: 0.0,
            player: state.player_no,
            item: HeldItem::None,
         }
      });
      let loopy = state.create_component::<Loopy>(init, res);
      let mut plane = loopy.plane.clone();

      // Set the plane's input map & connect it to the window.
      let input_map = PlaneInputMap { transform: plane.body.t.clone(), config: PlaneInputConfig::default() };
      plane.control.replace_map(input_map);
      plane.control.connect(window, 0);

      state.owned_entities.push(plane.weak().any());

      let mut render = RenderSystem::new(&mut *state.world.storage, res);
      render.follow = plane.body.t.clone();

      Playing { plane, render, state }
   }

   pub fn tick(&mut self) {
      self.state.tick();
   }

   pub fn handle(&mut self, res: &AtlasSource, msg: Message) {
      match msg.data.unwrap() {
         MessageData::Game(data) => {
            self.state.receive(data);
            while let Some(_ev) = self.state.next_ev(res) {
               // ...
            }
         },
         _ => (),
      }
   }

   pub fn draw(&mut self, frame: &mut (impl Surface + 'static), fraction: f32, params: DrawParameters, res: &AtlasSource) {
      self.render.draw(frame, fraction, params, res);
   }
}


pub fn create_client_socket() -> Socket<MessageData, MessageDataRule> {
   let mut addr: SocketAddr = "0.0.0.0:27280".parse().unwrap();
   for _ in 0..100 {
      match UdpSocket::bind(addr) {
         Ok(socket) => {
            return Socket::from_net_socket(socket, MessageDataRule).unwrap()
         },
         Err(e) => {
            match e.kind() {
               io::ErrorKind::AddrInUse => {
                  let port = addr.port() + 1;
                  addr.set_port(port);
               }
               _ => panic!("Failed to bind socket: {}", e)
            }
         }
      }
   }
   panic!("Failed to find free port in range 27280 - 27380");
}

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();

   let mut window = Window::new("Flight Demo", vec2i(1280, 720));
   //let mut window = Window::new("Flight Demo", vec2i(640, 360));

   let root = Resources::new(ROOT_DIR);
   let res = AtlasSource::new(root.subdir(RESOURCE_DIR)).unwrap();
   let maps = root.subdir(MAP_DIR);

   let server_addr: SocketAddr = "127.0.0.1:27277".parse().unwrap();
   let mut socket = create_client_socket();

   let (out_s, out_r) = unbounded();
   let (in_s, in_r) = unbounded();
   let (tick_s, tick_r) = unbounded();

   let tick_ns = 33_333_333; // length of a tick in nanoseconds

   let mut pings = Pings::new(out_s.clone());
   let last_tick = Arc::new(AtomicU64::new(precise_time_ns()));

   // Start the socket thread
   let _last_tick = last_tick.clone();
   thread::spawn(move || {
      let tick = tick(Duration::new(0, tick_ns));
      pings.get(server_addr); // start pinging the server

      loop {
         select! {
            recv(out_r, msg) => {
               socket.connections.send(msg.unwrap());
            }
            recv(socket.raw_rx, msg) => {
               socket.connections.recv(msg.unwrap());
            }
            recv(socket.incoming_rx, msg) => {
               let msg = msg.expect("Socket closed unexpectedly");
               match msg.data {
                  Some(MessageData::Ping(data)) => pings.process(data, msg.addr),
                  _ => in_s.send(msg),
               }
            }
            recv(tick) => {
               _last_tick.store(precise_time_ns(), Ordering::SeqCst);
               tick_s.send(());
               pings.update();
               socket.connections.update();
            }
         }
      }
   });

   // First, attempt to join a server:
   let mut state = GameState::Joining(
      JoinServer::new(server_addr, out_s.clone(), JoinRequest {
         password: None,
         auth: ClientAuth::None,
         version: Version{ major: 0, minor: 1, patch: 0 },
         id: ClientID {
            id: UUID::default(),
            nickname: "xal".to_string()
         }
      }, &res, &maps));


   // Main game loop
   let mut real_tick = precise_time_ns();
   loop {
      window.poll_events();
      if window.close_requested { break }

      // Handle incoming packets
      while let Some(msg) = in_r.try_recv() {
         state = match state {
            GameState::Joining(join) => {
               match join.handle(msg) {
                  JoinServer::Ready{ state, .. } => {
                     // clear pending ticks
                     while let Some(_) = tick_r.try_recv() {}

                     GameState::Playing(Playing::new(&res, &mut window, state))
                  },
                  joining => GameState::Joining(joining),
               }
            }
            GameState::Playing(mut playing) => {
               playing.handle(&res, msg);
               GameState::Playing(playing)
            }
         };
      }

      // Tick
      let mut count = 0;
      while let Some(_) = tick_r.try_recv() {
         count += 1;
         if count > 1 { continue; }

         real_tick = precise_time_ns();
         hprof::start_frame(); // start profiling
         match state {
            GameState::Joining(ref mut join) => {
               join.tick()
            }
            GameState::Playing(ref mut playing) => {
               playing.tick();
            }
         }
         hprof::end_frame();
      }

      if count > 1 {
         warn!("Can't keep up; skipping (ran 1/{} ticks this frame)", count);
      }

      // Draw the scene
      let mut frame = window.display.draw();
      frame.clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);

      let mut params = DrawParameters::default();
      params.blend = Blend::alpha_blending();
      params.viewport = Some(window.viewport());

      let last_tick = last_tick.load(Ordering::SeqCst);
      let mut fraction = (precise_time_ns() - last_tick) as f32 / tick_ns as f32;
      if last_tick > real_tick { fraction = 1.0; }

      match state {
         GameState::Joining(ref mut _join) => (), // TODO
         GameState::Playing(ref mut playing) => {
            playing.draw(&mut frame, fraction, params, &res);
         }
      }

      frame.finish().unwrap();
   }

   // TODO: disconnect
}
