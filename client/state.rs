
use std::net::SocketAddr;
use std::mem;
use esc::geom::*;
use esc::encode::*;
use esc::storage::*;
use esc::net::id::*;
use esc::net::sequencer::*;
use esc::net::socket::Sender;
use esc::error::*;
use log::*;

use altx::*;
use proto::*;
use model::events::*;
use model::components::*;
use model::state::*;
use model::world::*;

/// Manages world state, synchronising it with some server.
pub struct ClientState {
   server_addr: SocketAddr,
   ids: ClientIDMap,
   map_size: Vec2,
   pub world: World,
   pub player_no: i16,
   pub sequencer: Sequencer<ServerEv, ClientEv>,
   pub owned_entities: Vec<WeakAny>,
   incoming_state: Option<NestedData>,
   sender: Sender<Message>,
}

impl ClientState {
   /// Create a new `StateSync` with the given arena.
   pub fn new(arena: Arena, server_addr: SocketAddr, sender: Sender<Message>) -> ClientState {
      let mut state_sync = ClientState {
         server_addr,
         ids: ClientIDMap::new(),
         map_size: arena.map_size,
         world: World::new(arena),
         sequencer: Sequencer::new(1),
         player_no: -1,
         owned_entities: vec![],
         incoming_state: None,
         sender,
      };
      // Maps network IDs for components in the arena.
      state_sync.handle_world_evs();
      state_sync
   }

   /// Reserve an ID for a new component.
   pub fn reserve<T>(&mut self) -> NetID<T> {
      self.ids.reserve()
   }

   /// Create a new component.
   /// Returns a pointer to the most recently mapped component, which is probably the one just added.
   /// The type parameter T must match the type of initialisation data given.
   pub fn create_component<T>(&mut self, init: ComponentInit, res: &AtlasSource) -> Weak<T> {
      self.world_ev(WorldEv::CreateComponent { init }, res);
      self.ids.last_map.clone().to::<T>()
   }

   /// Handle an incoming game message
   pub fn receive(&mut self, mut message: GameMessage) {
      let up_to_date = self.sequencer.receive(&mut message.events);
      self.incoming_state = if up_to_date { Some(message.state) } else { None };
   }

   /// Take the next incoming server event.
   pub fn next_ev(&mut self, res: &AtlasSource) -> Option<ServerEv> {
      while let Some(ev) = { let rule = unsafe{ self.ev_rule() }; self.sequencer.next_ev(rule) } {
         debug!("an ev {:?}", ev);
         match ev {
            ServerEv::World(world_ev) => self.forward_world_ev(world_ev, res),
            ServerEv::IDMap { local, global } => self.ids.local_to_global(local, global),
            x => return Some(x),
         }
      }
      None
   }

   /// Update the world
   pub fn tick(&mut self) {
      // decode state
      let rule = unsafe{ self.ev_rule() };
      let mut components = vec![];
      read_component_state(&mut self.incoming_state, &mut components, rule).unwrap();
      mem::drop(rule);

      // tick
      for c in &components { c.apply_pre() }
      self.world.tick();
      self.handle_world_evs();
      for c in &components { c.apply_post() }


      // encode outgoing events & component state
      self.owned_entities.retain(WeakAny::is_alive);
      let components: Vec<_> = self.owned_entities.iter().filter_map(|e| get_state(*e)).collect();
      let state = write_component_state(&components, unsafe{ self.ev_rule() }).unwrap();
      let events = self.sequencer.make_msg().unwrap();

      let msg = MessageData::Game(GameMessage { events, state });
      self.sender.send(Message::new(self.server_addr, msg));
   }

   /// Apply the given world event and send it to the server.
   pub fn world_ev(&mut self, ev: WorldEv, res: &AtlasSource) {
      // Cycle the event to quantize floats etc.
      let ev_data = self.encode(&ev).unwrap();
      let ev = self.decode(&ev_data).unwrap();

      self.forward_world_ev(ev.clone(), res);
      self.send_ev(ClientEv::World(ev));
   }

   /// Send an event to the server.
   fn send_ev(&mut self, ev: ClientEv) {
      debug!("Sending event: {:?}", ev);
      let rule = unsafe{ self.ev_rule() };
      self.sequencer.add_event(rule, &ev).unwrap();
   }

   /// Forward an event to the world and handle any model events
   fn forward_world_ev(&mut self, ev: WorldEv, res: &AtlasSource) {
      self.world.handle_event(ev, res);
      self.handle_world_evs();
   }

   /// Encode a world event
   fn encode(&self, ev: &WorldEv) -> Result<Vec<u8>> {
      encode(&unsafe{self.ev_rule()}, ev)
   }

   /// Decode a world event
   fn decode(&self, data: &[u8]) -> Result<WorldEv> {
      decode(&unsafe{self.ev_rule()}, data)
   }

   /// Build a rule for encoding events. `ids` must not be modified while the rule exists.
   unsafe fn ev_rule(&self) -> EventRule {
      EventRule {
         ptr_rule: self.ids.ptr_rule(),
         map_size: self.map_size,
         id_rule: NetIDRule { local_to_global: None },
      }
   }

   /// Handle new internal events from the simulation.
   fn handle_world_evs(&mut self) {
      while let Some(ev) = self.world.outgoing_events.pop() {
         match ev {
            ModelEv::NewComponent { net_id, ptr } => {
               self.ids.map(net_id, ptr)
            }
            ModelEv::PowerupCollide { powerup, plane } => {
               if plane.player == self.player_no {
                  self.send_ev(ClientEv::PowerupCollide {
                     pos: powerup.body.t.position,
                     vel: powerup.body.t.linear_vel + plane.body.t.linear_vel,
                     plane,
                     powerup,
                  });
               }
            }
            _ => (),
         }
      }
   }
}

