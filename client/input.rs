use esc::input::*;
use esc::geom::*;
use esc::storage::*;
use model::components::PlaneControl;

#[derive(Copy, Clone, Debug)]
pub struct PlaneInputConfig {
   pub mouse_control: bool,
   pub throttle_up_key: VirtualKeyCode,
   pub throttle_down_key: VirtualKeyCode,
   pub use_item_key: VirtualKeyCode,
   pub left_key: VirtualKeyCode,
   pub right_key: VirtualKeyCode,
}

/// An input map for plane control.
#[derive(Clone)]
pub struct PlaneInputMap {
   pub config: PlaneInputConfig,
   /// A pointer to the plane's transform, required to calculate the relative turn values when
   /// using mouse control.
   pub transform: Ptr<Transform>,
}

impl InputMap<(), PlaneControl> for PlaneInputMap {
   fn update_state(&self, input: &InputState, state: &mut PlaneControl) {
      let config = &self.config;
      let mouse_angle = (input.mouse.relative_view_pos - vec2(0.5, 0.5)).inv_y().angle();
      let orientation = self.transform.orientation;

      state.throttle_up = input.keyboard.is_pressed(config.throttle_up_key);
      state.throttle_down = input.keyboard.is_pressed(config.throttle_down_key);
      state.use_item = input.keyboard.is_pressed(config.use_item_key);
      state.turn_left = if input.keyboard.is_pressed(config.left_key) { 180.0 } else {
         if config.mouse_control { clamp(wrap_angle(mouse_angle - orientation), 0.0, 180.0) } else { 0.0 }
      };
      state.turn_right = if input.keyboard.is_pressed(config.right_key) { 180.0 } else {
         if config.mouse_control { clamp(-wrap_angle(mouse_angle - orientation), 0.0, 180.0) } else { 0.0 }
      };
   }
}

impl Default for PlaneInputConfig {
   fn default() -> PlaneInputConfig {
      PlaneInputConfig {
         mouse_control: true,
         throttle_up_key: VirtualKeyCode::W,
         throttle_down_key: VirtualKeyCode::S,
         use_item_key: VirtualKeyCode::Space,
         left_key: VirtualKeyCode::A,
         right_key: VirtualKeyCode::D,
      }
   }
}
