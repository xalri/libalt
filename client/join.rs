
use std::net::SocketAddr;
use esc::net::socket::*;
use esc::encode::NestedData;
use esc::resources::*;
use altx::*;
use proto::*;
use model::world::*;
use state::*;

/// State machine for the join handshake
pub enum JoinServer {
   /// Waiting for a response
   RequestSent {
      addr: SocketAddr,
      sender: Sender<Message>,
      res: *const AtlasSource,
      maps: *const Resources,
   },
   /// The server denied our join request
   Denied {
      message: String,
   },
   /// There was an error loading the map
   LoadFailed {
      message: String,
   },
   /// Synchronising with the server's game world.
   /// Ends when the server sends a `SyncComplete` event.
   Sync {
      addr: SocketAddr,
      sender: Sender<Message>,
      state: ClientState,
      res: *const AtlasSource,
   },
   /// Ready to play
   Ready {
      state: ClientState,
      player_no: i16,
   }
}

impl JoinServer {
   /// Start joining a server
   pub fn new(addr: SocketAddr, sender: Sender<Message>, req: JoinRequest, res: &AtlasSource, maps: &Resources) -> JoinServer {
      sender.send(Message::new(addr, MessageData::Join(Join::Request(req))));
      JoinServer::RequestSent { sender, addr, res, maps }
   }

   pub fn tick(&mut self) {
      match self {
         JoinServer::Sync { sender, addr, state, res } => {
            // We need to send empty sequenced messages to request any missing event group
            sender.send(Message::new(*addr, MessageData::Game(GameMessage {
               events: state.sequencer.make_msg().unwrap(),
               state: NestedData::empty(),
            })));
         }
         _ => ()
      }
   }

   /// Handle an incoming message.
   /// For now this blocks to load resources.
   pub fn handle(self, msg: Message) -> JoinServer {
      match self {
         JoinServer::RequestSent { sender, addr, res, maps } => {
            if msg.addr == addr {
               match msg.data() {
                  MessageData::Join(Join::Load{ counter, resource }) => {
                     println!("Join success: {:?}", resource);

                     let _maps = unsafe{ &*maps };
                     let _res = unsafe{ &*res };

                     // TODO: error handling, different versions of maps..
                     let arena = Arena::load(_maps, _res, resource.name.as_str()).unwrap();
                     assert_eq!(arena.map_descriptor.crc32, resource.crc32);
                     assert_eq!(arena.map_descriptor.size, resource.size);

                     let mut state = ClientState::new(arena, addr, sender.clone());

                     sender.send(msg.reply(MessageData::Join(Join::LoadResp { resource: resource.clone() })));
                     JoinServer::Sync { sender, state, res, addr }
                  }
                  // TODO: join failures
                  _ => unimplemented!(),
               }
            } else {
               JoinServer::RequestSent { sender, addr, res, maps }
            }
         },
         JoinServer::Sync { sender, addr, mut state, res } => {
            if msg.addr == addr {
               match msg.data() {
                  MessageData::Game(data) => {
                     let _res = unsafe{ &*res };
                     state.receive(data.clone());
                     if let Some(ServerEv::SyncComplete { player_no }) = state.next_ev(_res) {
                        state.player_no = player_no;
                        JoinServer::Ready { state, player_no }
                     } else {
                        JoinServer::Sync{ sender, addr, state, res }
                     }
                  }
                  // TODO: map changes
                  _ => unimplemented!(),
               }
            } else {
               JoinServer::Sync { sender, addr, state, res }
            }
         },
         x => x
      }
   }
}
