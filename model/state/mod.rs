//! Structures representing changes to the externally modifiable state of the game world.
//!
//! Together with events and the initial arena these structures form a serializable interface to a game world
//! for synchronisation of multiple instances of the world over a network and saving replays to files.
//!
//! We use two types of data, pre-state and post-state. Pre-state is the input state, applied
//! before advancing the simulation, and is created by an external agent. Post-state is the most
//! recent known state given by some authority on a particular component and is applied after
//! advancing the simulation, such that the view of the player is accurate, if maybe slightly out of date.
//! Post-state is used to correct for the non-determinism which comes from different instances of
//! the world applying events and pre-state on different frames.
//!
//! Floats in pre-state & post-state are quantized, so whenever some state is sent over the network
//! the quantised version should be applied to _all_ the simulations, including that which created it.

mod snapshot;

pub use self::snapshot::*;

use log::*;
use esc::encode::*;
use esc::error::*;
use esc::storage::*;
use esc::net::*;
use esc::geom::*;

use events::EventRule;
use components::*;

#[derive(Encode, Debug, Clone)]
#[rule("EventRule")]
pub enum ComponentState {
   Plane {
      #[rule("rule.ptr_rule")]
      id: Weak<Plane>,
      pre: PlaneControl,
      post: PlanePostState,
   }
}

impl ComponentState {
   pub fn apply_pre(&self) {
      match *self {
         ComponentState::Plane { mut id, pre, .. } => {
            id.apply_pre(pre)
         }
      }
   }

   pub fn apply_post(&self) {
      match *self {
         ComponentState::Plane { mut id, post, .. } => {
            id.apply_post(post)
         }
      }
   }
}

pub fn read_component_state(data: &mut Option<NestedData>, components: &mut Vec<ComponentState>, rule: EventRule) -> Result<()> {
   if let Some(state) = data.take() {
      let mut reader = state.into_reader();
      while reader.pos() < state.len as usize {
         components.push(rule.read(&mut reader)?);
         trace!("Got component state {:?}", components.last().unwrap());
      }
   }
   Ok(())
}


pub fn write_component_state(state: &[ComponentState], rule: EventRule) -> Result<NestedData> {
   let mut writer = BitWriter::new();
   for s in state { rule.write(&mut writer, s)?; }
   Ok(NestedData::from_writer(&mut writer))
}

pub fn get_state(entity: WeakAny) -> Option<ComponentState> {
   if let Some(plane) = entity.try_to::<Plane>() {
      return Some(plane.upgrade().state())
   }

   None
}

#[derive(Clone, Copy, Encode, Debug, Default, PartialEquiv)]
#[partial_equiv(remote="Transform")]
#[rule("EventRule")]
pub struct TransformNet {
   #[rule("rule.position()")]
   position: Vec2,
   #[rule("ranged_float(-180.0, 180.0, 1.0)")]
   orientation: f32,
   #[rule("ranged_float(-40.0, 40.0, 5.0)")]
   angular_vel: f32,
   #[rule("ranged_vec(-30.0, 30.0, 5.0)")]
   linear_vel: Vec2,
}
