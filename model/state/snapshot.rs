use esc::encode::*;
use esc::error::*;

use events::*;
use super::*;

/// Recreates the state of a world at any point in time from the initial arena.
/// Used to synchronise new clients and for game replays.
#[derive(Encode, Debug)]
#[rule("EventRule")]
pub struct Snapshot {
   /// Events to initialise new components & delete dead arena components (turrets, bases, etc)
   /// Kept encoded because of component dependencies: events need to be read & applied in sequence
   /// such that the later events can reference components created by previous events.
   #[rule("NestedRule")]
   pub events: NestedData,
   /// The current state of each dynamic component.
   #[rule("NestedRule")]
   pub state: NestedData,
}

/// Un-encoded form of a snapshot
pub struct SyncState {
   pub events: Vec<WorldEv>,
   pub state: Vec<ComponentState>,
}
