
use esc::geom::*;
use esc::storage::*;
use esc::encode::*;
use esc::net::id::*;
use esc::error::*;

use components::*;

/// An event sent to the world from the upper layer of the game, possibly originating from another
/// client/server, to cause some change in the world's state. World events are one of the
/// simulation's main inputs, alongside real-time component state changes.
#[derive(Clone, Debug, Encode)]
#[rule("EventRule")]
pub enum WorldEv {
   /// Create a new component
   CreateComponent {
      /// Initialisation data.
      init: ComponentInit,
   },

   /// Flag a component for deletion.
   RemoveComponent {
      #[rule("rule.ptr_rule")]
      id: RemovableId,
   },

   /// A plane was destroyed
   PlaneDestroyed {
      #[rule("rule.ptr_rule")]
      plane: Weak<Plane>,
   },

   /// A plane picked up a powerup
   PowerupPickup {
      #[rule("rule.ptr_rule")]
      powerup: Weak<PowerupCommon>,
      #[rule("rule.ptr_rule")]
      plane: Weak<Plane>,
      #[rule("rule.position()")]
      pos: Vec2,
      #[rule("ranged_vec(-50.0, 50.0, 5.0)")]
      vel: Vec2,
      #[rule("BitableRule")]
      shatter_container: bool,
   },

   /// Drop a plane's held item
   DropItem {
      #[rule("rule.ptr_rule")]
      plane: Weak<Plane>,
      #[rule("rule.position()")]
      pos: Vec2,
      #[rule("ranged_vec(-50.0, 50.0, 5.0)")]
      vel: Vec2,
   },
}

/// Initialisation data needed for a new component, essentially reified constructors.
/// This plus the post-state of the component must be sufficient to re-create the component
/// at any time in the simulation.
///
/// For components which need to be referenced over the network the initialization data includes
/// a network ID. There may also be network IDs for nested components (e.g. PowerupCommon).
/// When the creation event is executed the world pushes `ModelEv::NewComponent`s giving the mapping
/// between these reserved network IDs and pointers to the actual components.
#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub enum ComponentInit {
   TurretBullet(TurretBulletInit),
   Loopy(LoopyInit),
   WallPowerup(WallPowerupInit),
   ShieldPowerup(ShieldPowerupInit),
   MissilePowerup(MissilePowerupInit),
   HealthPowerup(HealthPowerupInit),
}

/// A pointer to a top-level component which can be removed
#[derive(Clone, Encode, Debug)]
#[rule("PtrRule")]
pub enum RemovableId {
   Plane(Weak<Plane>)
}

/// A Rule for encoding events
#[derive(Copy, Clone)]
pub struct EventRule {
   pub ptr_rule: PtrRule,
   pub id_rule: NetIDRule,
   pub map_size: Vec2,
}

impl EventRule {
   /// Build a rule for encoding a position in the map.
   pub fn position(&self) -> Vec2Rule {
      position_vec(self.map_size, 500.0, 1.0)
   }
}

