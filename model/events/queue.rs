use reffers::rc::Strong;
use std::collections::VecDeque;
use std::iter::*;

/// A shared queue of events.
pub struct Queue<T> { inner: Strong<VecDeque<T>> }

impl<T> Queue<T> {
   pub fn new() -> Queue<T> { Queue { inner: Strong::new(VecDeque::new()) }}

   pub fn pop(&self) -> Option<T> { self.inner.get_mut().pop_front() }

   pub fn push(&self, ev: T) { self.inner.get_mut().push_back(ev) }

   pub fn len(&self) -> usize { self.inner.get().len() }

   pub fn extend<I: IntoIterator<Item=T>>(&self, iter: I) {
      self.inner.get_mut().extend(iter)
   }
}

impl<T> Clone for Queue<T> {
   fn clone(&self) -> Self { Queue{ inner: self.inner.clone() } }
}

impl<T> FromIterator<T> for Queue<T> {
   fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Queue<T> {
      let queue = Queue::new();
      queue.inner.get_mut().extend(iter);
      queue
   }
}
