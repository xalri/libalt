
use esc::storage::*;
use esc::net::id::*;
use components::*;

/// An event created by a `World` during a tick to notify the upper layers of the game of a
/// particular change in the world's state which they may react to.
#[derive(Debug)]
pub enum ModelEv {
   /// A powerup spawner is ready to spawn a new powerup
   PowerupSpawnerTimeout {
      spawner: Weak<PowerupSpawner>,
      kind: PowerupKind,
   },
   /// A turret can shoot at a target
   TurretShoot {
      turret: Weak<Turret>,
      orientation: f32,
      lifetime: i32,
   },
   /// A plane collided with a powerup
   PowerupCollide {
      powerup: Weak<PowerupCommon>,
      plane: Weak<Plane>,
   },
   /// A component has been created; gives the pointer to the component and it's network ID.
   NewComponent {
      net_id: NetID<()>,
      ptr: WeakAny,
   },
}
