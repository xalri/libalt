
use esc::storage::*;
use esc::geom::collide::*;
use esc::geom::contact::*;
use esc::util::hprof;
use components::*;
use events::*;

use storage::*;

pub struct CollisionSystem {
   broad_phase: BroadPhase,
}

impl CollisionSystem {
   pub fn new(storage: &mut Storage) -> CollisionSystem {
      CollisionSystem {
         broad_phase: BroadPhase::new(storage.cache()),
      }
   }

   pub fn run(&mut self, evs: &mut Queue<ModelEv>) {
      let _p = hprof::enter("collision");
      self.broad_phase.update();

      let _p = hprof::enter("narrow-phase");
      for &(mut a, mut b) in &self.broad_phase.pairs {
         let a = &mut *a;
         let b = &mut *b;
         if let Some(contact) = Contact::collide(a, b) {
            if let Some(mut obj) = a.parent.try_to::<Turret>() {
               obj.on_collide(contact, a, b);
               continue
            }

            if let Some(mut obj) = a.parent.try_to::<Plane>() {
               obj.on_collide(contact, a, b);
               continue
            }

            if let Some(mut obj) = a.parent.try_to::<PowerupCommon>() {
               obj.on_collide(contact, a, b, evs);
               continue
            }

            let (res_a, res_b) = contact.resolve(a, b);
            res_a.apply(a);
            res_b.apply(b);
         }
      }
   }
}
