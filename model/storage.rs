use esc::storage::*;
use esc::geom::*;
use esc::render::*;
use esc::util::time::*;

use components::*;

/// Stores components
#[derive(Default, Store)]
pub struct Storage {
   pub bases: Cache<Base>,
   pub turrets: Cache<Turret>,
   pub goals: Cache<Goal>,
   pub geometry: Cache<Geometry>,
   pub powerup_spawners: Cache<PowerupSpawner>,

   pub loopys: Cache<Loopy>,
   pub planes: Cache<Plane>,
   pub turret_bullets: Cache<TurretBullet>,
   pub bullets: Cache<Bullet>,
   pub health_powerups: Cache<HealthPowerup>,
   pub wall_powerups: Cache<WallPowerup>,
   pub shield_powerups: Cache<ShieldPowerup>,
   pub missile_powerups: Cache<MissilePowerup>,
   pub powerup_common: Cache<PowerupCommon>,
   pub powerup_container: Cache<PowerupContainer>,
   pub shattered_powerup_containers: Cache<ShatteredPowerupContainer>,
   pub container_fragments: Cache<ContainerFragment>,

   pub delays: Cache<Delay>,
   pub bodies: Cache<Body>,
   pub sprites: Cache<Sprite>,
   pub materials: Cache<Material>,
   pub polys: Cache<Poly>,
   pub nodes: Cache<SceneNode>,
}