//! A model of the altitude game

#![feature(vec_remove_item)]
#![feature(decl_macro)]
#![feature(type_ascription)]
#![feature(nll)]
#![feature(arbitrary_self_types)]

extern crate derivative;
extern crate log;
extern crate reffers;
extern crate rand;
extern crate esc;
extern crate altx;

pub mod components;
pub mod systems;
pub mod state;
pub mod world;
pub mod events;
pub mod storage;

pub use altx::team;

/// The number of ticks of the simulation per second.
const TICKS_PER_SECOND: u64 = 30;

/// Convert a time in seconds to ticks
pub fn seconds_to_ticks(s: f64) -> f64 {
   s * TICKS_PER_SECOND as f64
}

/// Convert a time in ticks to seconds
pub fn ticks_to_seconds(t: f64) -> f64 {
   t / TICKS_PER_SECOND as f64
}

pub mod collision_id {
   pub static PLANE: u32 = 0b00000001;
   pub static BULLET: u32 = 0b00000010;
   pub static LANDSCAPE: u32 = 0b00000100;
   pub static SHRAPNEL: u32 = 0b00001000;
   pub static POWERUP: u32 = 0b00010000;
   pub static DAMAGEABLE: u32 = 0b00100000;
   pub static SHIELD: u32 = 0b01000000;
   pub static GOAL: u32 = 0b10000000;

   pub static MISC: u32 = 0b100000000;
}

use esc::geom::Material;

pub fn default_material() -> Material {
   Material{
      density: 1.0,
      kinetic_friction: 0.1,
      static_friction: 0.35,
      elasticity: 0.125,
      separation: 0.35,
   }
}

use altx::*;
use esc::storage::*;
use esc::render::*;
use esc::geom::*;
use esc::error::*;

/// Load a body & sprite from a textured poly with shared geometry
pub fn body_sprite(res: &AtlasSource, xml: &xml::TexturedPoly) -> Result<(Ptr<Body>, Ptr<Sprite>)> {
   let tx = xml.texture.as_ref().unwrap();
   let geom = res.insert(xml.hull.to_poly());
   let sprite = _sprite(res, tx, geom.clone())?;
   let mat = res.emplace();
   let body = res.insert(Body::new(geom, res.emplace(), mat, false));
   Ok((body, sprite))
}

pub fn sprite(res: &AtlasSource, xml: &xml::TexturedPoly) -> Result<Ptr<Sprite>> {
   let tx = xml.texture.as_ref().unwrap();
   let geom = res.insert(xml.hull.to_poly());
   _sprite(res, tx, geom)
}

pub fn xml_poly_sprite(res: &AtlasSource, path: &str) -> Result<Ptr<Sprite>> {
   let xml = res.get::<XmlFormat<xml::TexturedPoly>>(path, ())?;
   sprite(res, &*xml)
}

pub fn body(res: &AtlasSource, poly: &xml::Hull) -> Ptr<Body> {
   let poly = res.insert(poly.to_poly());
   let mat = res.emplace();
   res.insert(Body::new(poly, res.emplace(), mat, false))
}

fn _sprite(res: &AtlasSource, tx: &xml::TransformedImage, geom: Ptr<Poly>) -> Result<Ptr<Sprite>> {
   let tx_transform = tx.xform.to_matrix().invert() * geom.aabb().interpolation_matrix().invert();
   let texture = res.load_image(&tx.image.path)?;
   if texture.is_none() {
      return Ok(res.insert(Sprite::empty(res.emplace())))
   }

   let mut sprite = res.insert(Sprite::new_anim([SpriteFrame {
      texture, tx_transform, geom: Some(geom), delay: 0.0
   }], res.emplace()));
   sprite.clip = false;
   Ok(sprite)
}

pub fn image_sprite(res: &AtlasSource, path: &str) -> Result<Ptr<Sprite>> {
   Ok(match res.load_image(path)? {
      Some(tx) => {
         let mut sprite = res.insert(Sprite::new_anim([SpriteFrame {
            tx_transform: Mat3::scale(tx.size()),
            geom: Some(res.insert(tx.poly())),
            texture: Some(tx),
            delay: 0.0,
         }], res.emplace()));
         sprite.clip = false;

         sprite
      }
      None => {
         res.insert(Sprite::empty(res.emplace()))
      }
   })
}
