use esc::encode::*;
use esc::error::*;

pub struct MapDescriptorRule;

/// Describes a map file.
#[derive(Clone, PartialEq, Debug, Encode)]
#[rule("MapDescriptorRule")]
pub struct MapDescriptor {
   /// Map name
   #[rule("StringRule::short()")]
   pub name: String,
   /// CRC32 checksum of the map
   #[rule("BitableRule")]
   pub crc32: u32,
   /// The size of the altx file in bytes
   #[rule("BitableRule")]
   pub size: u32,
}
