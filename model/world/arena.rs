
use std::str::FromStr;
use rand::*;

use esc::storage::*;
use esc::xml::Fragment;
use esc::resources::*;
use esc::error::*;
use esc::geom::*;
use esc::util::time::*;

use altx::*;
use altx::xml;

use super::MapDescriptor;
use storage::*;
use components::*;

/// The static elements which make up a map, used as the initial state of a game world.
pub struct Arena {
   /// The name, size, and checksum of the map archive.
   pub map_descriptor: MapDescriptor,
   /// The dimensions of the game layer
   pub map_size: Vec2,
   /// Obstacles
   pub geometry: Vec<Ptr<Geometry>>,
   /// Throwable-ball receptacles
   pub goals: Vec<Ptr<Goal>>,
   /// Points of increased contention
   pub powerup_spawners: Vec<Ptr<PowerupSpawner>>,
   /// Static offence
   pub turrets: Vec<Ptr<Turret>>,
   /// Bomb receptacles
   pub bases: Vec<Ptr<Base>>,
   /// Contiguous component storage.
   pub storage: Option<Box<Storage>>, // must be dropped last
}

impl Arena {
   /// Load an arena from a file
   pub fn load(maps: &Resources, res: &AtlasSource, path: &str) -> Result<Arena> {
      let altx = maps.get::<AltxFormat>(path, ())?;
      Arena::from_altx(&res, (*altx).clone(), path)
   }

   /// Load an arena from an altx file
   fn from_altx(res: &AtlasSource, altx: Altx, path: &str) -> Result<Arena> {
      let storage = Box::new(Storage::default());

      let alte = altx.files().find(|file| file.path.ends_with(".alte"));
      if alte.is_none() { bail!("No alte found in altx file") }
      let alte = alte.unwrap();
      let level = xml::Level::from_reader(::std::io::Cursor::new(alte.data))?;

      let map_descriptor = MapDescriptor {
         name: path.to_string(),
         size: altx.size() as u32,
         crc32: altx.crc32(),
      };

      let altx_res = AtlasSource::new(Resources::with_source(altx).subdir("_derived/dist"))?;
      let mut arena = Arena::from_level(res, storage, &altx_res, level)?;
      arena.map_descriptor = map_descriptor;
      Ok(arena)
   }

   /// Load an arena from a Level and altx resources.
   fn from_level(res: &AtlasSource, mut _storage: Box<Storage>, altx_res: &AtlasSource, level: xml::Level) -> Result<Arena> {
      use std::ops::DerefMut;
      let storage = unsafe { &mut *(_storage.deref_mut() as *mut Storage) };
      let mut arena = Arena {
         map_descriptor: MapDescriptor { name: "".to_string(), crc32: 0, size: 0 },
         storage: Some(_storage),
         map_size: Vec2::zero(),
         geometry: vec![],
         goals: vec![],
         powerup_spawners: vec![],
         turrets: vec![],
         bases: vec![],
      };

      let default_material = storage.insert(::default_material());
      let mut z_layer = 100;

      for v in &level.views {
         let parallax = v.bounds.size() / v.size.to_vec();
         let is_game_view = v.name == "Game";

         if is_game_view { z_layer = 0; } else { z_layer -= 1; }

         if is_game_view { arena.map_size = v.size.to_vec(); }

         if is_game_view {
            fn update_transform(t: &mut Transform, x: i32, y: i32, c: xml::TransformComponents) {
               *t = make_transform(x, y, c)
            }

            fn make_transform(x: i32, y: i32, c: xml::TransformComponents) -> Transform {
               let mut scale = Vec2::one() * c.scale;
               if c.flip_x { scale.x = -scale.x }
               if c.flip_y { scale.y = -scale.y }
               Transform {
                  position: vec2(x as f32, y as f32),
                  orientation: flip_angle(c.orientation, c.flip_x, c.flip_y),
                  scale,
                  .. Transform::default()
               }
            }

            for o in &v.spawners {
               let mut spawner = PowerupSpawner::new(res, storage, o.team);

               spawner.container.transform.position = vec2(o.x as f32, o.y as f32);
               spawner.delay = Delay::new(::seconds_to_ticks(o.spawn_time as f64 / 10.0) as f32);
               spawner.rotation = o.selected_powerups.iter().map(|s| PowerupKind::from_str(s)).collect::<Result<Vec<_>>>()?;
               thread_rng().shuffle(&mut spawner.rotation);
               spawner.never_fade = o.never_fade;

               if o.spawn_at_round_start { spawner.delay.remaining = 1.0; }

               arena.powerup_spawners.push(spawner);
            }

            for o in &v.turrets {
               let mut turret = Turret::new(res, storage, o.team, o.max_distance, (o.turn_left, o.turn_right));
               turret.update_transform(make_transform(o.x, o.y, o.transformation));
               //turret.set_layer(z_layer);

               turret.team = o.team;
               turret.health = o.health as f32;
               turret.cooldown = Delay::new(::seconds_to_ticks(o.gun_cooldown_seconds as f64) as f32);

               turret.params.barrel_length = 30.0;
               turret.params.max_health = o.health as f32;
               turret.params.bullet_speed = o.bullet_speed;
               turret.params.bullet_damage = o.bullet_direct_damage as f32;
               turret.params.blast_damage = o.bullet_blast_damage as f32;
               turret.params.blast_radius = o.bullet_blast_radius as f32;

               arena.turrets.push(turret);
            }

            for o in &v.bases {
               let mut base = Base::new(res, storage, o.team);
               update_transform(&mut *base.body.t, o.x, o.y, o.transformation);

               base.team = o.team;
               base.layer = o.layer;

               arena.bases.push(base);
            }

            for o in &v.goals {
               let mut goal = Goal::new(res, storage, o.team);
               update_transform(&mut *goal.body.t, o.x, o.y, o.transformation);

               goal.team = o.team;
               goal.layer = o.layer;

               arena.goals.push(goal);
            }
         }

         for g in &v.geometry {
            let transform = storage.insert(Transform {
               position: vec2(g.x as f32, g.y as f32),
               orientation: g.orientation,
               ..Default::default()
            });

            let geom = storage.insert(g.textured_poly.hull.to_poly());

            let visible = g.visible;
            let collidable = g.collidable && is_game_view;

            let body = if collidable {
               let material = storage.insert(Material { elasticity: g.elasticity, .. *default_material });
               let mut body = Body::new(geom.clone(), transform.clone(), material, true);
               body.collision_id = ::collision_id::LANDSCAPE;
               storage.insert(body)
            } else {
               Ptr::null()
            };

            let sprite = if visible {
               let mut sprite = poly_to_sprite(altx_res, &g.textured_poly, transform.clone(), Some(geom.clone()))?;
               sprite.parallax = parallax;
               sprite.depth.layer = z_layer;
               storage.insert(sprite)
            } else {
               Ptr::null()
            };

            arena.geometry.push(storage.insert(::components::Geometry {
               body,
               sprite,
               layer: g.layer,
               dmg_mod: g.damage_modifier
            }));
         }
      }

      Ok(arena)
   }
}