
use esc::storage::*;
use esc::geom::*;
use esc::util::hprof;
use esc::util::time::*;
use log::*;

use altx::*;

use super::*;
use esc::net::id::*;
use components::*;
use storage::*;
use systems::*;
use events::*;
use state::*;

/// A simulation of a game world. All external interaction with the simulation is through
/// serializable quantised events and state synchronisation structures.
pub struct World {
   // Top-level objects in the simulation
   arena: Arena,
   loopys: Vec<Ptr<Loopy>>,
   turret_bullets: Vec<Ptr<TurretBullet>>,
   health_powerups: Vec<Ptr<HealthPowerup>>,
   wall_powerups: Vec<Ptr<WallPowerup>>,
   shield_powerups: Vec<Ptr<ShieldPowerup>>,
   missile_powerups: Vec<Ptr<MissilePowerup>>,
   shattered_powerup_containers: Vec<Ptr<ShatteredPowerupContainer>>,

   /// Events created by the world, to be acted on by some higher-level structure.
   pub outgoing_events: Queue<ModelEv>,
   pub params: WorldParams,
   collision: CollisionSystem,
   // A closure which maps network IDs to pointers (sends ModelEv::NewComponent over outgoing_events).
   _id_map_fn: Box<Fn(WeakAny, NetID<()>)>,
   // component storage, must be dropped last.
   pub storage: Box<Storage>,
}

impl World {
   /// Create a world from an arena.
   pub fn new(mut arena: Arena) -> World {
      let mut storage = arena.storage.take().unwrap();
      storage.iter_mut().for_each(Turret::setup);

      fn build_evs(i: impl Iterator<Item=WeakAny>) -> impl Iterator<Item=ModelEv> {
         i.enumerate().map(|(idx, ptr)| ModelEv::NewComponent { net_id: NetID::new(idx as u16, false), ptr })
      }

      // Create events for the network IDs of entities in the arena
      let outgoing_events: Queue<ModelEv> =
         build_evs(arena.powerup_spawners.iter().map(|p| p.weak().any()))
         .chain(build_evs(arena.turrets.iter().map(|p| p.weak().any())))
         .chain(build_evs(arena.bases.iter().map(|p| p.weak().any())))
         .collect();

      let evs = outgoing_events.clone();
      let _id_map_fn = Box::new(move |ptr, net_id| evs.push(ModelEv::NewComponent { net_id, ptr }) );

      World {
         outgoing_events,
         collision: CollisionSystem::new(&mut *storage),
         storage,
         arena,
         params: WorldParams::default(),
         loopys: vec![],
         turret_bullets: vec![],
         health_powerups: vec![],
         wall_powerups: vec![],
         shield_powerups: vec![],
         missile_powerups: vec![],
         shattered_powerup_containers: vec![],
         _id_map_fn,
      }
   }

   /// Progress the simulation by one tick.
   pub fn tick(&mut self) {
      let _p = hprof::enter("tick");

      let mut evs = self.outgoing_events.clone();
      self.storage.iter_mut().for_each(Delay::tick);
      self.storage.iter_mut().for_each(Plane::tick);
      self.storage.iter_mut().for_each(PowerupCommon::tick);
      self.storage.iter_mut().for_each(Bullet::tick);
      self.storage.iter_mut().for_each(ContainerFragment::tick);
      self.storage.iter_mut().for_each(ShatteredPowerupContainer::tick);
      self.storage.turrets.iter_mut().for_each(|p| p.tick(&mut evs));
      self.storage.powerup_spawners.iter_mut().for_each(|p| p.tick(&mut evs));

      // Remove dead components
      self.turret_bullets.retain(|x| x.common.alive);
      self.health_powerups.retain(|x| x.common.alive);
      self.wall_powerups.retain(|x| x.common.alive);
      self.missile_powerups.retain(|x| x.common.alive);
      self.shield_powerups.retain(|x| x.common.alive);
      self.shattered_powerup_containers.retain(|x| x.alive);
      self.loopys.retain(|x| x.plane.alive);

      // Collision
      self.collision.run(&mut self.outgoing_events);

      self.storage.iter_mut().for_each(Transform::integrate);
   }

   /// Update a component
   pub fn apply_state(&mut self, _state: ComponentState) {
      unimplemented!()
   }

   /// Handle an external event
   pub fn handle_event(&mut self, ev: WorldEv, res: &AtlasSource) {
      match ev {
         WorldEv::CreateComponent { init } => {
            match init {
               ComponentInit::Loopy(init) => { let args = self.args(res, init); self.loopys.push(Loopy::new(args)) },
               ComponentInit::WallPowerup(init) => { let args = self.args(res, init); self.wall_powerups.push(WallPowerup::new(args)) },
               ComponentInit::MissilePowerup(init) => { let args = self.args(res, init); self.missile_powerups.push(MissilePowerup::new(args)) },
               ComponentInit::ShieldPowerup(init) => { let args = self.args(res, init); self.shield_powerups.push(ShieldPowerup::new(args)) },
               ComponentInit::HealthPowerup(init) => { let args = self.args(res, init); self.health_powerups.push(HealthPowerup::new(args)) },
               ComponentInit::TurretBullet(init) => { let args = self.args(res, init); self.turret_bullets.push(TurretBullet::new(args)) },
            }
         },

         WorldEv::RemoveComponent { id } => {
            match id {
               RemovableId::Plane(mut id) => id.alive = false,
            }
         }

         WorldEv::PowerupPickup { mut plane, mut powerup, pos, vel, shatter_container } => {
            if !powerup.alive() || !powerup.alive || !plane.alive() || !plane.alive {
               warn!("Invalid PowerupPickup event");
               return
            }

            if shatter_container {
               let c = ShatteredPowerupContainer::new(&mut *self.storage, res, pos, vel);
               self.shattered_powerup_containers.push(self.storage.insert(c));
            }

            powerup.pickup(plane);

            if let Some(_p) = powerup.parent.try_to::<HealthPowerup>() {
               // TODO: restore health
               powerup.alive = false;
            }

            if let Some(p) = powerup.parent.try_to::<MissilePowerup>() { plane.item = HeldItem::Missile(p); }
            if let Some(p) = powerup.parent.try_to::<ShieldPowerup>() { plane.item = HeldItem::Shield(p); }
            if let Some(p) = powerup.parent.try_to::<WallPowerup>() { plane.item = HeldItem::Wall(p); }
         }

         WorldEv::DropItem { mut plane, pos, vel } => {
            if plane.alive() {
               match plane.item {
                  HeldItem::None => (),
                  HeldItem::Wall(mut wall) => wall.common.on_drop(&*plane, pos, vel),
                  HeldItem::Missile(mut missile) => missile.common.on_drop(&*plane, pos, vel),
                  HeldItem::Shield(mut shield) => shield.common.on_drop(&*plane, pos, vel),
               }
               plane.item = HeldItem::None;
            }
         }
         _ => unimplemented!(),
      }
   }

   /// Create a snapshot from which the world can be recreated.
   pub fn sync_state(&self, ids: PtrRule) -> SyncState {
      // TODO:
      // - generate state for all components; destroyable arena entities must send their state

      let events = self.health_powerups.iter().map(|p| p.creation_ev(ids))
         .chain(self.wall_powerups.iter().map(|p| p.creation_ev(ids)))
         .chain(self.missile_powerups.iter().map(|p| p.creation_ev(ids)))
         .chain(self.shield_powerups.iter().map(|p| p.creation_ev(ids)))
         .chain(self.loopys.iter().map(|p| p.creation_ev(ids)))
         .chain(self.turret_bullets.iter().map(|p| p.creation_ev(ids)))
         .map(|init| WorldEv::CreateComponent{ init })
         .collect();

      SyncState {
         events,
         state: vec![],
      }
   }

   /// Get the `MapDescriptor` for the world's arena
   pub fn map_descriptor(&self) -> MapDescriptor {
      self.arena.map_descriptor.clone()
   }

   /// Create constructor arguments for some component.
   /// Contains pointers, ideally call inside the constructor `SomeComponent::new(self.args(..))`.
   fn args<I>(&mut self, res: &AtlasSource, init: I) -> Args<I> {
      Args::new(init, res, &mut *self.storage, &*self._id_map_fn)
   }
}
