
pub const DEFAULT_GRAVITY: f32 = -0.117;

pub struct WorldParams {
   /// Gravity (pixels/frame^2)
   pub gravity: f32,
   /// Global max ammo modifier
   pub ammo_mod: f32,
   /// Global max health modifier
   pub health_mod: f32,
   /// World view scale
   pub scale: f32,
   /// Plane scale
   pub plane_scale: f32,
   /// Crash damage modifier
   pub crash_dmg: f32,
   /// Elasticity modifier
   pub bounce_mod: f32,
}

impl Default for WorldParams {
   fn default() -> WorldParams {
      WorldParams {
         gravity: DEFAULT_GRAVITY,
         ammo_mod: 1.0,
         health_mod: 1.0,
         scale: 1.0,
         plane_scale: 1.0,
         crash_dmg: 1.0,
         bounce_mod: 1.0,
      }
   }
}