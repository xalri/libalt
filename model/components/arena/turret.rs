
use esc::storage::*;
use esc::geom::*;
use esc::render::*;
use esc::geom::contact::*;
use esc::util::time::*;

use altx::*;

use team::*;
use storage::*;
use events::*;
use components::*;

/// A fixed turret, fires bullets at passing enemy planes.
pub struct Turret {
   pub body: Ptr<Body>,
   pub detection_area: Ptr<Body>,
   pub base_sprite: Ptr<Sprite>,
   pub gun_sprite: Ptr<Sprite>,
   pub team: Team,
   pub layer: i32,
   pub health: f32,
   pub params: TurretParams,
   pub targets: Vec<(Vec2, Vec2)>,
   pub zero_gun_angle: f32,
   pub gun_angle: f32,
   pub cooldown: Delay,
}

/// Tracks nearby enemy planes
pub struct Tracker {
   pub t: Ptr<Transform>,
   pub body: Ptr<Body>,
   pub arc_radius: f32,
   pub arc_offset: f32,
   pub arc_angle: f32,
   pub speed: f32,
   pub in_range: Vec<Weak<Plane>>,
   pub target: Option<TrackingTarget>,
}

pub struct TrackingTarget {
   pub plane: Weak<Plane>,
   pub position: Vec2,
   pub linear_vel: Vec2,
   pub angle: f32,
   pub tti: i32,
}

const TURN_SPEED: f32 = 3.2;

#[derive(Clone, Copy, Default)]
pub struct TurretParams {
   /// The length of the barrel
   pub barrel_length: f32,
   /// How far to the left the turret can turn in degrees.
   pub turn_left: f32,
   /// How far to the right the turret can turn in degrees.
   pub turn_right: f32,
   /// The maximum distance a bullet can travel. Combined with `bullet_speed`,
   /// this determines the time each bullet will exist before exploding.
   pub distance: f32,
   pub max_health: f32,
   /// The speed of fired bullets in pixels per frame.
   pub bullet_speed: f32,
   /// The damage done on a direct hit.
   pub bullet_damage: f32,
   /// The damage done by the blast.
   pub blast_damage: f32,
   /// The radius of the blast.
   pub blast_radius: f32,
}

impl Init for Turret {
   fn init(&mut self, p: Weak<Turret>) {
      self.body.parent = Some(p.any());
      self.detection_area.parent = Some(p.any());
   }
}

impl Turret {
   /// Create a new turret in the given storage, loading textures & geometry from the resource directory.
   pub fn new(res: &AtlasSource, storage: &mut Storage, team: Team, radius: f32, turn: (f32, f32)) -> Ptr<Turret> {
      // Load an archetype, avoids duplication of geometry
      let archetype = res.get_with_fn(|| {
         let base_xml = res.get::<XmlFormat<xml::TexturedPoly>>(".poly/render/map/turret/0_128_5_base.poly", ())?;
         let gun_xml = res.get::<XmlFormat<xml::TexturedPoly>>(".poly/render/map/turret/0_128_5_gun.poly", ())?;
         let (mut body, base_sprite) = ::body_sprite(res, &*base_xml)?;
         let mut gun_sprite = ::sprite(res, &*gun_xml)?;

         body.is_static = true;
         body.update_properties();
         body.collision_id = ::collision_id::LANDSCAPE;
         gun_sprite.offset.position = vec2(0.0, 5.0);

         Ok(Turret { body, base_sprite, gun_sprite, .. Turret::default() })
      }, "archetypes::turret").unwrap();

      // Instantiate the sprites in the storage.
      let transform = storage.insert(Transform::default());

      let body = storage.insert(archetype.body.instance(transform.clone()));
      let mut gun_sprite = storage.insert(archetype.gun_sprite.instance(transform.clone()));
      let mut base_sprite = storage.insert(archetype.base_sprite.instance(transform.clone()));

      // Create the detection poly.
      let r = radius * 2.1;
      let mut detection_area = Body::new(
         storage.insert(Poly::ellipse(10, r, r)),
         storage.insert(Transform::default()),
         storage.insert(::default_material()),
         true);

      detection_area.collision_mask = ::collision_id::PLANE;
      detection_area.collision_id = ::collision_id::MISC;
      let detection_area = storage.insert(detection_area);

      // Load the correct textures for the team.
      base_sprite.frames[0].texture = res.load_image(&format!("render/map/turret/{}_base.png", team.string())).unwrap();
      gun_sprite.frames[0].texture = res.load_image(&format!("render/map/turret/{}_gun.png", team.string())).unwrap();

      let mut turret = storage.insert(Turret { body, base_sprite, gun_sprite, detection_area, team, .. Turret::default() });
      turret.params.turn_left = turn.0;
      turret.params.turn_right = turn.1;
      turret.params.distance = radius;
      turret
   }

   pub fn update_transform(&mut self, t: Transform) {
      *self.body.t = t;
      *self.detection_area.t = t;
      self.detection_area.t.scale = Vec2::one();
   }

   /// The maximum time in ticks a bullet shot from this turret can live for.
   pub fn max_bullet_time(&self) -> i32 {
      self.bullet_time(self.params.distance)
   }

   /// The time-to-live in ticks of a bullet shot from this turret with the
   /// given target distance
   pub fn bullet_time(&self, distance: f32) -> i32 {
      (distance / self.params.bullet_speed + 0.5) as i32
   }

   pub fn tick(&mut self, evs: &mut Queue<ModelEv>) {
      self.cooldown.tick();

      if let Some((angle, lifetime)) = self._track() {
         if self.cooldown.expired {
            self.cooldown.reset();

            evs.push(ModelEv::TurretShoot {
               turret: self.body.parent.to::<Turret>(),
               orientation: angle,
               lifetime,
            })
         }
      }

      self.targets.clear();
   }

   pub fn setup(&mut self) {
      self.zero_gun_angle = self.gun_sprite.get_matrix(0.0).apply_vec(vec2(0.0, 1.0)).angle();
      self.gun_angle = self.zero_gun_angle;
   }

   /// Handle a collision with the turret's detection radius.
   pub fn on_collide(&mut self, _c: Contact, _a: &mut Body, b: &mut Body) {
      if let Some(plane) = b.parent.try_to::<Plane>() {
         if plane.team != self.team {
            self.targets.push((b.t.position, b.t.linear_vel))
         }
      }
   }

   /// The world-space position of the gun barrel
   pub fn gun_origin(&self) -> Vec2 {
      let gun_offset = self.gun_sprite.get_matrix(0.0).apply_vec(vec2(0.0, self.params.barrel_length));
      self.body.t.position + gun_offset
   }

   /// The world-space position of the end of the barrel with the given orientation
   pub fn bullet_origin(&self, orientation: f32) -> Vec2 {
      let mut s = self.gun_sprite.clone();
      let tmp = s.offset.orientation;
      s.offset.orientation = self.sprite_angle(orientation - self.zero_gun_angle);
      let origin = self.gun_origin();
      s.offset.orientation = tmp;
      origin
   }

   pub fn bullet_velocity(&self, orientation: f32) -> Vec2 {
      let speed = self.params.bullet_speed;
      Vec2::from_angle(orientation) * speed
   }

   /// Move the gun towards the closest target, returns the angle & time to impact when ready to fire.
   fn _track(&mut self) -> Option<(f32, i32)> {
      let angle_offset = self.zero_gun_angle;
      let track_from = self.gun_origin();

      // The square of the distance to the closest target
      let mut closest = (self.params.distance + 200.0).powi(2);
      let mut target = None;

      for &(t_pos, t_vel) in &self.targets {
         let dist_sq = (track_from - t_pos).length_sq();
         if dist_sq < closest {
            if let Some((angle, tti)) = track(track_from, self.params.bullet_speed, t_pos, t_vel) {
               let relative_angle = wrap_angle(angle - angle_offset);

               if tti > self.max_bullet_time() as f32 { continue }
               if relative_angle > self.params.turn_left || relative_angle < -self.params.turn_right { continue }

               closest = dist_sq;
               // return the angle from the gun's zero position.
               target = Some((relative_angle, tti));
            }
         }
      }

      let mut has_target = false;
      let mut bullet_lifetime = 0;
      let mut diff;

      if let Some((angle, tti)) = target {
         let target_distance = clamp(tti * self.params.bullet_speed, 0.0, self.params.distance);
         bullet_lifetime = (target_distance / self.params.bullet_speed).ceil() as i32;
         diff = angle_diff(angle, self.gun_angle);
         has_target = true;
      } else {
         diff = -self.gun_angle;
      }

      if diff.abs() > TURN_SPEED {
         has_target = false;
         diff = clamp(diff, -TURN_SPEED, TURN_SPEED);
      }

      self.gun_angle = wrap_angle(self.gun_angle + diff);
      self.gun_sprite.offset.orientation = self.sprite_angle(self.gun_angle);

      if has_target {
         Some((self.gun_angle + angle_offset, bullet_lifetime))
      } else {
         None
      }
   }

   /// Get the sprite offset angle from a turret-space orientation
   fn sprite_angle(&self, x: f32) -> f32 {
      flip_angle(x, self.gun_sprite.transform.scale.x < 0.0, self.gun_sprite.transform.scale.y < 0.0)
   }
}

/// Calculate the angle and time to impact to hit a target from a
/// given position and speed.
pub fn track(
   pos: Vec2,
   speed: f32,
   target_pos: Vec2,
   target_vel: Vec2) -> Option<(f32, f32)>
{
   let v = target_vel; let p = target_pos; let q = pos; let s = speed;

   let diff = p - q;
   let gain_sq = v.length_sq() - s * s;

   // |(p + t*v) - q| = t*s, solve to get two possible values of t:
   let (t_a, t_b) = if gain_sq == 0.0 {
      let a = diff.length_sq();
      let b = (s * s - v.y * v.y).sqrt();
      let c = v.y * q.y - v.y * p.y;

      (a / (2.0 * (b * -diff.x + c)), a / (2.0 * (b *  diff.x + c)))
   } else {
      let a = v.x * -diff.x + v.y * -diff.y;
      let b = (-2.0 * a).powi(2) - 4.0 * gain_sq * diff.length_sq();

      ((0.5 * b.sqrt() + a) / gain_sq, (0.5 * -b.sqrt() + a) / gain_sq)
   };

   // If one solution is negative then use the other. Otherwise use the smallest.
   let time = if t_a < 0.0 { t_b } else if t_b < 0.0 { t_a } else { t_b.min(t_a) };
   if time < 0.0 { return None; }

   let angle = f32::atan2(diff.y + v.y * time, diff.x + v.x * time).to_degrees();
   Some((angle, time))
}

#[test] fn test_track() {
   assert_eq!(Some((0.0, 1.0)), track(vec2(0.0, 0.0), 2.0, vec2(2.0, 0.0), vec2(0.0, 0.0)));
   assert_eq!(Some((0.0, 2.0)), track(vec2(0.0, 0.0), 2.0, vec2(2.0, 0.0), vec2(1.0, 0.0)));
   assert_eq!(Some((0.0, 2.0)), track(vec2(0.0, 0.0), 1.0, vec2(2.0, 2.0), vec2(0.0, -1.0)));
}

