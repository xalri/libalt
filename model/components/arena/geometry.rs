
use esc::storage::*;
use esc::geom::*;
use esc::render::sprite::*;

/// An arbitrary static object in a map, makes up a map's obstacles and borders.
#[derive(Debug)]
pub struct Geometry {
   pub body: Ptr<Body>,
   pub sprite: Ptr<Sprite>,
   /// The layer on which the geometry is drawn
   pub layer: i32,
   /// The damage multiplier for collisions with the geometry.
   pub dmg_mod: f32,
}
