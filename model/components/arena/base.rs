
use esc::storage::*;
use esc::geom::contact::*;
use esc::geom::*;
use esc::render::*;
use altx::*;
use storage::*;
use team::*;

pub struct Base {
   pub body: Ptr<Body>,
   pub sprite: Ptr<Sprite>,
   pub under: Ptr<Sprite>,
   pub team: Team,
   pub layer: i32,
   pub runway_idx: isize,
}

impl Base {
   pub fn new(res: &AtlasSource, storage: &mut Storage, team: Team) -> Ptr<Base> {
      let archetype = res.get_with_fn(|| {
         let xml = res.get::<XmlFormat<xml::TexturedPoly>>(".poly/map/base/base.poly", ())?;
         let (mut body, mut sprite) = ::body_sprite(res, &*xml)?;
         let mut under = res.insert((*sprite).clone());
         body.collision_id = ::collision_id::LANDSCAPE;
         body.is_static = true;
         body.update_properties();

         sprite.depth.object = -1;
         under.depth.object = 1;
         under.hsv.value = 0.05;
         under.hsv.saturation = -0.2;
         under.offset.position = vec2(37.0, 49.0);

         let runway_idx = body.hull.longest_segment();

         Ok(Base { body, sprite, under, team: Team::Spectator, layer: 10, runway_idx })
      }, "altx::archetype::base").unwrap();

      // Instance the archetype in the given storage
      let transform = storage.insert(Transform::default());

      let body = storage.insert(archetype.body.instance(transform.clone()));
      let mut sprite = storage.insert(archetype.sprite.instance(transform.clone()));
      let mut under = storage.insert(archetype.under.instance(transform.clone()));

      sprite.frames[0].texture = res.load_image(&format!("render/map/base/{}_front.png", team.string())).unwrap();
      under.frames[0].texture = res.load_image(&format!("render/map/base/{}_back.png", team.string())).unwrap();

      let mut base = storage.insert(Base { body, sprite, under, team: archetype.team, layer: archetype.layer, runway_idx: archetype.runway_idx });
      base.body.parent = base.weak().any();
      base
   }

   /// Get the world space segment representing the runway
   pub fn runway(&self) -> (Vec2, Vec2) {
      let m = self.body.t.to_matrix();
      (m * self.body.hull[self.runway_idx], m * self.body.hull[self.runway_idx + 1])
   }

   /// Test if a collision is valid for landing on the base
   pub fn is_landing(&self, c: Contact, a: &Body) -> bool {
      let runway = self.runway();
      // Distance between plane contact point & runway
      let dist = segment_point_distance(c.contact_a, runway);
      if dist > 5.0 { return false }

      let facing = Vec2::from_angle(a.t.orientation);
      let tangent = (runway.1 - runway.0).normalize();
      let normal_speed = a.t.linear_vel.dot(tangent.perpendicular()).abs();

      facing.dot(tangent).abs() > 0.74 && normal_speed < 4.0
   }
}