
use esc::storage::*;
use esc::geom::*;
use esc::render::*;
use altx::*;
use team::*;
use storage::*;

/// A goal in the planeball game mode
pub struct Goal {
   pub body: Ptr<Body>,
   pub sprite: Ptr<Sprite>,
   pub net_body: Ptr<Body>,
   pub team: Team,
   pub layer: i32,
}

impl Goal {
   pub fn new(res: &AtlasSource, storage: &mut Storage, team: Team) -> Ptr<Goal> {
      // Load the archetype, an instance of the object in the resources storage with
      // null transforms.
      let archetype = res.get_with_fn(|| {
         let goal_xml = res.get::<XmlFormat<xml::TexturedPoly>>(".poly/map/goal/goal.poly", ())?;
         let net_xml = res.get::<XmlFormat<xml::TexturedPoly>>(".poly/map/goal/net.poly", ())?;
         let (mut body, sprite) = ::body_sprite(res, &*goal_xml)?;
         let mut net_body = ::body(res, &net_xml.hull);

         body.is_static = true;
         body.update_properties();
         body.collision_id = ::collision_id::LANDSCAPE;
         net_body.collision_id = ::collision_id::GOAL;

         Ok(Goal { body, sprite, net_body, team: Team::Spectator, layer: 10 })
      }, "altx::archetype::goal").unwrap();

      // Instance the archetype in the given storage
      let node = storage.insert(SceneNode::default());

      let body = storage.insert(archetype.body.instance(node.clone()));
      let net_body = storage.insert(archetype.net_body.instance(node.clone()));
      let mut sprite = storage.insert(archetype.sprite.instance(node.clone()));

      sprite.frames[0].texture = res.load_image(&format!("render/map/goal/{}.png", team.string())).unwrap();

      storage.insert(Goal { body, sprite, net_body, team: archetype.team, layer: archetype.layer })
   }
}