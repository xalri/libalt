
use esc::storage::*;
use esc::geom::*;
use esc::util::time::*;
use altx::*;

use components::*;
use team::*;
use events::*;
use storage::*;

/// Periodically spawns powerups.
#[derive(Clone)]
pub struct PowerupSpawner {
   pub container: Ptr<PowerupContainer>,
   /// The fully-spawned powerup sitting on this spawner.
   pub complete: Option<Weak<PowerupCommon>>,
   /// The order in which to spawn different powerup types
   pub rotation: Vec<PowerupKind>,
   /// The index in the rotation list of the spawning powerup.
   pub rotation_idx: usize,
   /// The delay before a new powerup is spawned.
   pub delay: Delay,
   /// Restricts the spawned powerups to a specific team.
   pub team: Team,
   /// Prevents the spawned powerups from fading
   pub never_fade: bool,
   me: Option<Weak<PowerupSpawner>>,
}

impl Init for PowerupSpawner {
   fn init(&mut self, me: Weak<PowerupSpawner>) {
      self.me = Some(me);
   }
}

impl PowerupSpawner {
   pub fn new(res: &AtlasSource, storage: &mut Storage, team: Team) -> Ptr<PowerupSpawner> {
      let container = PowerupContainer::new(res, storage, team);
      storage.insert(PowerupSpawner {
         container,
         complete: None,
         rotation: vec![],
         rotation_idx: 0,
         delay: Delay::new(0.0),
         team,
         never_fade: true,
         me: None,
      })
   }

   pub fn on_spawn(&mut self, powerup: Weak<PowerupCommon>) {
      self.complete = Some(powerup);
      self.rotation_idx = wrap_max(self.rotation_idx + 1, self.rotation.len());
      self.delay.reset();
   }

   pub fn tick(&mut self, evs: &mut Queue<ModelEv>) {
      let mut alpha = 0.0;

      if let Some(powerup) = self.complete {
         if !powerup.alive() || !(*powerup).alive {
            self.complete = None;
            let _expired = self.delay.expired;
            self.delay.tick();

            if !_expired && self.delay.expired {
               evs.push(ModelEv::PowerupSpawnerTimeout {
                  spawner: self.me.unwrap(),
                  kind: self.rotation[self.rotation_idx],
               });
            }

            alpha = self.delay.fraction_expired().powi(3);
         }
      }

      self.container.set_alpha(alpha);
   }
}
