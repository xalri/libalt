use esc::storage::*;
use esc::net::id::*;
use altx::*;
use storage::*;

/// Arguments for a component constructor.
pub struct Args<I> {
   pub init: I,
   res: *const AtlasSource,
   storage: *mut Storage,
   map_component_id: *const Fn(WeakAny, NetID<()>),
}

impl<I> Args<I> {
   pub fn new(init: I, res: *const AtlasSource, storage: *mut Storage, map_component_id: *const Fn(WeakAny, NetID<()>)) -> Args<I> {
      Args {init, res, storage, map_component_id }
   }

   /// Associate a network ID with a pointer to the newly created component.
   pub fn map_component_id<C>(&self, ptr: &Ptr<C>, net_id: NetID<C>) {
      let f = unsafe{ &*self.map_component_id };
      f(ptr.weak().any(), NetID::new(net_id.id, net_id.sender_local))
   }

   /// Replace the `init` field with another for constructing a child component
   pub fn map<J>(&self, init: J) -> Args<J> {
      Args {
         res: self.res,
         storage: self.storage,
         map_component_id: self.map_component_id,
         init
      }
   }

   /// Get a reference to the resource manager
   pub fn res(&self) -> &AtlasSource {
      unsafe{ &*self.res }
   }

   /// Get a reference to the component storage
   pub fn storage(&self) -> &mut Storage {
      unsafe{ &mut *self.storage }
   }
}
