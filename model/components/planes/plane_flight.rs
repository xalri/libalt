use derivative::*;
use esc::storage::*;
use esc::geom::contact::Contact;
use esc::geom::*;
use esc::encode::*;
use esc::error::*;
use esc::net::*;

use events::EventRule;

pub const MIN_SPEED: f32 = 2.45;
pub const RECOVER_SPEED: f32 = 4.24;
pub const LINEAR_MOMENTUM_SCALE: f32 = 0.953;
pub const ANGULAR_MOMENTUM_SCALE: f32 = 0.8;

/// The dynamics of a plane in flight.
/// Thou shalt not directly modify a plane's velocity outside of collision response.
/// Use `add_linear_vel(..)` and `add_angular_vel(..)` instead.
#[derive(Debug, Derivative)]
#[derivative(Default)]
pub struct PlaneFlight {
   #[derivative(Default(value="true"))]
   pub active: bool,
   pub control: FlightControl,
   pub params: FlightParams,
   pub env: FlightEnv,
   #[derivative(Default(value="Ptr::null()"))]
   pub body: Ptr<Body>,
   pub pos_prev: Vec2,
   /// Engine throttle, 0-1000
   #[derivative(Default(value="1000.0"))]
   pub throttle: f32,
   /// Kinetic energy, determines forward velocity in flight
   pub energy: f32,
   pub stalled: bool,
   /// The component of angular velocity from the plane's control surfaces.
   pub turn: f32,
   /// The fraction of the maximum turn rate.
   pub relative_turn: f32,
   /// Linear velocity from external forces.
   pub linear_vel: Vec2,
   pub linear_vel_prev: Vec2,
   /// Angular velocity from external forces.
   pub angular_vel: f32,

   // Stored velocities from before collision resolution, so we can update energy
   // for forces added by other objects (thermobarics, etc) during collision
   pub pre_collision_vel: Vec2,
   pub pre_collision_angular_vel: f32,
}

/// The encodable subset of PlaneFlight used for synchronization & replays
#[derive(Clone, Copy, Encode, Debug, Default, PartialEquiv)]
#[partial_equiv(remote="PlaneFlight")]
#[rule("EventRule")]
pub struct PlaneFlightNet {
   #[rule("rule.position()")]
   pub pos_prev: Vec2,
   #[rule("ranged_float(0.0, 1000.0, 0.5)")]
   pub throttle: f32,
   #[rule("BitableRule")]
   pub stalled: bool,
   #[rule("ranged_float(-40.0, 40.0, 5.0)")]
   pub turn: f32,
   #[rule("ranged_vec(-30.0, 30.0, 5.0)")]
   pub linear_vel: Vec2,
   #[rule("ranged_vec(-30.0, 30.0, 5.0)")]
   pub linear_vel_prev: Vec2,
   #[rule("ranged_float(-40.0, 40.0, 5.0)")]
   pub angular_vel: f32,
}

/// The control state of a flying object.
#[derive(Debug, Clone, Copy, Default)]
pub struct FlightControl {
   pub throttle_diff: f32,
   pub turn: f32,
   pub turn_clamp: f32,
}

/// Flight parameters.
#[derive(Debug, Clone, Copy, Default)]
pub struct FlightParams {
   pub max_turn: f32,
   pub engine_speed: f32,
   pub acceleration: f32,
   pub turn_penalty: f32,
}

/// Parameters of the environment in which a plane is flying.
#[derive(Clone, Copy, Debug, Default)]
pub struct FlightEnv {
   pub gravity: f32,
}

impl PlaneFlight {
   /// Create a flight component attached to the given body.
   pub fn new(body: Ptr<Body>) -> PlaneFlight {
      PlaneFlight { pos_prev: body.t.position, body, .. Default::default() }
   }

   /// Ready the plane for flight. Starts at 100% throttle & maximum energy
   pub fn initialize(&mut self, engine_speed: f32) {
      self.body.t.linear_vel = Vec2::zero();
      self.body.t.angular_vel = 0.0;
      self.linear_vel = Vec2::zero();
      self.angular_vel = 0.0;
      self.stalled = false;
      self.throttle = 1000.0;
      self.energy = engine_speed * engine_speed;
   }

   /// Update flight with the current environment, parameters, and control state.
   pub fn fly(&mut self) {
      if !self.active { return }

      // Update angular vel
      self.turn = if self.turn.signum() == self.control.turn.signum() {
         self.turn + self.control.turn
      } else {
         self.control.turn
      };
      self.turn = clamp(self.turn, -self.control.turn_clamp, self.control.turn_clamp);
      self.turn = clamp(self.turn, -self.params.max_turn, self.params.max_turn);

      self.relative_turn = self.turn.abs() / self.params.max_turn;
      assert!(self.relative_turn.abs() <= 1.0);

      // Sum the angular velocities due to the plane's control surfaces and from external forces.
      self.body.t.angular_vel = self.turn + self.angular_vel;

      if self.angular_vel < 0.1 { self.angular_vel = 0.0; } else { self.angular_vel *= ANGULAR_MOMENTUM_SCALE }

      // Update throttle
      self.throttle = clamp(self.throttle + self.control.throttle_diff, 0.0, 1000.0);
      if self.stalled { self.throttle = 1000.0; }

      // Update velocity

      if self.stalled {
         self.body.t.linear_vel = self.linear_vel;
      } else {
         let speed = self.energy.sqrt() * self.relative_throttle();
         let relative_speed = speed / self.params.engine_speed;

         // Drift
         if self.relative_turn > 0.5 && relative_speed > 0.65 {
            let turn_mult = (self.relative_turn - 0.5) * 2.0;
            let speed_mult = -(13.0 / 27.0) + f32::min(2.0, relative_speed) * (20.0 / 27.0);
            // TODO: this doesn't feel right
            //let c = 0.55 * turn_mult * speed_mult;
            let c = 0.15 * turn_mult * speed_mult;

            let t = ((self.body.t.linear_vel - self.linear_vel_prev) - self.forward(speed)) * c;
            self.add_linear_vel(t);
         }

         // Combine the forward velocity from the engine with velocity from other sources.
         self.body.t.linear_vel = self.forward(speed) + self.linear_vel;
      }
      self.linear_vel_prev = self.linear_vel;

      // Stalling and recovery:

      let throttle = self.relative_throttle();

      if !self.stalled && self.forward_speed() < MIN_SPEED {
         self.stalled = true;
      } else if self.stalled && self.forward_speed() * throttle > RECOVER_SPEED {
         self.stalled = false;
         self.energy = self.forward_speed() * self.forward_speed();
         self.linear_vel -= self.forward(self.forward_speed() * throttle);
      }

      // Update energy:

      // Loose energy for hard turns
      if self.relative_turn > 0.5 { self.energy *= self.params.turn_penalty; }

      // Gravitational energy
      self.energy += -2.0 * self.env.gravity * (self.pos_prev.y - self.body.t.position.y) as f32;

      // Minimum energy
      self.energy = f32::max(self.energy, 0.1);

      let engine_speed_sq = self.params.engine_speed * self.params.engine_speed;
      let forward_speed = self.forward_speed();

      fn drag(speed_sq: f32, threshold_sq: f32) -> f32 {
         let drag = (1.0 - (0.0019 * (speed_sq as f64 / threshold_sq as f64).powf(2.5))) as f32;
         assert!(drag <= 1.0);
         drag
      }

      if self.stalled {
         self.linear_vel.y += self.env.gravity;

         // Apply drag to fast-moving stalled planes (e.g. when using reverse thrust)
         if self.linear_vel.length_sq() > engine_speed_sq {
            let speed_sq = self.linear_vel.length_sq();
            self.linear_vel *= drag(speed_sq, engine_speed_sq);
         }
      } else {
         if self.linear_vel.length_sq() < 0.01 {
            self.linear_vel = vec2(0.0, 0.0);
         } else {
            self.linear_vel *= LINEAR_MOMENTUM_SCALE;
         }

         if self.energy < engine_speed_sq && forward_speed < self.params.engine_speed {
            self.energy += self.params.acceleration;
         } else if self.energy > 1.005 * engine_speed_sq || forward_speed > self.params.engine_speed {
            let drag = drag(self.energy, engine_speed_sq);

            self.energy = f32::max(self.energy * drag * drag, 0.1);
         }
      }

      self.pos_prev = self.body.t.position;
   }

   /// Resolve a collision
   pub fn resolve_collision(&mut self, contact: Contact, mut body: Weak<Body>) {
      let (res_a, res_b) = contact.resolve(&*self.body, &*body);
      res_a.apply(&mut *self.body);
      res_b.apply(&mut *body);
   }

   /// Stores the body's velocity before collision response, to handle changes in
   /// a way consistent with the energy model.
   ///
   /// During collision response some well-meaning components might try to
   /// alter our velocity, for example a thermobaric cloud may want to push
   /// us into a wall. We're concerned with more than just velocity, so we
   /// capture the changes they try to make and instead apply them ourselves,
   /// veering towards that wall while also stalling our engine.
   pub fn pre_collide(&mut self) {
      self.pre_collision_vel = self.body.t.linear_vel;
      self.pre_collision_angular_vel = self.body.t.angular_vel;
   }

   /// Handle any changes to the plane's velocity during collision response.
   pub fn post_collide(&mut self) {
      self.add_linear_vel(self.body.t.linear_vel - self.pre_collision_vel);
      self.add_angular_vel(self.body.t.angular_vel - self.pre_collision_angular_vel);
      self.body.t.linear_vel = self.pre_collision_vel;
      self.body.t.angular_vel = self.pre_collision_angular_vel;
   }

   /// The fraction of maximum throttle
   fn relative_throttle(&self) -> f32 {
      self.throttle / 1000.0
   }

   /// The speed of the plane in the direction it's facing
   pub fn forward_speed(&self) -> f32 {
      self.body.t.linear_vel.x * self.angle().cos() + self.body.t.linear_vel.y * self.angle().sin()
   }

   /// Create a forward vector with the given magnitude
   pub fn forward(&self, value: f32) -> Vec2 {
      vec2(value * self.angle().cos(), value * self.angle().sin())
   }

   /// Get the plane's angle in radians
   pub fn angle(&self) -> f32 {
      (self.body.t.orientation).to_radians()
   }

   /// Add some external rotational velocity to the plane
   pub fn add_angular_vel(&mut self, a: f32) {
      self.angular_vel += a;
   }

   /// Add some external linear velocity to the plane, updating energy to match.
   pub fn add_linear_vel(&mut self, mut force: Vec2) {
      if !self.stalled {
         // Redirect the forward component of the force to modifying engine energy.
         let diff = force.x * self.angle().cos() + force.y * self.angle().sin();
         let new_speed = self.energy.sqrt() + diff;
         if new_speed > 0.0 {
            force -= self.forward(diff);
            self.energy = new_speed * new_speed;
         } else {
            force += self.forward(self.energy.sqrt());
            self.energy = 0.0;
         }
      }

      self.linear_vel += force;
   }

   /// Add some speed in the direction we're facing
   pub fn add_forward_speed(&mut self, force: f32) {
      let angle = self.angle();
      self.add_linear_vel(vec2(force * angle.cos(), force * angle.sin()));
   }
}
