//! The Plane entity and it's associated types.

use esc::storage::*;
use esc::geom::*;
use esc::geom::contact::*;
use esc::util::time::*;
use esc::render::*;
use esc::error::*;
use esc::net::id::*;
use esc::net::*;
use esc::encode::*;
use esc::input::*;

use team::*;
use events::EventRule;
use components::*;
use state::*;

/// The basic plane type
#[derive(Debug)]
pub struct Plane {
   pub body: Ptr<Body>,
   pub control: Controller<(), PlaneControl>,
   pub player: i16,
   pub team: Team,
   pub alive: bool,
   pub exp: i32,
   pub ammo: f32,
   pub params: PlaneParams,
   pub timers: PlaneTimers,
   /// Flight dynamics
   pub flight: PlaneFlight,
   /// Calculates roll & flipping for animation
   pub spin: PlaneSpin,
   /// Maps spin to sprites & geometry.
   pub anim: PlaneAnim,
   pub landing_gear: LandingGear,
   pub item: HeldItem,
}

/// Common initialization data for a plane
#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub struct PlaneInit {
   #[rule("rule.id_rule")]
   pub id: NetID<Plane>,
   #[rule("TeamRule")]
   pub team: Team,
   #[rule("rule.position()")]
   pub position: Vec2,
   #[rule("angle_rule(1.0)")]
   pub orientation: f32,
   #[rule("BitableRule")]
   pub player: i16,
   pub item: HeldItem,
}

/// Plane input
#[derive(Copy, Clone, Default, Debug, Encode)]
#[rule("EventRule")]
pub struct PlaneControl {
   #[rule("BitableRule")]
   pub throttle_up: bool,
   #[rule("BitableRule")]
   pub throttle_down: bool,
   #[rule("ranged_float(0.0, 180.0, 10.0)")]
   pub turn_left: f32,
   #[rule("ranged_float(0.0, 180.0, 10.0)")]
   pub turn_right: f32,
   #[rule("BitableRule")]
   pub use_item: bool,
}

/// The encodable state of a plane used for network synchronization and replays.
#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub struct PlanePostState {
   transform: TransformNet,
   flight: PlaneFlightNet,
   timers: PlaneTimersNet,
}

impl Plane {
   /// Create a new plane
   pub fn new(a: Args<PlaneInit>, proto: &Sprite, params: PlaneParams) -> Ptr<Plane> {
      let t = a.storage().insert(Transform {
         position: a.init.position,
         orientation: a.init.orientation,
         .. Transform::default()
      });

      let anim = PlaneAnim::new(a.storage().insert(proto.instance(t.clone())));
      let body = Body::new(anim.get_hull(), t, a.storage().insert(Material::default()), false);

      let mut body = a.storage().insert(body);

      body.collision_id = ::collision_id::PLANE;
      body.collision_mask |= ::collision_id::LANDSCAPE;

      let mut plane = a.storage().insert(Plane {
         body: body.clone(),
         control: Controller::dummy(),
         player: a.init.player,
         team: a.init.team,
         alive: true,
         exp: 0,
         ammo: params.max_ammo,
         timers: PlaneTimers::default(),
         flight: PlaneFlight::new(body.clone()),
         spin: PlaneSpin::new(params.bank_angle),
         params,
         anim,
         landing_gear: LandingGear::Flying,
         item: a.init.item.clone(),
      });

      if let Some(mut powerup) = plane.item.common() {
         powerup.pickup(plane.weak())
      }

      plane.flight.initialize(params.engine_speed);
      plane.timers.paused = ::seconds_to_ticks(1.5) as i32;
      plane.body.parent = plane.weak().any();

      a.map_component_id(&plane, a.init.id);
      plane
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr_rule: PtrRule) -> PlaneInit {
      PlaneInit {
         id: ptr_rule.weak_to_net(self.weak()).unwrap(),
         team: self.team,
         position: self.body.t.position,
         orientation: self.body.t.orientation,
         player: self.player,
         item: self.item.clone(),
      }
   }

   pub fn apply_pre(&mut self, control: PlaneControl) {
      *self.control = control;
   }

   pub fn apply_post(&mut self, state: PlanePostState) {
      state.transform.partial_to(&mut *self.body.t);
      state.flight.partial_to(&mut self.flight);
      state.timers.partial_to(&mut self.timers);
   }

   pub fn state(self: &Ptr<Self>) -> ComponentState {
      fn partial_from<T: PartialEquiv + Default>(target: &T::Target) -> T {
         let mut t = T::default();
         t.partial_from(target);
         t
      }

      ComponentState::Plane {
         id: self.weak(),
         pre: *self.control,
         post: PlanePostState {
            transform: partial_from::<TransformNet>(&self.body.t),
            flight: partial_from::<PlaneFlightNet>(&self.flight),
            timers: partial_from::<PlaneTimersNet>(&self.timers),
         }
      }
   }

   /// Update the plane
   pub fn tick(&mut self) {
      use esc::util::hprof;
      hprof::enter("Updating plane");

      self.timers.tick();

      if self.timers.paused == 0 {
         if let LandingGear::Landed { .. } = self.landing_gear {
            // Reset the landing gear, prevent re-landing for 1 second
            self.landing_gear = LandingGear::Launched { delay: Delay::new(30.0) }
         }
      }

      self.update_landing_gear();
      self.update_flight_params();
      self.flight.fly();
      self.afterburner();
      self.reverse_thrust();

      self.spin.update(&self.flight);
      self.anim.update(self.spin.spin);
      self.body.change_hull(self.anim.get_hull());

      self.ammo = clamp(self.ammo + self.ammo_regen(), 0.0, self.params.max_ammo);

      if !self.alive {
         if let Some(mut item) = self.item.common() { item.alive = false }
      }
   }

   /// Handle a collision
   pub fn on_collide(&mut self, c: Contact, a: &mut Body, b: &mut Body) {
      self.flight.pre_collide();

      if let Some(base) = b.parent.try_to::<Base>() {
         // Low-impact collisions with the runway of a base may initiate landing.
         if base.is_landing(c, a) {
            self.landing_gear.on_collide(base);
         }
      }

      let (res_a, res_b) = c.resolve(a, b);
      res_a.apply(a);
      res_b.apply(b);

      // Direct changes in velocity from collision resolution to reducing energy.
      self.flight.post_collide();
   }

   /// Calculate the turn increment and clamp values from the target orientations given by the
   /// controller. Turn increment is the angle in degrees to turn this frame, turn clamp is
   /// the value at which to clamp orientation (so as to not overshoot the target orientation)
   pub fn calculate_turn(&self) -> (f32, f32) {
      let (turn_inc, turn_max) = self.turn_rate();

      let l = self.control.turn_left;
      let r = self.control.turn_right;
      let mut turn_clamp = turn_max;

      let mut turn = 0.0;

      if l > 0.1 && r < 0.1 {
         turn_clamp = clamp(turn_clamp, 0.0, l);
         turn = turn_inc;
      } else if r > 0.1 && l < 0.1 {
         turn_clamp = clamp(turn_clamp, 0.0, r);
         turn = -turn_inc
      } else if r < 0.1 && l < 0.1 {
         turn_clamp = 0.0
      }

      (turn, turn_clamp)
   }

   /// Calculate the change in throttle from the control state
   pub fn calculate_throttle(&self) -> f32 {
      match (self.control.throttle_up, self.control.throttle_down) {
         (_    , true) => -self.throttle_rate(),
         (true , false) => self.throttle_rate(),
         (false, false) => 0.0,
      }
   }

   fn update_landing_gear(&mut self) {
      if self.control.throttle_up {
         self.landing_gear.abort()
      }

      self.landing_gear.tick(&mut self.flight);
      if self.is_flying() {
         if let LandingGear::Landed { base: _ } = self.landing_gear {
            self.timers.paused = 30;
            self.flight.initialize(self.engine_speed());
            // TODO: ... modelev? Might want a game event for defusing, though unnecessary for healing
         }
      }
   }

   fn is_flying(&self) -> bool {
      self.timers.paused <= 0 && self.alive
   }

   /// Update the `PlaneFlight`s parameters & controls.
   fn update_flight_params(&mut self) {
      self.flight.active = self.is_flying();

      self.flight.env = FlightEnv{
         gravity: ::world::DEFAULT_GRAVITY
      };

      self.flight.params = FlightParams {
         max_turn: self.turn_rate().1,
         engine_speed: self.engine_speed(),
         acceleration: self.params.acceleration,
         turn_penalty: self.params.fast_turn_penalty,
      };

      self.flight.control = FlightControl {
         throttle_diff: self.calculate_throttle(),
         turn: self.calculate_turn().0,
         turn_clamp: self.calculate_turn().1,
      };
   }

   fn afterburner(&mut self) {
      if !self.is_flying() { return }
      if self.calculate_throttle() <= 0.0 || self.flight.throttle < 999.9 { return }

      let power = f32::min(self.ammo, self.params.afterburner_power);
      let mut force = 0.3 * power / self.params.afterburner_power * self.load_factor();

      // A power boost inversely proportional to the current forward speed, helps
      // in recovering from stalls.
      if self.params.easy_stall_recovery || self.flight.env.gravity == 0.0 {
         let x = map(self.flight.forward_speed(),
                     [4.24, 2.756 + 0.35 * self.engine_speed()],
                     [2.0, 1.0]);
         force *= x;
      }

      self.ammo -= power;
      self.flight.add_forward_speed(force);
   }

   /// Some enterprising pilots strap solid fuel rockets to the front of their planes, allowing
   /// backward flight!
   fn reverse_thrust(&mut self) {
      if !self.params.reverse_thrust { return }
      if !self.is_flying() { return }

      if self.calculate_throttle() < 0.0 && (self.flight.stalled || self.flight.throttle < 0.1) {
         self.ammo -= f32::min(self.ammo, 0.8333 * self.ammo_regen());
         let fraction = map(self.flight.forward_speed(),
                            [-self.params.engine_speed, 0.1 * -self.params.engine_speed],
                            [0.0, 2.9]);

         let f = -0.44 * fraction * self.load_factor();
         self.flight.add_forward_speed(f)
      }
   }

   // ================================================================= //
   //                         Derived Stats
   // ================================================================= //

   /// A force multiplier caused by the powerup being carried
   pub fn load_factor(&self) -> f32 {
      /*
      match self.powerup.as_ref().map(|x| x.kind()) {
         Some(PowerupKind::Bomb) | Some(PowerupKind::Ball) =>
            self.kind.bomb_afterburner_power(),
         _ => 1.0,
      }
      */
      1.0
   }

   /// The rate at which the plane regenerates ammo.
   pub fn ammo_regen(&self) -> f32 {
      let per_bar = if self.params.ace_instincts { 1.019 } else { 1.013 };
      self.params.ammo_regen * f32::powi(per_bar, self.vet_bars())
   }

   /// Calculate the plane's engine speed after modifiers.
   pub fn engine_speed(&self) -> f32 {
      let base = self.params.engine_speed;
      let mut speed = base;

      let mut per_bar = base * 0.01;
      if self.params.ace_instincts { per_bar *= 1.4; }
      speed += self.vet_bars() as f32 * per_bar;

      /*
      if let Some(ref powerup) = self.powerup {
         match powerup.kind() {
            PowerupKind::Bomb => speed -= base * (1.0 - self.params.bomb_speed),
            PowerupKind::Ball => speed -= base * (1.0 - self.params.ball_speed),
            _ => (),
         }
      }
      */

      speed
   }

   /// The rate at which throttle can in/decrease
   pub fn throttle_rate(&self) -> f32 {
      let mut rate = self.params.throttle_rate;
      if self.timers.is_emp() { rate *= 0.5 }
      rate
   }

   /// The increase in turn rate per tick and maximum turn rate of the plane.
   pub fn turn_rate(&self) -> (f32, f32) {
      let mut inc = self.params.turn_speed;
      let mut max = self.params.max_turn;

      let per_bar = if self.params.ace_instincts { 1.014717 } else { 1.01 };

      inc *= f32::powi(per_bar, self.vet_bars());
      max *= f32::powi(per_bar, self.vet_bars());

      if self.timers.is_emp() && max > 3.385 {
         let mut x = (3.385 + (max - 3.385) * 0.33) / max;
         x = self.params.emp_resistance + ((1.0 - self.params.emp_resistance) * x);

         inc *= x;
         max *= x;
      }

      (inc, max)
   }

   /// The plane's damage multiplier
   pub fn damage_multi(&self) -> f32 {
      let per_bar = if self.params.ace_instincts { 0.03 } else { 0.02 };
      1.0 + self.vet_bars() as f32 * per_bar
   }

   /// The number of veteran bars the plane has (0-9)
   pub fn vet_bars(&self) -> i32 {
      let exp_per_bar = if self.params.ace_instincts { 8 } else { 10 };

      let mut bars = self.exp / exp_per_bar;
      if bars > 9 { bars = 9; }
      bars as i32
   }
}
