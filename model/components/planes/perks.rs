use components::*;

/// A Plane setup
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PlaneSetup {
   pub plane: PlaneKind,
   pub green_perk: GreenPerk,
   pub blue_perk: BluePerk,
}

/// The type of a plane and primary perk.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PlaneKind {
   Biplane(BiplanePerk),
   Bomber(BomberPerk),
   Explodet(ExplodetPerk),
   Loopy(LoopyPerk),
   Miranda(MirandaPerk),
}

/// A Loopy's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum LoopyPerk { Tracker, DoubleFire, Acid }

/// A Bomber's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BomberPerk { Suppressor, Bombs, Flak }

/// An Explodet's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum ExplodetPerk { Director, Thermo, Remote }

/// A Biplane's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BiplanePerk { Dogfighter, Recoilless, HeavyCannon }

/// A Miranda's main perk
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum MirandaPerk { Trickster, Laser, TimeAnchor }

/// A green perk.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum GreenPerk { None, Rubber, Heavy, Repair, Flexi }

/// A blue perk.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum BluePerk { None, Turbo, Ultra, Reverse, Ace }

impl GreenPerk {
   /// Apply the perk to a set of plane parameters
   pub fn update_params(&self, params: &mut PlaneParams) {
      match *self {
         GreenPerk::Rubber => params.compound_emp_resist(0.2),
         GreenPerk::Heavy => params.max_health = 17.0 + params.max_health * 1.16,
         GreenPerk::Flexi => {
            params.fast_turn_penalty = 0.995;
            params.turn_speed *= 1.2;
            params.max_turn *= 1.2;
         }
         GreenPerk::Repair => (),
         GreenPerk::None => (),
      }
   }
}

impl BluePerk {
   /// Apply the perk to a set of plane parameters
   pub fn update_params(&self, params: &mut PlaneParams) {
      match *self {
         BluePerk::Ultra => params.max_ammo = 1250.0,
         BluePerk::Turbo => params.ammo_regen *= 1.2,
         BluePerk::Ace => params.ace_instincts = true,
         BluePerk::Reverse => {
            params.reverse_thrust = true;
            params.throttle_rate *= 1.9;
         }
         BluePerk::None => (),
      }
   }
}

impl PlaneKind {
   /// The name of the plane kind
   pub fn name(&self) -> &'static str {
      match *self {
         PlaneKind::Biplane(_) => "biplane",
         PlaneKind::Bomber(_) => "bomber",
         PlaneKind::Explodet(_) => "explodet",
         PlaneKind::Loopy(_) => "loopy",
         PlaneKind::Miranda(_) => "miranda",
      }
   }

   /// The default plane parameters for this type of plane.
   pub fn params(&self) -> PlaneParams {
      match *self {
         PlaneKind::Biplane(_) => PlaneParams {
            weight: 1.02,
            engine_speed: 9.87,
            turn_speed: 0.66,
            max_turn: 6.6,
            max_health: 250.0,
            ammo_regen: 20.0,
            afterburner_power: 76.0,
            acceleration: 0.63 * 0.8,
            bomb_speed: 0.88,
            ball_speed: 0.88,
            ball_shoot_speed: 18.0,
            bomb_afterburner_power: 0.5,
            emp_resistance: 0.2,
            fast_turn_penalty: 0.989,
            easy_stall_recovery: false,
            reverse_thrust: false,
            max_ammo: 1000.0,
            throttle_rate: 35.0,
            ace_instincts: false,
            bank_angle: 18.0,
         },

         PlaneKind::Bomber(_) => PlaneParams {
            weight: 1.15,
            engine_speed: 9.1,
            turn_speed: 0.69,
            max_turn: 6.9,
            max_health: 275.0,
            ammo_regen: 15.0,
            afterburner_power: 80.0,
            acceleration: 0.85 * 0.8,
            bomb_speed: 0.99,
            ball_speed: 0.98,
            ball_shoot_speed: 20.0,
            bomb_afterburner_power: 0.5,
            emp_resistance: 0.0,
            fast_turn_penalty: 0.989,
            easy_stall_recovery: false,
            reverse_thrust: false,
            max_ammo: 1000.0,
            throttle_rate: 35.0,
            ace_instincts: false,
            bank_angle: 20.0,
         },

         PlaneKind::Explodet(_) => PlaneParams {
            weight: 1.25,
            engine_speed: 8.916,
            turn_speed: 0.69,
            max_turn: 5.6,
            max_health: 295.0,
            ammo_regen: 13.0,
            afterburner_power: 76.0,
            acceleration: 0.48 * 0.8,
            bomb_speed: 0.99,
            ball_speed: 0.99,
            ball_shoot_speed: 21.0,
            bomb_afterburner_power: 0.5,
            emp_resistance: 0.0,
            fast_turn_penalty: 0.989,
            easy_stall_recovery: false,
            reverse_thrust: false,
            max_ammo: 1000.0,
            throttle_rate: 35.0,
            ace_instincts: false,
            bank_angle: 18.0,
         },

         PlaneKind::Loopy(_) => PlaneParams {
            weight: 0.98,
            engine_speed: 10.947,
            turn_speed: 0.9,
            max_turn: 8.4,
            max_health: 190.0,
            ammo_regen: 20.0,
            afterburner_power: 90.0,
            acceleration: 0.48 * 0.8,
            bomb_speed: 0.86,
            ball_speed: 0.78,
            ball_shoot_speed: 14.5,
            bomb_afterburner_power: 0.5,
            emp_resistance: 0.0,
            fast_turn_penalty: 0.989,
            easy_stall_recovery: false,
            reverse_thrust: false,
            max_ammo: 1000.0,
            throttle_rate: 35.0,
            ace_instincts: false,
            bank_angle: 42.0,
         },

         PlaneKind::Miranda(_) => PlaneParams {
            weight: 0.98,
            engine_speed: 9.374,
            turn_speed: 0.7,
            max_turn: 7.0,
            max_health: 180.0,
            ammo_regen: 13.0,
            afterburner_power: 100.0,
            acceleration: 0.5 * 0.8,
            bomb_speed: 0.86,
            ball_speed: 0.83,
            ball_shoot_speed: 16.8,
            bomb_afterburner_power: 0.43,
            emp_resistance: 0.0,
            fast_turn_penalty: 0.989,
            easy_stall_recovery: false,
            reverse_thrust: false,
            max_ammo: 1000.0,
            throttle_rate: 35.0,
            ace_instincts: false,
            bank_angle: 30.0,
         },
      }
   }
}

