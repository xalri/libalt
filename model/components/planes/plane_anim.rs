
use esc::storage::*;
use esc::geom::*;
use esc::render::*;

/// The animation data for a plane.
#[derive(Debug)]
pub struct PlaneAnim {
   pub angles: Vec<f32>,
   pub sprite: Ptr<Sprite>,
}

impl PlaneAnim {
   pub fn new(mut sprite: Ptr<Sprite>) -> PlaneAnim {
      let mut angles = vec![];
      for f in &mut sprite.frames {
         angles.push(wrap_angle(f.delay as f32 / 10.0));
         f.delay = 0.0;
      }
      PlaneAnim { angles, sprite }
   }

   /// Get the hull
   pub fn get_hull(&self) -> Ptr<Poly> {
      self.sprite.frame().geom.clone()
   }

   pub fn update(&mut self, spin: f32) {
      let idx = self.frame_idx(spin);
      self.sprite.set_frame(idx);
   }

   /// Get the frame index which corresponds to the given spin angle.
   pub fn frame_idx(&self, spin: f32) -> usize {
      let mut diff = 360.0;
      let mut frame = 0;

      for i in 0..self.angles.len() {
         let d = wrap_angle(self.angles[i] - spin).abs();
         if d < diff {
            frame = i;
            diff = d;
         }
      }

      frame
   }
}
