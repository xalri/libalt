use esc::encode::*;
use esc::error::*;
use esc::net::*;
use events::EventRule;

/// Various timers in planes.
#[derive(PartialEq, Debug, Clone, Copy, Default)]
pub struct PlaneTimers {
   /// The time in ticks for which the plane will be frozen (when landed
   /// on a base or when spawning)
   pub paused: i32,
   /// The time until a charge plant will have completed
   pub plant_charge: i32,
   /// The time in ticks since the plane spawned.
   pub age: i32,
   /// The time in ticks since a powerup was last picked up.
   pub last_pickup: i32,
   /// The remaining time the plane will be emp'd.
   pub emp: i32,
   /// The remaining time the plane will be affected by acid.
   pub acid: i32,
}

#[derive(PartialEq, Debug, Clone, Copy, Default, Encode, PartialEquiv)]
#[partial_equiv(remote="PlaneTimers")]
#[rule("EventRule")]
pub struct PlaneTimersNet {
   // TODO: proper ranges, other timers, ..
   #[rule("BitableRule")]
   pub paused: i32,
}

impl PlaneTimers {
   pub fn is_acid(&self) -> bool { self.acid > 0 }
   pub fn is_emp(&self) -> bool { self.emp > 0 }

   pub fn tick(&mut self) {
      self.age += 1;
      self.last_pickup += 1;
      if self.paused >= 0 { self.paused -= 1; }
      if self.acid >= 0 { self.acid -= 1; }
      if self.emp >= 0 { self.emp -= 1; }
      if self.plant_charge >= 0 { self.plant_charge -= 1; }
   }
}
