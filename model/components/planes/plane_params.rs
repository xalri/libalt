
#[derive(Debug, Clone, Copy)]
pub struct PlaneParams {
   /// The plane's weight, determines resistance to external forces (explosions)
   pub weight: f32,
   /// The plane's base speed
   pub engine_speed: f32,
   /// Turning acceleration
   pub turn_speed: f32,
   /// The maximum turn speed
   pub max_turn: f32,
   /// The plane's maximum health
   pub max_health: f32,
   /// The rate at which energy is regenerates, given as energy per tick
   pub ammo_regen: f32 ,
   /// The power of the plane's afterburner
   pub afterburner_power: f32,
   /// The rate of change of the square of the plane's speed
   pub acceleration: f32,
   /// The fraction of the plane's speed achieved while carrying a bomb
   pub bomb_speed: f32,
   /// The fraction of the plane's speed achieved while carrying a ball
   pub ball_speed: f32,
   /// The base speed of a fired ball
   pub ball_shoot_speed: f32,
   /// The fraction of the afterburner's power available while carrying a bomb.
   pub bomb_afterburner_power: f32,
   /// The fraction of emp's turning reduction to resist
   pub emp_resistance: f32,
   /// The energy multiplier while in a tight turn.
   pub fast_turn_penalty: f32,
   /// Gives a boost to the afterburner inversely proportional to the plane's forward
   /// speed to help recover from stalls.
   pub easy_stall_recovery: bool,
   /// The plane's ammo capacity
   pub max_ammo: f32,
   /// The rate at which the plane's throttle can change
   pub throttle_rate: f32,
   /// Allows the plane to accelerate in reverse
   pub reverse_thrust: bool,
   /// Veteran bars give a greater boost
   pub ace_instincts: bool,
   /// The degree to which the plane rolls (used in animation)
   pub bank_angle: f32,
}

impl PlaneParams {
   /// Add some amount of emp resistance (non-linearly)
   pub fn compound_emp_resist(&mut self, b: f32) {
      self.emp_resistance += (1.0 - self.emp_resistance) * b;
   }
}
