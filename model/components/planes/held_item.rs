use esc::storage::*;
use esc::encode::*;
use esc::error::*;
use components::*;
use events::EventRule;

/// An item held by a plane
#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub enum HeldItem {
   None,
   Wall(#[rule("rule.ptr_rule")] Weak<WallPowerup>),
   Shield(#[rule("rule.ptr_rule")] Weak<ShieldPowerup>),
   Missile(#[rule("rule.ptr_rule")] Weak<MissilePowerup>),
}

impl HeldItem {
   pub fn common(&self) -> Option<Weak<PowerupCommon>> {
      Some(match self {
         HeldItem::None => return None,
         HeldItem::Wall(p) => p.common.weak(),
         HeldItem::Shield(p) => p.common.weak(),
         HeldItem::Missile(p) => p.common.weak(),
      })
   }

   pub fn is_none(&self) -> bool {
      match self {
         HeldItem::None => true,
         _ => false,
      }
   }

   pub fn is_some(&self) -> bool {
      !self.is_none()
   }
}