
use esc::storage::*;
use esc::error::*;
use esc::encode::*;
use esc::net::id::*;
use altx::*;

use components::*;
use events::ComponentInit;
use events::EventRule;

pub struct Loopy {
   pub plane: Ptr<Plane>,
}

/// Reified constructor parameters for a loopy
#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub struct LoopyInit {
   #[rule("rule.id_rule")]
   pub id: NetID<Loopy>,
   pub common: PlaneInit,
}

impl Loopy {
   pub fn new(a: Args<LoopyInit>) -> Ptr<Loopy> {
      let team = a.init.common.team;
      let params = PlaneKind::Loopy(LoopyPerk::Tracker).params();

      let path = format!(".poly/render/planes/loopy/{}.animatedpoly", team.string());
      let loopy_xml = a.res().get::<XmlFormat<xml::AnimatedPoly>>(&path, ()).unwrap();
      let sprite = anim_poly_to_sprite(&a.res(), &*loopy_xml, a.res().emplace()).unwrap();

      let plane = Plane::new(a.map(a.init.common), &sprite, params);

      let loopy = a.storage().insert(Loopy{ plane });
      a.map_component_id(&loopy, a.init.id);
      loopy
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr_rule: PtrRule) -> ComponentInit {
      ComponentInit::Loopy(LoopyInit {
         id: ptr_rule.weak_to_net(self.weak()).unwrap(),
         common: self.plane.creation_ev(ptr_rule),
      })
   }
}

// TODO: abilities
