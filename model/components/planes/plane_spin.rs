
use esc::geom::*;
use components::*;

const FLIP_THRESHOLD_LEFT: f32 = 90.0 + 60.0;
const FLIP_THRESHOLD_RIGHT: f32 = 90.0 - 60.0;

/// Tracks the roll angle of a plane
#[derive(Debug)]
pub struct PlaneSpin {
   pub spin: f32,
   pub spin_rate: f32,
   pub flip_rate: f32,
   pub bank_angle: f32,
   pub max_bank_rate: f32,
   pub flipped: bool,
   pub flip_in_progress: bool,
}

impl PlaneSpin {
   pub fn new(bank_angle: f32) -> PlaneSpin {
      let rate_of_role = 16.0;
      let degrees_to_roll = 180.0 - 2.0 * bank_angle;
      let updates_to_roll = (degrees_to_roll / rate_of_role).ceil();
      let flip_rate = degrees_to_roll / updates_to_roll - 0.1;

      PlaneSpin {
         spin: 0.0,
         spin_rate: 0.0,
         flip_rate,
         bank_angle,
         max_bank_rate: bank_angle / 6.0,
         flipped: false,
         flip_in_progress: false,
      }
   }

   /// Update the spin angle.
   pub fn update(&mut self, flight: &PlaneFlight) {
      let o = wrap_angle(flight.body.t.orientation);

      let flip_left = o > FLIP_THRESHOLD_LEFT || o < -FLIP_THRESHOLD_LEFT;
      let flip_right = o < FLIP_THRESHOLD_RIGHT && o > -FLIP_THRESHOLD_RIGHT;

      let prevent_flip = !flight.active || flight.stalled || flight.relative_turn > 0.97;

      if !self.flip_in_progress && !prevent_flip && ((!self.flipped && flip_left) || (self.flipped && flip_right)) {
         self.flip_in_progress = true;
         self.spin_rate = if flip_left ^ (self.spin < 0.0) { self.flip_rate } else { -self.flip_rate };
      }

      if self.flip_in_progress {
         let final_angle = self.bank_angle;
         let new_spin = wrap_angle(self.spin + self.spin_rate);
         if self.flipped {
            if -final_angle < new_spin && new_spin < final_angle {
               self.flipped = false;
               self.flip_in_progress = false;
               self.spin_rate = 0.0;
            }
         } else {
            if new_spin > 180.0 - final_angle || new_spin < 180.0 - final_angle {
               self.flipped = true;
               self.flip_in_progress = false;
               self.spin_rate = 0.0;
            }
         }
      }

      if !self.flip_in_progress {
         let relative_turn = flight.relative_turn * flight.turn.signum();// + flight.body.t.angular_vel * 0.5;
         assert!(relative_turn.abs() < 1.4);
         let mut so = self.spin;
         if self.flipped { so = if so < 0.0 { so + 180.0 } else { so -180.0 } }
         let target = self.bank_angle * relative_turn;
         self.spin_rate = clamp(target - so, -self.max_bank_rate, self.max_bank_rate);
      }

      self.spin = wrap_angle(self.spin + self.spin_rate);
   }
}
