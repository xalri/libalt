
use esc::util::time::*;
use esc::storage::*;
use esc::geom::*;
use components::*;

/// Pulls a plane into land after approaching a base
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum LandingGear {
   /// Just launched, ignore 'landing' collisions for some number of frames.
   Launched { delay: Delay, },
   /// Plane is flying and ready to land
   Flying,
   /// Approaching a base, applies a corrective force to smooth landing
   Approaching { base: Weak<Base>, },
   /// Completed landing, on which the `Plane` must pause flight & reset to `Launched` when
   /// appropriate (e.g. after planting a charge or healing)
   Landed { base: Weak<Base>, },
}

impl LandingGear {
   /// Handle a landing collision with a base
   pub fn on_collide(&mut self, base: Weak<Base>) {
      if let LandingGear::Flying = *self {
         *self = LandingGear::Approaching{ base }
      }
   }

   /// Update the landing gear.
   /// After first touching a base, a slight corrective force fixes the angle & velocity
   /// (pulling the plane toward the base & slowing them down) for a smoother landing.
   pub fn tick(&mut self, flight: &mut PlaneFlight) {
      match *self {
         LandingGear::Launched { mut delay } => {
            delay.tick();
            *self = if delay.expired { LandingGear::Flying } else { LandingGear::Launched { delay } }
         }
         LandingGear::Approaching { base } => {
            let runway = base.runway();

            // Calculate the relative position & angle of the plane from the runway
            let o = flight.body.t.orientation;
            let p = flight.body.t.position;
            let tangent = (runway.0 - runway.1).normalize();
            let position_diff = project_on_segment(p, runway) - p;
            let mut angle_diff = angle_diff(tangent.angle(), o);
            if angle_diff.abs() > 90.0 { angle_diff = wrap_angle(angle_diff + 180.0) }

            // Abort the approach without correction if we're too far away or at too great an angle
            if position_diff.length() > 20.0 || angle_diff.abs() > 25.0 {
               self.abort();
               return;
            }

            // Correct the approach angle
            if angle_diff.abs() > 1.0 { flight.body.t.orientation += if angle_diff > 0.0 { 1.0 } else { -1.0 } }

            if flight.body.t.linear_vel.length() < 1.0 {
               if angle_diff < 2.0 {
                  // Landed when velocity & relative angle are both low
                  *self = LandingGear::Landed { base }
               }
            } else {
               // If moving too quickly, slow the plane & pull toward the runway
               flight.add_forward_speed(-flight.forward_speed() * 0.1);
               flight.add_linear_vel(position_diff * 0.02); // since we abort on high position diff max magnitude is 0.4
            }
         }
         _ => (),
      }
   }

   /// Abort an approach (canceling the corrective force), triggered by the player accelerating
   /// away from the base
   pub fn abort(&mut self) {
      *self = LandingGear::Flying
   }
}
