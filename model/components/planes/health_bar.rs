
/*
use esc::storage::*;
use esc::render::*;
use esc::geom::*;
use storage::*;

/// A single health bar
pub struct HealthBar {
   background: Ptr<Sprite>,
   bar: Ptr<Sprite>,
}

/// Displays the current health, throttle, and ammo of a plane.
pub struct PlaneBars {
   health: HealthBar,
   throttle: HealthBar,
   ammo: HealthBar,
}

impl HealthBar {
   pub fn new(_storage: &mut Storage, _t: Ptr<Transform>, _offset: Transform, _width: f32, _colour: Colour) -> HealthBar {
      unimplemented!()
   }

   pub fn set(&mut self, _value: f32) {
      unimplemented!()
   }
}
*/