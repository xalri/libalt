
pub struct Health {
   pub real: f32,
   pub target: f32,
   pub max: f32,
   pub increment: f32,
   pub alive: bool,
}

impl Health {
   pub fn tick(&mut self) {
      if self.real < self.target {

      }
   }

   pub fn take_damage(&mut self, value: f32) {

   }

   pub fn restore(&mut self, value: f32) {

   }

   pub fn increase_max(&mut self, value: f32) {

   }
}