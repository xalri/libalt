use esc::storage::*;
use esc::geom::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;

use altx::*;
use components::*;
use events::*;

/// A powerup which restores a plane's health
pub struct HealthPowerup {
   pub common: Ptr<PowerupCommon>,
   /// The amount of health to restore
   pub health: i32,
}

/// Health powerup constructor arguments
#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub struct HealthPowerupInit {
   #[rule("rule.id_rule")]
   pub id: NetID<HealthPowerup>,
   pub common: PowerupInit,
   #[rule("ranged_int(0, 100)")]
   pub health: i32,
}

impl HealthPowerup {
   pub fn new(a: Args<HealthPowerupInit>) -> Ptr<HealthPowerup> {
      // Initialise the common powerup data (with the collision body; powerup container; etc)
      let mut common = PowerupCommon::new(a.map(a.init.common));

      // Allow pickup even if already holding a powerup
      common.is_autouse = true;

      // Load the health pack sprite
      let sprite_archetype = a.res().get_with_fn(|| {
         let xml = a.res().get::<XmlFormat<xml::AnimatedPoly>>(".poly/planes/powerup/health/healthpack.animatedpoly", ())?;
         let mut sprite = anim_poly_to_sprite(a.res(), &*xml, Ptr::null())?;
         sprite.offset.scale = Vec2::one() * 0.8;
         sprite.depth.object = -1;
         for f in &mut sprite.frames {
            f.delay = 60.0;
         }
         Ok(sprite)
      }, "health_sprite").unwrap();

      // Set the powerup container's inner sprite to an instance of the health pack sprite
      common.sprite = a.storage().insert(sprite_archetype.instance(common.container.transform.clone()));
      common.sprite_scale = 0.8;

      let mut p = a.storage().insert(HealthPowerup { common, health: a.init.health });
      a.map_component_id(&p, a.init.id);
      p.common.parent = p.weak().any();
      p
   }

   /// Construct an event which re-creates the powerup
   pub fn creation_ev(self: &Ptr<Self>, ptr: PtrRule) -> ComponentInit {
      ComponentInit::HealthPowerup(HealthPowerupInit {
         id: ptr.weak_to_net(self.weak()).unwrap(),
         common: self.common.creation_ev(ptr),
         health: self.health,
      })
   }
}
