use esc::storage::*;
use esc::render::*;
use esc::error::*;
use esc::encode::*;
use esc::geom::*;
use esc::geom::contact::*;
use esc::net::id::*;
use esc::util::time::*;

use altx::*;
use components::*;
use events::*;
use team::*;

/// Common powerup state.
pub struct PowerupCommon {
   /// Collision body
   pub body: Ptr<Body>,
   /// Sprites making up the powerup shell
   pub container: Ptr<PowerupContainer>,
   /// Some representation of the powerup, set by the parent.
   pub sprite: Ptr<Sprite>,
   pub sprite_scale: f32,
   /// The powerup spawner which created the powerup. None for powerups owned by planes.
   pub state: PowerupState,
   /// The powerup's team -- only members of this team can pick up the powerup
   pub team: Team,
   /// A countdown timer for the powerup fading.
   pub fade_timer: Option<Delay>,
   /// True while the powerup is alive.
   pub alive: bool,
   /// The parent component.
   pub parent: WeakAny,
   pub is_autouse: bool,
   old_transform: Ptr<Transform>,
   me: Weak<PowerupCommon>,
}

/// Common initialization data for a powerup
#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub struct PowerupInit {
   #[rule("rule.id_rule")]
   pub id: NetID<PowerupCommon>,
   #[rule("rule.position()")]
   pub position: Vec2,
   #[rule("ranged_vec(-50.0, 50.0, 5.0)")]
   pub linear_vel: Vec2,
   #[rule("TeamRule")]
   pub team: Team,
   pub state: PowerupState,
}

#[derive(Clone, Copy, Debug, Encode, PartialEq)]
#[rule("EventRule")]
pub enum PowerupState {
   /// Attached to a powerup spawner, waiting to be picked up
   Spawned {
      #[rule("rule.ptr_rule")]
      spawner: Weak<PowerupSpawner>
   },
   /// Held by a plane
   Held,
   /// Dropped by a plane
   Dropped,
}

impl PowerupCommon {
   pub fn new(a: Args<PowerupInit>) -> Ptr<PowerupCommon> {
      let archetype = a.res().get_with_fn(|| {
         let xml = a.res().get::<XmlFormat<xml::TexturedPoly>>(".poly/planes/powerup/container.poly", ())?;
         let mut body = ::body(a.res(), &xml.hull);
         body.collision_id = ::collision_id::MISC;
         body.collision_mask = ::collision_id::PLANE | ::collision_id::LANDSCAPE;
         Ok(body)
      }, "powerup_common").unwrap();

      let container = PowerupContainer::new(a.res(), a.storage(), a.init.team);
      let mut body = a.storage().insert(archetype.instance(container.transform.clone()));
      body.t.position = a.init.position;

      let mut p = a.storage().insert(PowerupCommon {
         body,
         container,
         sprite: Ptr::null(),
         sprite_scale: 1.0,
         state: a.init.state,
         team: a.init.team,
         fade_timer: None,
         alive: true,
         me: Weak::null(),
         parent: WeakAny::null(),
         is_autouse: false,
         old_transform: Ptr::null(),
      });

      if let PowerupState::Spawned{ mut spawner } = p.state {
         if spawner.alive() { spawner.on_spawn(p.weak()); }
      }

      p.me = p.weak().clone();
      p.body.parent = p.weak().any();
      a.map_component_id(&p, a.init.id);
      p
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr: PtrRule) -> PowerupInit {
      PowerupInit {
         id: ptr.weak_to_net(self.weak()).unwrap(),
         state: self.state,
         team: self.team,
         position: self.body.t.position,
         linear_vel: self.body.t.linear_vel,
      }
   }

   pub fn pickup(&mut self, plane: Weak<Plane>) {
      self.old_transform = self.sprite.transform.clone();
      self.body.collision_mask = 0;
      self.body.collision_id = 0;

      if let PowerupState::Spawned { mut spawner } = self.state {
         if spawner.alive() { spawner.complete = Weak::null() }
      }

      // Use the plane's transform with an offset & mask to place the sprite above the plane and
      // ignoring it's orientation & scale
      let offset = Transform { position: vec2(0.0, 40.0), scale: vec2(0.5, 0.5), .. Transform::default() };
      let mask = TransformMask { orientation: false, angular_vel: false, scale: false, .. TransformMask::default() };
      self.update_sprite(plane.body.t.clone(), offset, mask);

      self.state = PowerupState::Held;
   }

   pub fn on_drop(&mut self, plane: &Plane, position: Vec2, linear_vel: Vec2) {
      if let PowerupState::Held = self.state {
         self.body.collision_mask = ::collision_id::PLANE | ::collision_id::LANDSCAPE;
         self.body.collision_id = ::collision_id::MISC;

         // Disconnect the sprite from the plane's transform
         *self.old_transform = Transform { scale: Vec2::one(), position, linear_vel, .. *plane.body.t };
         self.update_sprite(self.old_transform.clone(), Transform::default(), TransformMask::default());

         self.state = PowerupState::Dropped;
      } else {
         panic!("Powerup::drop called on non-held powerup")
      }
   }

   /// Set the sprite's transform, transform mask & offset.
   fn update_sprite(&mut self, t: Ptr<Transform>, offset: Transform, mask: TransformMask) {
      self.body.t = t.clone();
      self.sprite.transform = t.clone();
      self.sprite.offset = offset;
      self.sprite.offset.scale *= self.sprite_scale;
      self.sprite.transform_mask = mask;
      self.container.set_transform(t.clone());
      self.container.set_offset(offset);
      self.container.set_mask(mask);
   }

   pub fn tick(&mut self) {
      if self.state == PowerupState::Dropped {
         let v = &mut self.body.t.linear_vel;
         let gravity = ::world::DEFAULT_GRAVITY; // TODO
         v.y += gravity * 0.9;

         pub const TERMINAL_VEL_SQ: f32 = 9.0 * 9.0;

         if v.length_sq() > TERMINAL_VEL_SQ {
            // Apply drag inversely proportional to speed.
            let rel_speed = (v.length_sq() / TERMINAL_VEL_SQ).sqrt();
            *v *= map(rel_speed, [1.0, 2.0], [1.0, 0.98]);
         }
      }

      if let Some(ref mut timer) = self.fade_timer {
         timer.tick();
         if timer.expired { self.alive = false; }
      }
   }

   pub fn on_collide(&mut self, c: Contact, a: &mut Body, b: &mut Body, evs: &mut Queue<ModelEv>) {
      if let Some(plane) = b.parent.try_to::<Plane>() {
         if !plane.alive { return }
         if !self.is_autouse && plane.item.is_some() { return }
         if self.team != Team::Spectator && plane.team != self.team { return }
         evs.push(ModelEv::PowerupCollide {
            powerup: self.me,
            plane,
         });
         return;
      }

      let (res_a, res_b) = c.resolve(a, b);
      res_a.apply(a);
      res_b.apply(b);
   }
}
