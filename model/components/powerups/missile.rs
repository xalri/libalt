use esc::storage::*;
use esc::geom::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;

use altx::*;
use components::*;
use events::*;

pub struct Missile {

}

pub struct MissilePowerup {
   pub common: Ptr<PowerupCommon>,
}

#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub struct MissilePowerupInit {
   #[rule("rule.id_rule")]
   pub id: NetID<MissilePowerup>,
   pub common: PowerupInit,
}

impl MissilePowerup {
   pub fn new(a: Args<MissilePowerupInit>) -> Ptr<MissilePowerup> {
      let mut common = PowerupCommon::new(a.map(a.init.common));

      let sprite_archetype = a.res().get_with_fn(|| {
         let xml = a.res().get::<XmlFormat<xml::AnimatedPoly>>(".poly/render/powerups/missile/93_138_168.animatedpoly", ())?;
         let mut sprite = anim_poly_to_sprite(a.res(), &*xml, Ptr::null())?;
         sprite.offset.scale = Vec2::one() * 0.95;
         sprite.depth.object = -1;
         for f in &mut sprite.frames { f.delay = 60.0; }
         Ok(sprite)
      }, "missile_sprite").unwrap();

      common.sprite = a.storage().insert(sprite_archetype.instance(common.container.transform.clone()));
      common.sprite_scale = 0.95;

      let mut p = a.storage().insert(MissilePowerup { common });
      a.map_component_id(&p, a.init.id);
      p.common.parent = p.weak().any();
      p
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr: PtrRule) -> ComponentInit {
      ComponentInit::MissilePowerup(MissilePowerupInit {
         id: ptr.weak_to_net(self.weak()).unwrap(),
         common: self.common.creation_ev(ptr),
      })
   }
}
