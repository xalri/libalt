use std::str::FromStr;

use esc::error::*;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum PowerupKind { Shield, Wall, Missile, Charge, Bomb, Health, Ball }

use self::PowerupKind::*;

impl PowerupKind {
   pub fn lifetime(&self) -> f32 {
      match *self {
         Missile | Wall | Shield | Bomb | Charge | Ball => ::seconds_to_ticks(6.3) as f32,
         Health => 100.0,
      }
   }
}

impl FromStr for PowerupKind {
   type Err = Error;
   fn from_str(s: &str) -> Result<Self> {
      Ok(match &s.to_lowercase()[..] {
         "homing missile" | "missile" => PowerupKind::Missile,
         "health" => PowerupKind::Health,
         "wall" => PowerupKind::Wall,
         "shield" => PowerupKind::Shield,
         "ball" => PowerupKind::Ball,
         "bomb" => PowerupKind::Bomb,
         "charge" | "demolition charge" => PowerupKind::Charge,
         x => bail!("Unknown powerup '{}'", x),
      })
   }
}