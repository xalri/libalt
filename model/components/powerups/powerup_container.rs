
use esc::storage::*;
use esc::render::*;
use esc::geom::*;

use team::*;
use storage::*;
use altx::*;

#[derive(Clone)]
pub struct PowerupContainer {
   pub under_sprite: Ptr<Sprite>,
   pub over_sprite: Ptr<Sprite>,
   pub outline_sprite: Ptr<Sprite>,
   pub transform: Ptr<Transform>,
}

impl PowerupContainer {
   pub fn new(res: &AtlasSource, storage: &mut Storage, team: Team) -> Ptr<PowerupContainer> {
      let archetype = res.get_with_fn(|| {
         let mut under_sprite = ::image_sprite(res, "planes/powerup/container_under.png")?;
         let mut over_sprite = ::image_sprite(res, "planes/powerup/container_over.png")?;
         let mut outline_sprite = ::image_sprite(res, "planes/powerup/container_outline.png")?;
         under_sprite.depth.local = 1;
         over_sprite.depth.local = -1;

         under_sprite.depth.object = -1;
         over_sprite.depth.object = -1;
         outline_sprite.depth.object = -1;

         Ok(PowerupContainer {
            under_sprite,
            over_sprite,
            outline_sprite,
            transform: Ptr::null(),
         })
      }, "altx::archetype::powerup_container").unwrap();

      let transform = storage.insert(Transform::default());

      let under_sprite = storage.insert(archetype.under_sprite.instance(transform.clone()));
      let over_sprite = storage.insert(archetype.over_sprite.instance(transform.clone()));

      let outline_sprite = if team == Team::Spectator { Ptr::null()} else {
         let mut outline = archetype.outline_sprite.instance(transform.clone());
         outline.colour = team.color();
         storage.insert(outline)
      };

      storage.insert(PowerupContainer { under_sprite, over_sprite, outline_sprite, transform })
   }

   pub fn set_alpha(&mut self, a: f32) {
      self.under_sprite.colour.a = a;
      self.over_sprite.colour.a = a;
      if !self.outline_sprite.is_null() { self.outline_sprite.colour.a = a; }
   }

   pub fn set_offset(&mut self, t: Transform) {
      self.under_sprite.offset = t;
      self.over_sprite.offset = t;
      if !self.outline_sprite.is_null() { self.outline_sprite.offset = t; }
   }

   pub fn set_mask(&mut self, t: TransformMask) {
      self.under_sprite.transform_mask = t;
      self.over_sprite.transform_mask = t;
      if !self.outline_sprite.is_null() { self.outline_sprite.transform_mask = t; }
   }

   pub fn set_transform(&mut self, t: Ptr<Transform>) {
      self.under_sprite.transform = t.clone();
      self.over_sprite.transform = t.clone();
      if !self.outline_sprite.is_null() { self.outline_sprite.transform = t.clone(); }
   }
}
