
use rand::*;

use esc::storage::*;
use esc::render::*;
use esc::geom::*;
use esc::util::time::*;

use altx::*;
use storage::*;

/// Fragments of a used powerup container.
pub struct ShatteredPowerupContainer {
   fragments: Vec<Ptr<ContainerFragment>>,
   pub alive: bool,
}

/// A single fragment of a powerup container
pub struct ContainerFragment {
   body: Ptr<Body>,
   sprite: Ptr<Sprite>,
   lifetime: Ptr<Delay>,
}

// Static fragment polygons
struct FragmentPoly {
   pos: Vec2,
   points: &'static [(f32, f32)]
}
const SHATTERED_CIRCLE: [FragmentPoly; 19] = [
   FragmentPoly{ pos: vec2(-44.3, 5.7), points: &[(-6.7, -18.7), (-7.7, -6.7), (-6.7, 10.3), (-1.7, 20.3), (5.3, 13.3), (17.3, -18.7), ] },
   FragmentPoly{ pos: vec2(-30.0, -17.8), points: &[(-21.0, 4.8), (-17.0, -5.2), (-9.0, -17.2), (12.0, -4.2), (32.0, 16.8), (3.0, 4.8), ] },
   FragmentPoly{ pos: vec2(-27.8, 25.0), points: &[(-18.2, 1.0), (-11.2, -6.0), (29.8, -26.0), (11.8, 1.0), (-1.2, 18.0), (-11.2, 12.0), ] },
   FragmentPoly{ pos: vec2(-21.3, 1.7), points: &[(-17.7, 17.3), (-5.7, -14.7), (23.3, -2.7), ] },
   FragmentPoly{ pos: vec2(-8.8, 43.6), points: &[(-20.2, -0.6), (-9.2, 5.4), (1.8, 8.4), (15.8, 8.4), (11.8, -21.6), ] },
   FragmentPoly{ pos: vec2(-8.0, 21.3), points: &[(11.0, 0.7), (10.0, -22.3), (-21.0, 21.7), ] },
   FragmentPoly{ pos: vec2(15.3, 7.7), points: &[(-12.3, -8.7), (10.7, 0.3), (1.7, 8.3), ] },
   FragmentPoly{ pos: vec2(14.7, 24.3), points: &[(-12.7, -25.3), (-9.7, 11.7), (22.3, 13.7), ] },
   FragmentPoly{ pos: vec2(18.5, 43.2), points: &[(18.5, -5.2), (6.5, 3.8), (-11.5, 8.8), (-13.5, -7.2), ] },
   FragmentPoly{ pos: vec2(7.2, -6.2), points: &[(-17.2, -7.8), (3.8, -11.8), (18.8, 14.2), (-5.2, 5.2), ] },
   FragmentPoly{ pos: vec2(34.8, 22.2), points: &[(-8.8, -14.2), (15.2, -4.2), (9.2, 8.8), (2.2, 15.8), (-17.8, -6.2), ] },
   FragmentPoly{ pos: vec2(38.0, -1.5), points: &[(-20.0, -3.5), (-7.0, -11.5), (12.0, -16.5), (15.0, 2.5), (12.0, 19.5), (-12.0, 9.5), ] },
   FragmentPoly{ pos: vec2(22.5, -26.2), points: &[(-11.5, 8.2), (-8.5, -9.8), (-0.5, -22.8), (16.5, -9.8), (8.5, 13.2), (-4.5, 21.2), ] },
   FragmentPoly{ pos: vec2(41.0, -24.0), points: &[(-10.0, 11.0), (9.0, 6.0), (3.0, -5.0), (-2.0, -12.0), ] },
   FragmentPoly{ pos: vec2(-21.7, -32.0), points: &[(11.7, 18.0), (6.7, 2.0), (2.7, -17.0), (-8.3, -11.0), (-17.3, -3.0), (4.7, 11.0), ] },
   FragmentPoly{ pos: vec2(-10.0, -38.6), points: &[(0.0, 24.6), (6.0, 12.6), (7.0, -14.4), (-4.0, -12.4), (-9.0, -10.4), ] },
   FragmentPoly{ pos: vec2(10.8, -39.6), points: &[(-14.8, 13.6), (-5.8, 2.6), (6.2, -10.4), (11.2, -9.4), (3.2, 3.6), ] },
   FragmentPoly{ pos: vec2(2.8, -23.5), points: &[(11.2, -12.5), (-6.8, -2.5), (-12.8, 9.5), (8.2, 5.5), ] },
   FragmentPoly{ pos: vec2(2.4, -41.4), points: &[(-5.4, 15.4), (14.6, -8.6), (2.6, -10.6), (-5.4, -11.6), (-6.4, 15.4), ] },
];

impl ContainerFragment {
   pub fn tick(&mut self) {
      self.sprite.colour.a = self.lifetime.fraction_remaining().powi(3);
      self.body.t.linear_vel.y += ::world::DEFAULT_GRAVITY;
      self.body.t.linear_vel *= 0.98;
   }
}

impl ShatteredPowerupContainer {
   pub fn new(storage: &mut Storage, res: &AtlasSource, center: Vec2, vel: Vec2) -> ShatteredPowerupContainer {

      // Create an archetype, multiple instances of each fragment will use the same VBO etc.
      let archetype = res.get_with_fn(|| {
         let mat = res.insert(Material { density: 0.05, .. Material::default() });

         Ok(SHATTERED_CIRCLE.iter().map(|f| {
            let poly = res.insert(Poly::new(f.points));
            let mut t = res.emplace::<Transform>();
            let scale = 0.3;
            t.scale = Vec2::one() * scale;
            t.position = f.pos * scale;
            t.linear_vel = f.pos * 0.12; // expand from the container's center

            let mut body = Body::new(poly.clone(), t.clone(), mat.clone(), false);
            body.collision_id = ::collision_id::MISC;
            body.collision_mask |= ::collision_id::LANDSCAPE;
            body.collision_mask |= ::collision_id::PLANE;

            let mut sprite = Sprite::filled_poly(poly, t.clone());
            sprite.colour = Colour::from_u8(95, 169, 221, 255);

            ContainerFragment { body: res.insert(body), sprite: res.insert(sprite), lifetime: Ptr::null() }
         }).collect::<Vec<_>>())
      }, "container_fragments").unwrap();

      ShatteredPowerupContainer {
         fragments: archetype.iter().map(|f| {
            let mut t = storage.insert(*f.body.t);
            t.position += center;
            t.linear_vel += vel;

            let mut rng = thread_rng();
            let lifetime = rng.gen_range(20.0, 50.0);
            t.orientation = rng.gen_range(-20.0, 20.0);
            t.angular_vel = rng.gen_range(-1.0, 1.0);

            let f = ContainerFragment {
               body: storage.insert(f.body.instance(t.clone())),
               sprite: storage.insert(f.sprite.instance(t.clone())),
               lifetime: storage.insert(Delay::new(lifetime)),
            };
            storage.insert(f)
         }).collect(),
         alive: true,
      }
   }

   pub fn tick(&mut self) {
      self.fragments.retain(|f| !f.lifetime.expired);
      if self.fragments.is_empty() { self.alive = false }
   }
}
