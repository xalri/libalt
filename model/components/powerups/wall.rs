use esc::storage::*;
use esc::geom::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;

use components::*;
use events::*;

/// A portable wall made of solidified lightning or something. Nobody's quite sure how it works, just
/// try not to run into it.
/// Acts as an obstacle for enemy planes & projectiles, friendly entities can pass through.
pub struct Wall {

}

pub struct WallPowerup {
   pub common: Ptr<PowerupCommon>,
}

#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub struct WallPowerupInit {
   #[rule("rule.id_rule")]
   pub id: NetID<WallPowerup>,
   pub common: PowerupInit,
}

impl WallPowerup {
   pub fn new(a: Args<WallPowerupInit>) -> Ptr<WallPowerup> {
      let mut common = PowerupCommon::new(a.map(a.init.common));

      let sprite_archetype = a.res().get_with_fn(|| {
         let mut sprite = ::xml_poly_sprite(a.res(), ".poly/planes/powerup/wall.poly")?;
         sprite.offset.scale = Vec2::one() * 0.3;
         sprite.depth.object = -1;
         Ok(sprite)
      }, "wall_sprite").unwrap();

      common.sprite = a.storage().insert(sprite_archetype.instance(common.container.transform.clone()));
      common.sprite_scale = 0.3;

      let mut p = a.storage().insert(WallPowerup { common });
      a.map_component_id(&p, a.init.id);
      p.common.parent = p.weak().any();
      p
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr: PtrRule) -> ComponentInit {
      ComponentInit::WallPowerup( WallPowerupInit {
         id: ptr.weak_to_net(self.weak()).unwrap(),
         common: self.common.creation_ev(ptr),
      })
   }
}
