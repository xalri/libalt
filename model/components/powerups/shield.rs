use esc::storage::*;
use esc::geom::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;

use components::*;
use events::*;

pub struct ShieldPowerup {
   pub common: Ptr<PowerupCommon>,
}

#[derive(Clone, Encode, Debug)]
#[rule("EventRule")]
pub struct ShieldPowerupInit {
   #[rule("rule.id_rule")]
   pub id: NetID<ShieldPowerup>,
   pub common: PowerupInit,
}

impl ShieldPowerup {
   pub fn new(a: Args<ShieldPowerupInit>) -> Ptr<ShieldPowerup> {
      let mut common = PowerupCommon::new(a.map(a.init.common));

      let sprite_archetype = a.res().get_with_fn(|| {
         let mut sprite = ::xml_poly_sprite(a.res(), ".poly/planes/powerup/shield/shield.poly")?;
         sprite.offset.scale = Vec2::one() * 0.25;
         sprite.depth.object = -1;
         Ok(sprite)
      }, "shield_sprite").unwrap();

      common.sprite = a.storage().insert(sprite_archetype.instance(common.container.transform.clone()));
      common.sprite_scale = 0.25;

      let mut p = a.storage().insert(ShieldPowerup { common });
      a.map_component_id(&p, a.init.id);
      p.common.parent = p.weak().any();
      p
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr: PtrRule) -> ComponentInit {
      ComponentInit::ShieldPowerup(ShieldPowerupInit {
         id: ptr.weak_to_net(self.weak()).unwrap(),
         common: self.common.creation_ev(ptr),
      })
   }
}
