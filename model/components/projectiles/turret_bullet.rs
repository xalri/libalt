use esc::storage::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;
use events::EventRule;
use components::args::*;
use components::*;
use events::*;
use altx::*;

#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub struct TurretBulletInit {
   #[rule("rule.id_rule")]
   pub id: NetID<TurretBullet>,
   pub common: BulletInit,
}

pub struct TurretBullet {
   pub common: Ptr<Bullet>,
}

impl TurretBullet {
   pub fn new(a: Args<TurretBulletInit>) -> Ptr<TurretBullet> {
      let archetype = a.res().get_with_fn(|| {
         let xml = a.res().get::<XmlFormat<xml::TexturedPoly>>(".poly/map/turret/bullet.poly", ())?;
         let (body, sprite) = ::body_sprite(a.res(), &*xml)?;

         Ok((body, sprite))
      }, "altx::archetypes::turret_bullet").unwrap();

      let common = Bullet::new(a.map(a.init.common), &archetype);

      let bullet = a.storage().insert(TurretBullet { common });

      a.map_component_id(&bullet, a.init.id);
      bullet
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr_rule: PtrRule) -> ComponentInit {
      ComponentInit::TurretBullet(TurretBulletInit {
         id: ptr_rule.weak_to_net(self.weak()).unwrap(),
         common: self.common.creation_ev(ptr_rule),
      })
   }
}
