
use esc::render::*;
use esc::geom::*;
use esc::storage::*;
use esc::encode::*;
use esc::error::*;
use esc::net::id::*;
use esc::util::time::*;
use events::EventRule;
use components::args::*;
use team::*;

pub struct Bullet {
   pub alive: bool,
   pub sprite: Ptr<Sprite>,
   pub body: Ptr<Body>,
   pub lifetime: Delay,
   pub team: Team,
}

/// Common initialization data for a bullet
#[derive(Clone, Copy, Encode, Debug)]
#[rule("EventRule")]
pub struct BulletInit {
   #[rule("rule.id_rule")]
   pub id: NetID<Bullet>,
   #[rule("TeamRule")]
   pub team: Team,
   #[rule("BitableRule")]
   pub lifetime: i32,
   #[rule("rule.position()")]
   pub position: Vec2,
   #[rule("ranged_vec(-50.0, 50.0, 5.0)")]
   pub linear_vel: Vec2,
   #[rule("angle_rule(1.0)")]
   pub orientation: f32,
}

impl Bullet {
   // TODO: TurretBullet etc types
   pub fn new(a: Args<BulletInit>, archetype: &Ptr<(Ptr<Body>, Ptr<Sprite>)>) -> Ptr<Bullet> {
      let transform = a.storage().insert(Transform {
         position: a.init.position,
         linear_vel: a.init.linear_vel,
         orientation: a.init.orientation,
         .. Transform::default()
      });

      let body = a.storage().insert(archetype.0.instance(transform.clone()));
      let sprite = a.storage().insert(archetype.1.instance(transform.clone()));

      let bullet = a.storage().insert(Bullet {
         body,
         sprite,
         alive: true,
         team: a.init.team,
         lifetime: Delay::new(a.init.lifetime as f32),
      });

      a.map_component_id(&bullet, a.init.id);
      bullet
   }

   pub fn creation_ev(self: &Ptr<Self>, ptr_rule: PtrRule) -> BulletInit {
      BulletInit {
         id: ptr_rule.weak_to_net(self.weak()).unwrap(),
         team: self.team,
         lifetime: self.lifetime.remaining as i32,
         position: self.body.t.position,
         linear_vel: self.body.t.linear_vel,
         orientation: self.body.t.orientation,
      }
   }

   pub fn tick(&mut self) {
      self.lifetime.tick();
      if self.lifetime.expired { self.alive = false }
   }
}

impl Init for Bullet {
   fn init(&mut self, _p: Weak<Bullet>) {
      //self.body.parent = p.any();
   }
}
