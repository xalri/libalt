
use std::fmt;

use esc::encode::*;
use esc::error::*;

use uuid::*;

pub struct ClientIDRule;

/// A Client ID as known to a server
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("ClientIDRule")]
pub struct ClientID {
   /// The player's current nickname
   #[rule("StringRule::short()")]
   pub nickname: String,
   /// The player's unique identifier
   #[rule("UUIDRule")]
   pub id: UUID,
}

impl fmt::Display for ClientID {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "'{}' ({})", self.nickname, self.id)
   }
}
