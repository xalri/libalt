//! Sending and tracking pings

use std::collections::HashMap;
use std::net::SocketAddr;

use esc::encode::*;
use esc::error::*;
use esc::util::time::*;
use esc::util::buffer::*;
use esc::net::socket::Sender;

use message::*;

/// The data for a ping message
#[derive(PartialEq, Debug, Clone, Copy, Encode)]
#[rule("MessageDataRule")]
pub enum Ping {
   Ping(#[rule("BitableRule")] u32),
   Pong(#[rule("BitableRule")] u32),
}

/// Ping responses are stored in a circular buffer -- only the newest responses
/// are kept. BUFFER_LEN defines the size of this buffer.
const BUFFER_LEN: usize = 20;

/// The time in milliseconds between pings to each client/server.
const PING_TIME_MS: f64 = 300.0;

/// The time after which to automatically stop pinging an unresponsive client/server.
const TIMEOUT_NS: u64 = 10_000_000_000;

/// The time before we consider a ping lost.
const LOST_NS: u64 = 800_000_000;

/// Ping tracking
pub struct Pings {
   /// Maps socket addresses to the individual ping trackers.
   clients: HashMap<SocketAddr, PingClient>,
   /// A channel on which to send messages to clients.
   sender: Sender<Message>,
   ping_counter: usize,
   ping_idle: usize,
   last_update: u64,
}

impl Pings {
   /// Create a new `Pings`
   pub fn new(send: Sender<Message>) -> Pings {
      Pings {
         clients: HashMap::new(),
         sender: send,
         ping_counter: 0,
         ping_idle: 0,
         last_update: precise_time_ns(),
      }
   }

   /// Get the `PingClient` for the given address, creating it if one doesn't
   /// exist.
   pub fn get(&mut self, addr: SocketAddr) -> &mut PingClient {
      self.clients.entry(addr).or_insert_with(|| PingClient::new())
   }

   /// Handle an incoming message.
   pub fn process(&mut self, ping: Ping, addr: SocketAddr) {
      match ping {
         Ping::Ping(id) => {
            let data = MessageData::Ping(Ping::Pong(id));
            let msg = Message::new_ping(addr, data);
            self.sender.send(msg);
         }
         Ping::Pong(id) => {
            self.get(addr).receive(id)
         }
      }
   }

   /// Update clients. Pings are sent to each client at an interval of `PING_TIME_MS`, pings to
   /// multiple clients are distributed uniformly over time.
   pub fn update(&mut self) {
      let diff = ns_to_ms(precise_time_ns() - self.last_update);
      if diff < 1.0 { return; }

      self.last_update = precise_time_ns();

      let updates_per_ping = PING_TIME_MS / diff;
      let target_pings: f64 = self.clients.len() as f64 / updates_per_ping;
      if self.ping_counter >= self.clients.len() {
         self.ping_counter = 0;
         if target_pings < 1.0 {
            self.ping_idle = ((1.0 - target_pings) * updates_per_ping) as usize;
         }
      }
      let mut target_pings = target_pings as usize;

      if self.ping_idle > 0 {
         self.ping_idle -= 1;
      } else {
         target_pings += 1;
      }

      let mut to_remove = Vec::new();
      let mut pings = 0;

      for (i, (&addr, client)) in self.clients.iter_mut().enumerate() {
         if precise_time_ns() - client.last_response > TIMEOUT_NS {
            to_remove.push(addr);
            continue;
         }

         if pings < target_pings && i >= self.ping_counter {
            pings += 1;
            self.ping_counter += 1;
            let data = client.send();
            let msg = Message::new_ping(addr, data);
            self.sender.send(msg);
         }
         client.update_lost();
      }

      for addr in to_remove {
         self.remove(addr);
      }
   }

   /// Remove a client
   pub fn remove(&mut self, addr: SocketAddr) {
      self.clients.remove(&addr);
   }
}

/// Tracks ping responses for a single client or server
pub struct PingClient {
   /// The time a ping was last sent
   last_sent: u64,
   /// The time a ping was last received
   last_response: u64,
   /// Pings for which we're currently waiting for a reply
   current: HashMap<u32, u64>,
   /// The ID of the last ping sent.
   id: u32,
   /// A ring buffer of ping responses. An entry Some(x) is added for every
   /// response received, and None for every lost packet.
   past: Buffer<Option<u64>>,
}

impl PingClient {
   /// Create a new ping client
   pub fn new() -> PingClient {
      PingClient {
         last_sent: precise_time_ns(),
         last_response: precise_time_ns(),
         current: HashMap::with_capacity(64),
         id: 0,
         past: Buffer::with_capacity(BUFFER_LEN),
      }
   }

   /// Calculates a weighted average ping time in milliseconds, where recent pings are worth
   /// more than older pings.
   pub fn ping(&self) -> Option<f64> {
      let mut nanos = self.past.iter().filter(|x| x.is_some()).map(|t| ns_to_ms(t.unwrap()));

      if let Some(first) = nanos.next() {
         let avr = nanos.fold(first, |acc, x| (acc / 2.0) + (x / 2.0));
         Some(avr)
      } else {
         None
      }
   }

   /// Calculates the packet loss
   pub fn packet_loss(&self) -> f64 {
      if self.past.is_empty() {
         return 0.;
      }
      self.past.iter().filter(|x| x.is_none()).count() as f64 / self.past.len() as f64
   }

   /// Get the most recent time we sent a ping
   pub fn last_sent(&self) -> u64 {
      self.last_sent
   }

   /// Ping this client. Returns the message data to send.
   fn send(&mut self) -> MessageData {
      let now = precise_time_ns();
      self.last_sent = now;
      self.id += 1;
      self.current.insert(self.id, now);
      MessageData::Ping(Ping::Ping(self.id))
   }

   /// Process an incoming ping
   fn receive(&mut self, id: u32) {
      if let Some(x) = self.current.remove(&id) {
         self.last_response = precise_time_ns();
         self.past.add(Some(precise_time_ns() - x));
      }
   }

   /// Check for lost pings.
   fn update_lost(&mut self) {
      // Separate the current pings into two groups, the first to keep waiting for,
      // the second to discard as lost.
      let (keep, lost) = self.current
         .drain()
         .partition(|&(_, sent)| (precise_time_ns() - sent) < LOST_NS);
      self.current = keep;

      // For each of the lost pings, add `None` to the buffer
      for _ in lost {
         self.past.add(None)
      }
   }
}
