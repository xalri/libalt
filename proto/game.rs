use esc::net::sequencer::*;
use esc::net::id::*;
use esc::encode::*;
use esc::geom::*;
use esc::error::*;
use esc::storage::*;
use model::events::*;
use model::components::*;
use message::*;

/// Game message, contains events & component state.
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("MessageDataRule")]
pub struct GameMessage {
   /// Sequenced game events.
   #[rule("SequentialDataRule")]
   pub events: SequentialData,
   /// Transient game state, not sequenced.
   #[rule("NestedRule")]
   pub state: NestedData,
}

/// An event sent by a client to the server.
#[derive(Debug, Encode)]
#[rule("EventRule")]
pub enum ClientEv {
   /// A change in world state
   World(WorldEv),
   /// A plane collided with a powerup
   PowerupCollide {
      #[rule("rule.ptr_rule")]
      powerup: Weak<PowerupCommon>,
      #[rule("rule.ptr_rule")]
      plane: Weak<Plane>,
      /// The position of the powerup at the time of collision
      #[rule("rule.position()")]
      pos: Vec2,
      /// The combined velocity of the powerup and plane at the time of collision
      #[rule("ranged_vec(-50.0, 50.0, 5.0)")]
      vel: Vec2,
   }
}

/// An event sent by the server to a client.
#[derive(Debug, Encode)]
#[rule("EventRule")]
pub enum ServerEv {
   /// A change in world state
   World(WorldEv),
   /// Initial world state synchronisation is complete
   SyncComplete {
      /// Identifies the player among other players on the server
      #[rule("BitableRule")]
      player_no: i16,
   },
   /// Gives the global ID for a new component created by a client.
   IDMap {
      #[rule("rule.id_rule")] local: NetID<()>,
      #[rule("rule.id_rule")] global: NetID<()>
   },
   /// The number of game ticks the client has been connected for. Used to correct for clock
   /// differences or skipped frames.
   Tick(#[rule("BitableRule")] u32),
}
