use esc::encode::*;
use esc::error::*;
use version::*;
use client_id::*;
use model::world::*;
use message::*;

/// A join message.
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("MessageDataRule")]
pub enum Join {
   /// A request to join a server
   Request(JoinRequest),
   /// A server's response to a join request
   JoinDenied {
      #[rule("StringRule::short()")]
      message: String,
   },
   /// A request for the client to load the included resources and configuration
   Load {
      #[rule("BitsRule(2)")]
      counter: u8,
      #[rule("MapDescriptorRule")]
      resource: MapDescriptor,
   },
   /// A response to `Load`, indicating that the given resource is loaded
   LoadResp {
      #[rule("MapDescriptorRule")]
      resource: MapDescriptor,
   },
   /// Sent while the client is loading, to indicate to the server that they
   /// are still connected.
   KeepAlive,
   /// A message sent when a client disconnects from the server
   Disconnect,
}

#[derive(Clone, PartialEq, Debug, Encode)]
#[rule("MessageDataRule")]
pub struct JoinRequest {
   /// The client's game version
   #[rule("VersionRule")]
   pub version: Version,
   /// An optional password, for joining password-protected servers
   #[rule("OptionRule::new(StringRule::short())")]
   pub password: Option<String>,
   /// Information about the player.
   #[rule("ClientIDRule")]
   pub id: ClientID,
   /// Authentication data
   pub auth: ClientAuth,
}

/// Client authentication data
#[derive(Clone, PartialEq, Debug, Encode)]
#[rule("MessageDataRule")]
pub enum ClientAuth {
   None,
   // TODO:
   /*
   Vapor {
      /// The client's WLAN address.
      #[rule("OptionRule::new(SocketAddrRule)")]
      addr: Option<SocketAddr>,
      /// The client's public session ID, used for verification.
      #[rule("UUIDRule")]
      session_id: Option<UUID>,
   }
   */
}
