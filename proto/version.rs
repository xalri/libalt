use std::str::FromStr;
use std::fmt;
use esc::error::*;
use esc::encode::*;

pub struct VersionRule;

/// A game version.
#[derive(Copy, Clone, PartialEq, Eq, Encode)]
#[rule("VersionRule")]
pub struct Version {
   #[rule("BitableRule")]
   pub major: u16,
   #[rule("BitableRule")]
   pub minor: u16,
   #[rule("BitableRule")]
   pub patch: u16,
}

impl FromStr for Version {
   type Err = Error;
   fn from_str(s: &str) -> Result<Version> {
      use std::result;
      let x: result::Result<Vec<u16>, _> = s.split('.')
         .map(|x| x.parse::<u16>())
         .collect();
      match x {
         Ok(ref parts) if parts.len() == 3 => {
            Ok(Version {
               major: parts[0],
               minor: parts[1],
               patch: parts[2],
            })
         }
         _ => bail!("Failed to parse version string '{}'", s),
      }
   }
}

impl fmt::Display for Version {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
   }
}

impl fmt::Debug for Version {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "{}", self)
   }
}
