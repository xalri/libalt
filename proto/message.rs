
use esc::net::guarantee::*;
use esc::encode::*;
use esc::error::*;

use super::*;

/// A top-level message, contains a header for delivery & sequencing guarantees and the message
/// payload.
pub type Message = Guaranteed<MessageData>;

/// A rule for encoding message data
#[derive(Debug, Clone, Copy)]
pub struct MessageDataRule;

/// The main message payload, each subsystem has it's own message type.
#[derive(PartialEq, Debug, Clone, Encode)]
#[rule("MessageDataRule")]
pub enum MessageData {
   /// A ping message
   Ping(Ping),
   /// Joining/leaving servers.
   Join(Join),
   /// The message type for in-game data (events & component updates)
   Game(GameMessage),
}
