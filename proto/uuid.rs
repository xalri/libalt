
use std::fmt;
use std::str::FromStr;

use esc::error::*;
use esc::encode::*;

pub struct UUIDRule;

/// A Unique identifier. Used for account identifiers and public and private
/// session IDs for vapor. The server is sent a user's vapor (account) ID and
/// their public session ID for verification.
#[derive(PartialEq, Eq, Clone, Copy, Default, Encode)]
#[rule("UUIDRule")]
pub struct UUID {
   #[rule("BitableRule")]
   pub a: u64,
   #[rule("BitableRule")]
   pub b: u64,
}

impl FromStr for UUID {
   type Err = Error;
   fn from_str(s: &str) -> Result<UUID> {
      use std::u64;
      if s.len() != 36 {
         bail!("Invalid length for UUID string, ({}, should be 36)", s.len());
      }
      let (mut a, mut b) = (0u64, 0u64);
      a |= u64::from_str_radix(&s[0..8], 16)? << 32;
      a |= u64::from_str_radix(&s[9..13], 16)? << 16;
      a |= u64::from_str_radix(&s[14..18], 16)?;
      b |= u64::from_str_radix(&s[19..23], 16)? << 48;
      b |= u64::from_str_radix(&s[24..36], 16)?;
      Ok(UUID { a, b })
   }
}

impl fmt::Display for UUID {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      let (a, b) = (self.a, self.b);
      write!(f,
             "{:08x}-{:04x}-{:04x}-{:04x}-{:012x}",
             a >> 32,
             a >> 16 & 0xFFFF,
             a & 0xFFFF,
             b >> 48,
             b & 0xFFFFFFFFFFFFu64)
   }
}

impl fmt::Debug for UUID {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      // Use the same format as `Display`
      write!(f, "{}", self)
   }
}

#[cfg(test)]
mod test {
   use super::*;

   /// UUIDRule works.
   #[test]
   fn uuid_string() {
      let s = "a9d97c25-09a5-432f-b221-7e3162296424";
      let uuid = UUID::from_str(s).unwrap();
      let result = format!("{}", uuid);
      assert_eq!(&result[..], s);
   }
}