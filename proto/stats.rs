
/// Stats for a single player
#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct PlayerStats {
   pub kills: i32,
   pub deaths: i32,
   pub crashes: i32,
   pub assists: i32,
   pub damage_dealt: f32,
   pub damage_taken: f32,
   pub structure_damage: f32,
   pub longest_life: i32,
   pub kill_streak: i32,
   pub multikill: i32,
   pub experience: i32,
   pub goals_scored: i32,
   pub goals_assisted: i32,
   pub ball_possession_time: i32,
}

/// The data shown on the scoreboard as sent by the server
pub struct Scoreboard {
   /// The pings and stats of the current players in ascending player number order.
   pub players: Vec<(u32, ScoreboardStats)>,
}

/// A single player's stats as shown on the scoreboard
#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct ScoreboardStats {
   pub kills: i32,
   pub deaths: i32,
   pub assists: i32,
}

impl PlayerStats {
   pub fn scoreboard(&self) -> ScoreboardStats {
      ScoreboardStats {
         kills: self.kills,
         deaths: self.deaths,
         assists: self.assists,
      }
   }
}
