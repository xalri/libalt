#!/bin/sh
openssl pkcs12 -export \
   -out hunt.pfx \
   -inkey certs/xa1.uk/privkey.pem \
   -in certs/xa1.uk/cert.pem \
   -certfile certs/xa1.uk/fullchain.pem

