#[macro_use] extern crate slog;
#[macro_use] extern crate error_chain;
extern crate antidote;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_tls;
extern crate native_tls;
extern crate futures;
extern crate bytes;
extern crate util;
extern crate proto;
extern crate model;
#[macro_use] extern crate net;

use slog::Logger as Log;

pub mod tls_listen;

pub mod error {
   error_chain! {
      links {
         Proto(::proto::error::Error, ::proto::error::ErrorKind);
      }
      foreign_links {
         IO(::std::io::Error);
         TLS(::native_tls::Error);
      }
      errors { }
   }
}
