//! A whole bunch of types used in many messages.

use event::ScoreboardStats;
use error::*;
use std::fmt;
use std::str::FromStr;
use std::net::SocketAddr;

/*

/// The game version implemented by this version of libalt.
pub const VERSION: Version = Version {
   major: 0,
   minor: 0,
   patch: 374,
};

/// A Client ID as known to a server
#[derive(PartialEq, Debug, Clone, Derivative)]
#[derivative(Default)]
pub struct ClientID {
   pub nickname: String,
   pub vapor: UUID,
   /// A unique identifier for differentiating between players with the same nickname.
   pub unique: u64,
   /// The address the client is connected from.
   pub addr: Option<SocketAddr>,
   /// The client's public vapor session ID, used for verification.
   pub session_id: Option<UUID>,
   /// The total time in milliseconds the client has been active in the game.
   pub play_time: i64,
   pub affiliate_code: u16,
   pub c_points: CommunityPoints,
   pub skins: Vec<u8>,
   pub logged_in: bool,
}

/// A client's level and ace
#[derive(PartialEq, Debug, Clone, Default)]
pub struct ClientLevel {
   pub ace: u8,
   pub level: u8,
}

/// A user's community points, split by the reason the points were given.
#[derive(PartialEq, Debug, Clone, Default)]
pub struct CommunityPoints {
   /// Clicks of the user's refer link.
   pub refer: u16,
   /// Connected to Facebook.
   pub facebook: u16,
   /// Referred player bought the game.
   pub refer_buy: u16,
   /// User gifted the game.
   pub gifted: u16,
   /// Blackmailed an admin.
   pub admin: u16,
   /// Joined the steam group.
   pub steam_group: u16,
}

*/

/*
/// A game resource -- typically a map.
#[derive(PartialEq, Debug, Clone)]
pub struct MapDescriptor {
   pub path: String,
   pub size: u32,
   pub crc32: u64,
}

/// An altitude game version
#[derive(PartialEq, Debug, Clone)]
pub struct Version {
   pub major: i32,
   pub minor: i32,
   pub patch: i32,
}

/// Some data with a signature which can be verified
#[derive(PartialEq, Debug, Clone)]
pub struct Signed<T> {
   pub signature: Vec<u8>,
   pub data: Vec<u8>,
   pub value: T,
}

/// The information sent to a client on sucessful login, contains their
/// account settings and `ClientID`.
#[derive(PartialEq, Debug, Clone)]
pub struct PrivClientID {
   pub vapor_opts: VaporSettings,
   pub id: ClientID,
}

/// A client's account settings
#[derive(PartialEq, Debug, Clone)]
pub struct VaporSettings {
   pub username: String,
   pub private_sid: Option<UUID>,
   pub hw_info_time: i64,
   pub logged_in_ms: i64,
   pub no_chat: bool,
   pub email_opt_in: bool,
   pub chat_filter: bool,
   pub map_filter: bool,
   pub is_guest: bool,
   pub name_filter: bool,
   pub steam_uid: Option<i64>,
   pub auth_method: AuthMethod,
   pub created: i64,
}

/// An identifier for a game which uses vapor
#[derive(PartialEq, Debug, Clone)]
pub struct GameID {
   pub id: i16,
   pub version: String,
   pub name: String,
}

/// Vapor's error format, includes the error message and the users' private ID
#[derive(PartialEq, Debug, Clone)]
pub struct VaporError {
   pub message: String,
   pub private_sid: UUID,
}

/// `Unknown`, `OnlineConduct`, or `AdminUpdate`
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum BanReason {
   Unknown,
   OnlineConduct,
   AdminUpdate,
}

/// `Windows`, `Mac`, `Linux`, or `Unknown`
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Platform {
   Unknown,
   Windows,
   Mac,
   Linux,
}

/// `Nimbly`, `Steam`, or `Unknown`
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Distributor {
   Unknown,
   Nimbly,
   Steam,
}

/// `Vapor`, `Steam`, or `Unknown`
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum AuthMethod {
   Vapor,
   Steam,
   Unknown,
}

// =====================================================
// -----------------  Various Impls --------------------
// =====================================================

impl FromStr for Version {
   type Err = Error;
   fn from_str(s: &str) -> Result<Version> {
      use std::result;
      let x: result::Result<Vec<i32>, _> = s.split('.')
         .map(|x| x.parse::<i32>())
         .collect();
      match x {
         Ok(ref parts) if parts.len() == 3 => {
            Ok(Version {
               major: parts[0],
               minor: parts[1],
               patch: parts[2],
            })
         }
         _ => Err(format!("Failed to parse version string '{}'", s).into()),
      }
   }
}

impl fmt::Display for Version {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
   }
}

impl fmt::Display for ClientID {
   fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "nick: {}, vapor: {}", self.nickname, self.vapor)
   }
}
*/
