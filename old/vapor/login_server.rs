extern crate vapor;
extern crate util;
extern crate model;
extern crate proto;
extern crate antidote;
extern crate tokio_core;
#[macro_use] extern crate slog;

use std::net::SocketAddr;
use std::sync::Arc;
use antidote::Mutex;
use tokio_core::reactor::Core;

use vapor::tls_listen;
use util::logger;
use model::types::*;
use proto::message::*;

pub fn main() {
   // Create a 'reactor core', an event loop on which futures run.
   let mut core = Core::new().unwrap();
   let log = logger::trace_file("login_server");

   let archive_pw = "fc5fa524677045819c31fee72f826ee1".to_string();

   let service = SharedService(Arc::new(Mutex::new(Service::new(log.clone()))));

   let tls_config = tls_listen::SocketConfig{
      log: log.clone(),
      tcp_addr: ([0,0,0,0],31373).into(),
      key_archive: tls_listen::pkcs12_from_file("keys/vapor.pfx", &archive_pw).unwrap(),
      service: Box::new(service),
   };

   tls_listen::listen(tls_config, core.handle()).unwrap();

   loop { core.turn(None) }
}

pub struct Service {
   log: slog::Logger,
}

pub struct SharedService(Arc<Mutex<Service>>);

impl Service {
   /// Create a new login service.
   pub fn new(log: slog::Logger) -> Service {
      Service { log }
   }

   fn handle_auth(&mut self, _addr: SocketAddr, msg: Auth) -> Auth {
      match msg {
         Auth::Login(Some(req)) => {
            info!(self.log, "Got login request! {:?}", req);
         }
         x => warn!(self.log, "Invalid message: {:?}", x),
      }

      Auth::Disconnected(VaporError{
         message: "Unimplemented".to_string(),
         private_sid: UUID{ a:0, b:0 },
      })
   }
}

impl tls_listen::Service for SharedService {
   fn handle_auth(&mut self, addr: SocketAddr, msg: Auth) -> Auth {
      self.0.lock().handle_auth(addr, msg)
   }
}
