use std::sync::Arc;
use std::net::SocketAddr;
use std::fs::File;
use std::io::Read;
use futures::*;
use tokio_core::net::TcpListener;
use tokio_core::reactor::Handle;
use tokio_io::*;
use antidote::Mutex;
use native_tls::{Pkcs12, TlsAcceptor};
use tokio_tls::*;
use tokio_io::codec::*;
use bytes::*;

use error::*;
use proto::message::*;
use proto::rules::*;
use net::encode::*;

/// A vapor service: takes requests, returns responses.
pub trait Service {
   fn handle_auth(&mut self, addr: SocketAddr, msg: Auth) -> Auth;
}

/// Socket configuration
pub struct SocketConfig {
   /// Some logger.
   pub log: ::Log,
   /// The address to bind to for plain TCP connections.
   pub tcp_addr: SocketAddr,
   /// An archive containing the SSL key and certificates.
   pub key_archive: Pkcs12,
   /// The service to run on the socket
   pub service: Box<Service>,
}

/// Read a PKCS #12 archive from a file.
pub fn pkcs12_from_file(file_name: &str, password: &str) -> Result<Pkcs12> {
   let mut file = File::open(file_name)?;
   let mut pkcs12 = Vec::new();
   file.read_to_end(&mut pkcs12)?;
   Ok(Pkcs12::from_der(&pkcs12, password)?)
}

/// Start listening for vapor connections.
pub fn listen(config: SocketConfig, handle: Handle) -> Result<()> {
   let log = config.log;
   let service = Arc::new(Mutex::new(config.service));

   let tls_acceptor = TlsAcceptor::builder(config.key_archive)?.build()?;
   let tls_acceptor = Arc::new(tls_acceptor);
   let socket = TcpListener::bind(&config.tcp_addr, &handle)?;

   let tcp_clients = socket.incoming()
      .map_err(clone!(log; |e| {
         error!(log, "socket stream error: {}", e);
      }));

   let tcp_done = tcp_clients
      .for_each(clone!(handle, log, service, tls_acceptor; |(stream, addr)| {
         // Create a logger with the client's address
         let log = log.new(o!("addr" => format!("{:?}", addr)));

         let tls_handshake = tls_acceptor.accept_async(stream)
            .map_err(clone!(log; |e| {
               error!(log, "TLS handshake failed: {}", e);
            }));

         let done = tls_handshake
            .and_then(clone!(log, service, addr; |stream| {
               info!(log, "New client connected from {:?}", addr);
               let (mut sink, stream) = stream.framed(Framing::new(log.clone())).split();

               let incoming = stream.for_each(clone!(service, addr; |msg| {
                     let resp = service.lock().handle_auth(addr, msg);

                     if sink.start_send(resp)?.is_not_ready() {
                        // Handle back-pressure by simply dropping the connection.
                        bail!("Stream buffer full, dropping connection");
                     }
                     if let Err(e) = sink.poll_complete() {
                        bail!("Poll complete failed: {}", e);
                     }
                     Ok(())
                  }))
                  .then(clone!(log; |res| {
                     match res {
                        Ok(_) => info!(log, "Connection closed"),
                        Err(e) => error!(log, "Connection error: {}", e),
                     }
                     Ok(())
                  }));

               incoming
            }));

         handle.spawn(done);
         Ok(())
      }));

   handle.spawn(tcp_done);
   Ok(())
}

struct Framing { log: ::Log }

impl Framing {
   /// Create a new `Framing`.
   pub fn new(log: ::Log) -> Framing {
      Framing{ log }
   }
}

impl Encoder for Framing {
   type Item = Auth;
   type Error = Error;
   fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<()> {
      let data = encode(self.log.clone(), &AuthRule, &item)?;
      dst.put(&data);
      Ok(())
   }
}

impl Decoder for Framing {
   type Item = Auth;
   type Error = Error;
   fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>> {
      match decode(self.log.clone(), &AuthRule, &*src) {
         Err(e) => {
            match *e.kind() {
               ::proto::error::ErrorKind::Net(::net::error::ErrorKind::UnexpectedEndOfData) => Ok(None),
               _ => Err(e.into()),
            }
         }
         Ok(item) => Ok(Some(item))
      }
   }
}

