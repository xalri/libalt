extern crate proxy;
extern crate server;
extern crate net;
extern crate util;
extern crate proto;
extern crate futures;
extern crate tokio_core;

use tokio_core::reactor::*;
use futures::Stream;

use util::logger;
use proxy::*;
use net::socket::*;
use proto::message::Message;
use proto::rules::MessageRule;
use proto::{Context, Sender};
use server::processors::ListJoiner;

fn main() {
   let log = logger::trace_file("proxy");
   let server_addr = ([192,227,140,237], 27276).into();

   let mut reactor = Core::new().unwrap();

   let socket = Socket::bind(
      ([0,0,0,0], 27276).into(),
      reactor.handle(),
      MessageRule(Context::Server),
      log.clone()).unwrap();

   let sender = Sender::from_socket(&socket);
   proto::lan::lan_listen(log.clone(), &[sender.clone()]);

   let mut proxy = Proxy::new(log.clone(), sender.clone(), server_addr);
   let mut list_joiner = ListJoiner::new(log.clone(), sender.clone());

   let msg = socket.for_each(move |msg| {
      let msg = Message(msg);
      list_joiner.process(&msg);
      proxy.process(&msg);
      Ok(())
   });

   reactor.run(msg).unwrap();
}

