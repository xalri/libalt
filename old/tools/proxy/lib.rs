//! A reverse proxy which tracks each client's world state for decoding events
//! and entity changes

#[macro_use] extern crate slog;
#[macro_use] extern crate net;
extern crate futures;
extern crate tokio_core;
extern crate reffers;
extern crate proto;

use net::socket::*;
use proto::Sender;
use proto::message::*;
use net::guarantee::Guaranteed;
use proto::Context;
use proto::rules::MessageRule;
use slog::Logger as Log;

use futures::sync::mpsc;
use futures::sync::mpsc::UnboundedSender as USender;
use futures::*;
use tokio_core::reactor::*;
use reffers::rc::Strong;
use std::net::SocketAddr;
use std::thread;
use std::collections::HashMap;

/// A reverse proxy.
pub struct Proxy {
   log: ::Log,
   server_addr: SocketAddr,
   clients: HashMap<SocketAddr, USender<Guaranteed<Data>>>,
   client_port: u16,
   to_clients: Sender,
}

/// A single client connected to the proxy.
pub struct ProxyClient {
   client_addr: SocketAddr,
   server_addr: SocketAddr,
   to_server: Sender,
   to_client: Sender,
}

impl Proxy {
   /// Create a new `Proxy`.
   pub fn new(log: ::Log, sender: Sender, server: SocketAddr) -> Proxy {
      info!(log, "Starting proxy, forwarding to {:?}", server);
      Proxy {
         server_addr: server,
         log: log,
         clients: HashMap::new(),
         client_port: 30000,
         to_clients: sender,
      }
   }

   /// Handle a message from a client.
   pub fn process(&mut self, msg: &Message) {
      // If we don't have an existing connection for this client, create one.
      if !self.clients.contains_key(&msg.addr()) {
         self.client_port += 1;
         let client = start_client(self.log.clone(),
                                   msg.addr(),
                                   self.server_addr,
                                   self.client_port,
                                   self.to_clients.clone());
         self.clients.insert(msg.addr(), client);
      }

      // Pass the message to the `ProxyClient`.
      self.clients.get(&msg.addr())
         .unwrap()
         .unbounded_send(msg.0.clone())
         .unwrap();
   }
}

impl ProxyClient {
   /// Called on receiving a message from the client.
   pub fn recv_client(&mut self, mut msg: Message) {
      println!("client -> server  {:?}", msg);

      // Forward the message to the server.
      msg.0.addr = self.server_addr;
      self.to_server.send(msg);
   }

   /// Called on receiving a message from the server.
   pub fn recv_server(&mut self, mut msg: Message) {
      println!("server -> client  {:?}", msg);

      // Forward the message to the client.
      msg.0.addr = self.client_addr;
      self.to_client.send(msg);
   }
}

/// Start a client on a new thread, returns a channel on which messages from
/// the client should be passed to the proxy.
pub fn start_client(log: ::Log,
                    client_addr: SocketAddr,
                    server_addr: SocketAddr,
                    port: u16,
                    to_client: Sender) -> USender<Guaranteed<Data>>
{
   let (to_server, from_client) = mpsc::unbounded();

   thread::spawn(move || {
      let mut reactor = Core::new().unwrap();

      let socket = Socket::bind(
         ([0,0,0,0], port).into(),
         reactor.handle(),
         MessageRule(Context::Client),
         log.clone()).unwrap();

      let to_server = Sender(socket.sender());

      let client: Strong<ProxyClient> = Strong::new(ProxyClient {
         client_addr,
         server_addr,
         to_client,
         to_server,
      });

      let from_client = from_client.for_each(clone!(client; |msg| {
         client.get_mut().recv_client(Message(msg));
         Ok(())
      }));

      let from_server = socket.for_each(move |msg| {
         client.get_mut().recv_server(Message(msg));
         Ok(())
      });

      reactor.handle().spawn(from_client);
      reactor.run(from_server).unwrap();
   });
   to_server
}

