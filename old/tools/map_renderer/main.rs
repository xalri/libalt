extern crate sfml;
extern crate model;
extern crate geom;
extern crate util;
extern crate resources;
extern crate render;
extern crate render_sfml;
#[macro_use] extern crate clap;
#[macro_use] extern crate error_chain;

use std::path::*;
use std::collections::BTreeMap;
use sfml::graphics::*;
use sfml::system::*;
use model::entity::*;
use model::shared_map::*;
use util::time;
use resources::*;
use resources::altx::*;
use resources::dir_source::*;
use render::*;
use geom::{transform, Dimensions};

use error::Result;

mod error {
   error_chain! {
      foreign_links {
         Model(::model::error::Error);
         IO(::std::io::Error);
      }
      errors { }
   }
}

fn main() {
   let app = clap_app!(packtool =>
      (version: "0.2.0")
      (author: "Lewis H. <lewis@xa1.uk>")
      (about: "Altitude map file renderer")
      (@arg out_dir: -o --out +takes_value +required "The directory to save images to")
      (@arg resource_dir: --resources +takes_value +required "Altitude's resource directory")
      (@arg max_width: -w --width +takes_value "The maximum width of rendered images")
      (@arg max_height: -h --height +takes_value "The maximum height of renderered images")
      (@arg parallax: -p --parallax "Draw all parallax layers")
      (@arg parallax_under: -P --parallax_under "Draw parallax layers which are under the main view")
      (@arg spawn_points: -s --spawn_points "Draw spawn points")
      (@arg input: ... "The map files to render")
   );
   let args = app.get_matches();

   let resources_path = args.value_of("resource_dir").unwrap().to_string();

   let settings = RenderSettings {
      max_width: value_t!(args, "max_width", f32).ok(),
      max_height: value_t!(args, "max_height", f32).ok(),
      draw_parallax_all: args.is_present("parallax"),
      draw_parallax_under: args.is_present("parallax_under"),
   };

   let out = PathBuf::from(args.value_of("out_dir").unwrap());

   for path in args.values_of("input").unwrap() {
      let path = Path::new(&path);
      println!("==========================================");
      println!("Loading altx archive {:?}..", path);
      let altx = match Altx::from_file(path) {
         Ok(x) => x,
         Err(e) => {
            println!("Failed to load archive, skipping: {}", e);
            continue;
         }
      };

      let game_res = Resources::new(DirSource::open(&resources_path).unwrap(), true).unwrap();
      let map = match draw_map(altx, &settings, game_res) {
         Ok(x) => x,
         Err(e) => {
            println!("Failed to render map, skipping: {}", e);
            continue;
         }
      };

      println!("Loading texture into image..");
      let map_image = map.texture().copy_to_image().unwrap();
      let image_path = out.join(
            path.with_extension("png").file_name().unwrap());

      println!("Saving image to {:?}..", image_path);
      map_image.save_to_file(image_path.to_str().unwrap());
      println!("Done");
   }
}

pub struct RenderSettings {
   pub draw_parallax_all: bool,
   pub draw_parallax_under: bool,
   pub max_width: Option<f32>,
   pub max_height: Option<f32>,
}

pub fn draw_map(altx: Altx, settings: &RenderSettings, game_res: ResourceDir) -> Result<RenderTexture> {
   println!("Loading map..");
   let map = SharedMap::from_altx("".to_string(), altx.clone()).unwrap();
   let map = map.data();

   println!("Loading map's textures..");
   let map_res = Resources::new(altx, true).unwrap();

   let target_size = render_scale(map.game_view().size, settings.max_width, settings.max_height);

   println!("Setting up render target..");
   let mut texture = RenderTexture::new(target_size[0] as u32, target_size[1] as u32, false).unwrap();
   let mut view = texture.view().to_owned();
   //view.set_size(Vector2f::new(target_size[0], -target_size[1]));
   view.set_size(Vector2f::new(target_size[0], target_size[1]));
   texture.set_view(&view);

   let col = map.background_color;
   let bg_col = Color::rgba(col.0[0], col.0[1], col.0[2], col.0[3]);
   texture.clear(&bg_col);


   println!("Drawing views to render target..");

   let mut textures = TextureCache::new(map_res, game_res);

   for view in &map.views {
      let is_game_view = &view.name == "Game";
      if !is_game_view && !settings.draw_parallax_all && !settings.draw_parallax_under {
         continue;
      }

      // Create an entity arena and add all the view's entities to it.
      let mut arena = arena::View::default();
      for e in view.create_entities(&mut textures.game_res).unwrap() {
         arena.add(e);
      }

      let scale = target_size[0] as f32 / view.size.width as f32;
      //let t = transform::scale(scale) * transform::flip(false, true);
      let t =
         transform::translate(0.0, target_size[1])
         * transform::flip(false, true)
         * transform::scale(scale);
      //let t = transform::scale(scale);

      let surface = render_sfml::Surface{ rt: &texture };
      let mut renderer = SurfaceRenderer {
         target: &surface,
         textures: &mut textures,
         view: t,
      };

      render_view(&arena, &mut renderer)?;

      if is_game_view && !settings.draw_parallax_all {
         break;
      }
   }

   texture.display();
   Ok(texture)
}

pub fn render_scale(
   map_size: Dimensions,
   max_width: Option<f32>,
   max_height: Option<f32> ) -> [f32; 2]
{
   let mut scale = 1.0;
   if let Some(w) = max_width {
      if map_size.width > w { scale = f32::min(scale, w / map_size.width); }
   }
   if let Some(h) = max_height {
      if map_size.height > h { scale = f32::min(scale, h / map_size.height); }
   }
   [(map_size.width * scale) as f32, (map_size.height * scale) as f32]
}

/// Draw all the entities in a view to a `Surface`.
pub fn render_view(view: &arena::View, renderer: &mut Renderer) -> Result<()> {
   for (_layer, ids) in layers(view) {
      for id in ids {
         let mut entity = view.entity_mut(id)?;
         if entity.is_spawner() {
            let spawner = entity.mut_spawner().unwrap();
            spawner.last_spawn -= time::seconds(10.0);
            //spawner.last_spawn =
            println!("spawner {:?}", spawner.last_spawn);
         }
         entity.draw(renderer)?;
      }
   }

   Ok(())
}

/// Split the entities in a view by layer, returning an ordered map of layer numbers
/// to lists of entity IDs.
pub fn layers(view: &arena::View) -> BTreeMap<i32, Vec<EntityID>> {
   let mut layers = BTreeMap::new();

   for &id in view.entity_set() {
      let layer = view.entity(id).unwrap().get_layer();
      layers.entry(layer).or_insert_with(|| Vec::with_capacity(32)).push(id);
   }
   layers
}