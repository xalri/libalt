//! A simple tool to continuously ping a server and display the results.

extern crate util;
extern crate proto;
extern crate net;
#[macro_use] extern crate slog;

use util::logger;
use proto::ping::*;
use proto::message::Message;
use proto::rules::MessageRule;
use proto::*;
use net::SimpleSocket;
use std::time::Duration;

fn main() {
   let log = logger::trace_file("ping");

   let server_addr = std::env::args().nth(1).unwrap_or_else(|| {
      println!("Usage: ping <ip>:<port>");
      std::process::exit(1)
   }).parse().unwrap_or_else(|e| {
      println!("Failed to parse server IP: {}", e);
      std::process::exit(1)
   });

   SimpleSocket {
      addr: ([0,0,0,0], 27290).into(),
      codec: MessageRule(Context::Client),
      log: log.clone(),
      tick: Duration::from_secs(1),
      init: &|sender| Pings::new(Sender(sender)),
      on_message: &mut |pings, msg| pings.process(&Message(msg)),
      on_tick: &mut |pings| {
         pings.update();
         let server = pings.get(server_addr);

         if let Some(ping) = server.ping_ms() {
            info!(log, "average ping: {:.2}ms, packet loss: {}%",
                  ping, server.packet_loss() * 100.);
         }
      },
   }.run().unwrap();
}
