extern crate altx;
extern crate image;
extern crate regex;
#[macro_use] extern crate clap;
#[macro_use] extern crate slog;
extern crate slog_term;

use altx::pack::*;
use clap::ArgMatches;
use std::fs;
use std::path::*;
use std::io::stderr;
use slog::*;
use slog_term::*;

fn logger() -> Logger {
   let drain = FullFormat::new(PlainSyncDecorator::new(stderr()))
      .build()
      .filter_level(Level::Info);
   Logger::root(drain.fuse(), o!())
}

fn main() {
   let log = logger();
   let app = clap_app!(packtool =>
      (version: "0.1.0")
      (author: "Lewis H. <lewis@xa1.uk>")
      (about: "A tool to manipulate Altitude's resource files")
      (@subcommand inspect =>
         (about: "List the sprites contained in one or more pack files")
         (version: "0.1.0")
         (@arg recursive: -r --recursive "Serch recursively in input directories")
         (@arg input: ... +required "The pack files to inspect")
      )
      (@subcommand unpack =>
         (about: "Unpack the resources from one or more pack files")
         (version: "0.1.0")
         (@arg recursive: -r --recursive "Serch recursively in input directories")
         (@arg out_dir: -o --out +takes_value "The directory to save images to")
         (@arg input: ... +required "The pack files to unpack")
      )
   );

   let args = app.get_matches();

   match args.subcommand(){
      ("inspect", Some(ref x)) => inspect(&log, x),
      ("unpack", Some(ref x)) => unpack(&log, x),
      _ => error!(log, "No subcommand specified, use `packtool help` for a list of \
                   subcommands")
   }
}

fn unpack(log: &Logger, args: &ArgMatches){
   let out_dir = args.value_of("out_dir").unwrap_or("./");
   let out_dir = Path::new(out_dir);
   info!(log, "extracting images to {:?}", out_dir);
   let paths: Vec<_> = args.values_of("input").unwrap().collect();
   let recursive = args.is_present("recursive");
   for_packs(log, paths, recursive, &|pack: TextureAtlas, path: PathBuf| {
      if let Err(e) = pack.save_images(&out_dir) {
         error!(log, "Failed to save: {}", e);
      }
   });
}

fn inspect(log: &Logger, args: &ArgMatches){
   let paths: Vec<_> = args.values_of("input").unwrap().collect();
   let recursive = args.is_present("recursive");
   for_packs(log, paths, recursive, &|_pack: TextureAtlas, path: PathBuf| {
      //info!("sprites: {:?}", pack.sprites);
      info!(log, "Found pack file {:?}", path);
   });
}

/// Load packs from the given list of paths, calling `f` for each pack.
/// If `recursive` is true, recursively searches directories for pack files.
fn for_packs<P, F>(log: &Logger, paths: Vec<P>, recursive: bool, f: &F)
where P: Into<PathBuf>, F: Fn(TextureAtlas, PathBuf) {
   // regex is valid, so unwrap is safe
   let pack_regex = regex::Regex::new("p[0-9]+").unwrap();
   for p in paths.into_iter().map(|p| p.into()) {
      match fs::metadata(&p){
         Ok(metadata) => {
            if metadata.is_dir(){
               if recursive {
                  let iter = match fs::read_dir(&p){ Ok(i) => i,
                     Err(e) => {
                        warn!(log, "Failed to read directory '{:?}': {}", p, e);
                        continue;
                     }
                  };
                  let mut files = Vec::new();
                  for i in iter {
                     match i {
                        Ok(x) => files.push(x.path()),
                        Err(e) => warn!(log, "Failed to get a file in \
                                        directory '{:?}': {}", p, e)
                     }
                  }
                  for_packs(log, files, true, f);
               } else {
                  warn!(log, "Ignoring directory '{:?}', use the --recursive (-r) flag to \
                         search directories", p);
               }
            }
            if metadata.is_file(){
               {
                  // Only fails if file name isn't valid unicode, so unwrap it.
                  let file_name = p.file_name().unwrap().to_str().unwrap();
                  if !pack_regex.is_match(&file_name) {
                     debug!(log, "Ignoring non-pack file '{:?}'", p);
                     continue
                  }
               }
               let mut file = match fs::File::open(&p){ Ok(f) => f,
                  Err(e) => {
                     warn!(log, "Failed to open file '{:?}': {}", p, e);
                     continue;
                  }};
               match TextureAtlas::read(&mut file) {
                  Ok(pack) => f(pack, p),
                  Err(e) => warn!(log, "Failed to parse pack '{:?}': {}",p, e)
               }
            }
         }
         Err(e) => {
            warn!(log, "Failed to open '{:?}' ({}), skipping", p, e);
         }
      }
   }
}

