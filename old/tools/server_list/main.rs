extern crate net;
extern crate util;
extern crate proto;

use std::time::Duration;
use util::logger;
use net::*;
use proto::message::*;
use proto::rules::MessageRule;
use proto::{Context, Sender};

fn main() {
   let log = logger::trace_file("server_info");

   let server_addr = ([192,227,140,237], 27276).into();

   SimpleSocket {
      addr: ([0,0,0,0], 27290).into(),
      codec: MessageRule(Context::Client),
      log: log.clone(),
      tick: Duration::from_secs(1),
      init: &|channel| {
         let sender = Sender(channel);
         sender.send(Message::new_order(server_addr, Data::ServerInfo(Info::Request)));
      },
      on_message: &mut |_, msg| {
         match msg.data {
            Data::ServerInfo(Info::Response(info)) => {
               println!("{} {} {} {}/{} level {}-{} version {}",
                        info.server_name.unwrap_or("unknown".to_string()),
                        msg.addr,
                        info.map_name.unwrap_or("unknown".to_string()),
                        info.num_players,
                        info.max_players,
                        info.min_level,
                        info.max_level,
                        info.version.unwrap());
            }
            _ => (),
         }
      },
      on_tick: &mut |_| (),
   }.run().unwrap();
}

