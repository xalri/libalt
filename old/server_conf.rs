//! A set of server parameters, sent to each client when they join or
//! when some parameter changes.

use esc::xml::*;
use esc::error::*;
use derivative::*;

pub const DEFAULT_GRAVITY: f32 = -0.117;

/// Server parameters which are sent to the client when they join.
#[derive(PartialEq, Debug, Clone, Derivative)]
#[derivative(Default)]
pub struct ServerConf {
   pub map_source: String,
   #[derivative(Default(value="1.0"))]
   pub energy_mod: f32,
   #[derivative(Default(value="1.0"))]
   pub health_mod: f32,
   pub disable_spawn: bool,
   #[derivative(Default(value="100"))]
   pub scale: u32,
   #[derivative(Default(value="100"))]
   pub plane_scale: u32,
   #[derivative(Default(value="true"))]
   pub gravity: bool,
   #[derivative(Default(value="true"))]
   pub ball_gravity: bool,
   pub disable_primary: bool,
   pub disable_secondary: bool,
   #[derivative(Default(value="100"))]
   pub crash_dmg: u32,
   #[derivative(Default(value="100"))]
   pub bounce_mod: u32,
   pub maps: Vec<String>,
   pub custom_cmds: Vec<String>,
   pub dynamic_cmds: Vec<String>,
   pub ffa_config: FFAConfig,
   pub tbd_config: TBDConfig,
   pub obj_config: ObjectiveConfig,
   pub ball_config: BallConfig,
   pub tdm_config: TDMConfig,
   pub prevent_switch: bool,
   pub no_bal_msg: bool,
   #[derivative(Default(value="true"))]
   pub bots_balance: bool,
   pub bot_spec_threshold: u16,
   pub max_ping: u32,
   pub tournament: bool,
}

impl ServerConf {
   pub fn gravity(&self) -> f32 {
      if self.gravity { DEFAULT_GRAVITY } else { 0.0 }
   }

   pub fn ball_gravity(&self) -> f32 {
      if self.ball_gravity { DEFAULT_GRAVITY } else { 0.0 }
   }
}

/// Configuration for a free-for-all game. Encoded as XML.
#[derive(Fragment, Debug, Clone, Copy, PartialEq)]
#[xml_default]
#[name="x"]
pub struct FFAConfig {
   #[default="0"] pub score_limit: u32,
   #[default="1"] pub round_limit: u32,
   #[default="420"] pub round_time_seconds: u32,
   #[default="10"] pub warmup_time_seconds: u32,
}

/// Configuration for a TBD game. Encoded as XML.
#[derive(Fragment, Debug, Clone, Copy, PartialEq)]
#[xml_default]
#[name="x"]
pub struct TBDConfig {
   #[default="1"] pub round_limit: u32,
   #[default="0"] pub round_time_seconds: u32,
   #[default="10"] pub warmup_time_seconds: u32,
}

/// Configuration for a 1de/1dm/1bd (?) game. Encoded as XML.
#[derive(Fragment, Debug, Clone, Copy, PartialEq)]
#[xml_default]
#[name="x"]
pub struct ObjectiveConfig {
   #[default="9"] pub games_per_round: u32,
   #[default="2"] pub games_per_switch_sides: u32,
   #[default="1"] pub game_win_margin: u32,
   #[default="6"] pub between_game_time_seconds: u32,
   #[default="1"] pub round_limit: u32,
   #[default="0"] pub round_time_seconds: u32,
   #[default="10"] pub warmup_time_seconds: u32,
}

/// Configuration for a ball game. Encoded as XML.
#[derive(Fragment, Debug, Clone, Copy, PartialEq)]
#[xml_default]
#[name="x"]
pub struct BallConfig {
   #[default="11"] pub goals_per_round: u32,
   #[default="1"] pub round_limit: u32,
   #[default="0"] pub round_time_seconds: u32,
   #[default="10"] pub warmup_time_seconds: u32,
}

/// Configuration for a TDM game. Encoded as XML.
#[derive(Fragment, Debug, Clone, Copy, PartialEq)]
#[xml_default]
#[name="x"]
pub struct TDMConfig {
   #[default="0"] pub score_limit: u32,
   #[default="1"] pub round_limit: u32,
   #[default="600"] pub round_time_seconds: u32,
   #[default="10"] pub warmup_time_seconds: u32,
}

