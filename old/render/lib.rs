extern crate geom;
extern crate cgmath;
extern crate resources;
#[macro_use] extern crate error_chain;

pub use resources::error::*;

use geom::point2;
use geom::poly::Poly;
use geom::transform::{TransformExt, self};
use geom::sprite::*;
use resources::*;
use std::collections::HashMap;
use cgmath::*;

/// A texture.
pub trait Texture: Clone {
   fn width(&self) -> u64;
   fn height(&self) -> u64;
   fn repeat(&self) -> bool;
}

/// One vertex of a polygon.
pub trait Vertex: Clone + Copy + std::fmt::Debug {
   fn new(pos: Point2<f32>, uv: Point2<f32>, col: Color) -> Self;
}

/// Something which can be drawn to.
pub trait Surface {
   /// The texture type used by the surface.
   type Texture: Texture;

   /// The vertex type used by the surface.
   type Vertex: Vertex;

   /// Draw a convex polygon to the surface.
   fn draw(&self, v: &[Self::Vertex], t: Matrix3<f32>, tx: Option<Self::Texture>) -> Result<()>;

   /// Load a raw texture into video memory.
   fn load_tx_raw(&self, data: &[u8], w: u32, h: u32, filter: bool, repeat: bool) -> Result<Self::Texture>;
}

/// Draws stuff. Only really exists to hide the type parameters of SurfaceRenderer.
pub trait Renderer {
   fn draw_sprite(&mut self, sprite: &Sprite) -> Result<()>;
   fn draw_image(&mut self, path: &str, color: Color, transform: Matrix3<f32>) -> Result<()>;
}


/// Caches textures
pub struct TextureCache<T> {
   textures: HashMap<String, T>,
   pub map_res: MapResources,
   pub game_res: ResourceDir,
}

/// Draws stuff onto `Surface`s.
pub struct SurfaceRenderer<'a, 'b, T: 'static, V: 'static> {
   pub textures: &'a mut TextureCache<T>,
   pub target: &'b Surface<Texture=T, Vertex=V>,
   pub view: Matrix3<f32>,
}

impl<T: Texture> TextureCache<T> {
   /// Create a new `TextureCache` given resources for the map (the map's `altx` archive)
   /// and the main resource directory.
   pub fn new(map_res: MapResources, game_res: ResourceDir) -> TextureCache<T> {
      TextureCache {
         textures: HashMap::new(),
         map_res,
         game_res,
      }
   }

   /// Attempt to get a shared reference to the texture given a path to an image.
   /// Fails if the image wasn't found or couldn't be loaded.
   pub fn image<V: Vertex>(&mut self, path: &str, surface: &Surface<Texture=T, Vertex=V>) -> Result<T> {
      if !self.textures.contains_key(path) {
         // Attempt to load the image.
         let image_meta = self.map_res.get_image(path)
            .or_else(|e| {
               match *e.kind() {
                  ErrorKind::ImageNotFound(_) => self.game_res.get_image(path),
                  _ => bail!("Failed to load image '{}': {}", path, e)
               }
            })?;

         let image = image_meta.image;
         let (w,h) = image.dimensions();

         let tx = surface.load_tx_raw(&image.into_raw(), w, h, image_meta.filter, image_meta.repeat)?;

         self.textures.insert(path.to_string(), tx);
      }

      Ok(self.textures.get(path).unwrap().clone())
   }
}

impl<'a, 'b, T: Texture, V: Vertex> Renderer for SurfaceRenderer<'a, 'b, T, V> {
   fn draw_sprite(&mut self, sprite: &Sprite) -> Result<()> {
      let body = &sprite.body;
      let tx = &sprite.image;
      let colors = &sprite.colors;

      let transform = self.view * body.matrix();
      let tx_path = tx.as_ref().map(|&(ref path, _)| path.as_str());
      let tx_transform = tx.as_ref().map(|&(_, t)| t.to_matrix())
         .unwrap_or(transform::id());

      draw_poly(
         self.textures,
         self.target,
         transform,
         &body.hull,
         colors[0],
         tx_path,
         tx_transform
      )
   }

   fn draw_image(&mut self, path: &str, color: Color, transform: Matrix3<f32>) -> Result<()> {
      let tx = self.textures.image(path, self.target)?;
      let transform = self.view * transform *
         transform::translate(-(tx.width() as f32), -(tx.height() as f32));
      draw_poly(
         self.textures,
         self.target,
         transform,
         &Poly::square(),
         color,
         Some(path),
         transform::id()
      )
   }
}

pub fn draw_poly<T: Texture, V: Vertex>(
   textures: &mut TextureCache<T>,
   target: &Surface<Texture=T, Vertex=V>,
   transform: Matrix3<f32>,
   poly: &Poly,
   color: Color,
   image: Option<&str>,
   image_transform: Matrix3<f32> ) -> Result<()>
{
   let tx = if let Some(path) = image {
      let tx = textures.image(path, target)?;
      if !tx.repeat() {
         let t = transform * image_transform;
         return target.draw(&tx_verts(color, &tx), t, Some(tx));
      }
      Some(tx)
   } else { None };

   let transform_uv = if let Some(ref tx) = tx {
      Transform::<Point2<f32>>::inverse_transform(&image_transform).unwrap()
   } else { transform::id() };

   for poly in poly.convex() {
      if poly.len() < 3 { continue; }
      // TODO: don't ignore colours..
      let mut verts: Vec<_> = poly.points()
         .map(|p| Vertex::new(p, transform_uv.apply(p), color))
         .collect();
      target.draw(&verts, transform, tx.clone())?;
   }

   Ok(())
}

fn tx_verts<T: Texture, V: Vertex>(color: Color, tx: &T) -> Vec<V> {
   let w = tx.width() as f32;
   let h = tx.height() as f32;
   //let (x1, x2, y1, y2) = (-w / 2.0, w / 2.0, -h / 2.0, h / 2.0);
   let (x1, x2, y1, y2) = (0.0, w, 0.0, h);

   vec![
      Vertex::new(point2(x1, y1), point2(0.0, h), color),
      Vertex::new(point2(x2, y1), point2(w, h), color),
      Vertex::new(point2(x2, y2), point2(w, 0.0), color),
      Vertex::new(point2(x2, y2), point2(w, 0.0), color),
      Vertex::new(point2(x1, y2), point2(0.0, 0.0), color),
      Vertex::new(point2(x1, y1), point2(0.0, h), color),
   ]
}
