//! Data structures representing altitude messages

use std::default::Default;
use std::net::SocketAddr;
use std::sync::Arc;
use std::cmp;
use model::types::*;
use model::server_conf::*;
use net::guarantee::*;

/// A top-level message.
#[derive(PartialEq, Debug, Clone)]
pub struct Message(pub Guaranteed<Data>);

/// The main payload of a message.
/// Currently some types contain the data in a Vec<u8>, this will
/// change as the last few message types are implemented.
#[derive(PartialEq, Debug, Clone)]
pub enum Data {
   /// For message with no data
   EmptyTag(u8),
   /// Friend updates and messages
   Friend((Vec<u8>, u64)),
   /// Client authentication (logging in)
   Auth(Auth),
   /// Validating a client's information
   Validation(Validation),
   /// Get the list of game servers from the master server
   ServerList(ServerList),
   /// A request for a game server to be added to the master server
   RegisterServer(RegisterServer),
   /// Find the master server's current game version
   Versioning(Versioning),
   /// Check if the server is accessible over the internet
   FirewallChecker(FirewallTest),
   /// Get some information about a server
   ServerInfo(Info),
   /// Server list pings
   ServerPing(Ping),
   /// A users game storage, contains the friend and block list
   GameStorage((Vec<u8>, u64)),
   /// Report the hardware the game is running on
   HardwareReport(HardwareReport),
   /// Joining a game server
   Join(Join),
   /// The message type for in-game data (events & entity updates)
   Game(Game),
   /// Downloading resources from a game server
   Dl(Download),
   /// Pings used during games
   Ping(Ping),
   /// Multicast message type used to find LAN servers
   Lan(Lan),
}

/// The sequence number of an event group.
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct GroupSeq(pub u16);

/// The sequence number of a game message.
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct MessageSeq(pub u16);

/// Data containing entity updates, part of a game message.
#[derive(Debug, Clone)]
pub struct EntityData {
   pub ids: Vec<u16>,
   pub data: (Vec<u8>, u64),
}

/// The main game message, contains entity data and sequential events.
#[derive(PartialEq, Debug, Clone)]
pub struct Game {
   /// The global game counter, reset when the map changes. A server will
   /// discard any messages with a counter which doesn't match it's own.
   pub counter: u8,
   /// The sequencing number of this game message, used to decide whether to
   /// update nuon data.
   pub seq: MessageSeq,
   /// The most recent event group sequencing number.
   pub group_seq: GroupSeq,
   /// Groups of events
   pub ev_groups: Vec<EvGroup>,
   /// A list of missing message IDs
   pub missing: Vec<MessageSeq>,
   /// Raw entity update data.
   pub entity_data: Option<EntityData>,
}

/// A group of game events.
#[derive(PartialEq, Debug, Clone)]
pub struct EvGroup {
   /// This event group's sequence number
   pub seq: GroupSeq,
   /// Exact length of the data in bits
   pub len: u64,
   /// The raw data
   pub data: Vec<u8>,
}

/// A download message.
#[derive(PartialEq, Debug, Clone)]
pub enum Download {
   /// A client's request to download some resource.
   Request(DlReq),
   /// A server accepting a request with the given ID.
   Accept(u8),
   /// A server sending some part of the requested resource.
   Data(DlData),
   /// A client requesting that some chunks be re-sent.
   Missing(DlMissing),
   /// A server saying that it has send all the data.
   Complete(u8),
}

/// A request from a client to download some resource
#[derive(PartialEq, Debug, Clone)]
pub struct DlReq {
   pub id: u8,
   pub resource: String,
}

/// Part of a resource being sent to a client.
#[derive(PartialEq, Debug, Clone)]
pub struct DlData {
   pub id: u8,
   pub chunk: u32,
   pub data: Arc<(Vec<u8>, u64)>,
}

/// A request to re-send some sections of a resource.
#[derive(PartialEq, Debug, Clone)]
pub struct DlMissing {
   pub unk: f32,
   pub chunks: Vec<u32>,
}

/// A vapor authentication message
#[derive(PartialEq, Debug, Clone)]
pub enum Auth {
   // TLS client -> server
   Login(Option<Login>),
   CreateAccount((Vec<u8>, u64)),
   UpdateAccount((Vec<u8>, u64)),
   RegisterKey((Vec<u8>, u64)),
   ResetPassword((Vec<u8>, u64)),
   RecoverAccount((Vec<u8>, u64)),

   // TLS server -> client
   LoginResp(LoginResp),
   CreateAccountResp((Vec<u8>, u64)),
   IpBlocked((Vec<u8>, u64)),
   RegisterKeyResp((Vec<u8>, u64)),
   UpdateAccountResp((Vec<u8>, u64)),
   ResetPasswordResp((Vec<u8>, u64)),
   RecoverAccountResp((Vec<u8>, u64)),

   // UDP client -> server
   LoginConfirm,
   Keepalive,
   Logout(UUID),

   // UDP server -> client
   LoginFail(String),
   LoginSuccess(LoginResp),
   KeepaliveResp,
   FloodWarning(VaporError),
   Disconnected(VaporError),
}

/// Data sent to reqeust to login to vapor.
#[derive(PartialEq, Debug, Clone)]
pub struct Login {
   pub username: Option<String>,
   pub password: Option<String>,
   pub encrypted_pass: Option<Vec<u8>>,
   pub steam_id: Option<u64>,
   pub steam_nick: Option<String>,
   pub unk: Option<Vec<u8>>,
   pub steam_friends: Option<SteamFriends>,
   pub distributor: Distributor,
   pub platform: Platform,
   pub upnp: bool,
}

/// Vapor's response to  a login request. What most of this actually is, I
/// have no idea.
#[derive(PartialEq, Debug, Clone)]
pub struct LoginResp {
   pub result: Option<String>,
   pub logged_in: bool,
   pub id: Option<PrivClientID>,
   pub authority: Option<String>,
   pub unk1: bool,
   pub games: Vec<GameID>,
   pub unk2: bool,
   pub unk3: bool,
   pub unk4: bool,
   pub unk5: i32,
   pub ban: Option<(i32, BanReason)>,
}

/// Identifies a player using steam.
#[derive(PartialEq, Debug, Clone)]
pub struct SteamID {
   /// A unique identifier used by steam.
   pub id: u64,
   /// The user's current nickname
   pub nick: String,
}

/// A list of a player's steam friends.
#[derive(PartialEq, Debug, Clone)]
pub struct SteamFriends {
   pub my_id: SteamID,
   pub friends: Vec<SteamID>,
}

/// A server list message.
#[derive(PartialEq, Debug, Clone)]
pub enum ServerList {
   Request(ListReq),
   Response(ListResp),
}

/// A message relating to adding servers to the server list.
#[derive(PartialEq, Debug, Clone)]
pub enum RegisterServer {
   AuthReq(u16),
   AuthResp(AuthResp),
   Hello(UUID),
   HelloResp(HelloResp),
   Keepalive,
   KeepaliveResp(KeepaliveResp),
}

/// A versioning message.
#[derive(PartialEq, Debug, Clone)]
pub enum Versioning {
   Request(VersionReq),
   Response(VersionResp),
}

/// A message type used in testing if a server is behind a firewall.
#[derive(PartialEq, Debug, Clone)]
pub enum FirewallTest {
   Request,
   Response,
}

/// A request for part of the server list.
#[derive(PartialEq, Debug, Clone)]
pub struct ListReq {
   pub game_id: i32,
   pub first_server: u32,
}

/// Part of the server list.
#[derive(PartialEq, Debug, Clone)]
pub struct ListResp {
   pub invalid_game: bool,
   pub message: Option<String>,
   pub servers: u16,
   pub data: Option<Vec<u8>>,
   pub a: bool,
   pub first_server: u16,
}

/// A response to an authentication request.
#[derive(PartialEq, Debug, Clone)]
pub struct AuthResp {
   pub message: Option<String>,
   pub is_ok: bool,
   pub is_listed: bool,
   // if is_listed is false, the game server sends a hello
   // to the vapor server containing this UUID.
   pub uuid: Option<UUID>,
   pub addr: SocketAddr,
}

#[derive(PartialEq, Debug, Clone)]
pub struct HelloResp {
   pub message: Option<String>,
   pub a: bool,
   pub addr: Option<SocketAddr>,
   pub b: bool,
}

#[derive(PartialEq, Debug, Clone)]
pub struct KeepaliveResp {
   pub is_err: bool,
   pub message: Option<String>,
}

#[derive(PartialEq, Debug, Clone)]
pub struct VersionReq {
   pub a: u16,
   pub b: Option<u64>,
}

#[derive(PartialEq, Debug, Clone)]
pub struct VersionResp {
   pub is_valid: bool,
   pub msg: Option<String>,
   pub version: Option<Version>,
   pub auth_method: Option<String>,
}


/// A message for validating clients - making sure they are who they claim
/// to be, and that they should be allowed to join.
#[derive(PartialEq, Debug, Clone)]
pub enum Validation {
   Request(ValidationCheck),
   Response(Signed<ClientValidation>),
}

/// Request validation data for the given player and game id.
#[derive(PartialEq, Debug, Clone)]
pub struct ValidationCheck {
   pub msg_header: u8,
   pub id: ClientID,
   pub game_id: i32,
}

/// Client validation information
#[derive(PartialEq, Debug, Clone)]
pub struct ClientValidation {
   pub id: ClientID,
   pub valid: bool,
   pub game_valid: bool,
   pub owns_game: bool,
   pub message: String,
   pub is_beta: bool,
   pub ip_valid: bool,
   pub ban: BanReason,
}

impl ValidationCheck {
   pub fn new(id: ClientID, game: i32) -> ValidationCheck {
      ValidationCheck {
         msg_header: 2,
         id: id,
         game_id: game,
      }
   }
}

/// A request or a response to a request for server information to be
/// displayed on the client's server list.
#[derive(PartialEq,Debug,Clone)]
pub enum Info {
   Request,
   Response(ServerInfo),
}

/// Server information to be displayed on the server list.
#[derive(PartialEq,Debug,Clone)]
pub struct ServerInfo {
   pub game_id: u16,
   pub addr: Option<SocketAddr>,
   pub map_name: Option<String>,
   pub max_players: u8,
   pub num_players: u8,
   pub server_name: Option<String>,
   pub pass_req: bool,
   pub hardcore: bool,
   pub min_level: u8,
   pub max_level: u8,
   pub disallow_demo: bool,
   pub version: Option<Version>,
}

/// A join message.
#[derive(PartialEq, Debug, Clone)]
pub enum Join {
   /// A request to join a server
   JoinReq(JoinReq),
   /// A server's response to a join request, when the request is accepted,
   /// the value is `None`; when it's denied the value is `Some(deny_reason)`
   JoinResp(Option<String>),
   /// A request for the client to load the included resources and configuration
   Load(Load),
   /// A response to `Load`, indicating that the given resource is loaded
   LoadResp(MapDescriptor),
   /// A message sent when a client disconnects from the server
   Disconnect,
   /// Sent while the client is loading, to indicate to the server that they
   /// are still connected.
   KeepAlive,
}

/// A request to join the server
#[derive(PartialEq, Debug, Clone)]
pub struct JoinReq {
   pub version: Version,
   pub password: String,
   pub level: ClientLevel,
   pub kills: u32,
   pub deaths: u32,
   pub id: ClientID,
}

/// A request that the client load the specified resource
#[derive(PartialEq, Debug, Clone)]
pub struct Load {
   pub counter: u8,
   pub resource: MapDescriptor,
   pub conf: ServerConf,
}

/// The data for a ping message
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Ping {
   pub server_ping: bool,
   pub respond: bool,
   pub id: u32,
}

/// A local 'discovery' packet - requests are sent multicast, servers join
/// the multicast group and respond to any requests with their local IP.
#[derive(PartialEq, Debug, Clone)]
pub enum Lan {
   Request,
   Response(Option<SocketAddr>),
}

/// A client's OpenGL version information, sent to the vapor server.
#[derive(PartialEq, Debug, Clone)]
pub struct HardwareReport {
   pub has_gl: bool,
   pub gl_vendor: String,
   pub gl_renderer: String,
   pub gl_version: String,
}

impl Message {
   /// Create a new Message with the given delivery guarantee
   pub fn new_guarantee(addr: SocketAddr, guarantee: GuaranteeLevel, data: Data) -> Message {
      Message(Guaranteed {
         addr: addr,
         guarantee: guarantee,
         guarantee_data: None,
         data: data,
      })
   }

   /// Create a new Message with no delivery guarantees
   pub fn new(addr: SocketAddr, data: Data) -> Message {
      Message::new_guarantee(addr, GuaranteeLevel::None, data)
   }

   /// Create a new Message with guaranteed delivery
   pub fn new_deliver(addr: SocketAddr, data: Data) -> Message {
      Message::new_guarantee(addr, GuaranteeLevel::Delivery, data)
   }

   /// Create a message with the guarantee that the recipient will get the message
   /// in the order it was sent relative to other ordered messages.
   pub fn new_order(addr: SocketAddr, data: Data) -> Message {
      Message::new_guarantee(addr, GuaranteeLevel::Order, data)
   }

   /// Create a new ping message
   pub fn new_ping(addr: SocketAddr, data: Data) -> Message {
      Message::new_guarantee(addr, GuaranteeLevel::Ping, data)
   }

   /// Create a new message in reply to this one.
   pub fn reply(&self, data: Data) -> Message {
      Message(Guaranteed {
         addr: self.0.addr,
         guarantee: self.0.guarantee,
         guarantee_data: None,
         data: data,
      })
   }

   /// Get the message's address (recipient for outgoing messages, sender for
   /// incoming messages)
   pub fn addr(&self) -> SocketAddr {
      self.0.addr
   }

   /// Get the message's data.
   pub fn data(&self) -> &Data {
      &self.0.data
   }

   /// Get the message's guarantee level.
   pub fn guarantee(&self) -> GuaranteeLevel {
      self.0.guarantee
   }

   /// Get the message's guarantee data.
   pub fn guarantee_data(&self) -> &Option<GuaranteeData> {
      &self.0.guarantee_data
   }
}

impl Default for Data {
   fn default() -> Data {
      Data::EmptyTag(0)
   }
}

impl cmp::PartialEq for EntityData {
   fn eq(&self, other: &EntityData) -> bool {
      (self.ids == other.ids) && (self.data.0 == other.data.0)
   }
}

impl GroupSeq {
   pub fn get(&self, prev: i64) -> i64 {
      get_seq(self.0 as i64, prev, 10)
   }
   pub fn new(value: i64) -> GroupSeq {
      GroupSeq(value as u16 & 0b1111111111)
   }
}

impl MessageSeq {
   pub fn get(&self, prev: i64) -> i64 {
      get_seq(self.0 as i64, prev, 11)
   }
   pub fn new(value: i64) -> MessageSeq {
      MessageSeq(value as u16 & 0b11111111111)
   }
}

/// Given a truncated sequencing number, the last known full sequencing number, and
/// a length in bits, try to calculate the full version of the truncated value.
pub fn get_seq(seq: i64, prev: i64, len: i64) -> i64 {
   let mut d = seq - prev;
   let a = 1i64 << len;
   while d < 0 {
      d += a
   }
   if d > (a / 2) {
      d -= a
   }
   prev + d
}

#[test]
fn get_seq_test() {
   assert_eq!(0, get_seq(0, 0, 10));
   assert_eq!(0, get_seq(0, -1, 10));
   assert_eq!(1, get_seq(1, 0, 10));
   assert_eq!(0, get_seq(0, 511, 10));
   assert_eq!(1024, get_seq(0, 1023, 10));
   assert_eq!(1025, get_seq(1, 1024, 10));
}

