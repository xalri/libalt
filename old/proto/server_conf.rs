//! The server configuration packet format and it's encoding.

use net::encode::*;
use model::server_conf::*;
use error::*;
use byteorder::{ReadBytesExt, WriteBytesExt};
use byteorder::BigEndian as BE;
use std::io::{Read, Write, Cursor};
use xml::Fragment;
use xz2;

/// Encoding for `ServerConf`. Uses lzma compression to reduce the size as much
/// as possible.
pub struct ServerConfRule;

// Read a utf8 String from a stream.
fn read_string<R: Read>(from: &mut R) -> Result<String> {
   let len = from.read_u16::<BE>()? as usize;
   let mut buf = vec![0u8; len];
   from.read_exact(&mut buf)?;
   Ok(String::from_utf8(buf)?)
}

// Write a utf8 String to a stream.
fn write_string<W: Write>(to: &mut W, value: &str) -> Result<()> {
   let bytes = value.as_bytes();
   to.write_u16::<BE>(bytes.len() as u16)?;
   to.write_all(bytes)?;
   Ok(())
}

// Read a float
fn read_f32<R: Read>(from: &mut R) -> Result<f32> {
   Ok(f32::from_bits(from.read_u32::<BE>()?))
}

// Write a float
fn write_f32<W: Write>(to: &mut W, value: f32) -> Result<()> {
   to.write_u32::<BE>(value.to_bits())?;
   Ok(())
}

// Implement `Encoding`
impl Encoding<ServerConf> for ServerConfRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<ServerConf> {
      let is_compressed: bool = BitableRule.read(reader)?;
      let data: Vec<u8> = VecRule::new(BitableRule, 12).read(reader)?;

      let data = if is_compressed {
         use std::u64;
         let s = xz2::stream::Stream::new_lzma_decoder(u64::MAX)?;
         let mut decoder = xz2::read::XzDecoder::new_stream(Cursor::new(data), s);
         let mut raw = Vec::new();
         decoder.read_to_end(&mut raw)?;
         raw
      } else {
         data
      };

      // Read the big endian data.
      let mut cursor = Cursor::new(data);
      let map_source = read_string(&mut cursor)?;
      let energy_mod = read_f32(&mut cursor)?;
      let health_mod = read_f32(&mut cursor)?;
      let disable_spawn = match cursor.read_u8()? {
         0 => false,
         _ => true,
      };

      let pos = cursor.position() as usize;
      let data = cursor.into_inner();

      Ok(ServerConf {
         map_source,
         energy_mod,
         health_mod,
         disable_spawn,
         ..decode(reader.log.clone(), &ServerConfInternal, &data[pos..data.len()])?
      })
   }

   fn write(&self, writer: &mut BitWriter, value: &ServerConf) -> Result<()> {
      // Write the first four fields to a new Vec<u8>
      let mut data = {
         let mut cursor = Cursor::new(Vec::new());
         write_string(&mut cursor, value.map_source.as_str())?;
         write_f32(&mut cursor, value.energy_mod)?;
         write_f32(&mut cursor, value.health_mod)?;
         cursor.write_u8(if value.disable_spawn { 1 } else { 0 })?;
         cursor.into_inner()
      };

      // Encode the rest of the struct and append the data to the Vec
      data.append(&mut encode(writer.log.clone(), &ServerConfInternal, value)?);

      println!("uncompressed = {}", data.len() );

      // We can't compress the packet here -- liblzma has a minimum dictionary
      // size of 4M, while altitude's _maximum_ is 512.

      /*
      // Create a compressed copy of the data
      let compressed = {
         let mut opts = xz2::stream::LzmaOptions::new_preset(1)?;
         opts.mode(xz2::stream::Mode::Fast);
         //This fails:
         //opts.dict_size(512);
         opts.dict_size(4096);
         let s = xz2::stream::Stream::new_lzma_encoder(&opts)?;
         let mut encoder = xz2::write::XzEncoder::new_stream(Cursor::new(Vec::new()), s);
         encoder.write_all(&data)?;
         encoder.finish()?.into_inner()
      };

      //Write whichever version of the data is smaller to the writer
      if compressed.len() < data.len() {
         BitableRule.write(writer, &true)?;
         VecRule::new(BitableRule, 12).write(writer, &compressed)?;
      } else { /*... */ }
      */


      BitableRule.write(writer, &false)?;
      VecRule::new(BitableRule, 12).write(writer, &data)?;
      Ok(())
   }
}

/// An internal encoding for most of `ServerConf`
struct ServerConfInternal;
encode_product!(ServerConf, ServerConfInternal, Error;
   // We ignore the first 4 fields, setting default values using `ReifiedRule`,
   // they're encoded differently, so are handled manually by the public encoding.
   map_source: ReifiedRule("".to_string()),
   energy_mod: ReifiedRule(0.),
   health_mod: ReifiedRule(0.),
   disable_spawn: ReifiedRule(false),

   scale: IntRule::new(40, 300),
   plane_scale: IntRule::new(40, 300),
   gravity: InvertedBoolRule,
   ball_gravity: InvertedBoolRule,
   disable_primary: BitableRule,
   disable_secondary: BitableRule,
   crash_dmg: IntRule::new(0, 1000),
   bounce_mod: IntRule::new(0, 1000),
   maps: VecRule::new(StringRule::short(), 32),
   custom_cmds: VecRule::new(StringRule::short(), 32),
   dynamic_cmds: VecRule::new(StringRule::short(), 32),
   ffa_config: XMLRule,
   tbd_config: XMLRule,
   obj_config: XMLRule,
   ball_config: XMLRule,
   tdm_config: XMLRule,
   prevent_switch: BitableRule,
   no_bal_msg: BitableRule,
   bots_balance: BitableRule,
   bot_spec_threshold: IntRule::new(0, 63),
   max_ping: IntRule::new(0, 1000),
   tournament: BitableRule);

/// A rule which inverts booleans when en/decoding
struct InvertedBoolRule;

impl Encoding<bool> for InvertedBoolRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<bool> {
      Ok(!BitableRule.read(reader)?)
   }
   fn write(&self, writer: &mut BitWriter, value: &bool) -> Result<()> {
      BitableRule.write(writer, &!value)?;
      Ok(())
   }
}

pub struct XMLRule;

macro_rules! xml_impl {
   ($s:ident) => (
      impl Encoding<$s> for XMLRule {
         type Error = Error;
         fn read(&self, reader: &mut BitReader) -> Result<$s> {
            let xml = StringRule::long().read(reader)?;
            Ok($s::from_xml(xml.as_str())?)
         }
         fn write(&self, writer: &mut BitWriter, x: &$s) -> Result<()> {
            Ok(StringRule::long().write(writer, &x.to_xml())?)
         }
      }
   )
}

xml_impl!(FFAConfig);
xml_impl!(TBDConfig);
xml_impl!(ObjectiveConfig);
xml_impl!(BallConfig);
xml_impl!(TDMConfig);
