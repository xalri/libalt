
#![feature(type_ascription)]
#![recursion_limit="256"]

#[macro_use] extern crate slog;
#[macro_use] extern crate error_chain;
extern crate byteorder;
extern crate fnv;
extern crate xz2;
extern crate futures;
extern crate tokio_core;
extern crate cgmath;
extern crate util;
extern crate geom;
extern crate model;
extern crate xml;
#[macro_use] extern crate net;

pub mod diff;
pub mod lan;
pub mod message;
pub mod ping;
pub mod rules;
pub mod sequencer;
pub mod server_conf;

use slog::Logger as Log;
use model::entity::*;
use geom::Dimensions;
use futures::sync::mpsc::UnboundedSender;
use error::*;

/// The context in which a message is being read or written.
/// Changes how entity IDs, events, and other messages are parsed such that
/// read/write symmetry is preserved for Server <-> Client communication.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Context {
   Client,
   Server,
}

/// Everything needed to en/decode events and entity data.
pub trait CodingState : IDMap {
   /// The context in which the message is being read/written.
   fn context(&self) -> Context;
   /// Get the size of the current map, used to encode positions in fewer bits.
   fn map_size(&self) -> Dimensions;
   /// Get a reference to an entity given it's ID.
   fn entity(&self, id: EntityID) -> Result<ReadGuard<Entity>>;
   /// Get the set of entity IDs
   fn entity_set(&self) -> &[EntityID];
}

impl Context {
   /// Invert the context, Client becomes Server and vice versa.
   pub fn invert(&self) -> Context {
      match *self {
         Context::Client => Context::Server,
         Context::Server => Context::Client
      }
   }
}

/// Sends messages, probably over a socket.
#[derive(Clone)]
pub struct Sender(pub UnboundedSender<net::guarantee::Guaranteed<message::Data>>);

impl Sender {
   pub fn from_socket<C>(socket: &net::socket::Socket<message::Data, C>) -> Sender where
      C: net::encode::Encoding<net::guarantee::Guaranteed<message::Data>>,
      C: Clone,
      C: 'static
   {
      Sender(socket.sender())
   }

   /// Send a message
   pub fn send(&self, message: message::Message) {
      self.0.unbounded_send(message.0).unwrap()
   }
}

pub mod error {
   error_chain! {
      links {
         Model(::model::error::Error, ::model::error::ErrorKind);
         Net(::net::error::Error, ::net::error::ErrorKind);
         XML(::xml::Error, ::xml::ErrorKind);
      }
      foreign_links {
         IO(::std::io::Error);
         LZMA(::xz2::stream::Error);
         UTF(::std::string::FromUtf8Error);
      }
      errors { }
   }
}


