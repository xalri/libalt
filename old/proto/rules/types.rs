//! Encoding rules for various types used in many messages.

use net::encode::*;
use model::types::*;
use model::team::*;
use cgmath::*;
use geom::point2;
use error::*;
use std::str::FromStr;
use std::net::SocketAddr;

/// Encoding for angles (-180 to 180)
pub struct AngleRule;

impl Encoding<i32> for AngleRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<i32> {
      Ok(IntRule::new(-180, 180).read(reader)?)
   }
   fn write(&self, writer: &mut BitWriter, value: &i32) -> Result<()> {
      let value = ::geom::math::wrap(*value, -180, 180);
      IntRule::new(-180, 180).write(writer, &value)?;
      Ok(())
   }
}

impl Encoding<f32> for AngleRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<f32> {
      let value: i32 = self.read(reader)?;
      Ok(value as f32)
   }
   fn write(&self, writer: &mut BitWriter, value: &f32) -> Result<()> {
      self.write(writer, &(value.round() as i32))
   }
}

/// Encoding for `ClientID`
pub struct ClientIDRule;
encode_product!(ClientID, ClientIDRule, Error;
   session_id: OptionRule::new(UUIDRule),
   play_time: BitableRule,
   affiliate_code: BitableRule,
   c_points: CommunityPointsRule,
   skins: VecRule::new(BitableRule, 3),
   addr: OptionRule::new(SocketAddrRule),
   logged_in: BitableRule,
   nickname: StringRule::short(),
   vapor: UUIDRule,
   unique: BitableRule);

/// Encoding for `ClientLevel`
pub struct ClientLevelRule;
encode_product!(ClientLevel, ClientLevelRule, Error;
   ace: BitsRule(5),
   level: BitsRule(7), // TODO: why is this 7 bits? (only needs 6) o.O
);

/// Encoding for `Team`
pub struct TeamRule;
encode_sum!( Team, TeamRule, Error, |_| BitsRule(8);
   0 => A,      1 => B,      2 => Spectator,
   3 => Red,    4 => Blue,   5 => Green,
   6 => Yellow, 7 => Orange, 8 => Purple,
   9 => Azure, 10 => Pink,  11 => Brown);

/// Encoding for `CommunityPoints`
pub struct CommunityPointsRule;
encode_product!(CommunityPoints, CommunityPointsRule, Error;
   refer: BitableRule,
   facebook: BitableRule,
   refer_buy:  BitableRule,
   gifted: BitableRule,
   admin: BitableRule,
   steam_group: BitableRule);

/// Encoding for `MapDescriptor`
pub struct MapDescriptorRule;
encode_product!(MapDescriptor, MapDescriptorRule, Error;
   path: StringRule::short(),
   size: BitableRule,
   crc32: BitableRule);

/// Encoding for `UUID`
pub struct UUIDRule;
encode_product!(UUID, UUIDRule, Error;
   a: BitableRule,
   b: BitableRule);

/// Encoding for a player number
pub struct PlayerNoRule;

impl Encoding<i16> for PlayerNoRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<i16> {
      let x: u8 = BitableRule.read(reader)?;
      Ok(x as i16 - 2)
   }
   fn write(&self, writer: &mut BitWriter, value: &i16) -> Result<()> {
      if *value < -2 {
         bail!("Player nuber '{}' must be > -2", value);
      }
      BitableRule.write(writer, &((value + 2) as u8))?;
      Ok(())
   }
}

/// Encoding for `Version`
pub struct VersionRule;
encode_product!(Version, VersionRule, Error;
   major: IntRule::new(0, 999),
   minor: IntRule::new(0, 999),
   patch: IntRule::new(0, 999) );


/// An encoding for a version, using it's string representation.
pub struct VersionStringRule;

impl Encoding<Version> for VersionStringRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Version> {
      let s = StringRule::short().read(reader)?;
      Ok(Version::from_str(s.as_str())?)
   }
   fn write(&self, writer: &mut BitWriter, value: &Version) -> Result<()> {
      StringRule::short().write(writer, &value.to_string())?;
      Ok(())
   }
}

/// Encode a `SocketAddr`
pub struct SocketAddrRule;

impl Encoding<SocketAddr> for SocketAddrRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<SocketAddr> {
      use std::net::IpAddr;

      let mut addr: SocketAddr = "0.0.0.0:0000".parse().unwrap();
      let is_v4: bool = BitableRule.read(reader)?;

      let ip = if is_v4 {
         IpAddr::V4((BitableRule.read(reader)?: u32).into())
      } else {
         IpAddr::V6((BitableRule.read(reader)?: u128).into())
      };

      addr.set_ip(ip);
      let port: u32 = BitableRule.read(reader)?;
      if port > 0xFFFF {
         return Err("Malformed packet, port > 0xFFFF".into());
      }
      addr.set_port(port as u16);
      Ok(addr)
   }
   fn write(&self, writer: &mut BitWriter, value: &SocketAddr) -> Result<()> {
      use std::net::IpAddr;

      match value.ip() {
         IpAddr::V4(a) => {
            true.write_to(writer);
            BitableRule.write(writer, &(a.into(): u32))?;
         }
         IpAddr::V6(a) => {
            false.write_to(writer);
            BitableRule.write(writer, &(a.into(): u128))?;
         }
      }
      (value.port() as u32).write_to(writer);
      Ok(())
   }
}

/// Encoding for signed data
pub struct SignedRule<E>(pub E);

impl<T, E> Encoding<Signed<T>> for SignedRule<E> where
   E: Encoding<T>,
   <E as Encoding<T>>::Error: From<::net::error::Error>
{
   type Error = <E as Encoding<T>>::Error;
   fn read(&self, reader: &mut BitReader) -> ::std::result::Result<Signed<T>, Self::Error> {
      let data_rule = VecRule::new(BitableRule, 21);
      let signature: Vec<u8> = data_rule.read(reader)?;
      let data: Vec<u8> = data_rule.read(reader)?;
      let value = self.0.read(&mut BitReader::open(reader.log.clone(), &data))?;
      Ok(Signed {
         signature: signature,
         data: data,
         value: value,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Signed<T>) -> ::std::result::Result<(), Self::Error> {
      let data_rule = VecRule::new(BitableRule, 21);
      data_rule.write(writer, &value.signature)?;
      data_rule.write(writer, &value.data)?;
      Ok(())
   }
}

/// Encoding for `PrivClientID`
pub struct PrivClientIDRule;
encode_product!( PrivClientID, PrivClientIDRule, Error;
   vapor_opts: VaporSettingsRule,
   id: ClientIDRule );

/// Encoding for `VaporSettings`
pub struct VaporSettingsRule;
encode_product!( VaporSettings, VaporSettingsRule, Error;
   username: StringRule::short(),
   private_sid: OptionRule::new(UUIDRule),
   hw_info_time: BitableRule,
   logged_in_ms: BitableRule,
   no_chat: BitableRule,
   email_opt_in: BitableRule,
   chat_filter: BitableRule,
   map_filter: BitableRule,
   is_guest: BitableRule,
   name_filter: BitableRule,
   steam_uid: OptionRule::new(BitableRule),
   auth_method: AuthMethodRule,
   created: BitableRule );

/// Encoding for `GameID`
pub struct GameIDRule;
encode_product!( GameID, GameIDRule, Error;
   id: BitableRule,
   version: StringRule::short(),
   name: StringRule::short() );

/// Encoding for `VaporError`
pub struct VaporErrorRule;
encode_product!( VaporError, VaporErrorRule, Error;
   message: StringRule::short(),
   private_sid: UUIDRule );

/// Encoding for `BanReason`
pub struct BanReasonRule;
encode_sum!( BanReason, BanReasonRule, Error, |_| BitsRule(2);
   0 => Unknown,
   1 => OnlineConduct,
   2 => AdminUpdate );

/// Encoding for `Platform`
pub struct PlatformRule;
encode_sum!( Platform, PlatformRule, Error, |_| BitsRule(2);
   0 => Unknown,
   1 => Windows,
   2 => Mac,
   3 => Linux );

/// Encoding for `Distributor`
pub struct DistributorRule;
encode_sum!( Distributor, DistributorRule, Error, |_| BitsRule(3);
   0 => Unknown,
   1 => Nimbly,
   5 => Steam );

/// Encoding for `AuthMethod`
pub struct AuthMethodRule;
encode_sum!( AuthMethod, AuthMethodRule, Error, |_| BitsRule(2);
   2 => Unknown,
   0 => Vapor,
   1 => Steam );

/// A rule to encode a `Vector2<f32>` as two ranged floats
pub struct Vec2fRule {
   pub min: f32,
   pub max: f32,
   pub scale: f32,
}

impl Vec2fRule {
   pub fn new(min: f32, max: f32, scale: f32) -> Vec2fRule {
      Vec2fRule {
         min: min,
         max: max,
         scale: scale,
      }
   }
   fn rule(&self) -> FloatRule {
      FloatRule::new(self.min, self.max, self.scale)
   }
}

impl Encoding<Vector2<f32>> for Vec2fRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Vector2<f32>> {
      let x = self.rule().read(reader)?;
      let y = self.rule().read(reader)?;
      Ok(vec2(x, y))
   }
   fn write(&self, writer: &mut BitWriter, value: &Vector2<f32>) -> Result<()> {
      self.rule().write(writer, &value.x)?;
      self.rule().write(writer, &value.y)?;
      Ok(())
   }
}

impl Encoding<Point2<f32>> for Vec2fRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Point2<f32>> {
      let x = self.rule().read(reader)?;
      let y = self.rule().read(reader)?;
      Ok(point2(x, y))
   }
   fn write(&self, writer: &mut BitWriter, value: &Point2<f32>) -> Result<()> {
      self.rule().write(writer, &value.x)?;
      self.rule().write(writer, &value.y)?;
      Ok(())
   }
}
