//! Rules for en/decoding messages and their inner types.

use net::encode::*;
use net::guarantee::*;
use model::types::*;
use server_conf::*;
use message::*;
use rules::*;
use error::*;
use Context;

/// Encoding for a message
#[derive(Clone)]
pub struct MessageRule(pub Context);

impl Encoding<Message> for MessageRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Message> {
      Ok(Message(self.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &Message) -> Result<()> {
      self.write(writer, &value.0)
   }
}

impl Encoding<Guaranteed<Data>> for MessageRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Guaranteed<Data>> {
      let g_level = GuaranteeLevelRule.read(reader)?;
      let tag: u8 = BitableRule.read(reader)?;
      let g_data = GuaranteeDataOptionRule(g_level).read(reader)?;
      Ok(Guaranteed {
         addr: ([0, 0, 0, 0], 0).into(),
         guarantee: g_level,
         guarantee_data: g_data,
         data: match g_data {
            Some(GuaranteeData::Open(_)) |
            Some(GuaranteeData::Data(_)) |
            None => {
               DataRule {
                     tag: tag,
                     context: self.0,
                  }.read(reader)?
            }
            _ => Data::EmptyTag(tag),
         },
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Guaranteed<Data>) -> Result<()> {
      GuaranteeLevelRule.write(writer, &value.guarantee)?;
      let tag: u8 = match value.data {
         Data::EmptyTag(tag) => tag,
         Data::Friend(_) => 0,
         Data::Auth(_) => 1,
         Data::Validation(_) => 2,
         Data::ServerList(_) => 3,
         Data::RegisterServer(_) => 4,
         Data::Versioning(_) => 5,
         Data::FirewallChecker(_) => 6,
         Data::ServerInfo(_) => 7,
         Data::ServerPing(_) => 8,
         Data::GameStorage(_) => 9,
         Data::HardwareReport(_) => 10,
         Data::Join(_) => 70,
         Data::Game(_) => 71,
         Data::Dl(_) => 72,
         Data::Ping(_) => 73,
         Data::Lan(_) => 74,
      };
      BitableRule.write(writer, &tag)?;
      GuaranteeDataOptionRule(value.guarantee).write(writer, &value.guarantee_data)?;
      DataRule {
            tag: tag,
            context: self.0,
         }
         .write(writer, &value.data)
   }
}

/// An encoding for `Data`
struct DataRule {
   pub context: Context,
   pub tag: u8,
}

encode_sum!{ Data, DataRule, Error, |x: &DataRule| ReifiedRule(x.tag as u64);
   0 => Friend(|_|AllDataRule),
   1 => Auth(|_|AuthRule),
   2 => Validation(|_|ValidationRule),
   3 => ServerList(|_|ServerListRule),
   4 => RegisterServer(|_|RegisterServerRule),
   5 => Versioning(|_|VersioningRule),
   6 => FirewallChecker(|_|FirewallTestRule),
   7 => ServerInfo(|_|InfoRule),
   8 => ServerPing(|_|PingRule(true)),
   9 => GameStorage(|_|AllDataRule),
   10 => HardwareReport(|_|HWRule),
   70 => Join(|x: &DataRule| JoinRule(x.context)),
   71 => Game(|_|GameRule),
   72 => Dl(|_|DownloadRule),
   73 => Ping(|_|PingRule(false)),
   74 => Lan(|x: &DataRule| LanRule(x.context)),
   // This tag should never match on read, and on write should write no data.
   // Since the tag rule above is `ReifiedRule`, the value here will never be
   // written, so just needs to be something which won't match on read (anything > 255).
   0xFFFF => EmptyTag(|_| ReifiedRule(0)),
}

/// Encoding for `Auth` messages
pub struct AuthRule;
encode_sum!( Auth, AuthRule, Error, |_| BitsRule(8);
   0  => Login(|_| LoginRule),
   1  => LoginResp(|_| LoginRespRule),
   2  => LoginFail(|_| StringRule::short()),
   3  => CreateAccount(|_| AllDataRule),
   4  => CreateAccountResp(|_| AllDataRule),
   7  => LoginSuccess(|_| LoginRespRule),
   9  => Logout(|_| UUIDRule),
   10 => IpBlocked(|_| AllDataRule),
   11 => Disconnected(|_| VaporErrorRule),
   12 => RegisterKey(|_| AllDataRule),
   13 => RegisterKeyResp(|_| AllDataRule),
   14 => Keepalive,
   15 => UpdateAccount(|_| AllDataRule),
   16 => UpdateAccountResp(|_| AllDataRule),
   17 => ResetPassword(|_| AllDataRule),
   18 => ResetPasswordResp(|_| AllDataRule),
   21 => RecoverAccount(|_| AllDataRule),
   22 => RecoverAccountResp(|_| AllDataRule),
   23 => KeepaliveResp,
   24 => LoginConfirm,
   27 => FloodWarning(|_| VaporErrorRule) );

/// Rule for optionally encoding `Login` data, on read the packet length is
/// checked, iff over 10 bytes the data is read.
struct LoginRule;
impl Encoding<Option<Login>> for LoginRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Option<Login>> {
      Ok(if reader.len() < 10 {
         None
      } else {
         Some(LoginDataRule.read(reader)?)
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Option<Login>) -> Result<()> {
      match *value {
         Some(ref x) => LoginDataRule.write(writer, x),
         None => Ok(()),
      }
   }
}

/// Rule for encoding a steam ID
struct SteamIDRule;
encode_product!( SteamID, SteamIDRule, Error;
   nick: StringRule::short(),
   id: BitableRule );

/// Rule for encoding a list of steam friends
struct SteamFriendsRule;
encode_product!( SteamFriends, SteamFriendsRule, Error;
   my_id: SteamIDRule,
   friends: VecRule::new(SteamIDRule, 11) );

/// Internal rule for encoding login data
struct LoginDataRule;
encode_product!( Login, LoginDataRule, Error;
   username: OptionRule::new(StringRule::short()),
   password: OptionRule::new(StringRule::short()),
   encrypted_pass: OptionRule::new(VecRule::new(BitableRule, 21)),
   steam_id: OptionRule::new(BitableRule),
   steam_nick: OptionRule::new(StringRule::short()),
   unk: OptionRule::new(VecRule::new(BitableRule, 21)),
   steam_friends: OptionRule::new(SteamFriendsRule),
   distributor: DistributorRule,
   platform: PlatformRule,
   upnp: BitableRule );

/// A rule for encoding a login response
struct LoginRespRule;
encode_product!( LoginResp, LoginRespRule, Error;
   result: OptionRule::new(StringRule::short()),
   logged_in: BitableRule,
   id: OptionRule::new(PrivClientIDRule),
   authority: OptionRule::new(StringRule::short()),
   unk1: BitableRule,
   games: VecRule::new(GameIDRule, 32),
   unk2: BitableRule,
   unk3: BitableRule,
   unk4: BitableRule,
   unk5: BitableRule,
   ban: OptionRule::new_invert(BanDataRule) );

struct BanDataRule;
impl Encoding<(i32, BanReason)> for BanDataRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<(i32, BanReason)> {
      Ok((BitableRule.read(reader)?, BanReasonRule.read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &(i32, BanReason)) -> Result<()> {
      BitableRule.write(writer, &value.0)?;
      BanReasonRule.write(writer, &value.1)
   }
}

struct ValidationRule;
encode_sum!( Validation, ValidationRule, Error, |_| BitsRule(8);
   3 => Request(|_| ValidationCheckRule),
   4 => Response(|_| SignedRule(ClientValidationRule)) );

struct ValidationCheckRule;
encode_product!( ValidationCheck, ValidationCheckRule, Error;
   msg_header: BitableRule,
   id: ClientIDRule,
   game_id: BitableRule );

struct ClientValidationRule;
encode_product!( ClientValidation, ClientValidationRule, Error;
   id: ClientIDRule,
   valid: BitableRule,
   game_valid: BitableRule,
   owns_game: BitableRule,
   message: StringRule::short(),
   is_beta: BitableRule,
   ip_valid: BitableRule,
   ban: BanReasonRule );

/// An encoding for `Download`
struct DownloadRule;
encode_sum!(Download, DownloadRule, Error, |_| BitsRule(3);
   0 => Request(|_| DlReqRule),
   1 => Accept(|_| BitableRule),
   2 => Missing(|_| DlMissingRule),
   3 => Data(|_| DlDataRule),
   4 => Complete(|_| BitableRule)
);

/// An encoding for `DlReq`
struct DlReqRule;
encode_product!(DlReq, DlReqRule, Error;
   id: BitableRule,
   resource: StringRule::short());

/// An encoding for `DlData`
struct DlDataRule;
encode_product!(DlData, DlDataRule, Error;
   id: BitableRule,
   chunk: BitsRule(17),
   data: ArcRule(AllDataRule) );

/// An encoding for `DlMissing`
struct DlMissingRule;
encode_product!(DlMissing, DlMissingRule, Error;
   unk: BitableRule,
   chunks: VecRule::new(BitsRule(17), 10));

/// Encoding for `Versioning` messages
struct ServerListRule;
encode_sum!( ServerList, ServerListRule, Error, |_| BitsRule(8);
   3 => Request(|_| ListReqRule),
   4 => Response(|_| ListRespRule),
);

/// Encoding for `RegisterServer` messages
struct RegisterServerRule;
encode_sum!( RegisterServer, RegisterServerRule, Error, |_| BitsRule(8);
   3  => AuthReq(|_| BitableRule),
   4  => AuthResp(|_| AuthRespRule),
   5  => Hello(|_| UUIDRule),
   7  => HelloResp(|_| HelloRespRule),
   6  => Keepalive,
   8  => KeepaliveResp(|_| KeepaliveRespRule)
);

/// Encoding for `Versioning` messages
struct VersioningRule;
encode_sum!( Versioning, VersioningRule, Error, |_| BitsRule(8);
   3 => Request(|_| VersionReqRule),
   4 => Response(|_| VersionRespRule),
);

/// Encoding for `FirewallTest` messages
struct FirewallTestRule;
encode_sum!( FirewallTest, FirewallTestRule, Error, |_| BitsRule(8);
   3 => Request,
   4 => Response,
);

struct ListReqRule;
encode_product!{ ListReq, ListReqRule, Error;
   game_id: BitableRule,
   first_server: BitableRule,
}

struct ListRespRule;
encode_product!{ ListResp, ListRespRule, Error;
   invalid_game: BitableRule,
   message: OptionRule::new(StringRule::short()),
   servers: BitableRule,
   data: OptionRule::new(VecRule::new(BitableRule, 20)),
   a: BitableRule,
   first_server: BitableRule,
}

struct AuthRespRule;
encode_product!( AuthResp, AuthRespRule, Error;
   message: OptionRule::new(StringRule::short()),
   is_ok: BitableRule,
   is_listed: BitableRule,
   uuid: OptionRule::new(UUIDRule),
   addr: SocketAddrRule );

struct HelloRespRule;
encode_product!( HelloResp, HelloRespRule, Error;
   message: OptionRule::new(StringRule::short()),
   a: BitableRule,
   addr: OptionRule::new(SocketAddrRule),
   b: BitableRule );

struct KeepaliveRespRule;
encode_product!( KeepaliveResp, KeepaliveRespRule, Error;
   is_err: BitableRule,
   message: OptionRule::new(StringRule::short()) );

struct VersionReqRule;
encode_product!( VersionReq, VersionReqRule, Error;
   a: BitableRule,
   b: OptionRule::new(BitableRule) );

struct VersionRespRule;
encode_product!( VersionResp, VersionRespRule, Error;
   is_valid: BitableRule,
   msg: OptionRule::new(StringRule::short()),
   version: OptionRule::new(VersionStringRule),
   auth_method: OptionRule::new(StringRule::short()) );

/// An encoding for `Info` messages which checks the length of the message to
/// determine the type.
struct InfoRule;
impl Encoding<Info> for InfoRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Info> {
      if reader.len() < 10 {
         Ok(Info::Request)
      } else {
         // Read the 1 bit enum tag, which is always 0 for a well formed packet
         // as there is only one response type.
         let tag: i32 = BitsRule(1).read(reader)?;
         if tag != 0 {
            return Err("Malformed packet, info response tag != 0b0".into());
         }
         Ok(Info::Response(ServerInfoRule.read(reader)?))
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &Info) -> Result<()> {
      match *value {
         Info::Response(ref x) => {
            // The response type is a tagged enum with a single value,
            // so the tag is always 0, and the value is always ServerInfo.
            BitsRule(1).write(writer, &0)?;
            ServerInfoRule.write(writer, x)?;
         }
         // Requests have no associated data.
         Info::Request => (),
      }
      Ok(())
   }
}

struct ServerInfoRule;
encode_product!(ServerInfo, ServerInfoRule, Error;
   game_id: BitableRule,
   addr: OptionRule::new(SocketAddrRule),
   map_name: OptionRule::new(StringRule::short()),
   max_players: BitableRule,
   num_players: BitableRule,
   server_name: OptionRule::new(StringRule::short()),
   pass_req: BitableRule,
   hardcore: BitableRule,
   min_level: IntRule::new(1, 60),
   max_level: IntRule::new(1, 60),
   disallow_demo: BitableRule,
   version: OptionRule::new(VersionRule));

impl Default for ServerInfo {
   fn default() -> ServerInfo {
      ServerInfo {
         game_id: 0,
         addr: None,
         map_name: None,
         max_players: 1,
         num_players: 0,
         server_name: Some("Altitude Server".to_string()),
         pass_req: false,
         hardcore: false,
         min_level: 1,
         max_level: 60,
         disallow_demo: false,
         version: Some(VERSION),
      }
   }
}

struct PingRule(bool);
encode_pass!(Ping, PingRule, Error;
   server_ping: |x: &PingRule| ReifiedRule(x.0),
   respond: |_| BitableRule,
   id: |_| BitableRule);

/// An encoding for `Lan` messages which checks the length of the message to
/// determine the type.
struct LanRule(Context);
impl Encoding<Lan> for LanRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Lan> {
      if self.0 == Context::Server {
         Ok(Lan::Request)
      } else {
         Ok(Lan::Response(OptionRule::new(SocketAddrRule).read(reader)?))
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &Lan) -> Result<()> {
      match *value {
         Lan::Response(ref x) => {
            OptionRule::new(SocketAddrRule).write(writer, x)?;
         }
         // Requests have no associated data.
         Lan::Request => (),
      }
      Ok(())
   }
}

/// An encoding for `HardwareReport`
struct HWRule;
impl Encoding<HardwareReport> for HWRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<HardwareReport> {
      let len: u8 = BitableRule.read(reader)?;
      let value = HardwareReport {
         has_gl: BitableRule.read(reader)?,
         gl_vendor: StringRule::short().read(reader)?,
         gl_renderer: StringRule::short().read(reader)?,
         gl_version: StringRule::short().read(reader)?,
      };

      // 7 = 1 byte for has_gl + 2 for the lengths of each string.
      let real_len = 7 + value.gl_vendor.len() + value.gl_renderer.len() + value.gl_version.len();
      if len as usize != real_len {
         return Err("Malformed packet, hwreport expected vs actual length mismatch".into());
      }

      if BitsRule(7).read(reader)?: u8 != 0 {
         return Err("Malformed packet, invalid data after hardware report".into());
      };

      Ok(value)
   }
   fn write(&self, writer: &mut BitWriter, value: &HardwareReport) -> Result<()> {
      let len = 7 + value.gl_vendor.len() + value.gl_renderer.len() + value.gl_version.len();
      BitableRule.write(writer, &(len as u8))?;
      BitableRule.write(writer, &value.has_gl)?;
      StringRule::short().write(writer, &value.gl_vendor)?;
      StringRule::short().write(writer, &value.gl_renderer)?;
      StringRule::short().write(writer, &value.gl_version)?;
      BitsRule(7).write(writer, &0)?;
      Ok(())
   }
}

/// A rule to (en/de)code join data for a specific context.
///
/// For a server, all reads are assumed to be of requests to join or responses
/// to loads requests, and all writes to be responses to join requests or
/// resource information (`Load`).
/// On write, if the context doesn't match the Join type, an error is returned.
struct JoinRule(pub Context);

impl Encoding<Join> for JoinRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Join> {
      Ok(match BitsRule(2).read(reader)? {
         0 => {
            match self.0 {
               Context::Server => Join::JoinReq(JoinReqRule.read(reader)?),
               Context::Client => {
                  Join::JoinResp(OptionRule::new(StringRule::short()).read(reader)?)
               }
            }
         }
         1 => {
            match self.0 {
               Context::Server => Join::LoadResp(MapDescriptorRule.read(reader)?),
               Context::Client => Join::Load(LoadRule.read(reader)?),
            }
         }
         2 => Join::Disconnect,
         3 => Join::KeepAlive,
         _ => unreachable!(),
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Join) -> Result<()> {
      let tag_rule = BitsRule(2);
      let check_context = |t: &str, context: Context| -> Result<()> {
         if self.0 != context {
            Err(format!("'{}' can't be written in context '{:?}'", t, self.0).into())
         } else {
            Ok(())
         }
      };
      match *value {
         Join::JoinReq(ref x) => {
            check_context("Join::JoinReq", Context::Client)?;
            tag_rule.write(writer, &0)?;
            JoinReqRule.write(writer, x)?;
         }
         Join::JoinResp(ref x) => {
            check_context("Join::JoinResp", Context::Server)?;
            tag_rule.write(writer, &0)?;
            OptionRule::new(StringRule::short()).write(writer, x)?;
         }
         Join::Load(ref x) => {
            check_context("Join::Load", Context::Server)?;
            tag_rule.write(writer, &1)?;
            LoadRule.write(writer, x)?;
         }
         Join::LoadResp(ref x) => {
            check_context("Join::LoadResp", Context::Client)?;
            tag_rule.write(writer, &1)?;
            MapDescriptorRule.write(writer, x)?;
         }
         Join::Disconnect => tag_rule.write(writer, &2)?,
         Join::KeepAlive => tag_rule.write(writer, &3)?,
      }
      Ok(())
   }
}

struct JoinReqRule;
encode_product!(JoinReq, JoinReqRule, Error;
   version: VersionStringRule,
   password: StringRule::short(),
   level: ClientLevelRule,
   kills: BitableRule,
   deaths: BitableRule,
   id: ClientIDRule);

struct LoadRule;
encode_product!(Load, LoadRule, Error;
   counter: BitsRule(2),
   resource: MapDescriptorRule,
   conf: ServerConfRule
   );

struct GameRule;
encode_product!{ Game, GameRule, Error;
   counter: BitsRule(2),
   seq: MessageSeqRule,
   group_seq: GroupSeqRule,
   ev_groups: VecRule::new(EvGroupRule, 2),
   missing: VecRule::new(MessageSeqRule, 2),
   entity_data: OptionRule::new_invert(EntityDataRule),
}

struct EntityDataRule;
encode_product!{ EntityData, EntityDataRule, Error;
   ids: VecRule::new(BitsRule(10), 10),
   data: AllDataRule,
}

struct EvGroupRule;
impl Encoding<EvGroup> for EvGroupRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<EvGroup> {
      let seq = GroupSeqRule.read(reader)?;
      let len = BitsRule(11).read(reader)?;
      let data = ExactDataRule(len).read(reader)?;
      Ok(EvGroup {
         seq: seq,
         len: len,
         data: data,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &EvGroup) -> Result<()> {
      GroupSeqRule.write(writer, &value.seq)?;
      BitsRule(11).write(writer, &value.len)?;
      ExactDataRule(value.len).write(writer, &value.data)?;
      Ok(())
   }
}

struct GroupSeqRule;
impl Encoding<GroupSeq> for GroupSeqRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<GroupSeq> {
      Ok(GroupSeq(BitsRule(10).read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &GroupSeq) -> Result<()> {
      BitsRule(10).write(writer, &value.0)?;
      Ok(())
   }
}

struct MessageSeqRule;
impl Encoding<MessageSeq> for MessageSeqRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<MessageSeq> {
      Ok(MessageSeq(BitsRule(11).read(reader)?))
   }
   fn write(&self, writer: &mut BitWriter, value: &MessageSeq) -> Result<()> {
      BitsRule(11).write(writer, &value.0)?;
      Ok(())
   }
}

