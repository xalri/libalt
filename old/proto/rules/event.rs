use net::encode::*;
use rules::*;
use model::event::*;
use model::entity::*;
use server_conf::*;
use error::*;
use {Context, CodingState};
use model::action::AbilityID;

/// A rule which en/decodes many types of events. Most events need only a
/// reference to the world for encoding, so we use a single rule for all events,
/// rather than define a rule for each.
#[derive(Clone)] pub struct EvRule<'a>(pub &'a CodingState);

impl<'a> EntityIDRule<'a> {
   pub fn from_ev(rule: &'a EvRule) -> EntityIDRule<'a> {
      EntityIDRule(rule.0)
   }
}

impl<'a> NewIDRule<'a> {
   pub fn from_ev(rule: &'a EvRule) -> NewIDRule<'a> {
      NewIDRule(rule.0)
   }
}

/// The identity function
fn id<'a>(this: &'a EvRule<'a>) -> &'a EvRule<'a> { this }

/// Creates a `RemoveEntityRule` from an `EvRule`.
fn remove_rule<'a>(this: &'a EvRule<'a>) -> RemoveEntityRule<'a> {
   RemoveEntityRule(this.0)
}

encode_sum! {Event, EvRule<'a>, Error, |_| BitsRule(6);
   0  => NewEntity(id),
   1  => RemoveEntity(remove_rule),
   2  => PlayerInfo(id),
   3  => AssignPlayerID(id),
   4  => EntityMap(id),
   5  => ProjectileHit(id),
   6  => AbilityUse(id),
   7  => Kill(id),
   8  => Damage(id),
   9  => PlaneForce(id),
   10 => RemovePlane(id),
   11 => PowerupCollide(id),
   12 => PowerupGet(id),
   13 => Chat(id),
   14 => EmpHit(id),
   15 => AcidHit(id),
   16 => ThermoHit(id),
   17 => LaserHit(id),
   18 => Scoreboard(|_| VecRule::new(BitableRule, 12)),
   19 => GameModeStatus(|_| VecRule::new(BitableRule, 12)),
   20 => GameMode(|_| VecRule::new(BitableRule, 12)),
   21 => ServerConfig(|_| ServerConfRule),
   22 => KillBase(id),
   23 => KillTurret(id),
   24 => GetExp(id),
   25 => RoundEnd(|_| VecRule::new(BitableRule, 12)),
   26 => GameModeReset,
   27 => View(|x: &EvRule| IntPositionRule::new(x.0.map_size(), 0)),
   28 => ProjectileExplode(id),
   29 => Deflect(id),
   30 => Command(id),
   31 => CmdResp(id),
   32 => VoteStart(id),
   33 => ReqServerChange(id),
   34 => VoteEnd(id),
   35 => VoteCast(id),
   36 => Count(|_| BitsRule(10)), // 10 bits
   37 => Defuse(id),
   38 => SpawnReq(id),
   39 => SpawnResp(id),
   40 => AssignTeam(id),
   41 => BallScore(id),
   42 => BallUpdate(id),
   43 => InterceptBomb(id),
}

struct RemoveEntityRule<'a>(pub &'a CodingState);

impl<'a> Encoding<EntityID> for RemoveEntityRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<EntityID> {
      let id = BitsRule(10).read(reader)?;
      let local = self.0.context() == Context::Server;

      Ok(self.0.read_id(id, local))
   }
   fn write(&self, writer: &mut BitWriter, value: &EntityID) -> Result<()> {
      let id = self.0.write_id(*value)?.0;
      BitsRule(10).write(writer, &id)?;
      Ok(())
   }
}

encode_product!{ AssignPlayerID, EvRule<'a>, Error;
   player: PlayerNoRule,
   team: TeamRule
}

encode_product!{ PlayerInfo, EvRule<'a>, Error;
   connected: BitableRule,
   id: PlayerNoRule,
   message: OptionRule::new(StringRule::short()),
   nick: StringRule::short(),
   vapor_id: UUIDRule,
   unique: BitableRule,
   setup: PlaneSetupRule,
   team: TeamRule,
   level: IntRule::new(1, 60),
   ace: BitableRule
}

encode_product!{ Chat, EvRule<'a>, Error;
   player: PlayerNoRule,
   is_team: BitableRule,
   msg: StringRule::short()
}

encode_pass!{ KillAssister, EvRule<'a>, Error;
   player: |_| PlayerNoRule,
   plane: EntityIDRule::from_ev,
}

fn assists_rule<'a>(x: &'a EvRule) -> VecRule<EvRule<'a>> {
   VecRule::new(EvRule(x.0), 3)
}

encode_pass!{ Kill, EvRule<'a>, Error;
   target: EntityIDRule::from_ev,
   turret_team: |_| OptionRule::new_invert(TeamRule),
   assists: assists_rule,
}

encode_product!{ SpawnReq, EvRule<'a>, Error;
   team: TeamRule,
   setup: PlaneSetupRule,
   random: RandomTypeRule,
}

encode_pass!{ SpawnResp, EvRule<'a>, Error;
   team: |_| TeamRule,
   setup: |_| PlaneSetupRule,
   pos: IntPositionRule::from_ev,
   angle: |_| AngleRule,
   reason: |_| StringRule::short(),
   timeout: |_| IntRule::new(0, 450),
}

encode_pass!{ Damage, EvRule<'a>, Error;
   source: EntityIDRule::from_ev,
   target: EntityIDRule::from_ev,
   damage: |_| FloatRule::new(0.0, 3276.0, 10.0),
   angle: |_| FloatRule::new(-180.0, 180.0, 1.0 / 22.0),
}

encode_pass!{ PlaneForce, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   force: |_| Vec2fRule::new(-15.0, 15.0, 8.5),
}

encode_pass!{ RemovePlane, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   had_powerup: |_| BitableRule,
}

encode_pass!{ PowerupCollide, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   powerup: EntityIDRule::from_ev,
}

encode_pass!{ PowerupGet, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   powerup: EntityIDRule::from_ev,
}

fn assign_plane_rule<'a>(x: &'a EvRule) -> OptionRule<EntityIDRule<'a>> {
   OptionRule::new_invert(EntityIDRule::from_ev(x))
}

encode_pass!{ AssignTeam, EvRule<'a>, Error;
   player: |_| PlayerNoRule,
   team: |_| TeamRule,
   plane: assign_plane_rule,
}

encode_pass!{ BallScore, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   team: |_| TeamRule,
}

encode_product!{ ReqServerChange, EvRule<'a>, Error;
   server: StringRule::short(),
   password: StringRule::short(),
   uuid: UUIDRule,
}

encode_product!{ Command, EvRule<'a>, Error;
   command: StringRule::short(),
}

encode_product!{ CmdResp, EvRule<'a>, Error;
   command: StringRule::short(),
   result: StringRule::short(),
}

encode_product!{ VoteStart, EvRule<'a>, Error;
   descriptions: VecRule::new(StringRule::short(), 32),
   colours: VecRule::new(BitableRule, 32),
   ext_descriptions: VecRule::new(StringRule::short(), 32),
   ext_colours: VecRule::new(BitableRule, 32),
   message: StringRule::short(),
   options: VecRule::new(StringRule::short(), 32),
   hide_after_vote: BitableRule,
   append_question_mark: BitableRule,
}

encode_product!{ VoteEnd, EvRule<'a>, Error;
   message: StringRule::short(),
}

encode_product!{ VoteCast, EvRule<'a>, Error;
   message: StringRule::short(),
   value: BitableRule,
}

fn entity_rule<'a>(x: &'a EvRule) -> EntityRule<'a> {
   EntityRule(x.0)
}

encode_pass!{ NewEntity, EvRule<'a>, Error;
   id: NewIDRule::from_ev,
   entity: entity_rule,
}

encode_product!{ EntityMap, EvRule<'a>, Error;
   local: BitsRule(10),
   global: BitsRule(10),
}

fn action_rule<'a>((x,ability,user): (&'a EvRule, &AbilityID, &EntityID)) -> ActionRule<'a> {
   ActionRule::new(*user, *ability, x.0)
}

encode_chain!{ AbilityUse, EvRule<'a>, Error;
   action: action_rule;
   ability: |_| AbilityIDRule;
   user: EntityIDRule::from_ev
}

fn hit_data_rule<'a>((x,_,id): (&'a EvRule, &EntityID, &EntityID)) -> HitDataRule<'a> {
   HitDataRule(x.0, *id)
}

fn tuple_eid_rule<'a>((x,_): (&'a EvRule, &EntityID)) -> EntityIDRule<'a> {
   EntityIDRule::from_ev(x)
}

encode_chain!{ ProjectileHit, EvRule<'a>, Error;
   data: hit_data_rule;
   target: tuple_eid_rule;
   entity: EntityIDRule::from_ev
}

encode_pass!{ EmpHit, EvRule<'a>, Error;
   player: |_| PlayerNoRule,
   target: EntityIDRule::from_ev,
   duration: |_| BitsRule(12),
   drain: |_| BitsRule(7),
}

encode_pass!{ AcidHit, EvRule<'a>, Error;
   entity: EntityIDRule::from_ev,
   target: EntityIDRule::from_ev,
   value: |_| BitsRule(12),
}

encode_pass!{ ThermoHit, EvRule<'a>, Error;
   entity: EntityIDRule::from_ev,
   target: EntityIDRule::from_ev,
   strength: |_| FloatRule::new(-25.0, 25.0, 10.0),
   pos: |x: &EvRule| IntPositionRule::new(x.0.map_size(), 50),
}

encode_pass!{ LaserHit, EvRule<'a>, Error;
   randa: EntityIDRule::from_ev,
   target: EntityIDRule::from_ev,
   beam: |_| BitableRule,
   angle: |_| AngleRule,
}

impl<'a> Encoding<KillBase> for EvRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<KillBase> {
      let len: u32 = BitsRule(3).read(reader)?;
      if len != 2 {
         bail!("Invalid entity count ({}) for `KillBase` interaction event", len);
      }
      Ok(KillBase{
         base: EntityIDRule::from_ev(self).read(reader)?,
         entity: EntityIDRule::from_ev(self).read(reader)?,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &KillBase) -> Result<()> {
      BitsRule(3).write(writer, &2)?;
      EntityIDRule::from_ev(self).write(writer, &value.base)?;
      EntityIDRule::from_ev(self).write(writer, &value.entity)?;
      Ok(())
   }
}

impl<'a> Encoding<KillTurret> for EvRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<KillTurret> {
      let len: u32 = BitsRule(3).read(reader)?;
      if len != 2 {
         bail!("Invalid entity count ({}) for `KillTurret` interaction event", len);
      }
      Ok(KillTurret{
         turret: EntityIDRule::from_ev(self).read(reader)?,
         entity: EntityIDRule::from_ev(self).read(reader)?,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &KillTurret) -> Result<()> {
      BitsRule(3).write(writer, &2)?;
      EntityIDRule::from_ev(self).write(writer, &value.turret)?;
      EntityIDRule::from_ev(self).write(writer, &value.entity)?;
      Ok(())
   }
}

fn option_entity_rule<'a>(x: &'a EvRule) -> OptionRule<EntityIDRule<'a>> {
   OptionRule::new_invert(EntityIDRule(x.0))
}

encode_pass!{ GetExp, EvRule<'a>, Error;
   player_no: |_| PlayerNoRule,
   plane_id: option_entity_rule,
   source_id: option_entity_rule,
   exp: |_| IntRule::new(0, 1000),
}

encode_pass!{ BallUpdate, EvRule<'a>, Error;
   entity: EntityIDRule::from_ev,
   pos: |x: &EvRule| FloatPositionRule::new(x.0.map_size(), 300.0, 64.0),
   vel: |_| Vec2fRule::new(-50.0, 50.0, 640.0),
}

encode_pass!{ ProjectileExplode, EvRule<'a>, Error;
   projectile: EntityIDRule::from_ev,
}

encode_pass!{ Deflect, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   projectile: EntityIDRule::from_ev,
   angle: |_| AngleRule,
   speed: |_| FloatRule::new(0.0, 100.0, 10.0),
}

encode_pass!{ Defuse, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   base: EntityIDRule::from_ev,
}

encode_pass!{ InterceptBomb, EvRule<'a>, Error;
   plane: EntityIDRule::from_ev,
   bomb: EntityIDRule::from_ev,
   pos: |x: &EvRule| FloatPositionRule::new(x.0.map_size(), 300.0, 1.0),
}

pub struct ScoreboardRule {
   /// Send a diff of the previous scoreboard.
   pub diff: bool,
   /// The previous scoreboard, used for generating & applying diffs.
   /// Must have the same number of players as the current board
   /// (when new players are added they should be retroactively added
   /// to the previous scoreboard.)
   pub prev: Vec<ScoreboardStats>,
}

impl Encoding<Scoreboard> for ScoreboardRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Scoreboard> {
      let diff: bool = !BitableRule.read(reader)?;

      let players = self.prev.iter().map(|stats| {
         let ping: u32 = BitsRule(10).read(reader)?;

         let stats = if diff {
            let dirty = BitableRule.read(reader)?;
            if dirty {
               ScoreboardStatsRule::Diff(stats.clone()).read(reader)?
            } else {
               stats.clone()
            }
         } else {
            ScoreboardStatsRule::Full.read(reader)?
         };

         Ok((ping, stats))
      }).collect::<Result<Vec<_>>>()?;

      Ok(Scoreboard{ players })
   }
   fn write(&self, writer: &mut BitWriter, value: &Scoreboard) -> Result<()> {
      BitableRule.write(writer, &!self.diff)?;

      for (old, &(ping, ref new)) in self.prev.iter().zip(value.players.iter()) {
         BitsRule(10).write(writer, &ping)?;
         if self.diff {
            let dirty = old != new;
            BitableRule.write(writer, &dirty)?;
            if dirty {
               ScoreboardStatsRule::Diff(old.clone()).write(writer, new)?;
            }
         } else {
            ScoreboardStatsRule::Full.write(writer, new)?;
         }
      }

      Ok(())
   }
}

#[derive(Clone)]
enum ScoreboardStatsRule {
   Full,
   Diff(ScoreboardStats),
}

struct BoardStatRule {
   min: i32,
   max: i32,
   old: i32,
   diff: bool,
}

impl BoardStatRule {
   fn new(min: i32, max: i32, old: i32, diff: bool) -> BoardStatRule {
      BoardStatRule { min, max, old, diff }
   }
}

impl Encoding<i32> for BoardStatRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<i32> {
      Ok(if !self.diff || BitableRule.read(reader)? {
         IntRule::new(self.min, self.max).read(reader)?
      } else { self.old })
   }

   fn write(&self, writer: &mut BitWriter, value: &i32) -> Result<()> {
      let write = if self.diff {
         let write = *value != self.old;
         BitableRule.write(writer, &write)?;
         write
      } else { true };

      if write {
         IntRule::new(self.min, self.max).write(writer, value)?;
      }
      Ok(())
   }
}

impl Encoding<ScoreboardStats> for ScoreboardStatsRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<ScoreboardStats> {
      let (diff, mut stats) = match self.clone() {
         ScoreboardStatsRule::Diff(stats) => (true, stats),
         ScoreboardStatsRule::Full => (false, ScoreboardStats::default()),
      };

      stats.kills = BoardStatRule::new(-128, 895, stats.kills, diff).read(reader)?;
      stats.assists = BoardStatRule::new(0, 1023, stats.assists, diff).read(reader)?;
      stats.deaths = BoardStatRule::new(0, 1023, stats.deaths, diff).read(reader)?;

      Ok(stats)
   }

   fn write(&self, writer: &mut BitWriter, new: &ScoreboardStats) -> Result<()> {
      let (diff, prev) = match self.clone() {
         ScoreboardStatsRule::Diff(stats) => (true, stats),
         ScoreboardStatsRule::Full => (false, ScoreboardStats::default()),
      };

      BoardStatRule::new(-128, 895, prev.kills, diff).write(writer, &new.kills)?;
      BoardStatRule::new(0, 1023, prev.assists, diff).write(writer, &new.assists)?;
      BoardStatRule::new(0, 1023, prev.deaths, diff).write(writer, &new.deaths)?;

      Ok(())
   }
}

#[derive(Clone, Copy)]
pub struct GameTimerRule {
   pub round_time: Option<u32>,
   pub warmup_time: u32,
}

impl Encoding<GameTimers> for GameTimerRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<GameTimers> {
      let time = (FloatRule::new(0.0, 3600.0, 4.0).read(reader)? * 30.0) as u32;
      let game_end = IntRule::new(0, 18000).read(reader)?;
      let round_end = if let Some(max) = self.round_time {
         IntRule::new(0, max as i32).read(reader)?
      } else { 0 };
      let warmup_end = IntRule::new(0, self.warmup_time as i32).read(reader)?;

      Ok(GameTimers {time, game_end, round_end, warmup_end})
   }

   fn write(&self, writer: &mut BitWriter, value: &GameTimers) -> Result<()> {
      let seconds = (value.time as f32 / 30.0).min(3600.0);
      FloatRule::new(0.0, 3600.0, 4.0).write(writer, &seconds)?;
      IntRule::new(0, 18000).write(writer, &value.game_end)?;
      if let Some(max) = self.round_time {
         println!("round time etc {}", max);
         IntRule::new(0, max as i32).write(writer, &value.round_end)?
      }
      IntRule::new(0, self.warmup_time as i32).write(writer, &value.warmup_end)?;

      Ok(())
   }
}

encode_pass!{ FFAModeStatus, GameTimerRule, Error;
   timers: |x: &GameTimerRule| *x,
}
