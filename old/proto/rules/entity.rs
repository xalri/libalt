
use net::encode::*;
use {CodingState, Context};
use rules::*;
use geom::sprite::*;
use model::action::*;
use model::entity::*;
use util::time::*;
use cgmath::*;
use geom::*;
use error::*;

// ================================================================= //
//                     Entity Encoding `impl`s
// ================================================================= //

/// A rule for en/decoding `EntityID`
pub struct EntityIDRule<'a>(pub &'a CodingState);

/// A rule for reserving server entity IDs on read
pub struct NewIDRule<'a>(pub &'a CodingState);

/// Rule to encode the data for creating an `Entity`
pub struct EntityRule<'a>(pub &'a CodingState);

impl<'a> Encoding<Entity> for EntityRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Entity> {
      let tag: u8 = BitsRule(6).read(reader)?;

      // We read the tag then try to match it against each kind of entity in turn.

      if let Some(kind) = ProjectileKind::from_tag(tag) {
         NewProjectileRule(kind).read(reader).map(Entity::Projectile)
      }
      else if let Some(kind) = PlaneKind::from_tag(tag) {
         PlaneRule(kind, self.0).read(reader).map(Entity::Plane)
      }
      else if let Some(kind) = PowerupKind::from_tag(tag) {
         PowerupRule(kind, self.0).read(reader).map(Entity::Powerup)
      }
      else if let Some(kind) = CloudKind::from_tag(tag) {
         CloudRule(kind, self.0).read(reader).map(Entity::Cloud)
      } else {
         bail!("Invalid tag for entity: {}", tag)
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &Entity) -> Result<()> {
      match *value {
         Entity::Projectile(ref p) => {
            BitsRule(6).write(writer, &p.kind.to_tag())?;
            NewProjectileRule(p.kind).write(writer, p)
         }
         Entity::Plane(ref p) => {
            BitsRule(6).write(writer, &p.kind.to_tag())?;
            PlaneRule(p.kind, self.0).write(writer, p)
         }
         Entity::Powerup(ref p) => {
            BitsRule(6).write(writer, &p.kind().to_tag())?;
            PowerupRule(p.kind(), self.0).write(writer, p)
         }
         Entity::Cloud(ref p) => {
            BitsRule(6).write(writer, &p.kind.to_tag())?;
            CloudRule(p.kind, self.0).write(writer, p)
         }
         _ => Err("Attempt to create map entity with EntityRule".into()),
      }
   }
}

impl<'a> Encoding<EntityID> for EntityIDRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<EntityID> {
      let id = BitsRule(10).read(reader)?;
      let local = if self.0.context() == Context::Client { false }
      else { BitableRule.read(reader)? };

      Ok(self.0.read_id(id, local))
   }
   fn write(&self, writer: &mut BitWriter, value: &EntityID) -> Result<()> {
      let (id, local) = self.0.write_id(*value)?;
      BitsRule(10).write(writer, &id)?;
      if self.0.context() == Context::Client {
         BitableRule.write(writer, &local)?;
      } else if local {
         panic!("Server attempted to write local entity ID");
      }
      Ok(())
   }
}

impl<'a> Encoding<EntityID> for NewIDRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<EntityID> {
      let local: bool = BitableRule.read(reader)?;
      if local ^ (self.0.context() == Context::Server) {
         bail!("NewID Context mismatch o.O");
      }

      let id = BitsRule(10).read(reader)?;
      Ok(self.0.reserve_id(id))
   }
   fn write(&self, writer: &mut BitWriter, value: &EntityID) -> Result<()> {
      let (id, local) = self.0.write_id(*value)?;
      let local = if self.0.context() == Context::Server { false } else { local };
      BitableRule.write(writer, &local)?;
      BitsRule(10).write(writer, &id)?;
      Ok(())
   }
}

// ================================================================= //
//                          Action Rules
// ================================================================= //

/// Encoding for `AbilityID`
pub struct AbilityIDRule;
encode_sum!( AbilityID, AbilityIDRule, Error, |_| BitsRule(2);
   0 => Primary, 1 => Secondary, 2 => Tertiary, 3 => Powerup );

/// An encoding for an action, part of `AbilityUseEv`
pub struct ActionRule<'a> {
   user: EntityID,
   ability: AbilityID,
   world: &'a CodingState,
}

impl<'a> ActionRule<'a> {
   /// Create a new ActionRule
   pub fn new(user: EntityID, ability: AbilityID, world: &'a CodingState) -> ActionRule<'a> {
      ActionRule { user, ability, world }
   }

   /// Get the type of action referred to by the ability in this ActionRule, by
   /// checking the entity referred to by the given entity ID.
   pub fn get_kind(&self) -> Option<ActionType> {
      match self.world.entity(self.user) {
         Ok(e) => e.action_kind(self.ability),
         Err(_) => None
      }
   }
}

impl<'a> Encoding<Action> for ActionRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Action> {
      use self::ActionType::*;
      match self.get_kind() {
         None => {
            Err(format!("No matching action for user: {:?}, ability: {:?}",
                        self.user,
                        self.ability)
               .into())
         }
         Some(Shoot(p)) => {
            let rule = ProjectileRule::new(p, self.user, self.world);
            Ok(Action::Shoot(rule.read(reader)?))
         }
         Some(ShootDetonate(p)) => {
            let detonate: bool = BitableRule.read(reader)?;
            if detonate {
               let id = NewIDRule(self.world).read(reader)?;
               Ok(Action::Detonate(id))
            } else {
               let rule = ProjectileRule::new(p, self.user, self.world);
               Ok(Action::ShootDetonatable(rule.read(reader)?))
            }
         }
         Some(DoubleShoot(p)) => {
            let rule = ProjectileRule::new(p, self.user, self.world);
            Ok(Action::DoubleShoot(rule.read(reader)?, rule.read(reader)?))
         }
         Some(BallFire) => Ok(Action::BallFire),
         Some(ChargeFire) => Ok(Action::ChargeFire),
         Some(UseShield) => Ok(Action::UseShield),
         Some(UseWall) => {
            let pos = IntPositionRule::new(self.world.map_size(), 0).read(reader)?;
            let angle = AngleRule.read(reader)?;
            Ok(Action::UseWall(pos, angle))
         }
         Some(Heal) => Ok(Action::Heal),
         Some(RandaTA) => {
            let data = RandaTARule(self.world).read(reader)?;
            Ok(Action::RandaTA(data))
         }
         Some(RandaFlip) => {
            let value = FloatRule::new(0.0, 1.0, 15.0).read(reader)?;
            Ok(Action::RandaFlip(value))
         }
         Some(RandaWarp) => Ok(Action::RandaWarp),
         Some(Nothing) => Ok(Action::Nothing),
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &Action) -> Result<()> {
      use self::Action::*;
      match *value {
         Shoot(ref x) => {
            let rule = ProjectileRule::new(x.0.kind, self.user, self.world);
            rule.write(writer, x)?;
         }
         DoubleShoot(ref a, ref b) => {
            let rule = ProjectileRule::new(a.0.kind, self.user, self.world);
            rule.write(writer, a)?;
            rule.write(writer, b)?;
         }
         ShootDetonatable(ref x) => {
            BitableRule.write(writer, &false)?;
            let rule = ProjectileRule::new(x.0.kind, self.user, self.world);
            rule.write(writer, x)?;
         }
         Detonate(ref id) => {
            BitableRule.write(writer, &true)?;
            EntityIDRule(self.world).write(writer, id)?;
         }
         RandaTA(ref data) => RandaTARule(self.world).write(writer, data)?,
         RandaFlip(ref x) => FloatRule::new(0.0, 1.0, 15.0).write(writer, x)?,
         UseWall(ref pos, ref angle) => {
            IntPositionRule::new(self.world.map_size(), 0).write(writer, pos)?;
            AngleRule.write(writer, angle)?;
         }
         _ => (),
      }
      Ok(())
   }
}

struct RandaTARule<'a>(pub &'a CodingState);
encode_pass!{ RandaTA, RandaTARule<'a>, Error;
   a: |_| FloatRule::new(0.0, 1.0, 15.0),
   pos: |x: &RandaTARule| IntPositionRule::new(x.0.map_size(), 50),
   angle: |_| AngleRule,
   b: |_| FloatRule::new(0.0, 20.0, 10.0),
}


// ================================================================= //
//                          Position Rules
// ================================================================= //


/// A rule for encoding an integer position vector
pub struct IntPositionRule {
   pub world_size: Dimensions,
   pub padding: u32,
}

/// A rule for encoding a floating point position vector
pub struct FloatPositionRule {
   pub world_size: Dimensions,
   pub padding: f32,
   pub scale: f32,
}

impl IntPositionRule {
   /// Create a new `IntPositionRule`
   pub fn new(world_size: Dimensions, padding: u32) -> IntPositionRule {
      IntPositionRule { world_size, padding }
   }

   pub fn from_ev(x: &EvRule) -> IntPositionRule {
      IntPositionRule::new(x.0.map_size(), 50)
   }

   /// Get the `IntRule` used to read/write each part of the position vector.
   fn rule(&self, v: f32) -> IntRule {
      let min = 0i32 - self.padding as i32;
      let max = v as i32 - 1 + self.padding as i32;
      IntRule::new(min, max)
   }
}

impl FloatPositionRule {
   /// Create a new `FloatPositionRule`
   pub fn new(world_size: Dimensions, padding: f32, scale: f32) -> FloatPositionRule {
      FloatPositionRule { world_size, padding, scale }
   }
   /// Get the `FloatRule` used to read/write each part of the position vector.
   fn rule(&self, v: f32) -> FloatRule {
      let min = 0f32 - self.padding;
      let max = v as f32 + self.padding;
      FloatRule::new(min, max, self.scale)
   }
}

impl Encoding<Point2<i32>> for IntPositionRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Point2<i32>> {
      let x = self.rule(self.world_size.width).read(reader)?;
      let y = self.rule(self.world_size.height).read(reader)?;
      Ok(point2(x, y))
   }
   fn write(&self, writer: &mut BitWriter, value: &Point2<i32>) -> Result<()> {
      self.rule(self.world_size.width).write(writer, &value.x)?;
      self.rule(self.world_size.height).write(writer, &value.y)?;
      Ok(())
   }
}

impl Encoding<Point2<f32>> for FloatPositionRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Point2<f32>> {
      let x = self.rule(self.world_size.width).read(reader)?;
      let y = self.rule(self.world_size.height).read(reader)?;
      Ok(point2(x, y))
   }
   fn write(&self, writer: &mut BitWriter, value: &Point2<f32>) -> Result<()> {
      self.rule(self.world_size.width).write(writer, &value.x)?;
      self.rule(self.world_size.height).write(writer, &value.y)?;
      Ok(())
   }
}


// ================================================================= //
//                            Cloud Rule
// ================================================================= //

struct CloudRule<'a>(pub CloudKind, pub &'a CodingState);

impl<'a> Encoding<Cloud> for CloudRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Cloud> {
      let mut sprite = Sprite::from_poly(Cloud::poly());
      sprite.position = IntPositionRule::new(self.1.map_size(), 50).read(reader)?.cast();
      sprite.angle = AngleRule.read(reader)?;

      Ok(Cloud {
         kind: self.0,
         sprite,
         fade: CountdownRule(self.0.max_time()).read(reader)?,
         owner: EntityIDRule(self.1).read(reader)?,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Cloud) -> Result<()> {
      IntPositionRule::new(self.1.map_size(), 50).write(writer, &value.sprite.position.cast())?;
      AngleRule.write(writer, &value.sprite.angle)?;
      CountdownRule(self.0.max_time()).write(writer, &value.fade)?;
      EntityIDRule(self.1).write(writer, &value.owner)?;
      Ok(())
   }
}

struct CountdownRule(i32);
impl Encoding<Countdown> for CountdownRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Countdown> {
      let mut countdown = Countdown::new(self.0 as f32);
      countdown.remaining = (IntRule::new(0, self.0).read(reader)?: i32) as f32;
      Ok(countdown)
   }
   fn write(&self, writer: &mut BitWriter, value: &Countdown) -> Result<()> {
      IntRule::new(0, self.0).write(writer, &(value.remaining as i32))?;
      Ok(())
   }
}

// ================================================================= //
//                            Plane Rules
// ================================================================= //

/// A rule for creating a new plane -- used in `NewEntityRule`
struct PlaneRule<'a>(pub PlaneKind, pub &'a CodingState);

fn powerup_rule<'a>(x: &'a PlaneRule) -> OptionRule<PlanePowerupRule<'a>> {
   OptionRule::new_invert(PlanePowerupRule(x.1))
}

struct PlanePowerupRule<'a>(pub &'a CodingState);
impl<'a> Encoding<Powerup> for PlanePowerupRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Powerup> {
      let entity = EntityRule(self.0).read(reader)?;
      Ok(entity.as_powerup()?.clone())
   }
   fn write(&self, writer: &mut BitWriter, value: &Powerup) -> Result<()> {
      EntityRule(self.0).write(writer, &Entity::Powerup(value.clone()))
   }
}

/// The identity function for a `PlaneRule`
fn id<'a>(this: &'a PlaneRule<'a>) -> &'a PlaneRule<'a> { this }

impl<'a> Encoding<AnimSprite> for PlaneRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<AnimSprite> {
      // The sprite is setup by Plane::init which runs between entity updates
      // and post state being applied.. Here we just give it the position & angle,
      // post state gives all the other flight-related fields.
      let mut sprite = AnimSprite::empty();
      let pos = IntPositionRule::new(self.1.map_size(), 50).read(reader)?;
      let angle: i32 = IntRule::new(-180, 180).read(reader)?;
      sprite.position = pos.cast();
      sprite.angle = angle as f32;
      Ok(sprite)
   }
   fn write(&self, writer: &mut BitWriter, value: &AnimSprite) -> Result<()> {
      let int_pos = value.body.position.cast();
      let int_angle = value.body.angle.round() as i32;
      IntPositionRule::new(self.1.map_size(), 50).write(writer, &int_pos)?;
      IntRule::new(-180, 180).write(writer, &int_angle)?;
      Ok(())
   }
}

encode_pass_default!(Plane, PlaneRule<'a>, Error;
   kind: |x: &PlaneRule| ReifiedRule(x.0),
   player:|_| PlayerNoRule,
   team:|_| TeamRule,
   setup:|_| PlaneSetupRule,

   body: id,
   //pos:|x: &PlaneRule| IntPositionRule::new(x.1.map_size(), 50),
   //angle:|_| IntRule::new(-180, 180),
   powerup: powerup_rule,
   dead:|_| BitableRule,
   timers: id,
   exp:|_| IntRule::new(0, 90),
);

encode_pass_default!(PlaneAttributes, PlaneRule<'a>, Error;
   age:|_| BitsRule(16),
   last_pickup:|_| BitsRule(16),
);

/// Encoding for `PlaneSetup`
pub struct PlaneSetupRule;

impl Encoding<PlaneSetup> for PlaneSetupRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<PlaneSetup> {
      if BitableRule.read(reader)?: bool {
         if BitableRule.read(reader)?: bool {
            Ok(PlaneSetup {
               random: RandomType::Full,
               .. Default::default()
            })
         } else {
            Ok(PlaneSetup {
               random: RandomType::Custom,
               .. Default::default()
            })
         }
      } else {
         let plane = PlaneKindRule.read(reader)?;
         let red_perk_id = PerkRule.read(reader)?;

         Ok(PlaneSetup {
            random: RandomType::None,
            red_perk: plane.red_perk(red_perk_id),
            green_perk: PerkRule.read(reader)?,
            blue_perk: PerkRule.read(reader)?,
            skin: SkinRule.read(reader)?,
         })
      }
   }
   fn write(&self, writer: &mut BitWriter, value: &PlaneSetup) -> Result<()> {
      match value.random {
         RandomType::Full => {
            BitableRule.write(writer, &true)?;
            BitableRule.write(writer, &true)?;
         }
         RandomType::Custom => {
            BitableRule.write(writer, &true)?;
            BitableRule.write(writer, &false)?;
         }
         RandomType::None => {
            BitableRule.write(writer, &false)?;
            PlaneKindRule.write(writer, &value.plane())?;
            PerkRule.write(writer, &value.red_perk_id())?;
            PerkRule.write(writer, &value.green_perk)?;
            PerkRule.write(writer, &value.blue_perk)?;
            // TODO: 0-255 -> 8 bits might have broken things, tests needed..
            SkinRule.write(writer, &value.skin)?;
         }
      }
      Ok(())
   }
}

/// Encoding for `PlaneKind`
struct PlaneKindRule;
encode_sum!( PlaneKind, PlaneKindRule, Error, |_| BitsRule(3);
   0 => Biplane, 1 => Bomber, 2 => Explodet,
   3 => Loopy,   4 => Miranda );

/// Encoding for `Skin`
pub struct SkinRule;
encode_sum!( Skin, SkinRule, Error, |_| BitsRule(8);
   0 => None, 1 => Shark, 2 => Zebra, 3 => Checker, 4 => Flame, 5 => Santa );

/// Encoding for `RandomType`
pub struct RandomTypeRule;
encode_sum!( RandomType, RandomTypeRule, Error, |_| BitsRule(2);
   0 => None, 1 => Custom, 2 => Full );

/// Encoding for the perk enums.
struct PerkRule;

encode_sum!( RedPerkID, PerkRule, Error, |_| BitsRule(4);
   4 => First, 5 => Second, 6 => Third );

encode_sum!( GreenPerk, PerkRule, Error, |_| BitsRule(4);
   4 => None, 5 => Rubber, 6 => Heavy, 7 => Repair, 8 => Flexi );

encode_sum!( BluePerk, PerkRule, Error, |_| BitsRule(4);
   4 => None, 5 => Turbo, 6 => Ultra, 7 => Reverse, 8 => Ace );

pub struct PlaneControlRule;
encode_product!{ PlaneControl, PlaneControlRule, Error;
   left: BitableRule,
   right: BitableRule,
   throttle_up: BitableRule,
   throttle_down: BitableRule,
   afterburner: BitableRule,
}


// ================================================================= //
//                          Powerup Rules
// ================================================================= //

/// The scale to use for encoding the velocity and position of a powerup
fn scale(fine: bool) -> f32 {
   if fine { 64f32 } else { 1f32 }
}

/// A rule for creating a new powerup -- used in `NewEntityRule`
struct PowerupRule<'a>(pub PowerupKind, pub &'a CodingState);

impl<'a> Encoding<Powerup> for PowerupRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Powerup> {
      let falling = BitableRule.read(reader)?;
      let pos = FloatPositionRule::new(self.1.map_size(), 300., scale(falling)).read(reader)?;
      let vel = Vec2fRule::new(-50., 50., 10. * scale(falling)).read(reader)?;
      let team = TeamRule.read(reader)?;
      let fade_timer = OptionRule::new(FloatRule::new(0., Powerup::max_fade_time() as f32, 1.))
         .read(reader)?
         .map(|duration| Countdown::new(duration as f32));
      let spawner = OptionRule::new_invert(EntityIDRule(self.1)).read(reader)?;
      let shoot = OptionRule::new_invert(PairRule(PlayerNoRule, IntRule::new(0, 300))).read(reader)?;
      let kind: PowerupKindData = self.read(reader)?;

      let mut sprite = Sprite::empty();
      sprite.position = pos;
      sprite.velocity = vel;

      Ok(Powerup {
         sprite, kind, falling, team, fade_timer, spawner, shoot,
         alive: true,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Powerup) -> Result<()> {
      let pos = value.sprite.position;
      let vel = value.sprite.velocity;

      BitableRule.write(writer, &value.falling)?;
      FloatPositionRule::new(self.1.map_size(), 300., scale(value.falling)).write(writer, &pos)?;
      Vec2fRule::new(-50., 50., 10. * scale(value.falling)).write(writer, &vel)?;
      TeamRule.write(writer, &value.team)?;
      OptionRule::new(FloatRule::new(0., Powerup::max_fade_time() as f32, 1.))
         .write(writer, &value.fade_timer.as_ref()
            .map(|countdown| countdown.duration as f32))?;

      println!("writing spawner {:?}", value.spawner);
      OptionRule::new_invert(EntityIDRule(self.1)).write(writer, &value.spawner)?;
      //OptionRule::new_invert(EntityIDRule(self.1)).write(writer, &None)?;
      OptionRule::new_invert(PairRule(PlayerNoRule, IntRule::new(0, 300))).write(writer, &value.shoot)?;
      self.write(writer, &value.kind)?;

      Ok(())
   }
}

impl<'a> Encoding<PowerupKindData> for PowerupRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<PowerupKindData> {
      Ok(match self.0 {
         PowerupKind::Shield => PowerupKindData::Shield,
         PowerupKind::Wall => PowerupKindData::Wall,
         PowerupKind::Missile => PowerupKindData::Missile,
         PowerupKind::Charge => PowerupKindData::Charge,
         PowerupKind::Bomb => PowerupKindData::Bomb,
         PowerupKind::Health => PowerupKindData::Health{
            health_value: IntRule::new(0, 100).read(reader)?,
            b: BitableRule.read(reader)?,
         },
         PowerupKind::Ball => PowerupKindData::Ball{
            carrier: OptionRule::new_invert(BallCarrierRule).read(reader)?,
            c: BitableRule.read(reader)?,
            last_carriers: ArrayRule::new(PlayerNoRule, 3).read(reader)?,
         }
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &PowerupKindData) -> Result<()> {
      match *value {
         PowerupKindData::Health { ref health_value, ref b } => {
            IntRule::new(0, 100).write(writer, health_value)?;
            BitableRule.write(writer, b)?;
         }
         PowerupKindData::Ball { ref carrier, ref c, ref last_carriers, } => {
            OptionRule::new_invert(BallCarrierRule).write(writer, carrier)?;
            BitableRule.write(writer, c)?;
            ArrayRule::new(PlayerNoRule, 3).write(writer, last_carriers)?;
         }
         _ =>  (),
      }

      Ok(())
   }
}

/// A rule to encode a `BallCarrier`
struct BallCarrierRule;
encode_pass!(BallCarrier, BallCarrierRule, Error;
   team: |_| TeamRule,
   id: |_| PlayerNoRule );


// ================================================================= //
//                         Projectile Rules
// ================================================================= //

// A few common encoding rules
static VEL_RULE: Vec2fRule = Vec2fRule {
   min: -25.,
   max: 25.,
   scale: 10.,
};
static MINE_VEL_RULE: Vec2fRule = Vec2fRule {
   min: -45.,
   max: 45.,
   scale: 10.,
};
static ANGLE_RULE: FloatRule = FloatRule {
   min: -180.,
   max: 180.,
   scale: 2.8,
};

/// A rule to en/decode a projectile of a given type, as part of an `AbilityUseEv`.
/// When writing, the given type is ignored in favour of the type in the
/// projectile's data.
pub struct ProjectileRule<'a> {
   kind: ProjectileKind,
   creator: EntityID,
   world: &'a CodingState,
}

impl<'a> ProjectileRule<'a> {
   /// Create a new ProjectileRule
   pub fn new(kind: ProjectileKind, creator: EntityID, world: &'a CodingState) -> ProjectileRule<'a> {
      ProjectileRule { kind, creator, world }
   }
}

impl<'a> Encoding<(Projectile, EntityID)> for ProjectileRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<(Projectile, EntityID)> {
      use self::ProjectileKind::*;

      let pos = if self.kind != ProjectileKind::TurretShot {
         Some(IntPositionRule::new(self.world.map_size(), 50).read(reader)?)
      } else {
         None
      };

      let velocity = if self.kind != TurretShot && self.kind != TailGun {
         match self.kind {
            Mine => Some(MINE_VEL_RULE.read(reader)?),
            _ => Some(VEL_RULE.read(reader)?),
         }
      } else {
         None
      };

      let angle = if self.kind == TurretShot {
         AngleRule.read(reader)?
      } else {
         ANGLE_RULE.read(reader)?
      };

      let time = if self.kind == TurretShot {
         let max = self.world.entity(self.creator)?.as_turret()?.max_bullet_time();
         Some(IntRule::new(0, max as i32).read(reader)?)
      } else {
         None
      };

      let entity_id = NewIDRule(self.world).read(reader)?;

      // TODO: error if incorrect kind?
      let _kind: u8 = BitsRule(6).read(reader)?;

      let size = if self.kind == RandaShot || self.kind == Laser {
         Some(BitsRule(10).read(reader)?)
      } else {
         None
      };

      let can_bounce = if self.kind == RandaShot {
         Some(BitableRule.read(reader)?)
      } else {
         None
      };

      let silent = if self.kind == Tracker {
         Some(BitableRule.read(reader)?)
      } else {
         None
      };

      Ok((Projectile {
             kind: self.kind,
             creator: Some(self.creator),
             pos,
             velocity,
             angle,
             time,
             size,
             can_bounce,
             silent,

             vel_normal: vec2(0.0, 0.0),
             speed: 0.0,
             target: None,
             alive: true,
             countdown: Countdown::new(self.kind.time_to_live() * 30.0),
          },
          entity_id))
   }
   fn write(&self, writer: &mut BitWriter, value: &(Projectile, EntityID)) -> Result<()> {
      use self::ProjectileKind::*;

      let p = &value.0;
      if let Some(ref pos) = p.pos {
         IntPositionRule::new(self.world.map_size(), 50).write(writer, pos)?;
      }

      if let Some(ref vel) = p.velocity {
         match self.kind {
            Mine => MINE_VEL_RULE.write(writer, vel)?,
            _ => VEL_RULE.write(writer, vel)?,
         }
      }

      if self.kind == TurretShot {
         AngleRule.write(writer, &p.angle)?;
      } else {
         ANGLE_RULE.write(writer, &p.angle)?;
      }

      if let Some(ref time) = p.time {
         let max = self.world.entity(self.creator)?.as_turret()?.max_bullet_time();
         IntRule::new(0, max as i32).write(writer, time)?
      }

      EntityIDRule(self.world).write(writer, &value.1)?;
      BitsRule(6).write(writer, &p.kind.to_tag())?;

      if let Some(ref size) = p.size {
         BitsRule(10).write(writer, size)?;
      }
      if let Some(ref can_bounce) = p.can_bounce {
         BitableRule.write(writer, can_bounce)?;
      }
      if let Some(ref silent) = p.silent {
         BitableRule.write(writer, silent)?;
      }

      Ok(())
   }
}

/// A rule to en/decode a projectile as part of a new entity event.
struct NewProjectileRule(pub ProjectileKind);

impl Encoding<Projectile> for NewProjectileRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Projectile> {
      let mut p = Projectile::default();
      p.kind = self.0;

      match p.kind {
         ProjectileKind::RandaShot => {
            p.size = Some(BitsRule(10).read(reader)?);
            p.can_bounce = Some(BitableRule.read(reader)?);
         }
         ProjectileKind::Laser => p.size = Some(BitsRule(10).read(reader)?),
         ProjectileKind::Tracker => p.silent = Some(BitableRule.read(reader)?),
         _ => (),
      }

      Ok(p)
   }
   fn write(&self, writer: &mut BitWriter, value: &Projectile) -> Result<()> {
      if let Some(size) = value.size {
         BitsRule(10).write(writer, &size)?;
      }
      if let Some(can_bounce) = value.can_bounce {
         BitableRule.write(writer, &can_bounce)?;
      }
      if let Some(silent) = value.silent {
         BitableRule.write(writer, &silent)?;
      }
      Ok(())
   }
}

/// A rule for optionally encoding `ProjectileHitData` for a given projectile
/// type.
pub struct HitDataRule<'a>(pub &'a CodingState, pub EntityID);
struct HitDataRuleInner(pub ProjectileKind);

impl<'a> Encoding<Option<HitData>> for HitDataRule<'a> {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Option<HitData>> {
      HitDataRuleInner(self.0.entity(self.1)?.as_projectile()?.kind)
         .read(reader)
   }
   fn write(&self, writer: &mut BitWriter, value: &Option<HitData>) -> Result<()> {
      HitDataRuleInner(self.0.entity(self.1)?.as_projectile()?.kind)
         .write(writer, value)
   }
}

impl Encoding<Option<HitData>> for HitDataRuleInner {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Option<HitData>> {
      Ok(match self.0 {
         ProjectileKind::Grenade |
         ProjectileKind::RandaShot => {
            Some(HitData {
               angle: AngleRule.read(reader)?,
               value: FloatRule::new(-0.1, 1.0, 100.).read(reader)?,
            })
         }
         _ => None,
      })
   }
   fn write(&self, writer: &mut BitWriter, value: &Option<HitData>) -> Result<()> {
      if let Some(ref x) = *value {
         AngleRule.write(writer, &x.angle)?;
         FloatRule::new(-0.1, 1.0, 100.).write(writer, &x.value)?;
      }
      Ok(())
   }
}
