#[macro_use] extern crate slog;
extern crate net_emu;
extern crate util;
extern crate proto;
extern crate geom;
extern crate model;

use util::logger;
use geom::*;
use model::entity::*;
use model::event::*;
use proto::Context;
use proto::message::*;
use proto::rules::*;
use proto::sequencer::*;
use proto::CodingState;
use net_emu::*;

#[test]
fn many_events() {
   let log = logger::term();

   const MESSAGES: usize = 10;
   const EVENTS: usize = 1;

   let emu_conf = EmuParams {
      latency: 00.0,
      latency_deviation: 0.0,
      packet_loss: 0.00,
      packet_dup: 0.0,
   };

   let log_a = log.new(o!("endpoint" => "a"));
   let log_b = log.new(o!("endpoint" => "b"));

   let mut emu = NetEmu::new(emu_conf,
                             TestSeq(Sequencer::new(log_a, 0)),
                             TestSeq(Sequencer::new(log_b, 0)));

   let mut value_out = 0u64;
   let mut value_in = 0u64;

   for _ in 0..MESSAGES {
      for _ in 0..EVENTS {
         emu.send_to_b(Event::Count(value_out as u16));
         value_out += 1;
      }
      for ev in emu.recv_b() {
         assert_eq!(Event::Count((value_in & 0b1111111111) as u16), ev);
         value_in += 1;
      }
      emu.run_ms(20);
   }
   emu.run_ms(50);
   for ev in emu.recv_b() {
      assert_eq!(Event::Count((value_in & 0b1111111111) as u16), ev);
      value_in += 1;
   }

   assert_eq!(value_out, value_in);
}

struct TestSeq(Sequencer);

impl Endpoint<Event, Game> for TestSeq {
   fn send(&mut self, ev: Event, _: u64) -> Option<Game> {
      self.0.add_event(EvRule(&DummyWorld), &ev).unwrap();
      None
   }
   fn recv(&mut self, mut data: Game, _: u64) -> (Vec<Game>, Vec<Event>) {
      self.0.receive(&mut data);
      let mut evs = Vec::new();
      while let Some(ev) = self.0.next_ev(EvRule(&DummyWorld)) { evs.push(ev); }
      (vec![], evs)
   }
   fn update(&mut self, _: u64) -> Vec<Game> {
      vec![self.0.make_msg().unwrap()]
   }
}

struct DummyWorld;

impl IDMap for DummyWorld {
   fn read_id(&self, _id: u16, _client_local: bool) -> EntityID { panic!(); }
   fn write_id(&self, _id: EntityID) -> model::error::Result<(u16, bool)> { panic!(); }
   fn reserve_id(&self, _id: u16) -> EntityID { panic!(); }
}

impl CodingState for DummyWorld {
   fn context(&self) -> Context { panic!(); }
   fn map_size(&self) -> Dimensions { panic!(); }
   fn entity(&self, _id: EntityID) -> proto::error::Result<ReadGuard<Entity>> { panic!(); }
   fn entity_set(&self) -> &[EntityID] { panic!(); }
}
