#[macro_use] extern crate slog;
#[macro_use] extern crate error_chain;
extern crate util;
extern crate net;
extern crate proto;

mod runner;
mod capture;

use std::fs;
use proto::Context;
use proto::message::*;
use proto::rules::*;
use net::encode::*;
use runner::*;
use capture::*;

#[test]
pub fn verify_message() {
   let log = util::logger::term();

   let mut runner = SuiteRunner::new(log.clone(), "message parsing".into());
   let dir = fs::read_dir("./tests/capture/").unwrap();

   for dir_entry in dir {
      let file = dir_entry.unwrap();
      let path = file.path();
      let ext = path.extension();
      if ext.is_none() || ext.unwrap() != "cap" { continue; }
      runner.test(path.to_str().unwrap(), || {
         let mut file = fs::File::open(file.path()).unwrap();
         let cap = Capture::read(&mut file);
         if cap.is_err() {
            error!(log, "Parsing capture failed: {:?}", cap.unwrap_err());
            return false;
         }
         let cap = cap.unwrap();
         for c in cap.0 {
            let context = if c.inbound {
               Context::Server
            } else {
               Context::Client
            };
            match decode::<_, Message>(log.clone(), &MessageRule(context), &c.data) {
               Ok(msg) => {
                  // Flip the context for writing
                  let context = if c.inbound {
                     Context::Client
                  } else {
                     Context::Server
                  };
                  match encode(log.clone(), &MessageRule(context), &msg) {
                     Ok(data) => {
                        if data != c.data {
                           error!(log, "Re-encode packet does not match ({} vs {}):",
                                  data.len(),
                                  c.data.len());
                           error!(log, "Reencoded: {:?}", data);
                           error!(log, "Original: {:?}", c.data);
                           error!(log, "Decoded: {:?}", msg);
                           return false;
                        }
                     }
                     Err(e) => {
                        error!(log, "Failed to reencode message: {:?}", msg);
                        error!(log, "Error: {}", e);
                        return false;
                     }
                  }
               }
               Err(e) => {
                  error!(log, "Failed to decode packet: {}", e);
                  return false;
               }
            }
         }
         true
      })
   }
}
