//! Packet capture format, for recording conversations captured by the proxy. Used in the
//! sacred validation tests.

#![allow(dead_code)]

use std::ops::Deref;
use std::io::{self, Read, Write, BufReader, LineWriter};
use std::io::BufRead;

/// A single packet in the conversation.
#[derive(PartialEq, Debug)]
pub struct CapturedPacket {
   /// Whether it's an inbound packet, from the server.
   pub inbound: bool,
   /// Raw data it carries.
   pub data: Vec<u8>,
}

impl CapturedPacket {
   fn read_inbound(str: &str) -> Option<bool> {
      match str {
         "<-" => Some(true),
         "->" => Some(false),
         _ => None,
      }
   }

   fn read_data(line: &str) -> Option<Vec<u8>> {
      if line.len() < 4 {
         return None;
      }
      use std::u8;
      let slice = &line[1..line.len() - 1];
      let mut res = Vec::new();
      for expr in slice.split(',') {
         if let Ok(read) = u8::from_str_radix(expr.trim(), 16) {
            res.push(read)
         } else {
            return None;
         }
      }
      Some(res)
   }

   pub fn read_line(line: &str) -> Option<Self> {
      if line.len() < 2 {
         return None;
      }
      let (inbound, data) = line.split_at(2);
      Self::read_inbound(inbound).and_then(|read_inbound| {
         Self::read_data(data.trim()).map(|read_data| {
            CapturedPacket {
               inbound: read_inbound,
               data: read_data,
            }
         })
      })
   }

   fn write_data(data: &[u8]) -> String {
      format!("[{}]",
              data.iter()
                 .map(|b| format!("{:02X}", b))
                 .collect::<Vec<_>>()
                 .join(", "))
   }

   pub fn write_line(&self) -> String {
      let inbound = if self.inbound { "<-" } else { "->" };
      format!("{}{}", inbound, Self::write_data(&self.data))
   }
}

#[derive(PartialEq, Debug)]
pub struct Capture(pub Vec<CapturedPacket>);

impl Deref for Capture {
   type Target = Vec<CapturedPacket>;
   fn deref(&self) -> &Vec<CapturedPacket> {
      &self.0
   }
}

impl Capture {
   pub fn new(data: Vec<CapturedPacket>) -> Self {
      Capture(data)
   }

   fn read_line(line: &str) -> Option<Option<CapturedPacket>> {
      line.chars().next().and_then(|first| {
         if first == '#' {
            Some(None)
         } else {
            CapturedPacket::read_line(line).map(Some)
         }
      })
   }

   pub fn read<R: Read>(r: R) -> Result<Capture, String> {
      let mut vec = Vec::new();
      for (n, line) in BufReader::new(r).lines().enumerate() {
         let ln = line.map_err(|_| format!("Cannot read line {}", n))?;
         if let Some(res) = Self::read_line(&ln) {
            if let Some(packet) = res {
               vec.push(packet);
            }
         } else {
            bail!("Cannot parse line {}", n);
         }
      }
      Ok(Capture(vec))
   }

   pub fn write<W: Write>(&self, w: W) -> Result<(), io::Error> {
      let mut writer = LineWriter::new(w);
      for packet in &self.0 {
         writer.write_all(packet.write_line().as_bytes())?;
         writer.write_all(b"\n")?;
      }
      Ok(())
   }
}

#[cfg(tests)]
mod tests {
   use super::*;

   #[test]
   fn read_write_capture() {
      use std::io::Cursor;
      let capture = Capture(vec![CapturedPacket {
                                    inbound: false,
                                    data: vec![1, 5, 3],
                                 },
                                 CapturedPacket {
                                    inbound: true,
                                    data: vec![106, 23, 255, 5],
                                 }]);
      let mut printed = Cursor::new(Vec::new());
      capture.write(&mut printed).unwrap();

      let mut printed = Cursor::new(printed.into_inner());
      let decoded = Capture::read(&mut printed).unwrap();
      assert_eq!(decoded, capture);
   }
}
