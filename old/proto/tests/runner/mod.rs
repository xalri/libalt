#![allow(dead_code)]

/// Simple executor for verification test suites, providing canonical logging and utilities.
pub struct SuiteRunner {
   name: String,
   total: usize,
   passed: usize,
   log: ::slog::Logger,
}

impl SuiteRunner {
   pub fn new(log: ::slog::Logger, name: String) -> Self {
      info!(log, "*** Starting: {}", &name);
      info!(log, "***");
      SuiteRunner {
         name: name,
         total: 0,
         passed: 0,
         log: log,
      }
   }

   pub fn test<F>(&mut self, name: &str, process: F)
      where F: Fn() -> bool
   {
      info!(self.log, "*** Test {}: {}", self.total, name);

      self.total += 1;
      if process() {
         info!(self.log, "*** Passed: {} ***", name);
         self.passed += 1;
      } else {
         error!(self.log, "*** Failed: {} ***", name);
      }
   }
}

impl Drop for SuiteRunner {
   fn drop(&mut self) {
      info!(self.log, "***");
      info!(self.log, "*** Ended: {}", &self.name);
      info!(self.log, "*** Total: {}. Passed: {}. Failed: {}.",
            self.total,
            self.passed,
            self.total - self.passed);
      info!(self.log, "****************************************");
      if self.total - self.passed > 0 {
         panic!();
      }
   }
}
