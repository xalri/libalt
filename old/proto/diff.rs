//! Entity updates

use cgmath::*;
use geom::*;
use model::entity::*;
use net::encode::*;
use CodingState;
use message::EntityData;
use rules::*;
use error::*;

/// A trait implemented by entities which can be updated as part of a game message,
/// defines functions to write and read data to and from a message.
pub trait Diffable {
   /// Read pre-state for this entity -- a change to the entity which is applied
   /// as soon as the message it comes from is parsed.
   /// `apply` is false when the packet the data comes from is older than that
   /// of the last update
   fn read_pre(&mut self, r: &mut BitReader, apply: bool) -> Result<()>;

   /// Write pre-state for this entity
   fn write_pre(&self, w: &mut BitWriter) -> Result<()>;

   /// Read post-state for this entity -- an update to the entity applied as
   /// part of each tick of the game loop.
   fn read_post(&mut self, r: &mut BitReader, s: Dimensions, apply: bool) -> Result<()>;

   /// Write post-state for this entity
   fn write_post(&self, w: &mut BitWriter, s: Dimensions) -> Result<()>;
   /*
   type PreState;
   type PostState;
   fn pre_state(&self) -> Self::PreState;
   fn post_state(&self) -> Self::PostState;
   fn apply_pre(&mut self, pre: Self::PreState>);
   fn apply_post(&mut self, post: Self::PostState>);
   */
}

/// Some data to be used to update entities on the next tick of the game loop
pub struct PostState {
   pub msg_id: u64,
   /// The IDs of the entities we have diffs for
   ids: Vec<(EntityID, bool)>,
   /// The raw data
   data: (Vec<u8>, u64),
   /// The position in bits of the data at which to start reading
   pos: usize,
}

impl PostState {
   /// Parse the post-state data, applying the diffs to entities. Returns the
   /// array of entities processed.
   pub fn process<F: Fn(EntityID) -> Result<WriteGuard<Entity>>>(
      self,
      log: &::Log,
      map_size: Dimensions,
      entity_mut: F) -> Result<Vec<(EntityID, bool)>>
   {
      let mut reader = BitReader::open(log.clone(), &self.data.0);
      reader.seek(self.pos);
      for &(id, apply) in &self.ids {
         entity_mut(id)?.read_post(&mut reader, map_size, apply)?;
      }
      Ok(self.ids)
   }
}

/// Parse and apply the entity data section of a message, returning a `PostState`
/// struct to be processed on the next tick.
pub fn read_diffs<E: Fn(EntityID) -> Result<WriteGuard<Entity>>,
                  M: Fn(EntityID) -> Result<MutexGuard<EntityMeta>>>(
   log: &::Log,
   data: EntityData,
   id_map: &IDMap,
   entity_mut: E,
   entity_meta: M,
   msg_id: u64) -> Result<PostState>
{
   let mut ids = Vec::with_capacity(data.ids.len());
   let pos = {
      let mut reader = BitReader::open(log.clone(), &data.data.0);

      for id in data.ids {
         let id = id_map.read_id(id, false);
         if id == INVALID_ID { bail!("Invalid entity ID") }

         let apply = entity_meta(id)?.should_update(msg_id);
         entity_mut(id)?.read_pre(&mut reader, apply)?;
         ids.push((id, apply));
      }
      reader.pos()
   };

   let data = data.data;
   Ok(PostState { msg_id, ids, pos, data })
}

/// Encode a set of entity diffs into an `EntityData` struct. Returns an error
/// if any of the entities aren't found or the encoding of any entities data fails.
pub fn write_diffs<S: CodingState>(log: &::Log, ids: &[EntityID], world: &S) -> Result<EntityData> {
   let mut writer = BitWriter::new(log.clone());
   let client_ids = (ids.iter().map(|id| Ok(world.write_id(*id)?.0) ).collect(): Result<_>)?;

   for id in ids { world.entity(*id)?.write_pre(&mut writer)?; }
   for id in ids { world.entity(*id)?.write_post(&mut writer, world.map_size())?; }

   let len = writer.pos();
   Ok(EntityData {
      ids: client_ids,
      data: (writer.into_inner(), len as u64),
   })
}

// Delegate `Diffable` to those entities which implement it.
impl Diffable for Entity {
   fn read_pre(&mut self, r: &mut BitReader, apply: bool) -> Result<()> {
      match *self {
         Entity::Plane(ref mut x) => x.read_pre(r, apply),
         _ => Ok(()),
      }
   }

   fn write_pre(&self, w: &mut BitWriter) -> Result<()> {
      match *self {
         Entity::Plane(ref x) => x.write_pre(w),
         _ => Ok(()),
      }
   }

   fn read_post(&mut self, r: &mut BitReader, size: Dimensions, apply: bool) -> Result<()> {
      match *self {
         Entity::Plane(ref mut x) => x.read_post(r, size, apply),
         Entity::Base(ref mut x) => x.read_post(r, size, apply),
         Entity::Turret(ref mut x) => x.read_post(r, size, apply),
         Entity::Projectile(ref mut x) => x.read_post(r, size, apply),
         _ => Ok(())
      }
   }

   fn write_post(&self, w: &mut BitWriter, size: Dimensions) -> Result<()> {
      match *self {
         Entity::Plane(ref x) => x.write_post(w, size),
         Entity::Base(ref x) => x.write_post(w, size),
         Entity::Turret(ref x) => x.write_post(w, size),
         Entity::Projectile(ref x) => x.write_post(w, size),
         _ => Ok(())
      }
   }
}


// ================================================================= //
//                            Map Entities
// ================================================================= //

impl Diffable for Base {
   fn read_pre(&mut self, _: &mut BitReader, _: bool) -> Result<()> { Ok(()) }
   fn write_pre(&self, _: &mut BitWriter) -> Result<()> { Ok(()) }

   fn read_post(&mut self, r: &mut BitReader, _: Dimensions, apply: bool) -> Result<()> {
      let health = IntRule::new(0, 4800).read(r)?;
      if apply { self.health = health; }
      Ok(())
   }

   fn write_post(&self, w: &mut BitWriter, _: Dimensions) -> Result<()> {
      IntRule::new(0, 4800).write(w, &self.health)?;
      Ok(())
   }
}

impl Diffable for Turret {
   fn read_pre(&mut self, _: &mut BitReader, _: bool) -> Result<()> { Ok(()) }
   fn write_pre(&self, _: &mut BitWriter) -> Result<()> { Ok(()) }

   fn read_post(&mut self, r: &mut BitReader, _: Dimensions, apply: bool) -> Result<()> {
      let health = IntRule::new(0, self.max_health as i32).read(r)?;
      if apply { self.health = health; }
      Ok(())
   }

   fn write_post(&self, w: &mut BitWriter, _: Dimensions) -> Result<()> {
      IntRule::new(0, self.max_health as i32).write(w, &self.health)?;
      Ok(())
   }
}

// ================================================================= //
//                              Planes
// ================================================================= //

impl Diffable for Plane {
   fn read_pre(&mut self, r: &mut BitReader, apply: bool) -> Result<()> {
      let control = PlaneControlRule.read(r)?;
      if apply { self.control = control; }
      Ok(())
   }
   fn write_pre(&self, w: &mut BitWriter) -> Result<()> {
      PlaneControlRule.write(w, &self.control)
   }

   fn read_post(&mut self, r: &mut BitReader, s: Dimensions, apply: bool) -> Result<()> {
      fn read_bool(reader: &mut BitReader) -> Result<bool> {
         Ok(BitableRule.read(reader)?)
      }

      if !read_bool(r)? { return Ok(()); }

      let mut diff = self.clone();

      diff.stalled = BitableRule.read(r)?;
      diff.throttle = FloatRule::new(0.0, 1000.0, 0.5).read(r)?;
      diff.body.angle = AngleRule.read(r)?;
      diff.turn = FloatRule::new(-25.0, 25.0, 10.0).read(r)?;
      diff.turn_prev = FloatRule::new(-25.0, 25.0, 10.0).read(r)?;
      diff.body.angular_vel = FloatRule::new(-35.0, 35.0, 7.0).read(r)?;
      if let Some(delta) = OptionRule::new_invert(FloatRule::new(-50.0, 50.0, 5.0)).read(r)? {
         diff.angular_momentum = delta;
         diff.angular_momentum_prev = delta / ANGULAR_MOMENTUM_SCALE;
      } else {
         diff.angular_momentum = 0.0;
         diff.angular_momentum_prev = 0.0;
      }
      diff.set_pos(IntPositionRule::new(s, 50).read(r)?.cast());
      diff.body.velocity = Vec2fRule::new(-25.0, 25.0, 10.0).read(r)?;
      if let Some(momentum) = OptionRule::new_invert(Vec2fRule::new(-25.0, 25.0, 5.0)).read(r)? {
         diff.momentum = momentum;
         diff.momentum_prev = momentum / LINEAR_MOMENTUM_SCALE;
      } else {
         diff.momentum = vec2(0.0, 0.0);
         diff.momentum_prev = vec2(0.0, 0.0);
      };
      diff.energy = FloatRule::new(0.0, 1000.0, 10.0).read(r)?;

      diff.health = FloatRule::new(0.0, 100.0, 0.5).read(r)?;
      diff.ammo = FloatRule::new(0.0, 1350.0, 0.1).read(r)?;
      diff.dead = BitableRule.read(r)?;

      if BitableRule.read(r)? {
         let launch_timer = IntRule::new(0, 250).read(r)?;
         if apply {
            self.timers.paused = launch_timer;
         }

         if BitableRule.read(r)? {
            unimplemented!();
            //b: OptionRule::new_invert(TripleRule(IntRule::new(0, 500),
                                                 //IntRule::new(0, 500),
                                                 //IntRule::new(0, 250))),
         }

         if BitableRule.read(r)? {
            unimplemented!();
            //c: OptionRule::new_invert(IntRule::new(0, 500)),
         }
      }

      diff.charging_shot = if self.kind == PlaneKind::Miranda {
         Some(BitsRule(10).read(r)?)
      } else { None };

      if apply {
         *self = diff;
         self.has_post.set(true);
      }

      Ok(())
   }

   fn write_post(&self, writer: &mut BitWriter, s: Dimensions) -> Result<()> {
      let has_post = self.has_post.get();
      self.has_post.set(false);
      writer.write(has_post);

      if !has_post { return Ok(()); }

      writer.write(self.stalled);
      FloatRule::new(0.0, 1000.0, 0.5).write(writer, &self.throttle)?;
      AngleRule.write(writer, &(self.body.angle.round() as i32))?;
      FloatRule::new(-25.0, 25.0, 10.0).write(writer, &self.turn)?;
      FloatRule::new(-25.0, 25.0, 10.0).write(writer, &self.turn_prev)?;
      FloatRule::new(-35.0, 35.0, 7.0).write(writer, &(self.body.angular_vel.round()))?;

      let write_delta = self.angular_momentum != 0.0 || self.angular_momentum_prev != 0.0;
      writer.write(write_delta);
      if write_delta {
         FloatRule::new(-50.0, 50.0, 5.0).write(writer, &self.angular_momentum)?;
      }

      IntPositionRule::new(s, 50).write(writer, &self.body.position.cast())?;
      Vec2fRule::new(-25.0, 25.0, 10.0).write(writer, &self.body.velocity)?;

      let write_accel = self.momentum != vec2(0.0, 0.0);
      writer.write(write_accel);
      if write_accel {
         Vec2fRule::new(-25.0, 25.0, 5.0).write(writer, &self.momentum)?;
      }

      FloatRule::new(0.0, 1000.0, 10.0).write(writer, &(self.energy))?;

      FloatRule::new(0.0, 100.0, 0.5).write(writer, &self.health)?;
      FloatRule::new(0.0, 1350.0, 0.1).write(writer, &self.ammo)?;
      writer.write(self.dead);

      let write_timers = false;
      writer.write(write_timers);
      if write_timers {
         unimplemented!();
      }

      if self.kind == PlaneKind::Miranda {
         if let Some(charge) = self.charging_shot {
            BitsRule(10).write(writer, &charge)?;
         } else {
            BitsRule(10).write(writer, &0usize)?;
         }
      }

      Ok(())
   }
}


// ================================================================= //
//                            Projectiles
// ================================================================= //

impl Diffable for Projectile {
   fn read_pre(&mut self, _: &mut BitReader, _: bool) -> Result<()> { Ok(()) }
   fn write_pre(&self, _: &mut BitWriter) -> Result<()> { Ok(()) }

   fn read_post(&mut self, r: &mut BitReader, s: Dimensions, apply: bool) -> Result<()> {
      let diff = match self.kind {
         ProjectileKind::Mine => {
            let diff: MineDiff = DiffRule(s).read(r)?;
            if apply {
               self.vel_normal = diff.vel_normal;
               self.target = diff.target;
            }
            diff.base
         }
         ProjectileKind::Missile => {
            let diff: MissileDiff = DiffRule(s).read(r)?;
            if apply {
               self.speed = diff.speed;
               self.target = diff.target;
            }
            diff.base
         }
         _ => DiffRule(s).read(r)?
      };
      if apply {
         self.pos = Some(diff.pos);
         self.angle = diff.angle as f32;
      }
      Ok(())
   }

   fn write_post(&self, w: &mut BitWriter, s: Dimensions) -> Result<()> {
      let base = ProjectileDiff {
         pos: self.pos.unwrap_or_else(|| [0, 0].into()),
         angle: self.angle as i32,
      };
      match self.kind {
         ProjectileKind::Mine =>
            DiffRule(s).write(w, &MineDiff {
               base,
               vel_normal: self.vel_normal,
               target: self.target,
            }),
         ProjectileKind::Missile =>
            DiffRule(s).write(w, &MissileDiff {
               base,
               speed: self.speed,
               target: self.target
            }),
         _ => DiffRule(s).write(w, &base)
      }
   }
}

#[derive(PartialEq, Debug, Clone)]
pub struct ProjectileDiff {
   pos: Point2<i32>,
   angle: i32,
}

#[derive(PartialEq, Debug, Clone)]
pub struct MissileDiff {
   base: ProjectileDiff,
   speed: f32,
   target: Option<(Point2<i32>, Vector2<f32>)>,
}

#[derive(PartialEq, Debug, Clone)]
pub struct MineDiff {
   base: ProjectileDiff,
   vel_normal: Vector2<f32>,
   target: Option<(Point2<i32>, Vector2<f32>)>,
}

#[derive(Clone)]
pub struct DiffRule(pub Dimensions);

encode_pass!{ ProjectileDiff, DiffRule, Error;
   pos: |x: &DiffRule| IntPositionRule::new(x.0, 50),
   angle: |_| AngleRule,
}

encode_pass!{ MissileDiff, DiffRule, Error;
   base: Clone::clone,
   speed: |_| FloatRule::new(-15.0, 30.0, 10.0),
   target: |x: &DiffRule| OptionRule::new_invert(PairRule(
      IntPositionRule::new(x.0, 50),
      Vec2fRule::new(-25.0, 25.0, 10.0))),
}

encode_pass!{ MineDiff, DiffRule, Error;
   base: Clone::clone,
   vel_normal: |_| MineVelRule,
   target: |x: &DiffRule| OptionRule::new_invert(PairRule(
      IntPositionRule::new(x.0, 50),
      Vec2fRule::new(-25.0, 25.0, 10.0))),
}

pub struct MineVelRule;

impl MineVelRule {
   pub fn rule(small: bool) -> Vec2fRule {
      if small { Vec2fRule::new(-6.0, 6.0, 5.0) }
      else { Vec2fRule::new(-20.0, 20.0, 6.0) }
   }
}

impl Encoding<Vector2<f32>> for MineVelRule {
   type Error = Error;
   fn read(&self, reader: &mut BitReader) -> Result<Vector2<f32>> {
      let small: bool = BitableRule.read(reader)?;
      MineVelRule::rule(small).read(reader)
   }
   fn write(&self, writer: &mut BitWriter, value: &Vector2<f32>) -> Result<()> {
      let small = value.x > -6.0 && value.x < 6.0 && value.y > -6.0 && value.y < 6.0;
      BitableRule.write(writer, &small)?;
      MineVelRule::rule(small).write(writer, value)
   }
}
