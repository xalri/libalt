//! Server discovery over LAN

use std::net::SocketAddr;
use std::thread;
use std::sync::{Arc, Mutex};
use futures::Stream;
use tokio_core::reactor;
use net::socket::*;
use rules::MessageRule;
use message::*;
use Context;

/// Responds to discovery messages; implemented for `proto::Sender`.
pub trait Responder: Clone + Send + 'static {
   /// Respond to a LAN discovery message.
   fn lan_respond(&self, addr: SocketAddr);
}

impl Responder for ::Sender {
   fn lan_respond(&self, addr: SocketAddr) {
      self.send(Message::new(addr, Data::Lan(Lan::Response(None))))
   }
}

/// Spawn a thread to listen for multicast packets on port 27260 and ping the
/// given list of servers to respond with their own addresses.
/// Panics if binding to 0.0.0.0:27260 fails.
pub fn lan_listen<R: Responder>(log: ::slog::Logger, servers: &[R]) {
   lan_listen_dynamic(log, Arc::new(Mutex::new(servers.to_vec())))
}

/// Run the discovery listener with an Arc<Mutex<_>> wrapped server list, so
/// new servers can be added dynamically. Panics if binding to 0.0.0.0:27260 fails.
pub fn lan_listen_dynamic<R: Responder>(log: ::slog::Logger, servers: Arc<Mutex<Vec<R>>>) {
   thread::spawn(move || {
      let mut reactor = reactor::Core::new().unwrap();

      let socket = Socket::bind_multicast(
         ([0,0,0,0], 27260).into(),
         reactor.handle(),
         MessageRule(Context::Server),
         log.clone()).unwrap();

      let done = socket.for_each(move |msg| {
         let msg = Message(msg);
         if let Data::Lan(Lan::Request) = *msg.data() {
            info!(log, "Responding to LAN sever request"; "requester" => %msg.addr());

            // Lock the server list and iterate through
            for x in servers.lock().unwrap().iter() {
               // Send a message telling the server to respond to the ping.
               x.lan_respond(msg.addr());
            }
         }
         Ok(())
      });

      reactor.run(done).unwrap();
   });
}
