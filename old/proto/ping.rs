//! Sending and tracking pings

use std::time::*;
use std::collections::HashMap;
use std::net::SocketAddr;

use message::Ping as PingData;
use message::{Message, Data};
use util::time::*;
use util::buffer::*;
use Sender;

/// Ping responses are stored in a circular buffer -- only the newest responses
/// are kept. BUFFER_LEN defines the size of this buffer.
const BUFFER_LEN: usize = 20;

/// The time in milliseconds between pings to each client/server.
const PING_TIME_MS: u64 = 500;

/// The time after which to automatically stop pinging an unresponsive client/server.
const TIMEOUT_MS: u64 = 10_000;

/// The time before we consider a ping lost.
const LOST_MS: u64 = 800;

/// A container of pings for multiple clients and/or servers.
pub struct Pings {
   /// Maps socket addresses to the individual ping trackers.
   clients: HashMap<SocketAddr, PingClient>,
   /// A channel on which to send messages to clients.
   sender: Sender,
   ping_counter: usize,
   ping_idle: usize,
   last_update: Instant,
}

impl Pings {
   /// Create a new `Pings`
   pub fn new(send: Sender) -> Pings {
      Pings {
         clients: HashMap::new(),
         sender: send,
         ping_counter: 0,
         ping_idle: 0,
         last_update: Instant::now(),
      }
   }

   /// Get the `PingClient` for the given address, creating it if one doesn't
   /// exist.
   pub fn get(&mut self, addr: SocketAddr) -> &mut PingClient {
      self.clients.entry(addr).or_insert_with(|| PingClient::new(false))
   }

   /// Handle an incoming message.
   pub fn process(&mut self, msg: &Message) {
      match *msg.data() {
         Data::Ping(ref ping) | Data::ServerPing(ref ping) => {
            if ping.respond {
               let data = PingData {
                  server_ping: ping.server_ping,
                  id: ping.id,
                  respond: false,
               };

               let data = if ping.server_ping { Data::ServerPing(data) }
                  else { Data::Ping(data) };

               let msg = Message::new_ping(msg.addr(), data);
               self.sender.send(msg);
            } else {
               self.get(msg.addr()).receive(*ping)
            }
         }
         _ => (),
      }
   }

   /// Update clients. Pings are sent to each client at an interval of `PING_TIME_MS`, pings to
   /// multiple clients are distributed uniformly over time.
   pub fn update(&mut self) {
      let diff = to_millis(self.last_update.elapsed());
      if diff == 0 { return; }

      self.last_update = Instant::now();

      let updates_per_ping = (PING_TIME_MS / diff) as f64;
      let target_pings: f64 = self.clients.len() as f64 / updates_per_ping;
      if self.ping_counter >= self.clients.len() {
         self.ping_counter = 0;
         if target_pings < 1.0 {
            self.ping_idle = ((1.0 - target_pings) * updates_per_ping) as usize;
         }
      }
      let mut target_pings = target_pings as usize;

      if self.ping_idle > 0 {
         self.ping_idle -= 1;
      } else {
         target_pings += 1;
      }

      let mut to_remove = Vec::new();
      let mut pings = 0;

      for (i, (&addr, client)) in self.clients.iter_mut().enumerate() {
         if client.last_response.elapsed() > Duration::from_millis(TIMEOUT_MS) {
            to_remove.push(addr);
            continue;
         }

         if pings < target_pings && i >= self.ping_counter {
            pings += 1;
            self.ping_counter += 1;
            let data = client.send();
            let msg = Message::new_ping(addr, data);
            self.sender.send(msg);
         }
         client.update_lost();
      }

      for addr in to_remove {
         self.remove(addr);
      }
   }

   /// Remove a client
   pub fn remove(&mut self, addr: SocketAddr) {
      self.clients.remove(&addr);
   }
}

/// Tracks ping responses for a single client or server
pub struct PingClient {
   /// The time a ping was last sent
   last_sent: Instant,
   /// The time a ping was last received
   last_response: Instant,
   /// Pings for which we're currently waiting for a reply
   current: HashMap<u32, Instant>,
   /// The ID of the last ping sent.
   id: u32,
   /// A ring buffer of ping responses. An entry Some(x) is added for every
   /// response received, and None for every lost packet.
   past: Buffer<Option<Duration>>,
   /// True if the endpoint is a server.
   is_server: bool,
}

impl PingClient {
   /// Create a new ping client
   pub fn new(is_server: bool) -> PingClient {
      PingClient {
         last_sent: Instant::now(),
         last_response: Instant::now(),
         current: HashMap::with_capacity(64),
         id: 0,
         past: Buffer::with_capacity(BUFFER_LEN),
         is_server
      }
   }

   /// Calculates a weighted average ping time, where recent pings are worth
   /// more than older pings.
   pub fn ping(&self) -> Option<Duration> {
      let mut nanos = self.past.iter()
         .filter(|x| x.is_some())
         .map(|x| x.unwrap().subsec_nanos() as f64);

      if let Some(first) = nanos.next() {
         let avr = nanos.fold(first, |acc, x| (acc / 2.0) + (x / 2.0));
         Some(Duration::new(0, avr as u32))
      } else {
         None
      }
   }

   /// Calculates a weighted average ping time in milliseconds
   pub fn ping_ms(&self) -> Option<f32> {
      self.ping().map(to_millis_exact)
   }

   /// Calculates the packet loss
   pub fn packet_loss(&self) -> f64 {
      if self.past.is_empty() {
         return 0.;
      }
      self.past.iter().filter(|x| x.is_none()).count() as f64 / self.past.len() as f64
   }

   /// Get the most recent time we sent a ping
   pub fn last_sent(&self) -> Instant {
      self.last_sent
   }

   /// Ping this client. Returns the message data to send.
   fn send(&mut self) -> Data {
      let now = Instant::now();
      self.last_sent = now;
      self.id += 1;
      self.current.insert(self.id, now);
      if self.is_server {
         Data::ServerPing(PingData {
            server_ping: true,
            id: self.id,
            respond: true,
         })
      } else {
         Data::Ping(PingData {
            server_ping: false,
            id: self.id,
            respond: true,
         })
      }
   }

   /// Process an incoming ping
   fn receive(&mut self, ping: PingData) {
      if let Some(x) = self.current.remove(&ping.id) {
         self.last_response = Instant::now();
         self.past.add(Some(x.elapsed()));
      }
   }

   /// Check for lost pings.
   fn update_lost(&mut self) {
      // Separate the current pings into two groups, the first to keep waiting for,
      // the second to discard as lost.
      let (keep, lost) = self.current
         .drain()
         .partition(|&(_, sent)| sent.elapsed() < Duration::from_millis(LOST_MS));
      self.current = keep;

      // For each of the lost pings, add `None` to the buffer
      for _ in lost {
         self.past.add(None)
      }
   }
}

