#![no_main]
#[macro_use] extern crate libfuzzer_sys;
extern crate libalt;

use libalt::util::logger;
use libalt::proto::Context;
use libalt::proto::message::*;
use libalt::proto::rules::*;
use libalt::proto::encode::*;

fuzz_target!(|data: &[u8]| {
   let read = MessageRule(Context::Server);
   let write = MessageRule(Context::Client);

   if let Ok((_, valid)) = decode_cycle_check::<_, Message>(&logger::none(), &read, &write, data) {
      assert!(valid);
   }
});
