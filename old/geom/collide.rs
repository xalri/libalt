//! 2D Collision detection & response.

use {Range, point2};
use math::*;
use body::*;
use transform::*;
use poly::*;
use cgmath::*;
use std::f32;

pub const SUPPORT_EDGE_THRESHOLD: f32 = 0.1;

/// Collision data.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Contact {
   pub intersection: Intersection,
   pub flush: bool,
   pub contact_a: Point2<f32>,
   pub contact_b: Point2<f32>,
}

/// An update to a body which solves a collision
pub struct Resolution {
   pub position: Point2<f32>,
   pub velocity: Vector2<f32>,
   pub angular_vel: f32,
}

/// An intersection between two bodies
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Intersection {
   normal: Vector2<f32>,
   /// The time to collision. A negative value represents the distance
   /// of an existing intersection.
   t: f32,
}

/// A supporting point or edge
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Support {
   min: Point2<f32>,
   max: Point2<f32>,
   min_dist: f32,
   max_dist: f32,
   is_point: bool,
}

impl Resolution {
   pub fn apply<'a>(&self, body: &'a mut Body) {
      body.position = self.position;
      body.velocity = self.velocity;
      body.angular_vel = self.angular_vel;
   }
}

impl Contact {
   /// Calculate the contact points for a collision between two bodies.
   /// Returns `None` if the bodies will not collide in the next time step.
   pub fn new(body_a: &Body, body_b: &Body) -> Option<Contact> {
      #[derive(Copy, Clone, Debug)]
      struct Data {
         intersection: Intersection,
         convex_a: ConvexBody,
         convex_b: ConvexBody,
      }
      let mut collision: Option<Data> = None;

      // Find the earliest collision between the two bodies.
      for &a in &body_a.convex {
         if !a.bounds.intersects(body_b.bounds) { continue }
         for &b in &body_b.convex {
            if !a.bounds.intersects(b.bounds) { continue }

            if let Some(c) = Intersection::new(&a, &b) {
               if collision.is_none() || c.t < collision.unwrap().intersection.t {
                  collision = Some(Data{
                     intersection: c,
                     convex_a: a,
                     convex_b: b
                  });
               }
            }
         }
      }

      if collision.is_none() { return None; }
      let collision = collision.unwrap();

      // Calculate support points
      let support_a = Support::new(collision.convex_a, collision.intersection);
      let support_b = Support::new(collision.convex_b, collision.intersection.invert_normal()).inv();

      // Calculate contact points from supports.
      if support_a.is_point && support_b.is_point {
         Some(Contact{
            intersection: collision.intersection,
            flush: false,
            contact_a: support_a.min,
            contact_b: support_b.min,
         })
      } else {
         let range_a = support_a.range();
         let range_b = support_b.range();

         if !range_a.intersects(range_b) { return None; }

         let mut ca;
         let mut cb;

         if range_a.min > range_b.min {
            ca = support_a.min;
            cb = project_on_segment(support_a.min, (support_b.min, support_b.max));
         } else {
            ca = project_on_segment(support_b.min, (support_a.min, support_a.max));
            cb = support_b.min;
         }

         if range_a.max < range_b.max {
            ca += support_a.max.to_vec();
            cb += project_on_segment(support_a.max, (support_b.min, support_b.max)).to_vec();
         } else {
            ca += project_on_segment(support_b.max, (support_a.min, support_a.max)).to_vec();
            cb += support_b.max.to_vec();
         }
         ca /= 2.0;
         cb /= 2.0;

         Some(Contact{
            intersection: collision.intersection,
            flush: !support_a.is_point && !support_b.is_point,
            contact_a: ca,
            contact_b: cb,
         })
      }
   }

   /// Get the updated linear momentum, angular momentum, and position of the bodies to solve the
   /// collision.
   pub fn resolve<'a, 'b>(self, body_a: &'a Body, body_b: &'b Body) -> (Resolution, Resolution) {
      let mut time = self.intersection.t;
      let normal = self.intersection.normal;

      let material_a = &body_a.material;
      let material_b = &body_b.material;

      let mut elasticity = clamp(material_b.elasticity, 0.0, 1.0);
      let static_friction = clamp((material_a.static_friction + material_b.static_friction) / 2.0, 0.0, 1.0);
      let kinetic_friction = clamp((material_a.kinetic_friction + material_b.kinetic_friction) / 2.0, 0.0, 1.0);

      let (mut pa, mut pb) = (body_a.position, body_b.position);
      let (ma, mb) = (body_a.inv_mass, body_b.inv_mass);
      let (ia, ib) = (body_a.inv_inertia, body_b.inv_inertia);
      let (va, vb) = (body_a.velocity, body_b.velocity);
      let (wa, wb) = (body_a.angular_vel, body_b.angular_vel);

      // If there's an existing overlap adjust the positions to fix it.
      if time < 0.0 {
         let d = self.contact_b.to_vec() - self.contact_a.to_vec();
         let separation = f32::max(material_a.separation, material_b.separation);
         let multiplier = separation / (ma + mb);

         if ma > 0.0 { pa += d * ma * multiplier }
         if mb > 0.0 { pb += -d * mb * multiplier }

         time = 0.0;
      }


      // Contact point at time of impact, relative to the center of mass
      let ra = self.contact_a - (pa + (va * time));
      let rb = self.contact_b - (pb + (vb * time));

      // Velocity at contact point
      let vpa = va + (vec2(-ra.y, ra.x) * wa.to_radians());
      let vpb = vb + (vec2(-rb.y, rb.x) * wb.to_radians());

      // Impact velocity
      let vc = vpa - vpb;
      let v = vc.dot(normal);
      if v > 0.0 {
         // Bodies are already separating along the collision normal, no need to resolve.
         return ((pa, va, wa).into(), (pb, vb, wb).into());
      }

      // Remove elasticity when the velocity along the normal is small, or the
      // surfaces will never meet and there will be no kinetic friction.
      if -v < 3.0 { elasticity = 0.0 }

      // Transverse velocity
      let mut vt = vc - (normal * v);
      if vt.magnitude2() > 0.0 { vt = vt.normalize() }

      // Inertia at the contact point
      let _ia = ia * ra.perp_dot(normal).powi(2);
      let _ib = ib * rb.perp_dot(normal).powi(2);

      // Impulse
      let denom = ma + mb + _ia + _ib;
      let jn = normal * -(1.0 + elasticity) * v / denom; // impulse along the normal
      let jt = vt * kinetic_friction * v / denom; // transverse impulse
      let j = jn + jt; // combined

      let solve_angular = |r: Vector2<f32>, i: f32, w: f32| {
         if i == 0.0 { return w; }
         let mut w_ = w + (r.perp_dot(j) * i).to_degrees();
         if w_ * w < 0.0 {
            // Apply damping when the direction changes
            w_ *= 0.9;
            if self.flush && w_.abs() < 2.0 { w_ = 0.0; }
         }
         w_
      };

      let solve_linear = |mut v: Vector2<f32>, dv: Vector2<f32>, m: f32| {
         if m == 0.0 { return v; }
         v = v + jn * m;
         let vt = vt * dv.dot(vt);
         let friction = jt * m;
         if friction.dot(vt) < 0.0 {
            let friction_magnitude = friction.magnitude() * f32::max(1.0, static_friction / kinetic_friction);
            if friction_magnitude > vt.magnitude() {
                // Friction exceeds transverse momentum, remove transverse velocity.
                v -= vt;
            } else {
                v += friction;
            }
         }
         v
      };

      ((pa, solve_linear(va, va - vb, ma), solve_angular(ra, ia, wa)).into(),
       (pb, solve_linear(vb, vb - va, -mb), solve_angular(-rb, ib, wb)).into())
   }
}

impl Support {
   /// Find the world-space support points for a collided body
   fn new(body: ConvexBody, intersection: Intersection) -> Support {
      let mut support = Support {
         min: point2(0.0, 0.0),
         max: point2(0.0, 0.0),
         min_dist: f32::MAX,
         max_dist: f32::MIN,
         is_point: true,
      };

      let normal = body.orientation.transpose().apply_vec(intersection.normal);
      let tangent = vec2(-intersection.normal.y, intersection.normal.x);
      let pos = body.pos + body.vel * f32::max(0.0, intersection.t);

      let mut closest = f32::MAX;
      for p in unsafe{ &*body.hull }.points() {
         let dist = (p * body.scale).dot(normal);
         if dist < closest { closest = dist; }
      }

      let mut candidates = 0;

      for p in unsafe{ &*body.hull }.points() {
         let p = p.to_vec() * body.scale;
         if p.dot(normal) < closest + SUPPORT_EDGE_THRESHOLD {
            candidates += 1;

            let world_space = pos + body.orientation.apply_vec(p);
            let perp_dist = world_space.dot(tangent);

            if perp_dist < support.min_dist {
               support.min = world_space;
               support.min_dist = perp_dist;
            }
            if perp_dist > support.max_dist {
               support.max = world_space;
               support.max_dist = perp_dist;
            }
         }
      }

      support.is_point = candidates == 1;
      support
   }

   /// Get the contact's range along the perpendicular vector to the collision normal.
   fn range(&self) -> Range {
      Range{ min: self.min_dist, max: self.max_dist }
   }

   fn inv(self) -> Support {
      Support{ min: self.max, max: self.min, min_dist: -self.max_dist, max_dist: -self.min_dist,
         is_point: self.is_point }
   }
}

impl Intersection {
   /// Check for an intersection between two convex bodies.
   pub fn new(body_a: &ConvexBody, body_b: &ConvexBody) -> Option<Intersection> {
      let hull_a = unsafe{ &*body_a.hull };
      let hull_b = unsafe{ &*body_b.hull };
      let oa = body_a.orientation;
      let ob = body_b.orientation;
      let ob_ = ob.transpose();

      // parameters of body_a in the reference frame of body_b
      let orientation = oa * ob_;
      let offset = ob_.apply_vec(body_a.pos - body_b.pos);
      let velocity = ob_.apply_vec(body_a.vel - body_b.vel);

      let mut collision = Intersection{ normal: vec2(0.0, 0.0), t: -f32::MIN_POSITIVE };
      let mut overlap = Intersection{ normal: vec2(0.0, 0.0), t: -f32::MAX };

      // Test for collision against a given axis
      let mut test = |normal: Vector2<f32>| -> bool {
         let i = Intersection::axis_intersect(
            normal, hull_a, hull_b, orientation, offset, velocity, 1.0, body_a.scale, body_b.scale);
         if i.is_none() { return false }
         let i = i.unwrap();
         if i.t > collision.t { collision = i }
         else if i.t > overlap.t { overlap = i }
         true
      };

      // Check possible axes, returns `None` if the bodies are separated
      // along any axis.
      for normal in hull_a.surface_normals() {
         if !test(orientation.apply_vec(normal)) { return None; }
      }
      for normal in hull_b.surface_normals() {
         if !test(normal) { return None; }
      }

      // Bodies intersect on all axes -- take the minimum translation
      // and fix the reference frame.
      let mut min = if collision.t > 0.0 { collision } else { overlap };
      min.normal = ob.apply_vec(min.normal.normalize());
      let center_a = oa.apply(hull_a.centroid()) + body_a.pos.to_vec();
      let center_b = ob.apply(hull_b.centroid()) + body_b.pos.to_vec();
      if min.normal.dot(center_a - center_b) < 0.0 {
         min.normal = vec2(-min.normal.x, -min.normal.y);
      }

      Some(min)
   }

   /// Calculate the intersection of two polygons along an axis, parameters are
   /// in the reference frame of hull_b.
   pub fn axis_intersect(
      axis: Vector2<f32>,
      hull_a: &Poly,
      hull_b: &Poly,
      orientation: Matrix3<f32>,
      offset: Vector2<f32>,
      velocity: Vector2<f32>,
      max_time: f32,
      scale_a: f32,
      scale_b: f32) -> Option<Intersection>
   {
      let mut projection_a = hull_a.project(orientation.transpose().apply_vec(axis)) * scale_a;
      let projection_b = hull_b.project(axis) * scale_b;

      let h = offset.dot(axis);
      projection_a.min += h;
      projection_a.max += h;

      let d0 = projection_a.min - projection_b.max;
      let d1 = projection_b.min - projection_a.max;

      if d0 >= 0.0 || d1 >= 0.0 {
         let v = velocity.dot(axis);
         if let Some(time) = smallest_positive(-d0/v, d1/v) {
            if time < max_time { return Some(Intersection{ t: time, normal: axis }) }
         }
         None
      } else {
         let len = axis.magnitude();
         Some(Intersection{ t: f32::max(d0, d1) / len, normal: axis / len })
      }
   }

   pub fn invert_normal(&self) -> Intersection {
      Intersection {
         t: self.t,
         normal: -self.normal,
      }
   }
}

impl From<(Point2<f32>, Vector2<f32>, f32)> for Resolution {
   fn from((position, velocity, angular_vel): (Point2<f32>, Vector2<f32>, f32))
           -> Resolution {
      Resolution{ position, velocity, angular_vel }
   }
}

fn smallest_positive(a: f32, b: f32) -> Option<f32> {
   if a >= 0.0 && b >= 0.0 { Some(f32::min(a, b)) }
   else if a >= 0.0 { Some(a) }
   else if b >= 0.0 { Some(b) }
   else { None }
}

#[cfg(test)] use Bounds;

#[test] fn test_collision_data() {
   let poly = Poly::new(&[
      Point2{ x: 1.0, y: 1.0 },
      Point2{ x: 1.0, y: -1.0 },
      Point2{ x: -1.0, y: -1.0 },
      Point2{ x: -1.0, y: 1.0 }
   ]);

   let mut body_a = Body::dynamic(poly.clone());
   let mut body_b = Body::dynamic(poly.clone());

   body_b.position = Point2{ x: 1.0, y: 0.0 };

   body_a.update_collision();
   body_b.update_collision();

   assert_eq!(
      Some(Contact{
         intersection: Intersection{ normal: vec2(-1.0, 0.0), t: -1.0 },
         flush: true,
         contact_a: point2(1.0, 0.0),
         contact_b: point2(0.0, 0.0),
      }),
      Contact::new(&body_a, &body_b));

   let body_a = Body::dynamic(Poly::new(&[
      point2(0.0, 0.0),
      point2(2.0, -2.0),
      point2(-2.0, -2.0),
   ]));

   let mut body_b = Body::dynamic(Poly::new(&[
      point2(2.0, 2.0),
      point2(2.0, -2.0),
      point2(-2.0, -2.0),
      point2(-2.0, 2.0)
   ]));
   body_b.position = point2(5.0, 1.0);
   body_b.angle = 45.0;

   assert_eq!(None, Contact::new(&body_a, &body_b));
}

#[test] fn test_intersection() {
   let poly = Poly::square();
   let body_a = ConvexBody {
      hull: &poly,
      pos: point2(10.0, 0.0),
      vel: vec2(0.0, 0.0),
      scale: 1.0,
      orientation: rotate(0.0),
      bounds: Bounds{ x: Range::new(0.0, 0.0), y: Range::new(0.0, 0.0) },
   };

   let body_b = ConvexBody {
      hull: &poly,
      pos: point2(0.0, 0.0),
      vel: vec2(0.0, 0.0),
      scale: 1.0,
      orientation: rotate(0.0),
      bounds: Bounds{ x: Range::new(0.0, 0.0), y: Range::new(0.0, 0.0) },
   };

   let intersection = Intersection::new(&body_a, &body_b);
   assert_eq!(Some(Intersection{ normal: vec2(1.0, 0.0), t: -10.0 }), intersection);
}

#[test] fn test_axis_intersect() {
   let poly_a = Poly::new(&[
      point2(0.0, 0.0),
      point2(2.0, -2.0),
      point2(-2.0, -2.0),
   ]);

   let poly_b = Poly::new(&[
      point2(2.0, 2.0),
      point2(2.0, -2.0),
      point2(-2.0, -2.0),
      point2(-2.0, 2.0)
   ]);

   assert_eq!(None, Intersection::axis_intersect(
      vec2(1.0, 0.0),
      &poly_a,
      &poly_b,
      rotate(-45.0),
      vec2(-4.2426405, 2.828427),
      vec2(0.0, 0.0),
      1.0,
      1.0, 1.0));
}