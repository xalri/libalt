use cgmath::*;
use transform;
use {Bounds, Range};
use poly::*;

/// A rigid body.
#[derive(Debug, Clone)]
pub struct Body {
   pub hull: Poly,
   pub position: Point2<f32>,
   pub velocity: Vector2<f32>,
   pub angle: f32,
   pub scale: f32,
   pub angular_vel: f32,
   pub collision_id: u32,
   pub collision_mask: u32,
   pub material: Material,
   /// Inverse of the mass, set to 0 to prevent movement.
   pub inv_mass: f32,
   /// Inverse of the rotational inertia, set to 0 to prevent rotation.
   pub inv_inertia: f32,
   /// Collision bounds, must be explicitly updated
   pub bounds: Bounds,
   /// Convex components, must be explicitly updated
   pub convex: Vec<ConvexBody>,
}

impl PartialEq<Body> for Body {
   fn eq(&self, other: &Body) -> bool {
      self.hull == other.hull &&
         self.hull == other.hull &&
         self.position == other.position &&
         self.velocity == other.velocity &&
         self.angle == other.angle &&
         self.scale == other.scale &&
         self.angular_vel == other.angular_vel &&
         self.collision_id == other.collision_id &&
         self.collision_mask == other.collision_mask &&
         self.material == other.material
   }
}

#[derive(PartialEq, Debug, Clone, Derivative)]
#[derivative(Default)]
pub struct Material {
   #[derivative(Default(value="1.0"))]
   pub density: f32,
   #[derivative(Default(value="0.1"))]
   pub static_friction: f32,
   #[derivative(Default(value="0.35"))]
   pub kinetic_friction: f32,
   #[derivative(Default(value="0.125"))]
   pub elasticity: f32,
   #[derivative(Default(value="0.35"))]
   pub separation: f32,
}

#[derive(Debug, Clone, Copy)]
pub struct ConvexBody {
   pub hull: *const Poly,
   pub pos: Point2<f32>,
   pub vel: Vector2<f32>,
   pub orientation: Matrix3<f32>,
   pub scale: f32,
   pub bounds: Bounds,
}

impl Body {
   /// Create a new dynamic body
   pub fn dynamic(hull: Poly) -> Body {
      let mass = hull.area();
      let inertia = mass * hull.inertia();
      Body {
         inv_mass: 1.0 / mass,
         inv_inertia: 1.0 / inertia,
         .. Body::immovable(hull)
      }
   }

   /// Create a new immovable body
   pub fn immovable(hull: Poly) -> Body {
      Body {
         hull,
         position: Point2{ x: 0.0, y: 0.0 },
         velocity: vec2(0.0, 0.0),
         angle: 0.0,
         scale: 1.0,
         angular_vel: 0.0,
         collision_id: 0,
         collision_mask: 0,
         material: Default::default(),
         inv_mass: 0.0,
         inv_inertia: 0.0,
         bounds: Bounds{ x: Range::new(0.0, 0.0), y: Range::new(0.0, 0.0) },
         convex: Vec::new(),
      }
   }

   /// Set the hull & update properties.
   pub fn change_hull(&mut self, hull: Poly) {
      self.hull = hull;
      self.update_properties();
   }

   /// Recalculate the body's mass & inertia.
   pub fn update_properties(&mut self) {
      let mass = self.material.density * self.hull.area();
      let inertia = mass * self.hull.inertia();
      if self.inv_mass != 0.0 { self.inv_mass = 1.0 / mass; }
      if self.inv_inertia != 0.0 { self.inv_inertia = 1.0 / inertia; }
   }

   /// Increment the position & angle by the velocity & angular velocity respectively.
   pub fn tick(&mut self) {
      self.position += self.velocity;
      self.angle += self.angular_vel;
   }

   /// Update the collision bounds & convex components
   pub fn update_collision(&mut self) {
      self.bounds = self.hull.swept_bounds(self.position, self.angle, self.scale, self.velocity);
      self.convex.clear();
      let m = transform::rotate(self.angle);
      for p in self.hull.convex() {
         self.convex.push(ConvexBody {
            hull: p as *const Poly,
            pos: self.position,
            vel: self.velocity,
            orientation: m,
            scale: self.scale,
            bounds: if self.hull.convex().len() == 1 { self.bounds }
                    else { p.swept_bounds(self.position, self.angle, self.scale, self.velocity) },
         })
      }
   }

   /// Get the bodies world-space transformation matrix
   pub fn matrix(&self) -> Matrix3<f32> {
      transform::rotate(self.angle) * transform::translate(self.position.x, self.position.y)
   }
}

