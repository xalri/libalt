// A port of polypartition's convex partitioning algorithm
// https://github.com/ivanfratric/polypartition/blob/master/src/

use cgmath::*;
use poly::*;
use math::*;
use std::collections::VecDeque;

#[derive(Default, Clone, Debug)]
struct DPState {
   visible: bool,
   weight: usize,
   pairs: VecDeque<(isize, isize)>,
}

/// Find the minimum convex decomposition of the poly, returns indices.
pub fn convex_partition(poly: &Poly) -> Vec<Vec<isize>> {
   let n = poly.len();

   let mut is_convex: Vec<_> = (0..n)
      .map(|i| !is_reflex(poly[i - 1], poly[i], poly[i + 1]))
      .collect();

   let mut state = vec![vec![DPState::default(); n as usize]; n as usize];

   // init weights and visibility
   for i in 0..n {
      for j in (i + 1)..n {
         state[i as usize][j as usize].visible = true;
         state[i as usize][j as usize].weight = if j == i + 1 { 0 } else { 2147483647 };

         if j == i + 1 { continue; }

         if !locally_inside(poly[i - 1], poly[i], poly[i + 1], poly[j]) {
            state[i as usize][j as usize].visible = false;
            continue;
         }
         if !locally_inside(poly[j - 1], poly[j], poly[j + 1], poly[i]) {
            state[i as usize][j as usize].visible = false;
            continue;
         }

         let dist_sq = (poly[i] - poly[j]).magnitude2();
         for k in 0..n {
            if k == i || wrap_max(k + 1, n) == i { continue; }

            if let Some(p) = intersection_point((poly[i], poly[j]), (poly[k], poly[k + 1])) {
               if (p - poly[i]).magnitude2() < dist_sq - 0.00001 {
                  state[i as usize][j as usize].visible = false;
                  break;
               }
            }
         }
      }
   }

   for i in 0..(n - 2) {
      let j = i + 2;
      if state[i as usize][j as usize].visible {
         state[i as usize][j as usize].weight = 0;
         state[i as usize][j as usize].pairs.push_back((i + 1, i + 1));
      }
   }

   state[0][(n - 1) as usize].visible = true;
   is_convex[0] = false;

   for gap in 3..n {
      for i in 0..(n - gap) {
         if is_convex[i as usize] { continue }
         let k = i + gap;
         if state[i as usize][k as usize].visible {
            if !is_convex[k as usize] {
               for j in (i + 1)..k { type_a(i, j, k, poly, &mut state); }
            } else {
               for j in (i + 1)..(k - 1) {
                  if is_convex[j as usize] { continue }
                  type_a(i, j, k, poly, &mut state);
               }
               type_a(i, k - 1, k, poly, &mut state);
            }
         }
      }
      for k in gap..n {
         if is_convex[k as usize] { continue; }
         let i = k - gap;
         if is_convex[i as usize] && state[i as usize][k as usize].visible {
            type_b(i, i + 1, k, poly, &mut state);
            for j in (i + 2)..k {
               if is_convex[j as usize] { continue; }
               type_b(i, j, k, poly, &mut state);
            }
         }
      }
   }

   let mut diagonals = VecDeque::new();
   diagonals.push_front((0, n - 1));

   while let Some(diagonal) = diagonals.pop_front() {
      if diagonal.1 - diagonal.0 <= 1 { continue; }
      let pairs = &state[diagonal.0 as usize][diagonal.1 as usize].pairs;
      if pairs.is_empty() { panic!("Polygon decomposition failed -- self-intersection?"); }

      if !is_convex[diagonal.0 as usize] {
         let last = *pairs.back().unwrap();
         let j = last.1;
         diagonals.push_front((j, diagonal.1));

         if j - diagonal.0 > 1 {
            if last.0 != last.1 {
               let pairs2 = &mut state[diagonal.0 as usize][j as usize].pairs;
               loop {
                  if pairs2.is_empty() { panic!("Polygon decomposition failed -- self-intersection?"); }
                  let last2 = *pairs2.back().unwrap();
                  if last.0 != last2.0 { pairs2.pop_back(); } else { break; }
               }
            }
            diagonals.push_front((diagonal.0, j));
         }
      } else {
         let first = pairs[0];
         let j = first.0;
         diagonals.push_front((diagonal.0, j));

         if diagonal.1 - j > 1 {
            if first.0 != first.1 {
               let pairs2 = &mut state[j as usize][diagonal.1 as usize].pairs;
               loop {
                  if pairs2.is_empty() { panic!() }
                  let first2 = pairs2[0];
                  if first.1 != first2.1 { pairs2.pop_front(); } else { break; }
               }
            }
            diagonals.push_front((j, diagonal.1));
         }
      }
   }

   let mut parts = Vec::new();
   let mut diagonals2 = VecDeque::new();

   diagonals.push_front((0, n - 1));

   while let Some(diagonal) = diagonals.pop_front() {
      if diagonal.1 - diagonal.0 <= 1 { continue; }

      let mut indices = Vec::new();
      diagonals2.clear();

      indices.push(diagonal.0);
      indices.push(diagonal.1);
      diagonals2.push_front(diagonal);

      while let Some(diagonal) = diagonals2.pop_front() {
         if diagonal.1 - diagonal.0 <= 1 { continue; }
         let mut ijreal = true;
         let mut jkreal = true;

         let pairs = &state[diagonal.0 as usize][diagonal.1 as usize].pairs;

         let j = if !is_convex[diagonal.0 as usize] {
            let last = *pairs.back().unwrap();
            if last.0 != last.1 { ijreal = false; }
            last.1
         } else {
            let first = pairs[0];
            if first.0 != first.1 { jkreal = false; }
            first.0
         };

         if ijreal {
            diagonals.push_back((diagonal.0, j));
         } else {
            diagonals2.push_back((diagonal.0, j));
         }

         if jkreal {
            diagonals.push_back((j, diagonal.1));
         } else {
            diagonals2.push_back((j, diagonal.1));
         }

         indices.push(j);
      }

      indices.sort();

      parts.push(indices);
   }

   parts
}

fn is_reflex(a: Point2<f32>, b: Point2<f32>, c: Point2<f32>) -> bool {
   cross(&[a, b, c]) > 0.0
}

fn update_state(a: isize, b: isize, w: usize, i: isize, j: isize, state: &mut Vec<Vec<DPState>>) {
   let w2 = state[a as usize][b as usize].weight;
   if w > w2 { return; }

   let pairs = &mut state[a as usize][b as usize].pairs;

   if w < w2 {
      pairs.clear();
      pairs.push_back((i, j));
      state[a as usize][b as usize].weight = w;
   } else {
      if !pairs.is_empty() && i <= pairs[0].0 { return; }
      while !pairs.is_empty() && pairs[0].1 >= j { pairs.pop_front(); }
      pairs.push_front((i, j));
   }
}

fn type_a(i: isize, j: isize, k: isize, poly: &Poly, state: &mut Vec<Vec<DPState>>) {
   if !state[i as usize][j as usize].visible { return; }

   let mut top = j;
   let mut w = state[i as usize][j as usize].weight;

   if k - j > 1 {
      if !state[j as usize][k as usize].visible { return; }
      w += state[j as usize][k as usize].weight + 1;
   }

   if j - i > 1 {
      let pairs = &state[i as usize][j as usize].pairs;

      let mut last = None;
      for pair in pairs.iter().rev() {
         if !is_reflex(poly[pair.1], poly[j], poly[k]) { last = Some(pair.0); } else { break; }
      }

      match last {
         Some(idx) => {
            if is_reflex(poly[k], poly[i], poly[idx]) { w += 1 } else { top = idx }
         }
         None => w += 1,
      }
   }
   update_state(i, k, w, top, j, state);
}

fn type_b(i: isize, j: isize, k: isize, poly: &Poly, state: &mut Vec<Vec<DPState>>) {
   if !state[j as usize][k as usize].visible { return; }

   let mut top = j;
   let mut w = state[j as usize][k as usize].weight;

   if j - i > 1 {
      if !state[i as usize][j as usize].visible { return; }
      w += state[i as usize][j as usize].weight + 1;
   }
   if k - j > 1 {
      let pairs = &state[j as usize][k as usize].pairs;

      if !pairs.is_empty() && !is_reflex(poly[i], poly[j], poly[pairs[0].0]) {
         let mut last = pairs[0].1;

         for pair in pairs {
            if !is_reflex(poly[i], poly[j], poly[pair.0]) { last = pair.1; } else { break; }
         }
         if is_reflex(poly[last], poly[k], poly[i]) { w += 1; } else { top = last; }
      } else {
         w += 1;
      }
   }
   update_state(i, k, w, j, top, state);
}
