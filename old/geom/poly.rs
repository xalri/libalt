use cgmath::*;
use std::ops;
use std::f32::consts::PI;
use std::f32;
use lazycell::LazyCell;
use transform::*;
use transform::Transform;
use math::{wrap_max, clamp};
use {Bounds, Range, point2};

/// A 2D polygon.
#[derive(Debug)]
pub struct Poly {
   points: Vec<Point2<f32>>,

   bounds: LazyCell<Bounds>,
   convex: LazyCell<Vec<Poly>>,
   area: LazyCell<f32>,
   base_inertia: LazyCell<f32>,
   radius: LazyCell<f32>,
   centroid: LazyCell<Point2<f32>>,
}

impl PartialEq for Poly {
   fn eq(&self, other: &Poly) -> bool {
      self.points().zip(other.points()).all(|(a, b)| a == b)
   }
}

impl Clone for Poly {
   fn clone(&self) -> Poly {
      Poly { points: self.points.clone(), .. Poly::empty() }
   }
}

impl Poly {
   /// Create a rectangle with the given dimensions.
   pub fn rect(width: f32, height: f32) -> Poly {
      let w = width / 2.0;
      let h = height / 2.0;
      Poly::new(&[[w, h], [w, -h], [-w, -h], [-w, h]])
   }

   pub fn square() -> Poly { Self::rect(20.0, 20.0) }

   /// Approximate an ellipse with the given vertex count & dimensions
   pub fn ellipse(v: u32, width: f32, height: f32) -> Poly {
      if v < 3 { panic!("Poly::ellipse requires at least three points") }
      Poly::new(&(0..v)
         .map(|i| (PI / v as f32) + (2.0 * PI * i as f32 / v as f32))
         .map(|angle| (angle.cos() * width / 2.0, angle.sin() * height / 2.0))
         .collect::<Vec<_>>())
   }

   /// Create a new `Poly` from an array of points, panics if less than
   /// three points are given
   pub fn new<P: Into<Point2<f32>> + Copy>(points: &[P]) -> Poly {
      if points.len() < 3 { panic!("Poly::new requires at least three points") }

      let mut points: Vec<_> = points.iter().map(|&p| p.into()).collect();
      if cross(&points) > 0.0 { points.reverse(); }

      Poly { points, .. Poly::empty() }
   }

   /// Create an empty polygon.
   fn empty() -> Poly {
      Poly {
         points: Vec::new(),
         bounds: LazyCell::new(),
         convex: LazyCell::new(),
         area: LazyCell::new(),
         base_inertia: LazyCell::new(),
         centroid: LazyCell::new(),
         radius: LazyCell::new(),
      }
   }

   /// Test if the poly is self-intersecting..
   pub fn intersects_self(&self) -> bool {
      for i in 0..self.len() {
         let dist_sq = (self[i] - self[i+1]).magnitude2();
         for j in 0..self.len() {
            if j == i || wrap_max(j + 1, self.len()) == i { continue; }

            if let Some(p) = intersection_point((self[i], self[i + 1]), (self[j], self[j + 1])) {
               if (p - self[i]).magnitude2() < dist_sq - 0.00001 {
                  return true;
               }
            }
         }
      }
      false
   }

   /// An iterator over the polygon's points
   pub fn points<'a>(&'a self) -> impl Iterator<Item=Point2<f32>> + 'a {
      self.points.iter().cloned()
   }

   /// An iterator over the polygon's segments
   pub fn segments<'a>(&'a self) -> impl Iterator<Item=(Point2<f32>, Point2<f32>)> + 'a {
      (0..self.len()).map(move |i| (self.points[i as usize], self[i+1]))
   }

   /// An iterator over the surface normals
   pub fn surface_normals<'a>(&'a self) -> impl Iterator<Item=Vector2<f32>> + 'a {
      self.segments().map(|(a, b)| vec2(a.y - b.y, b.x - a.x))
   }

   /// An iterator over segments of length 3, with the n'th segment having the n'th vertex in the
   /// center.
   pub fn tris<'a>(&'a self) -> impl Iterator<Item=(Point2<f32>, Point2<f32>, Point2<f32>)> + 'a {
      (0..self.len()).map(move |i| (self[i-1], self.points[i as usize], self[i+1]))
   }

   /// An iterator over the vertex normals
   pub fn vertex_normals<'a>(&'a self) -> impl Iterator<Item = Vector2<f32>> + 'a {
      // TODO: test
      self.tris().map(|(a, b, c)| if cross(&[a, b, c]) > 0.0 { 1.0 } else { -1.0 } * ((a - b) + (c - b)))
   }

   /// The polygon's axis-aligned bounding box.
   pub fn aabb(&self) -> Bounds {
      *self.bounds.borrow_with(|| {
         let mut x = Range{ min: self[0].x, max: self[0].x };
         let mut y = Range{ min: self[0].y, max: self[0].y };
         for v in 0..self.len() {
            let point = self[v];
            if point.x < x.min { x.min = point.x }
            if point.y < y.min { y.min = point.y }
            if point.x > x.max { x.max = point.x }
            if point.y > y.max { y.max = point.y }
         }
         Bounds { x, y }
      })
   }

   /// Get the cached minimal convex decomposition.
   pub fn convex(&self) -> &[Poly] {
      self.convex.borrow_with(|| {
         if self.is_convex() { return vec![self.clone()] }
         ::decomp::convex_partition(self).into_iter()
            .map(|idx| {
               let mut poly = Poly {
                  points: idx.into_iter().map(|i| self.points[i as usize]).collect(),
                  .. Poly::empty()
               };
               poly.simplify(0.0001); // remove collinear points
               poly
            }).collect()
      })
   }

   /// Simplify the polygon by removing points which are close to the line connecting their
   /// adjacent points. Panics if epsilon is negative.
   pub fn simplify(&mut self, e: f32) {
      let len = self.len();
      self.douglas_peucker(0, len - 1, e);
      // Remove the first point if it's collinear with it's adjacent points
      if self.len() > 2 && perpendicular_distance(self[-1], self[1], self[0]) < e {
         self.points.remove(0);
      }
      // Remove the last point if it's collinear with it's adjacent points
      if self.len() > 2 && perpendicular_distance(self[-2], self[0], self[-1]) < e {
         self.points.pop();
      }
   }

   /// Recursively simplify the polyline a->b using Douglas-Peucker.
   /// Panics if `e` is negative or if b > a.
   fn douglas_peucker(&mut self, a: isize, b: isize, e: f32) {
      if e.is_sign_negative() { panic!("douglas_peucker called with negative epsilon"); }
      if a > b { panic!("douglas_peucker invalid range {} -> {}", a, b) }

      if b - a < 2 { return; }

      // Find the point furthest from the line a -> b
      let mut distance = 0.0;
      let mut index = a + 1;

      for i in a+1 .. b {
         let d = perpendicular_distance(self[a], self[b], self[i]);
         if d > distance { index = i; distance = d; }
      }

      if distance < e {
         // If all points are all close to the line a -> b we remove them.
         self.points.drain((a as usize + 1)..(b as usize));
         self.reset_cache();
      } else {
         // Recursively simplify the segments to the left and right of the point furthest
         // from the line. The right half is simplified first to preserve indices.
         self.douglas_peucker(index, b, e);
         self.douglas_peucker(a, index, e);
      }
   }

   /// Remove cached values for convex decomposition etc.
   pub fn reset_cache(&mut self) {
      if self.bounds.filled() { self.bounds = LazyCell::new(); }
      if self.convex.filled() { self.convex = LazyCell::new(); }
      if self.area.filled() { self.area = LazyCell::new(); }
      if self.base_inertia.filled() { self.base_inertia = LazyCell::new(); }
      if self.centroid.filled() { self.centroid = LazyCell::new(); }
      if self.radius.filled() { self.radius = LazyCell::new(); }
   }

   /// The number of vertices in the poly.
   pub fn len(&self) -> isize {
      self.points.len() as isize
   }

   /// Project the polygon onto an axis.
   pub fn project(&self, axis: Vector2<f32>) -> Range {
      Range::from_iter(self.points().map(|p| p.to_vec().dot(axis))).unwrap()
   }

   /// The average of the polygon's points.
   pub fn centroid(&self) -> Point2<f32> {
      *self.centroid.borrow_with(|| {
         self.points().fold(point2(0.0, 0.0), |acc, x| point2(acc.x + x.x, acc.y + x.y))
            / self.len() as f32
      })
   }

   /// The radius of the polygon's bounding circle
   pub fn radius(&self) -> f32 {
      *self.radius.borrow_with(|| {
         let c = self.centroid();
         let mut max = f32::MIN;
         for p in &self.points {
            let m = (*p - c).magnitude2();
            if m > max { max = m }
         }
         max.sqrt()
      })
   }

   /// The poly's world-space bounding box
   pub fn bounds(&self, pos: Point2<f32>, angle: f32, scale: f32) -> Bounds {
      if relative_eq!(angle, 0.0) {
         self.aabb() * scale + pos.to_vec()
      } else {
         let c = pos.to_vec() + rotate(angle).apply_vec(self.centroid().to_vec()) * scale;
         let r = self.radius() * scale;
         Bounds { x: Range{ min: c.x - r, max: c.x + r }, y: Range{ min: c.y - r, max: c.y + r } }
      }
   }

   /// The poly's swept bounding box
   pub fn swept_bounds(&self, pos: Point2<f32>, angle: f32, scale: f32, vel: Vector2<f32>) -> Bounds {
      let mut b = self.bounds(pos, angle, scale);
      if vel.x < 0.0 { b.x.min += vel.x; }
      else if vel.x > 0.0 { b.x.max += vel.x; }
      if vel.y < 0.0 { b.y.min += vel.y; }
      else if vel.y > 0.0 { b.y.max += vel.y; }
      b
   }

   /// Test if the polygon is convex
   pub fn is_convex(&self) -> bool {
      if self.len() == 3 { return true; }
      for i in 0..self.len() {
         if cross(&[self[i], self[i + 1], self[i + 2]]) > 0.0 { return false }
      }
      true
   }

   /// Calculate the relative rotational inertia.
   pub fn inertia(&self) -> f32 {
      *self.base_inertia.borrow_with(|| {
         let mut numerator = 0.0;
         let mut denominator = 0.0;
         for (u, v) in self.segments() {
            let a = u.to_vec().perp_dot(v.to_vec()).abs();
            let b = u.x * (u.x + v.x) + u.y * (u.y + v.y) + v.x * v.x + v.y * v.y;
            numerator += a * b;
            denominator += a;
         }
         4.0 * numerator / (6.0 * denominator)
      })
   }

   /// Calculate the area of the polygon
   pub fn area(&self) -> f32 {
      *self.area.borrow_with(|| {
         0.5 * self.segments()
            .map(|(a, b)| (a.to_vec(), b.to_vec()))
            .map(|(a, b)| a.perp_dot(b).abs())
            .sum::<f32>()
      })
   }

   /// Create a copy of the poly with the given transformation applied.
   pub fn transformed(&self, t: Transform) -> Poly {
      let t = t.to_matrix();
      let points: Vec<_> = self.points()
         .map(|point| t.apply(point) )
         .collect();
      Poly::new(&points)
   }

   /// Apply a transformation matrix to all points
   pub fn transform(&mut self, t: Transform) {
      let t = t.to_matrix();
      for p in &mut self.points { *p = t.apply(*p) }
      if cross(&self.points) > 0.0 { self.points.reverse(); }
      self.reset_cache();
   }
}

impl Into<Vec<Point2<f32>>> for Poly {
   fn into(self) -> Vec<Point2<f32>> {
      self.points
   }
}

impl ops::Index<isize> for Poly {
   type Output = Point2<f32>;
   fn index(&self, idx: isize) -> &Point2<f32> {
      let idx = wrap_max(idx, self.points.len() as isize) as usize;
      &self.points[idx]
   }
}

/// Calculate the convex hull of a set of points.
pub fn convex_hull(points: &[Point2<f32>]) -> Vec<usize> {
   let mut left = 0;
   for i in 0..points.len() { if points[i].x < points[left].x { left = i } }

   let mut u = wrap_max(left + 1, points.len());
   let mut v = wrap_max(u + 1, points.len());

   let mut ret = vec![left, u];

   while v != left {
      if ret.len() > 1 && cross(
         &[points[ret[ret.len() - 2]], points[ret[ret.len() - 1]], points[v]]) > 0.0 {
         ret.pop();
      } else {
         ret.push(v);
         u = wrap_max(u + 1, points.len());
         v = wrap_max(v + 1, points.len());
      }
   }

   ret
}

/// Test if a polygon is convex
pub fn is_convex(poly: &[Point2<f32>]) -> bool {
   if poly.len() < 3 { return true; }

   let mut a = false;
   let mut b = false;

   for u in 0..poly.len() {
      let v = wrap_max(u + 1, poly.len());
      let w = wrap_max(u + 2, poly.len());

      let cross = cross(&[poly[u], poly[v], poly[w]]);

      if cross > 0.0 { a |= true }
      if cross < 0.0 { b |= true }

      if a && b { return false; }
   }

   true
}

/// Triangulate a polygon using ear-clipping, returns a vector of indices.
pub fn triangulate(poly: &[Point2<f32>]) -> Option<Vec<usize>> {
   let n = poly.len();
   if n < 3 { return None; }

   let mut verts: Vec<usize> = (0..n).collect();
   if cross(poly) < 0.0 { verts.reverse(); }

   let mut result = Vec::new();
   let mut count = verts.len() * 2;
   let mut u;
   let mut v = verts.len() - 1;
   let mut w;

   while verts.len() > 2 {
      if count == 0 { return None; }
      count -= 1;

      u = v;     if u >= verts.len() { u = 0; }
      v = u + 1; if v >= verts.len() { v = 0; }
      w = v + 1; if w >= verts.len() { w = 0; }

      if cross(&[poly[verts[u]], poly[verts[v]], poly[verts[w]]]) == 0.0 {
         verts.remove(v);
         continue;
      }

      if is_ear(poly, &verts, u, v, w) {
         result.extend_from_slice(&[verts[u], verts[v], verts[w]]);
         verts.remove(v);
         count = verts.len() * 2;
      }
   }

   Some(result)
}

/// Test if the segments a and b intersect.
pub fn intersects(a: (Point2<f32>, Point2<f32>), b: (Point2<f32>, Point2<f32>)) -> bool {
   let o1 = cross(&[a.0, a.1, b.0]);
   let o2 = cross(&[a.0, a.1, b.1]);
   let o3 = cross(&[b.0, b.1, a.0]);
   let o4 = cross(&[b.0, b.1, a.1]);

   if (o1 > 0.0) != (o2 > 0.0) && (o3 > 0.0) != (o4 > 0.0) { return true; }

   false

   /*
   // collinear
   if o1 == 0.0 && o2 == 0.0 && o3 == 0.0 && o4 == 0.0 { return false; }

   // glancing: _|, -|, etc..
   o1 == 0.0 && in_segment(a.0, b.0, a.1) ||
   o2 == 0.0 && in_segment(a.0, b.1, a.1) ||
   o3 == 0.0 && in_segment(b.0, a.0, b.1) ||
   o4 == 0.0 && in_segment(b.0, a.1, b.1)
   */
}

/// Get the intersection point of segments a & b. Returns `None` if the lines don't intersect
/// or are collinear
pub fn intersection_point(a: (Point2<f32>, Point2<f32>), b: (Point2<f32>, Point2<f32>)) -> Option<Point2<f32>> {
   if !intersects(a, b) { return None }

   let a_ = a.1 - a.0;
   let b_ = b.1 - b.0;
   let cross = a_.perp_dot(b_);
   if cross == 0.0 { return None }

   let diff = a.0 - b.0;
   let t = b_.perp_dot(diff) / cross;

   Some(Point2{ x: a.0.x + (t * a_.x), y: a.0.y + (t * a_.y) })
}

/// Test if some three points in a polygon are an ear.
fn is_ear(poly: &[Point2<f32>], verts: &[usize], u: usize, v: usize, w: usize) -> bool {
   let tri = [poly[verts[u]], poly[verts[v]], poly[verts[w]]];
   if cross(&tri) < 0.0 { return false; }

   !verts.iter()
      .enumerate()
      .filter(|&(i,_)| !(i == u || i == v || i == w))
      .map(|(_,&i)| &poly[i])
      .any(|p| point_in_triangle(tri[0], tri[1], tri[2], *p))
}

/// Test if a diagonal a1->b is locally inside a clockwise polygon
/// (a0, a1, a2, ... b)
pub fn locally_inside(a0: Point2<f32>, a1: Point2<f32>, a2: Point2<f32>, b: Point2<f32>) -> bool {
   if cross(&[a0, a1, a2]) > 0.0 { right_of(a0, a1, b) || right_of(a1, a2, b) }
   else { right_of(a0, a1, b) && right_of(a1, a2, b) }
}

/// Test if point p is to the left of a -> b
pub fn left_of(a: Point2<f32>, b: Point2<f32>, p: Point2<f32>) -> bool {
   (b - a).perp_dot(p - a) > 0.0
}

/// Test if point p is to the right of a -> b
pub fn right_of(a: Point2<f32>, b: Point2<f32>, p: Point2<f32>) -> bool {
   (b - a).perp_dot(p - a) < 0.0
}

/// Calculate the perpendicular distance between the line a->b and the point p.
pub fn perpendicular_distance(a: Point2<f32>, b: Point2<f32>, p: Point2<f32>) -> f32 {
   let c = p.x * (b.y - a.y) - p.y * (b.x-a.x) + b.x * a.y - b.y * a.x;
   let d = (b.y - a.y).powi(2) + (b.x - a.x).powi(2);
   c.abs() / d.sqrt()
}

/// Find the closest point on a segment to a point.
pub fn project_on_segment(v: Point2<f32>, s: (Point2<f32>, Point2<f32>)) -> Point2<f32> {
   let av = v.to_vec() - s.0.to_vec();
   let ab = s.1.to_vec() - s.0.to_vec();
   let t = clamp(av.dot(ab) / ab.dot(ab), 0.0, 1.0);
   s.0 + ab * t
}

/// For collinear points a, b, and c, test if b lies on a->c.
pub fn in_segment(a: Point2<f32>, b: Point2<f32>, c: Point2<f32>) -> bool {
   b.x <= f32::max(a.x, c.x) && b.x >= f32::min(a.x, b.x) &&
   b.y <= f32::max(a.y, c.y) && b.y >= f32::min(a.y, b.y)
}

/// Compute the scalar cross product of a polygon -- positive when the polygon
/// is counter-clockwise.
pub fn cross(poly: &[Point2<f32>]) -> f32 {
   let mut p = &poly[poly.len() - 1];
   poly.iter().map(|q| {
      let area = p.x * q.y - q.x * p.y;
      p = q;
      area
   }).sum()
}

/// Check if a point lies within a convex triangle
fn point_in_triangle(a: Point2<f32>, b: Point2<f32>, c: Point2<f32>, p: Point2<f32>) -> bool {
   (c.x - p.x) * (a.y - p.y) - (a.x - p.x) * (c.y - p.y) >= 0.0 &&
   (a.x - p.x) * (b.y - p.y) - (b.x - p.x) * (a.y - p.y) >= 0.0 &&
   (b.x - p.x) * (c.y - p.y) - (c.x - p.x) * (b.y - p.y) >= 0.0
}


// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[test] fn test_cross() {
   assert_eq!(8.0, cross(&[
      Point2 { x: -1.0, y: -1.0 },
      Point2 { x: 1.0, y: -1.0 },
      Point2 { x: 1.0, y: 1.0 },
      Point2 { x: -1.0, y: 1.0 },
   ]));

   assert_eq!(-8.0, cross(&[
      Point2 { x: -1.0, y: -1.0 },
      Point2 { x: -1.0, y: 1.0 },
      Point2 { x: 1.0, y: 1.0 },
      Point2 { x: 1.0, y: -1.0 },
   ]));
}

#[test] fn test_inside_triangle() {
   let tests = &[
      (true, Point2{ x: 1.0, y: 0.5 }),
      (true, Point2{ x: 0.5, y: 0.4 }),
      (false, Point2{ x: 0.5, y: 0.6 }),
      (false, Point2{ x: 1.0, y: -0.1 }),
   ];

   for &(expected, point) in tests {
      assert_eq!(expected, point_in_triangle(
         Point2 { x: 0.0, y: 0.0 },
         Point2 { x: 2.0, y: 0.0 },
         Point2 { x: 1.0, y: 1.0 },
         point
      ));
   }
}

#[test] fn test_is_ear() {
   let poly = &[
      Point2 { x: 0.0, y: 0.0 },
      Point2 { x: -1.0, y: 1.0 },
      Point2 { x: 0.0, y: -1.0 },
      Point2 { x: 1.0, y: 1.0 },
   ];

   let verts = &[0, 1, 2, 3];

   assert_eq!(true, is_ear(poly, verts, 0, 1, 2));
   assert_eq!(false, is_ear(poly, verts, 1, 2, 3));
   assert_eq!(true, is_ear(poly, verts, 2, 3, 0));
   assert_eq!(false, is_ear(poly, verts, 3, 0, 1));
}

#[test] fn test_triangulate() {
   let poly = &[
      Point2 { x: 0.0, y: 0.0 },
      Point2 { x: -1.0, y: 1.0 },
      Point2 { x: 0.0, y: -1.0 },
      Point2 { x: 1.0, y: 1.0 },
   ];

   assert!(triangulate(poly).is_some());
}

#[test] fn test_intersection() {
   assert_eq!(None, intersection_point(
      (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 }),
      (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 })));

   assert_eq!(Some(Point2{ x: 0.5, y: 0.5 }), intersection_point(
      (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 }),
      (Point2{ x: 1.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 })));
}

#[test] fn test_intersects() {
   assert!(!intersects(
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 }),
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 })));

   assert!(!intersects(
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 2.0 }),
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 })));

   assert!(intersects(
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 }),
         (Point2{ x: 0.0, y: 1.0 }, Point2{ x: 1.0, y: 0.0 })));

   assert!(intersects(
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 }),
         (Point2{ x: 1.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 })));

   assert!(!intersects(
         (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 }),
         (Point2{ x: 1.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 })));

   assert!(!intersects(
      (Point2{ x: 0.0, y: 0.0 }, Point2{ x: 0.0, y: 1.0 }),
      (Point2{ x: 0.0, y: 1.0 }, Point2{ x: 0.0, y: 0.0 })))
}

#[test] fn test_right_of() {
   assert!(!right_of(point2(0.0, 0.0), point2(0.0, 2.0), point2(0.0, 1.0)));
   assert!(!right_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(-1.0, 0.0)));
   assert!(!right_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(-1.0, 2.0)));
   assert!(right_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(1.0, 0.0)));
   assert!(right_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(1.0, 2.0)));
}

#[test] fn test_left_of() {
   assert!(!left_of(point2(0.0, 0.0), point2(0.0, 2.0), point2(0.0, 1.0)));
   assert!(left_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(-1.0, 0.0)));
   assert!(left_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(-1.0, 2.0)));
   assert!(!left_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(1.0, 0.0)));
   assert!(!left_of(point2(0.0, 0.0), point2(0.0, 1.0), point2(1.0, 2.0)));
}

#[test] fn test_locally_inside() {
   assert!(locally_inside(
         Point2{ x: 1.0, y: -1.0 }, Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 },
         Point2{ x: 2.0, y: 0.0 }));

   assert!(locally_inside(
         Point2{ x: 1.0, y: 1.0 }, Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: -1.0 },
         Point2{ x: -2.0, y: 0.0 }));

   assert!(!locally_inside(
      Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 }, Point2{ x: 0.0, y: -1.0 },
      Point2{ x: 1.0, y: 1.0 }));

   assert!(!locally_inside(
         Point2{ x: 1.0, y: -1.0 }, Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: 1.0 },
         Point2{ x: -2.0, y: 0.0 }));

   assert!(!locally_inside(
         Point2{ x: 1.0, y: 1.0 }, Point2{ x: 0.0, y: 0.0 }, Point2{ x: 1.0, y: -1.0 },
         Point2{ x: 2.0, y: 0.0 }));
}

/// Creates a set of points from a slice `&[[x, y], [x, y], ..]`
#[cfg(test)] fn points<T: Copy>(x: &[[T;2]]) -> Vec<Point2<T>> {
   x.iter().map(|&v| point2(v[0], v[1])).collect()
}

#[test] fn test_is_convex() {
   assert!(!is_convex(&points(&[[0.0,0.0], [1.0,1.0], [0.0,-1.0], [-1.0,1.0]])));
   assert!(!is_convex(&points(&[[0.0,0.0], [-1.0,1.0], [0.0,-1.0], [1.0,1.0]])));

   assert!(is_convex(&points(&[[0.0,1.0], [1.0,1.0], [0.0,-1.0], [-1.0,1.0]])));
   assert!(is_convex(&points(&[[0.0,1.0], [-1.0,1.0], [0.0,-1.0], [1.0,1.0]])));
}

#[test] fn test_convex_hull() {
   assert_eq!(
      convex_hull(&points(&[[0.0,0.0], [1.0,1.0], [0.0,-1.0], [-1.0,1.0]])),
      vec![3, 1, 2]
   );
}

#[test] fn test_perpendicular_distance() {
   assert_eq!(1.0,
              perpendicular_distance(Point2{x: 0.0, y: 0.0}, Point2{x: 2.0, y: 0.0}, Point2{x: 0.0, y: 1.0}));
   assert_eq!(1.0,
              perpendicular_distance(Point2{x: 0.0, y: 0.0}, Point2{x: 2.0, y: 0.0}, Point2{x: 0.0, y: -1.0}));
   assert_eq!(1.0,
              perpendicular_distance(Point2{x: 0.0, y: 0.0}, Point2{x: 2.0, y: 0.0}, Point2{x: 2.0, y: -1.0}));
}

#[test] fn test_convex_decomposition() {
   fn case(expected: Vec<Vec<Point2<f32>>>, points: Vec<Point2<f32>>) {
      let poly = Poly::new(&points);
      let decomp = poly.convex();
      for p in decomp { assert!(p.is_convex()) }
      let result: Vec<Vec<Point2<f32>>> = decomp.iter().map(|p| p.clone().into()).collect();
      assert_eq!(expected, result);
   }

   case(
      vec![
         points(&[[0.0, 0.0], [0.0, -1.0], [-1.0, 1.0]]),
         points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0]]),
      ],
      points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]));

   case(
      vec![
         // reversed as source is ccw
         points(&[[1.0, -1.0], [-1.0, -1.0], [-1.0, 1.0], [1.0, 1.0]]),
      ],
      points(&[[1.0, 1.0], [-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]]));

   case(
      vec![
         points(&[[ 1.0, -1.0], [-1.0, -1.0], [1.0, 1.0]]),
         points(&[[-1.0, -1.0], [-1.0, 1.0], [0.0, 0.0]]),
      ],
      points(&[[1.0, 1.0], [0.0, 0.0], [-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]]));
}

#[test] fn test_project() {
   let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]));
   assert_eq!(Range::new(-1.0, 1.0), poly.project(vec2(1.0, 0.0)));
   assert_eq!(Range::new(-1.0, 1.0), poly.project(vec2(-1.0, 0.0)));
   assert_eq!(Range::new(-1.0, 1.0), poly.project(vec2(0.0, 1.0)));
}

#[test] fn test_surface_normals() {
   let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -1.0], [-1.0, 1.0]]));
   assert_eq!(
      vec![vec2(-1.0, 1.0), vec2(2.0, -1.0), vec2(-2.0, -1.0), vec2(1.0, 1.0)],
      poly.surface_normals().collect::<Vec<_>>());
}

#[test] fn test_project_on_segment() {
   assert_eq!(
      point2(0.0, 0.0),
      project_on_segment(point2(-1.0, 1.0), (point2(-1.0, -1.0), point2(1.0, 1.0))));

   assert_eq!(
      point2(0.0, 0.0),
      project_on_segment(point2(-1.0, 0.0), (point2(0.0, 0.0), point2(1.0, 1.0))));

   assert_eq!(
      point2(0.0, 0.0),
      project_on_segment(point2(-1.0, 0.0), (point2(1.0, 1.0), point2(0.0, 0.0))));
}

#[test] fn test_area() {
   let poly = Poly::new(&points(&[[0.0, 0.0], [1.0, 1.0], [0.0, -2.0], [-1.0, 1.0]]));
   assert_eq!(poly.area(), 2.0);
}

#[cfg(test)] extern crate test;
#[cfg(test)] use self::test::Bencher;

#[test]
fn convex_decomp_a() {
   Poly::new(&[
      point2(1.0, 1.0),
      point2(2.0, 1.0),
      point2(2.0, -1.0),
      point2(1.0, -1.0),
      point2(1.0, -2.0),
      point2(-1.0, -2.0),
      point2(-1.0, -1.0),
      point2(-2.0, -1.0),
      point2(-2.0, 1.0),
      point2(-1.0, 1.0),
      point2(-1.0, 2.0),
      point2(1.0, 2.0),
   ]).convex();
}

#[bench]
fn convex_decomp_small(bench: &mut Bencher) {
    let mut x = test::black_box(Poly::new(&[
       point2(1.0, 2.0),
       point2(1.0, 1.0),
       point2(2.0, 1.0),
       point2(1.0, -1.0),
       point2(1.0, -2.0),
       point2(-1.0, 2.0),
    ]));

    bench.iter(|| {
       x.reset_cache();
       let _ = x.convex();
    });
}

#[bench]
fn convex_decomp_large(bench: &mut Bencher) {
   let mut x = Poly::new(&[
      point2(156.0,-6.0),
      point2(-128.0,-6.0),
      point2(-128.0,-12.0),
      point2(-123.0,-16.0),
      point2(-99.0,-16.0),
      point2(-94.0,-29.0),
      point2(-106.0,-29.0),
      point2(-109.0,-31.0),
      point2(-109.0,-34.0),
      point2(-123.0,-34.0),
      point2(-128.0,-37.0),
      point2(-130.0,-42.0),
      point2(-132.0,-45.0),
      point2(-137.0,-48.0),
      point2(-142.0,-51.0),
      point2(-143.0,-67.0),
      point2(-140.0,-68.0),
      point2(-140.0,-77.0),
      point2(-129.0,-80.0),
      point2(-128.0,-84.0),
      point2(-80.0,-84.0),
      point2(-77.0,-80.0),
      point2(-69.0,-80.0),
      point2(-58.0,-67.0),
      point2(-51.0,-64.0),
      point2(-51.0,-58.0),
      point2(-36.0,-51.0),
      point2(-34.0,-45.0),
      point2(15.0,-45.0),
      point2(15.0,-50.0),
      point2(30.0,-59.0),
      point2(31.0,-64.0),
      point2(38.0,-67.0),
      point2(42.0,-74.0),
      point2(46.0,-74.0),
      point2(49.0,-80.0),
      point2(56.0,-80.0),
      point2(60.0,-84.0),
      point2(107.0,-84.0),
      point2(109.0,-80.0),
      point2(118.0,-79.0),
      point2(119.0,-68.0),
      point2(123.0,-67.0),
      point2(123.0,-60.0),
      point2(120.0,-58.0),
      point2(123.0,-56.0),
      point2(133.0,-51.0),
      point2(133.0,-45.0),
      point2(123.0,-29.0),
      point2(119.0,-28.0),
      point2(106.0,-28.0),
      point2(104.0,-25.0),
      point2(97.0,-23.0),
      point2(97.0,-21.0),
      point2(118.0,-21.0),
      point2(122.0,-15.0),
      point2(133.0,-15.0),
      point2(149.0,-15.0),
      point2(156.0,-10.0),
   ]);

   assert!(!x.intersects_self());

   bench.iter(|| {
      x.reset_cache();
      let _ = x.convex();
   });
}
