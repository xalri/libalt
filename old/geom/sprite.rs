/// Altitude's graphics primitives

use cgmath::*;
use body::Body;
use poly::Poly;
use transform::Transform;
use std::ops::{Deref, DerefMut};

impl Deref for Sprite {
   type Target = Body;
   fn deref(&self) -> &Body { &self.body }
}

impl DerefMut for Sprite {
   fn deref_mut(&mut self) -> &mut Body { &mut self.body }
}

impl Deref for AnimSprite {
   type Target = Body;
   fn deref(&self) -> &Body { &self.body }
}

impl DerefMut for AnimSprite {
   fn deref_mut(&mut self) -> &mut Body { &mut self.body }
}

impl From<[u8; 4]> for Color {
   fn from(x: [u8; 4]) -> Self {
      Color(x)
   }
}

impl Default for Color {
   fn default() -> Color { DEFAULT_COLOR }
}