use cgmath::*;

/// Extends `Matrix3` for applying transformations to 2D points & vectors without type
/// annotations.
pub trait TransformExt {
   /// Apply a transformation matrix to a point
   fn apply(&self, point: Point2<f32>) -> Point2<f32>;

   /// Apply a transformation matrix to a vector. Note that _translation_ has no effect
   /// on vectors.
   fn apply_vec(&self, vec: Vector2<f32>) -> Vector2<f32>;
}

/// The identity matrix.
pub fn id() -> Matrix3<f32> {
   One::one()
}

/// Create a translation matrix.
pub fn translate(x: f32, y: f32) -> Matrix3<f32> {
   [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [x, y, 1.0]].into()
}

/// Create a transformation matrix to flip a point along the axes.
pub fn flip(h: bool, v: bool) -> Matrix3<f32> {
   let x = if h { -1.0 } else { 1.0 };
   let y = if v { -1.0 } else { 1.0 };
   [[x, 0.0, 0.0], [0.0, y, 0.0], [0.0, 0.0, 1.0]].into()
}

/// Create a transformation matrix which scales a point.
pub fn scale(scale: f32) -> Matrix3<f32> {
   [[scale, 0.0, 0.0], [0.0, scale, 0.0], [0.0, 0.0, 1.0]].into()
}

/// Create a transformation matrix which rotates a point by the given angle
/// in degrees.
pub fn rotate(angle: f32) -> Matrix3<f32> {
   Matrix2::from_angle(Deg(angle)).into()
}

pub fn rotate_about(angle: f32, pos: Point2<f32>) -> Matrix3<f32> {
   translate(pos.x, pos.y) * rotate(angle) * translate(-pos.x, -pos.y)
}

/// A 2D transformation, composed of a translation, rotation, scale, and
/// reflection (a similarity).
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Transform {
   pub position: Vector2<f32>,
   pub scale: f32,
   pub orientation: f32,
   pub flip_x: bool,
   pub flip_y: bool,
}

impl Transform {
   /// Create a transformation matrix
   pub fn to_matrix(&self) -> Matrix3<f32> {
      translate(self.position.x, self.position.y)
         * flip(self.flip_x, self.flip_y)
         * rotate(self.orientation)
         * scale(self.scale)
   }

   /// Compose two transformations
   pub fn compose(&self, other: &Transform) -> Transform {
      let mut t = Transform::default();
      t.orientation = self.orientation + other.orientation * if self.flip_x ^ self.flip_y { -1.0 } else { 1.0 };
      t.scale = self.scale * other.scale;
      t.position = rotate(other.orientation).apply_vec(self.position);
      if other.flip_x { t.position.x *= -1.0 }
      if other.flip_y { t.position.y *= -1.0 }
      t.position *= other.scale;
      t.position += other.position;
      t.flip_x = self.flip_x ^ other.flip_x;
      t.flip_y = self.flip_y ^ other.flip_y;
      t
   }
}

impl Default for Transform {
   fn default() -> Self {
      Transform {
         position: vec2(0.0, 0.0),
         scale: 1.0,
         orientation: 0.0,
         flip_x: false,
         flip_y: false,
      }
   }
}

impl TransformExt for Matrix3<f32> {
   fn apply(&self, point: Point2<f32>) -> Point2<f32> {
      ::cgmath::Transform::<Point2<f32>>::transform_point(self, point)
   }

   fn apply_vec(&self, vec: Vector2<f32>) -> Vector2<f32> {
      ::cgmath::Transform::<Point2<f32>>::transform_vector(self, vec)
   }
}


// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[cfg(test)] use point2;

#[test]
fn test_id() {
   let point = point2(1.0, 2.0);
   assert_relative_eq!(point, id().apply(point));
}

#[test]
fn test_translate() {
   let point = point2(1.0, 2.0);
   assert_relative_eq!(point2(2.0, 4.0), translate(1.0, 2.0).apply(point));
   assert_relative_eq!(vec2(1.0, 2.0), translate(1.0, 2.0).apply_vec(point.to_vec()));
}

#[test]
fn test_flip() {
   let point = point2(1.0, 2.0);
   assert_relative_eq!(point2(1.0, 2.0), flip(false, false).apply(point));
   assert_relative_eq!(point2(-1.0, 2.0), flip(true, false).apply(point));
   assert_relative_eq!(point2(1.0, -2.0), flip(false, true).apply(point));
   assert_relative_eq!(point2(-1.0, -2.0), flip(true, true).apply(point));
}

#[test]
fn test_scale() {
   let point = point2(1.0, 2.0);
   assert_relative_eq!(point2(1.0, 2.0), scale(1.0).apply(point));
   assert_relative_eq!(point2(2.0, 4.0), scale(2.0).apply(point));
   assert_relative_eq!(point2(-1.0, -2.0), scale(-1.0).apply(point));
   assert_relative_eq!(point2(-2.0, -4.0), scale(-2.0).apply(point));
}

#[test]
fn test_rotate() {
   let point = point2(1.0, 2.0);
   assert_relative_eq!(point2(2.0, -1.0), rotate(-90.0).apply(point));
   assert_relative_eq!(point2(-2.0, 1.0), rotate(90.0).apply(point));
}

#[test]
fn test_composite_transform() {
   let point = point2(1.0, 2.0);
   let transform = Transform {
      position: vec2(1.0, 2.0),
      scale: 2.0,
      orientation: 90.0,
      flip_x: true,
      flip_y: false,
   }.to_matrix();
   assert_relative_eq!(point2(5.0, 4.0), transform.apply(point));
}

