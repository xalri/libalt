use num::*;
use std::cmp::PartialOrd;
pub use num::clamp;

/// Wrap a value between 0 and a given maximum.
pub fn wrap_max<T>(value: T, max: T) -> T where T: Num + Copy {
   (max + value % max) % max
}

/// Wrap a value in a given range [min, max).
pub fn wrap<T>(value: T, min: T, max: T) -> T where T: Num + Copy {
   min + wrap_max(value - min, max - min)
}

/// Map a value from it's relative position in one range to the value of that
/// relative position in another, e.g. `map(1, 0..2, 10..20) = 15`
pub fn map<T>(value: T, from: [T; 2], to: [T; 2]) -> T where T: Num + Copy + PartialOrd {
   let value = clamp(value, from[0], from[1]);
   let k = (value - from[0]) / (from[1] - from[0]);
   to[0] + ((to[1] - to[0]) * k)
}

// ========================================================================
// -------------------------------- Tests ---------------------------------
// ========================================================================

#[test]
fn test_map() {
   assert_relative_eq!(map(0.5, [0.0, 1.0], [10.0, 20.0]), 15.0);
   assert_relative_eq!(map(2.0, [0.0, 1.0], [10.0, 20.0]), 20.0);
   assert_relative_eq!(map(2.0, [0.0, 1.0], [20.0, 10.0]), 10.0);
   assert_relative_eq!(map(0.7, [0.0, 1.0], [10.0, 20.0]), 17.0);
   assert_relative_eq!(map(0.7, [0.0, 1.0], [20.0, 10.0]), 13.0);
   assert_relative_eq!(map(7.0, [5.0, 10.0], [20.0, 10.0]), 16.0);
   assert_relative_eq!(map(7.0, [5.0, 10.0], [10.0, 20.0]), 14.0);
}

#[test]
fn test_wrap() {
   assert_relative_eq!(wrap(0.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(-0.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(-0.2, 0.0, 1.0), 0.8);
   assert_relative_eq!(wrap(1.5, 0.0, 1.0), 0.5);
   assert_relative_eq!(wrap(1.2, 0.0, 1.0), 0.2);
   assert_relative_eq!(wrap(0.0, 0.0, 1.0), 0.0);
   assert_relative_eq!(wrap(-1.0, 0.0, 1.0), 0.0);
   assert_relative_eq!(wrap(1.0, 0.0, 1.0), 0.0);
}

