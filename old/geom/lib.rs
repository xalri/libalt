#![feature(try_trait)]
#![feature(test)]
#![feature(nll)]

#[macro_use] extern crate derivative;
#[macro_use] extern crate approx;
extern crate cgmath;
extern crate num;
extern crate lazycell;

use cgmath::*;
use std::ops::*;

pub mod collide;
pub mod poly;
pub mod decomp;
pub mod body;
pub mod transform;
pub mod math;
pub mod sprite;

/// Shorthand to construct a `cgmath::Point2`.
pub fn point2<T>(x: T, y: T) -> Point2<T> {
   Point2{ x, y }
}

/// The width and height of a 2D space.
#[derive(PartialEq, Debug, Clone, Copy, Default)]
pub struct Dimensions {
   pub width: f32,
   pub height: f32,
}

/// A range.
#[derive(PartialEq, Debug, Clone, Copy, Default)]
pub struct Range {
   pub min: f32,
   pub max: f32,
}

/// A 2D axis-aligned bounding box
#[derive(PartialEq, Debug, Clone, Copy, Default)]
pub struct Bounds {
   pub x: Range,
   pub y: Range,
}

impl Dimensions {
   /// Create a new `Dimensions`
   pub fn new(width: f32, height: f32) -> Dimensions {
      Dimensions{ width, height }
   }
}

impl Range {
   /// Create a new `Range`
   pub fn new(min: f32, max: f32) -> Range {
      Range { min, max }
   }

   /// Create a new `Range` from an iterator, returns `None` if the iterator
   /// is empty.
   pub fn from_iter<I: Iterator<Item=f32>>(mut iter: I) -> Option<Range> {
      let mut min = iter.next()?;
      let mut max = min;
      for x in iter {
         if x < min { min = x }
         if x > max { max = x }
      }
      Some(Range { min, max })
   }

   /// Test if a value is contained in the range.
   pub fn contains(&self, value: f32) -> bool {
      value >= self.min && value <= self.max
   }

   pub fn contains_range(&self, r: Range) -> bool {
      self.min <= r.min && self.max >= r.max
   }

   pub fn intersects(&self, other: Range) -> bool {
      !(self.min > other.max || self.max < other.min)
   }

   pub fn combine(&self, other: Range) -> Range {
      Range { min: f32::min(self.min, other.min), max: f32::max(self.max, other.max) }
   }

   pub fn expand(&self, distance: f32) -> Range {
      Range { min: self.min - distance, max: self.max + distance }
   }

   pub fn width(&self) -> f32 {
      self.max - self.min
   }

   pub fn center(&self) -> f32 {
      self.min + (self.width() / 2.0)
   }
}

impl Add<f32> for Range {
   type Output = Range;
   fn add(self, rhs: f32) -> Self::Output {
      Range { min: self.min + rhs, max: self.max + rhs }
   }
}

impl Sub<f32> for Range {
   type Output = Range;
   fn sub(self, rhs: f32) -> Self::Output {
      Range { min: self.min - rhs, max: self.max - rhs }
   }
}

impl Mul<f32> for Range {
   type Output = Range;
   fn mul(self, rhs: f32) -> Self::Output {
      Range { min: self.min * rhs, max: self.max * rhs }
   }
}

impl Bounds {
   /// Create `Bounds` containing a given set of points. Panics if the given array is empty.
   pub fn containing(points: &[Point2<f32>]) -> Bounds {
      if points.is_empty() {
         panic!("Bounds::containing called with an empty array");
      }
      let mut x = Range{ min: points[0].x, max: points[0].x };
      let mut y = Range{ min: points[0].y, max: points[0].y };
      for point in points {
         if point.x < x.min { x.min = point.x }
         if point.y < y.min { y.min = point.y }
         if point.x > x.max { x.max = point.x }
         if point.y > y.max { y.max = point.y }
      }
      Bounds { x, y }
   }

   /// Test if a point is contained in the bounds.
   pub fn contains(&self, point: Point2<f32>) -> bool {
      self.x.contains(point.x) && self.y.contains(point.y)
   }

   pub fn contains_bounds(&self, other: &Bounds) -> bool {
      self.x.contains_range(other.x) && self.y.contains_range(other.y)
   }

   /// Tests for an intersection between two bounds.
   pub fn intersects(&self, other: Bounds) -> bool {
      !(self.x.min > other.x.max ||
         self.x.max < other.x.min ||
         self.y.min > other.y.max ||
         self.y.max < other.y.min)
   }

   pub fn combine(&self, other: Bounds) -> Bounds {
      Bounds { x: self.x.combine(other.x), y: self.y.combine(other.y) }
   }

   pub fn expand(&self, distance: f32) -> Bounds {
      Bounds { x: self.x.expand(distance), y: self.y.expand(distance) }
   }

   pub fn perimeter(&self) -> f32 {
      2.0 * (self.x.width() + self.y.width())
   }

   pub fn center(&self) -> Point2<f32> {
      point2(self.x.center(), self.y.center())
   }
}

impl Add<Vector2<f32>> for Bounds {
   type Output = Bounds;
   fn add(self, rhs: Vector2<f32>) -> Self::Output {
      Bounds { x: self.x + rhs.x, y: self.y + rhs.y }
   }
}

impl Sub<Vector2<f32>> for Bounds {
   type Output = Bounds;
   fn sub(self, rhs: Vector2<f32>) -> Self::Output {
      Bounds { x: self.x - rhs.x, y: self.y - rhs.y }
   }
}

impl Mul<f32> for Bounds {
   type Output = Bounds;
   fn mul(self, rhs: f32) -> Self::Output {
      Bounds { x: self.x * rhs, y: self.y * rhs }
   }
}

#[test]
fn test_bounds_contains() {
   let bounds = Bounds{ x: Range::new(0.0, 100.0), y: Range::new(0.0, 100.0) };
   assert!(bounds.contains(Point2{ x: 100.0,   y: 100.0 }));
   assert!(bounds.contains(Point2{ x: 0.0,     y: 0.0 }));
   assert!(bounds.contains(Point2{ x: 50.0,    y: 50.0 }));
   assert!(bounds.contains(Point2{ x: 50.0,    y: 100.0 }));
   assert!(!bounds.contains(Point2{ x: 100.0,  y: -0.1 }));
   assert!(!bounds.contains(Point2{ x: 100.1,  y: 0.0 }));
}

#[test]
fn test_bounds_intersects() {
   // TODO
}