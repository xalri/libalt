extern crate render;
extern crate sfml;
extern crate cgmath;
extern crate geom;
#[macro_use] extern crate error_chain;

use sfml::graphics::{RenderTarget, Texture, Vertex, Color, Transform, RenderStates, BlendMode, PrimitiveType};
use sfml::system::{Vector2f};
use cgmath::*;
use std::rc::Rc;

/// A `Surface` for drawing to an SFML render target.
pub struct Surface<'a> {
   pub rt: &'a RenderTarget,
}

#[derive(Clone)]
pub struct Tx(Rc<Texture>);

#[derive(Clone, Copy, Debug)]
pub struct Vert(Vertex);

impl render::Texture for Tx {
   fn width(&self) -> u64 { self.0.size().x as u64 }
   fn height(&self) -> u64 { self.0.size().y as u64 }
   fn repeat(&self) -> bool { self.0.is_repeated() }
}

impl render::Vertex for Vert {
   fn new(pos: Point2<f32>, uv: Point2<f32>, col: geom::sprite::Color) -> Self {
      let col = col.0;
      Vert(Vertex::new(
         Vector2f::new(pos.x as f32, pos.y as f32),
         Color::rgba(col[0], col[1], col[2], col[3]),
         Vector2f::new(uv.x as f32, uv.y as f32)))
   }
}

impl<'a> render::Surface for Surface<'a> {
   type Texture = Tx;
   type Vertex = Vert;

   /// Draw a convex polygon to the surface.
   fn draw(&self, v: &[Vert], transform: Matrix3<f32>, tx: Option<Tx>) -> render::Result<()> {
      if v.len() < 3 { bail!("Surface::draw: Invalid polygon"); }

      // TODO: is this safe?...
      let v: &[Vertex] = unsafe{ std::mem::transmute(v) };

      let transform = Transform::new(*transform.transpose().cast().as_ref());
      let prim = PrimitiveType::TriangleFan;

      self.rt.draw_primitives(v, prim, RenderStates::new(
         BlendMode::default(),
         transform,
         if let Some(ref tx) = tx { Some(&*tx.0) } else { None },
         None));

      Ok(())
   }

   /// Load a raw texture into video memory.
   fn load_tx_raw(
      &self, data: &[u8], w: u32, h: u32, filter: bool, repeat: bool) -> render::Result<Tx>
   {
      // Create a new texture.
      let mut tx = Texture::new(w, h).unwrap();

      // Copy the image data into the texture and set it's parameters.
      tx.update_from_pixels(data, w, h, 0, 0);
      tx.set_repeated(repeat);
      tx.set_smooth(filter);

      Ok(Tx(Rc::new(tx)))
   }
}
