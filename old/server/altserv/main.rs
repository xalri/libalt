extern crate util;
extern crate model;
extern crate proto;
extern crate resources;
extern crate net;
extern crate server;
extern crate reffers;
extern crate futures;
extern crate tokio_core;
extern crate tokio_periodic;
#[macro_use] extern crate slog;

use std::time::*;
use reffers::rc::*;
use tokio_core::reactor::*;
use futures::Stream;
use tokio_periodic::PeriodicTimer;

use util::{Log, logger};
use model::types::VERSION;
use model::server_conf::FFAConfig;
use model::shared_map::*;
use proto::Context;
use proto::message::{ServerInfo, Message, Data, Info, Join};
use proto::ping::*;
use proto::Sender;
use proto::rules::MessageRule;
use resources::dir_source::*;
use resources::ResourceDir;
use net::socket::*;
use server::error::*;
use server::client::Client;
use server::game::*;
use server::logic::ffa::*;
use server::processors::*;

pub fn main() {
   let log = logger::trace_file("server");

   let mut reactor = Core::new().unwrap();

   enum Ev{ Timer, Msg(Message) }

   let socket = Socket::bind(
      ([0,0,0,0], 27276).into(),
      reactor.handle(),
      MessageRule(Context::Server),
      log.clone()).unwrap();

   let sender = proto::Sender::from_socket(&socket);
   proto::lan::lan_listen(log.clone(), &[sender]);

   let mut server = Server::new(log.clone(), Sender(socket.sender()));
   server.start_game("ffa_cave.altx").unwrap();

   let msg = socket
      .map(|msg| Ev::Msg(Message(msg)))
      .map_err(|e| panic!("oops: {}", e));

   let timer = PeriodicTimer::new(&reactor.handle()).unwrap();
   timer.reset(Duration::new(0, 33_333_333)).unwrap();
   let timer = timer
      .map(|_| Ev::Timer)
      .map_err(|e| panic!("Timer error: {}", e));

   let ev = msg.select(timer).for_each(move |ev| {
      match ev {
         Ev::Timer => server.update(),
         Ev::Msg(message) => server.process(&message),
      }
      Ok(())
   });

   info!(log, "Running server");
   reactor.run(ev).unwrap();
}

pub struct Server {
   pub pings: Pings,
   pub file_server: FileServer,
   pub server_list: ListJoiner,
   pub limbo: Limbo<Game>,
   pub map_source: MapSource,
   pub resources: ResourceDir,
   pub games: Vec<Strong<Game>>,
   pub log: ::Log,
   sender: Sender,
}

impl Server {
   /// Create a new server
   pub fn new(log: ::Log, sender: Sender) -> Server  {
      let res_dir = DirSource::open("/opt/altitude/resources/").unwrap();
      let map_dir = DirSource::open("/opt/altitude/maps/").unwrap();

      Server {
         map_source: MapSource::new(map_dir, 32),
         resources: ResourceDir::new(res_dir, false).unwrap(),
         file_server: FileServer::new(log.clone(), sender.clone()),
         server_list: ListJoiner::new(log.clone(), sender.clone()),
         limbo: Limbo::new(log.clone(), sender.clone()),
         pings: Pings::new(sender.clone()),
         games: Vec::new(),
         sender,
         log,
      }
   }

   pub fn start_game(&mut self, map_name: &str) -> Result<()> {
      info!(self.log, "Loading map '{}'", map_name);
      let map = self.map_source.get_map(map_name.to_string())?;

      self.file_server.add_to_cache(
         map.path().to_owned(),
         map.altx().compressed().to_vec());

      let mut config = FFAConfig::default();
      config.warmup_time_seconds = 2;

      let game = Game::new(
         self.log.clone(),
         self.sender.clone(),
         map,
         FFA::new(self.log.clone(), config),
         self.resources.clone());

      self.games.push(Strong::new(game));

      Ok(())
   }

   pub fn info(&self) -> ServerInfo {
      ServerInfo {
         game_id: 0,
         addr: None,
         map_name: Some(self.games[0].get().map.path().trim_right_matches(".altx").to_string()),
         max_players: 128,
         num_players: self.games[0].get().world.players().len() as u8,
         server_name: Some("libalt test".to_owned()),
         pass_req: false,
         hardcore: true,
         min_level: 1,
         max_level: 60,
         disallow_demo: false,
         version: Some(VERSION),
      }
   }

   pub fn update(&mut self) {
      for game in &self.games { game.get_mut().tick(); }
      self.pings.update();
      self.server_list.update();

      for lost in self.limbo.lost() {
         match lost {
            Lost::Disconnected(_client) => (),
            Lost::Timeout(_client) => (),
            Lost::Lost(_client) => unreachable!(),
         }
      }
   }

   /// Handle an incoming message.
   pub fn process(&mut self, msg: &Message) {
      let addr = msg.addr();

      self.pings.process(msg);
      self.file_server.process(msg);
      self.limbo.process(msg);
      self.server_list.process(msg);
      for game in &self.games {
         // TODO: map of client addresses to the game we put them in?
         if game.get().clients.contains_key(&addr) { game.get_mut().process(msg); }
      }

      match *msg.data() {
         Data::Join(Join::JoinReq(ref req)) => {
            let req = req.clone();
            if req.version != VERSION {
               let data = Join::JoinResp(Some("Client and server versions don't match.".to_string()));
               self.sender.send(msg.reply(Data::Join(data)));
               return;
            }

            let data = Join::JoinResp(None);
            self.sender.send(msg.reply(Data::Join(data)));

            let log = self.log.new(o!("client" => format!("{}", addr)));
            let client = Client::<()>::new(log, addr, req.id, req.level,
                                           self.games[0].get().config().clone());

            self.limbo.add_client(client, self.games[0].get_weak());
         }
         Data::ServerInfo(Info::Request) => {
            let response = Data::ServerInfo(Info::Response(self.info()));
            self.sender.send(msg.reply(response));
         }
         _ => (),
      }
   }
}

