#![feature(nll)]

#[macro_use] extern crate slog;
#[macro_use] extern crate error_chain;
#[macro_use] extern crate lazy_static;
extern crate proto;
extern crate reffers;
extern crate fnv;
extern crate cgmath;
extern crate geom;
extern crate model;
extern crate net;
extern crate resources;

pub mod commands;
pub mod client;
pub mod logic;
pub mod game;
pub mod processors;

use slog::Logger as Log;

pub mod error {
   error_chain! {
      links {
         Model(::model::error::Error, ::model::error::ErrorKind);
         Proto(::proto::error::Error, ::proto::error::ErrorKind);
         Net(::net::error::Error, ::net::error::ErrorKind);
         Resources(::resources::error::Error, ::resources::error::ErrorKind);
      }
      foreign_links {
         ParseInt(::std::num::ParseIntError);
      }
      errors { }
   }
}
