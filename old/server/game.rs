use resources::*;
use model::shared_map::*;
use model::types::MapDescriptor;
use model::server_conf::*;
use model::team::*;
use model::entity::*;
use model::event::*;
use model::world::*;
use proto::message::*;
use proto::Sender;
use logic::*;
use processors::Destination;
use client::*;
use error::*;

use reffers::rc::*;
use std::net::SocketAddr;
use std::collections::HashMap;
use std::ops::{Index, IndexMut};

/// A bunch of state which connects clients with a `World`, forwarding their
/// events and propagating their entity changes.
pub struct Game {
   log: ::Log,
   /// A channel over which we send messages to players.
   sender: Sender,
   /// A model of the game world.
   pub world: World,
   /// Tracks player numbers.
   pub player_ids: PlayerIDs,
   /// The map the game is being played on.
   pub map: SharedMap,
   /// The configuration we send to the players. Note that any changes
   /// need to be sent to players as a config event.
   pub conf: ServerConf,
   /// The thing we delegate all the game logic to.
   pub mode: Strong<Box<Logic>>,
   /// Resources, which we need for initialising entities.
   pub resources: ResourceDir,
   /// A map of addresses to clients.
   pub clients: HashMap<SocketAddr, Client<Playing>>,
}

pub struct PlayerIDs { next: i16, }

impl Game {
   /// Create a new `Game`.
   pub fn new<M: Logic + 'static>(
      log: ::Log,
      sender: Sender,
      map: SharedMap,
      mode: M,
      mut resources: ResourceDir) -> Game
   {
      let log = log.new(o!("map" => map.path().to_string()));
      let conf = ServerConf::default();
      let entities = map.data().game_view().create_entities(&mut resources).unwrap();

      Game {
         clients: HashMap::default(),
         world: World::with_entities(log.clone(), entities, conf.clone()),
         player_ids: PlayerIDs::new(),
         mode: Strong::new(Box::new(mode)),
         map, conf, log, resources, sender
      }
   }

   /// Remove a client from the game.
   pub fn remove_client(&mut self, addr: SocketAddr, reason: String) {
      if !self.clients.contains_key(&addr) { return; }

      let ev = self.player(addr).disconnect_ev(reason);
      self.global_ev(ev).unwrap();
      self.clients.remove(&addr);
   }

   /// Get the world's representation of a player from the client's address.
   /// Panics if the address isn't in this game.
   pub fn player(&self, addr: SocketAddr) -> &Player {
      self.world.player(self[addr].player_no()).unwrap()
   }

   /// Apply and send an event to all clients
   pub fn global_ev(&mut self, mut event: Event) -> Result<()> {
      self.world.apply_ev(&mut event)?;
      self.send_ev(&event);
      Ok(())
   }

   /// Progress the game by a tick.
   pub fn tick(&mut self) {
      // Update the world & propagate entity deletions
      self.world.tick(&mut self.resources);

      for id in self.world.deleted_entities.drain(..) {
         self.clients.values_mut().for_each(|c| c.remove_entity(id));
      }

      // Distribute server-authoritative events
      while let Some(ev) = self.world.server_events.pop_front() {
         let mut mode = self.mode.get_mut();
         mode.on_server_event(self, &ev);
         self.send_ev(&ev)
      }

      // Update the game mode
      let mut mode = self.mode.get_mut();
      mode.tick(self);

      // Send scoreboard updates
      let ev = score_ev(self.log.clone(), &mut self.world);
      self.send_ev(&ev);

      // Process post-state from client messages, overwriting any changes made in
      // `world.tick()` for entities owned by clients which have changed (e.g. a
      // client's plane changing direction)
      self.clients.values_mut().for_each(Client::run_post_state);

      // Then we propagate entity changes between clients -- send each
      // client updates for the data we processed above.
      let mut changed_entities = Vec::new();

      for client in self.clients.values() {
         changed_entities.extend(client.changed_entities())
      }

      for client in self.clients.values_mut() {
         client.update_entities(&changed_entities);
      }

      // Finally we construct and send an update packet for each client,
      // which contains any events which have been added as well as the
      // entity data we just updated.
      for (&addr, c) in &mut self.clients {
         match c.tick() {
            Ok(data) => self.sender.send(Message::new(addr, data)),
            Err(e) => error!(c.log, "Failed to build a message"; "error" => %e),
         }
      }

      // Clean up any old entities and free their network IDs
      let clients = &mut self.clients;
      self.world.clean(|id| for c in clients.values_mut() { c.forget_entity(id) } );

      // Remove unresponsive clients
      let unresponsive: Vec<_> = self.clients.iter()
         .filter(|&(_, c)| c.unresponsive())
         .map(|(&addr, _)| addr)
         .collect();

      for addr in unresponsive {
         self.remove_client(addr, "Connection Lost".to_string());
      }
   }

   /// Handle an incoming message
   pub fn process(&mut self, msg: &Message) {
      let source = msg.addr();
      if !self.clients.contains_key(&source) { return; }

      if let Data::Game(ref data) = *msg.data() {
         self[source].receive(data.clone());

         while let Some(mut ev) = self[source].next_ev() {
            if let Err(e) = self.world.apply_ev(&mut ev) {
               error!(self.log, "Failed to handle event: {}", e);
               continue;
            }

            let mut mode = self.mode.get_mut();
            mode.on_event(self, source, &ev);

            if ev.should_broadcast() {
               for (&addr, client) in &mut self.clients {
                  if addr == source { continue; }
                  client.add_event(&ev);
               }
            }
         }
      }

      match *msg.data() {
         Data::Join(Join::Disconnect) => self.remove_client(source, "".to_string()),
         _ => (),
      }
   }

   /// Send an event to all clients.
   pub fn send_ev(&mut self, ev: &Event) {
      for client in self.clients.values_mut() {
         client.add_event(ev);
      }
   }

   /*
   /// Add a new entity, sending it to all clients.
   pub fn link_entity(&mut self, id: EntityID) {
      self.world.link_entity(id);
      for c in self.clients.values_mut() {
         c.link_entity(id);
      }
      let entity = self.world.entity(id).unwrap().clone();
      self.send_ev(&Event::NewEntity(NewEntity { id, entity }))
   }
   */
}

impl Destination for Game {
   /// The resource the client must load before being allowed to join.
   fn descriptor(&self) -> MapDescriptor {
      MapDescriptor {
         path: self.map.path().to_string(),
         size: self.map.size() as u32,
         crc32: self.map.crc32() as u64,
      }
   }

   /// Get the destination's configuration.
   fn config(&self) -> ServerConf { self.conf.clone() }

   /// Add a new client to the game.
   fn add_client(&mut self, client: Client<Loading>) {
      let addr = client.addr();

      let mut client = client.play(
         self.player_ids.join(addr),
         self.world.view(),
         self.map.data().game_view().size);

      client.update_conf(&self.conf);

      for player in self.world.players().values() {
         client.add_event(&Event::PlayerInfo(player.info.clone()));
      }

      let new_client_ev = Event::PlayerInfo(PlayerInfo{
         connected: true,
         id: client.player_no(),
         message: None,
         nick: client.id.nickname.clone(),
         vapor_id: client.id.vapor,
         unique: client.id.unique,
         setup: PlaneSetup::default(),
         team: Team::Spectator,
         level: client.level.level,
         ace: client.level.ace,
      });

      info!(self.log, "Adding a client {:?}", new_client_ev);

      self.clients.insert(addr, client);

      // Apply the PlayerInfo event to the world and broadcasts to all players
      self.global_ev(new_client_ev).unwrap();

      // Send the score board & game mode status, then assign the players ID and team
      let client = self.clients.get_mut(&addr).unwrap();
      client.add_event(&score_ev(self.log.clone(), &mut self.world));
      client.add_event(&self.mode.get().status_ev());
      client.init(Team::Red); // TODO: Get the assigned team from `mode`?

      // Map entities are already known to the player, so need to be in the correct
      // order. We add them to the `client` before any others then skip them while
      // iterating through the full set.
      for &id in self.world.get_map_ids() {
         client.link_entity(id);
      }

      // Send the current state of all dynamically created entities
      for &id in self.world.entity_set() {
         let entity = self.world.entity(id).unwrap().clone();
         if !entity.has_network_id() { continue; }

         if entity.creation_type() != CreationType::Map {
            println!("Sending entity {:?}", entity);
            client.add_event(&Event::NewEntity(NewEntity { id, entity }))
         }
      }
   }
}

impl Index<SocketAddr> for Game {
   type Output = Client<Playing>;
   fn index(&self, index: SocketAddr) -> &Client<Playing> {
      self.clients.get(&index)
         .expect("Invalid client address")
   }
}

impl IndexMut<SocketAddr> for Game {
   fn index_mut(&mut self, index: SocketAddr) -> &mut Client<Playing> {
      self.clients.get_mut(&index)
         .expect("Invalid client address")
   }
}

impl PlayerIDs {
   pub fn new() -> PlayerIDs {
      PlayerIDs { next: -1, }
   }

   pub fn join(&mut self, _addr: SocketAddr) -> i16 {

      self.next += 1;
      self.next
   }
}

fn score_ev(log: ::Log, world: &mut World) -> Event {
   use net::encode::*;
   use proto::rules::ScoreboardRule;
   Event::Scoreboard(encode(
      log,
      &ScoreboardRule { diff: false, prev: world.prev_scoreboard() },
      &Scoreboard {
         players: world.scoreboard()
      }
   ).expect("Scoreboard encoding failed."))
}
