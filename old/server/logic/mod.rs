use model::event::*;
use game::Game;
use std::net::SocketAddr;

pub mod ffa;
pub mod chat;
pub mod spawn;

/// Something which does game logic and stuff.
pub trait Logic {
   /// Get a game mode status event, sent to new players as they join,
   /// and possibly at other times. The type of the event must match
   /// the game mode inferred from the current map, or the client will
   /// be unable to parse it.
   fn status_ev(&self) -> Event;

   /// Called when we receive an event from a client.
   fn on_event(&mut self, _game: &mut Game, _addr: SocketAddr, _ev: &Event) {}

   /// Called when an event is sent by the server's world
   fn on_server_event(&mut self, _game: &mut Game, _ev: &Event) {}

   /// Called before a tick of the game loop.
   fn tick(&mut self, _game: &mut Game) {}
}