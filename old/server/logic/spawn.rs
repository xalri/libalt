use model::event::*;
use geom::point2;
use game::Game;
use std::net::SocketAddr;

// TODO: use spawn points from map, send different players to different spawn points, ...
pub struct Spawner { }

impl Spawner {
   pub fn new() -> Spawner {
      Spawner{ }
   }

   pub fn handle(&mut self, game: &mut Game, addr: SocketAddr, ev: &SpawnReq) {
      let resp = SpawnResp {
         team: ev.team.clone(),
         setup: ev.setup.clone(),
         pos: point2(500, 400),
         angle: 0,
         reason: "".to_string(),
         timeout: 0
      };
      game[addr].add_event(&Event::SpawnResp(resp));
   }
}
