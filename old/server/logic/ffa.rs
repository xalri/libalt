
use model::event::*;
use model::server_conf::FFAConfig;
use proto::rules::*;
use net::encode::encode;
use game::Game;
use super::Logic;
use super::spawn::*;
use super::chat::*;

use std::net::SocketAddr;

/// A free-for-all game.
/// Be the first to a set number of kills to win.
pub struct FFA {
   log: ::Log,
   config: FFAConfig,
   timers: GameTimers,
   spawn: Spawner,
   chat: ChatGroup,
}

impl FFA {
   pub fn new(log: ::Log, conf: FFAConfig) -> FFA {
      FFA {
         log: log.clone(),
         config: conf,
         timers: GameTimers {
            time: 0,
            game_end: 0,
            round_end: conf.round_time_seconds * 30,
            warmup_end: conf.warmup_time_seconds * 30,
         },
         spawn: Spawner::new(),
         chat: ChatGroup::new(),
      }
   }
}

impl Logic for FFA {
   fn status_ev(&self) -> Event {
      Event::GameModeStatus(encode(
         self.log.clone(),
         &GameTimerRule {
            round_time: Some(self.config.round_time_seconds * 30),
            warmup_time: self.config.warmup_time_seconds * 30
         },
         &FFAModeStatus { timers: self.timers.clone() }
      ).unwrap())
   }

   //fn on_server_event(&mut self, game: &mut Game, ev: &Event) { }

   fn on_event(&mut self, game: &mut Game, addr: SocketAddr, ev: &Event) {
      let log = match *ev{ Event::View(_) => false, _ => true };
      if log { info!(self.log, "Got event: {:?} : {:?}", addr, ev); }

      match *ev {
         Event::Chat(ref chat_ev) => self.chat.handle(game, addr, chat_ev),
         Event::SpawnReq(ref req) => self.spawn.handle(game, addr, req),
         _ => (),
      }
   }

   fn tick(&mut self, _: &mut Game) {
      self.timers.tick(); // tock.
   }
}
