use model::event::*;
use game::Game;
use std::net::SocketAddr;

// TODO: chat logging, etc..
pub struct ChatGroup { }

impl ChatGroup {
   pub fn new() -> ChatGroup { ChatGroup{} }

   pub fn handle(&mut self, game: &mut Game, addr: SocketAddr, chat_ev: &Chat) {
      let player_no = game[addr].player_no();
      let senders_team = game.world.player(player_no).unwrap().info.team;
      let team_msg = chat_ev.is_team;

      let ev = Event::Chat(Chat {
         player: game[addr].player_no(),
         is_team: team_msg,
         msg: chat_ev.msg.clone(),
      });

      for client in game.clients.values_mut() {
         let recipient_team = game.world.player(client.player_no()).unwrap().info.team;
         if !team_msg || recipient_team == senders_team {
            client.add_event(&ev);
         }
      }
   }
}
