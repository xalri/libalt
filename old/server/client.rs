use proto::sequencer::*;
use proto::message::*;
use proto::rules::*;
use proto::diff::*;
use proto::{CodingState, Context};
use model::types::*;
use model::server_conf::*;
use model::event::*;
use model::entity::*;
use model::entity::arena::*;
use model::team::*;
use proto::error::Result as ProtoResult;
use geom::*;
use error::*;

use std::cell::{Cell, RefCell};
use std::net::SocketAddr;
use std::time::*;
use fnv::*;

/// Handles communications with a single client.
///
/// Clients can be in one of two states, represented by the type parameter.
/// On accepting a join request, we create a `Client<()>`, with no state. On
/// sending a load request it becomes `Client<Loading>`, then when the player
/// has joined a game it's `Client<Playing>`.
pub struct Client<S> {
   pub log: ::slog::Logger,
   pub id: ClientID,
   pub level: ClientLevel,
   pub counter: u8,
   /// The client's address
   addr: SocketAddr,
   /// The last server config sent to the client
   conf: ServerConf,
   /// The client's state (loading, playing, etc)
   s: S,
}

/// Client state for loading a map.
pub struct Loading {
   /// The player's currently loading resource
   resource: MapDescriptor,
}

/// Client state for participating in a game.
pub struct Playing {
   /// Entities which the client has modified since the last update.
   entities_in: Vec<EntityID>,
   /// A number identifying the player within a game.
   player_no: i16,
   /// Sequences events into groups.
   sequencer: RefCell<Sequencer>,
   /// Incoming updates which are queued to run on the next tick.
   post_state: Vec<PostState>,
   /// Entities which we will send updates for in the next message.
   entities_out: Vec<EntityID>,
   /// The number of game ticks for which the client has been connected.
   tick: u64,
   /// The number of updates written since we last received a message.
   unresponsive: u64,
   /// The size of the map, used in event encoding
   map_size: Dimensions,
   /// An arena view, through which we get, create and modify entities.
   view: View,
   /// Maps between altitude's entity ID format and our own, and manages
   /// client local IDS.
   id_map: IDManager,
   /// IDs of entities created in recent events
   new_ids: RefCell<Vec<EntityID>>,
   /// The instant we last sent a `Count` event.
   last_count: Instant,
}

impl<T> Client<T> {
   /// Create a new `Client`
   pub fn new(log: ::Log, addr: SocketAddr, id: ClientID, level: ClientLevel, conf: ServerConf) -> Client<()> {
      Client { log, addr, conf, level, id, s: (), counter: 0 }
   }

   /// Start loading a map, discards any current state.
   pub fn load(self, resource: MapDescriptor, conf: ServerConf) -> Client<Loading> {
      let counter = if self.counter < 3 { self.counter + 1 } else { 0 };
      Client {
         s: Loading{ resource },
         conf,
         counter,
         log: self.log,
         addr: self.addr,
         level: self.level,
         id: self.id,
      }
   }

   /// Get the address of the client.
   pub fn addr(&self) -> SocketAddr { self.addr }

   /// Get the last `ServerConf` sent to the client.
   pub fn config(&self) -> &ServerConf { &self.conf }
}

impl Client<Loading> {
   /// Join the game.
   pub fn play(self, player_no: i16, view: View, map_size: Dimensions) -> Client<Playing> {
      Client {
         s: Playing {
            player_no,
            map_size,
            sequencer: RefCell::new(Sequencer::new(self.log.clone(), self.counter)),
            post_state: Vec::with_capacity(32),
            entities_in: Vec::with_capacity(32),
            entities_out: Vec::with_capacity(32),
            tick: 0,
            unresponsive: 0,
            view,
            id_map: IDManager::new(self.log.clone()),
            last_count: Instant::now(),
            new_ids: RefCell::new(Vec::with_capacity(32)),
         },
         counter: self.counter,
         conf: self.conf,
         log: self.log,
         addr: self.addr,
         id: self.id,
         level: self.level,
      }
   }

   /// Get the resource for the game when the load started.
   pub fn resource(&self) -> MapDescriptor { self.s.resource.clone() }
}

impl Client<Playing> {
   /// Send the player an `Init` event, gives their player number and initial team.
   /// The given team is that which the player will first attempt to spawn with, not
   /// the team they're assigned to (which is probably the spectator team).
   pub fn init(&mut self, team: Team) {
      let player = self.s.player_no;
      self.add_event(&Event::AssignPlayerID(AssignPlayerID { player, team }));
   }

   /// Send an event to this client, adding any created entities to our view.
   pub fn add_event(&mut self, ev: &Event) {
      for id in ev.new_ids() {
         self.link_entity(id);
      }
      debug!(self.log, "adding event"; "event" => ?ev);
      let res = self.s.sequencer.borrow_mut().add_event(EvRule(self as &CodingState), ev);

      if let Err(e) = res {
         error!(self.log, "Failed to encode event"; "event" => ?ev, "error" => %e);
      }
   }

   /// Handle an incoming message, adding new events to the given queue.
   pub fn receive(&mut self, mut data: Game) {
      let up_to_date = self.s.sequencer.borrow_mut().receive(&mut data);
      self.s.unresponsive = 0;

      // IDs for player-created entities are assigned during decoding,
      // since we need a valid `EntityID` for if the entity is referenced
      // later in the same message.
      // Here we check for anything new and send an event telling the
      // client the 'global' ID of their new entity. (in actuality the server
      // has an extra layer of abstraction over IDs, so what we call the
      // 'global' ID is just the ID the server will use when talking to
      // this client about the entity, other clients may have a different
      // ID if we're showing them a different subset of the world)
      while let Some(map_ev) = self.s.id_map.pop_ev() {
         println!("Sending entity map ev {:?}", map_ev);
         let ev = Event::EntityMap(map_ev);
         self.add_event(&ev);
      }

      if up_to_date {
         // Read the entity update section of the message
         if let Some(data) = data.entity_data {
            let msg_id = self.s.sequencer.borrow().in_seq();
            let log = self.log.new(o!("msg_id" => msg_id));
            match read_diffs(&log, data, self,
                             |id| Ok(self.s.view.entity_mut(id)?),
                             |id| Ok(self.s.view.entity_meta(id)?), msg_id)
            {
               Ok(post_state) => self.s.post_state.push(post_state),
               Err(e) => error!(log, "Failed to read diffs: {}", e),
            }
         }
      }
   }

   /// Parse and take the next event from the sequencer, returns `None` if there are none.
   pub fn next_ev(&mut self) -> Option<Event> {
      loop {
         let ev = { self.s.sequencer.borrow_mut().next_ev(EvRule(self as &CodingState)) };

         if self.s.id_map.is_invalid() {
            warn!(self.log, "Event contains invalid entity ID, ignoring: {:?}", ev);
            continue;
         }

         // Add any entities created by this event to our view
         if let Some(ref ev) = ev {
            for id in ev.new_ids() { self.s.view.link_entity(id) }
         }

         return ev;
      }
   }

   /// Update the world from post-state data sent by the client. Runs between
   /// the arena's update and the sending of game messages.
   pub fn run_post_state(&mut self) {
      self.s.entities_in.clear();
      let post_state = self.s.post_state.drain(..).collect::<Vec<_>>();
      for x in post_state {
         let log = self.log.new(o!("msg_id" => x.msg_id));
         match x.process(&log, self.map_size(), |id| Ok(self.s.view.entity_mut(id)?)) {
            Ok(ids) => {
               for (id, apply) in ids {
                  if apply { self.s.entities_in.push(id); }
               }
            }
            Err(e) => error!(log, "Failed to parse post state: {}", e),
         }
      }
   }

   /// Create the next message to be sent to the client.
   pub fn tick(&mut self) -> Result<Data> {
      self.s.unresponsive += 1;

      if self.s.last_count.elapsed() > Duration::from_secs(1) {
         self.s.last_count = Instant::now();
         let tick = self.s.tick as u16;
         self.add_event(&Event::Count(tick));
      }

      // Create a new message to send to the client.
      let mut data = self.s.sequencer.borrow_mut().make_msg()?;
      if !self.s.entities_out.is_empty() {
         let entities_in = &self.s.entities_in;
         self.s.entities_out.sort();
         self.s.entities_out.dedup();
         self.s.entities_out.retain(|id| !entities_in.contains(id));
         data.entity_data = Some(
            write_diffs(&self.log, &self.s.entities_out, self)?);
      }
      self.s.entities_out.clear();

      self.s.tick += 1;
      Ok(Data::Game(data))
   }

   /// Get the entities updated by the client in the last tick
   pub fn changed_entities(&self) -> &[EntityID] {
      &self.s.entities_in
   }

   /// Add some entities to be updated in the next packet we send.
   pub fn update_entities(&mut self, ids: &[EntityID]) {
      self.s.entities_out.extend_from_slice(ids)
   }

   /// Forget about an entity, allowing the ID we assigned to be re-used.
   /// This should only be called some time _after_ an entity is queued to be
   /// removed, when the arena has actually deleted the entity.
   pub fn forget_entity(&mut self, id: EntityID) {
      self.s.id_map.free_id(id);
   }

   /// Remove an entity from the client's view -- note that this does not
   /// delete the entity itself.
   pub fn remove_entity(&mut self, id: EntityID) {
      self.s.view.forget(id);
      self.add_event(&Event::RemoveEntity(id));
   }

   /// Check if the client is unresponsive.
   pub fn unresponsive(&self) -> bool {
      self.s.unresponsive > 150
   }

   /// Get the client's player number.
   pub fn player_no(&self) -> i16 { self.s.player_no }

   /// Send the client an updated `ServerConf`.
   pub fn update_conf(&mut self, conf: &ServerConf) {
      if &self.conf != conf {
         self.add_event(&Event::ServerConfig(conf.clone()));
         self.conf = conf.clone();
      }
   }

   /// Send an existing entity to the client.
   pub fn link_entity(&mut self, id: EntityID) {
      self.s.view.link_entity(id);
      self.s.id_map.link_entity(id);
   }
}

impl IDMap for Client<Playing> {
   fn read_id(&self, id: u16, client_local: bool) -> EntityID {
      self.s.id_map.read_id(id, client_local)
   }
   fn write_id(&self, id: EntityID) -> ::model::error::Result<(u16, bool)> {
      Ok(self.s.id_map.write_id(id)?)
   }
   fn reserve_id(&self, id: u16) -> EntityID {
      let id = self.s.id_map.new_id(&self.s.view, id);
      self.s.new_ids.borrow_mut().push(id);
      id
   }
}

impl CodingState for Client<Playing> {
   fn context(&self) -> Context { Context::Server }
   fn map_size(&self) -> Dimensions { self.s.map_size }
   fn entity(&self, id: EntityID) -> ProtoResult<ReadGuard<Entity>> { Ok(self.s.view.entity(id)?) }
   fn entity_set(&self) -> &[EntityID] { self.s.view.entity_set() }
}

/// Manages entity IDs as seen by the client.
struct IDManager {
   log: ::Log,
   map_evs: RefCell<Vec<EntityMap>>,
   server_ids: RefCell<EntityIDMap>,
   client_ids: RefCell<EntityIDMap>,
   next_id: Cell<u16>,
   invalid_id: Cell<bool>,
}

/// The IDMap data structure, which isn't the IDMap trait. mmh.
struct EntityIDMap {
   a_b: FnvHashMap<u16, EntityID>,
   b_a: FnvHashMap<EntityID, u16>,
}

impl IDManager {
   pub fn new(log: ::Log) -> IDManager {
      IDManager {
         log,
         map_evs: RefCell::new(Vec::with_capacity(32)),
         server_ids: RefCell::new(EntityIDMap::new()),
         client_ids: RefCell::new(EntityIDMap::new()),
         next_id: Cell::new(0),
         invalid_id: Cell::new(false),
      }
   }

   pub fn pop_ev(&mut self) -> Option<EntityMap> {
      self.map_evs.borrow_mut().pop()
   }

   pub fn link_entity(&self, id: EntityID) -> u16 {
      if self.server_ids.borrow().contains_eid(id) {
         panic!("IDManager::link_entity called twice for the same entity");
      }

      let my_id = self.next_id();
      info!(self.log, "mapping server entity ID"; "server_id" => my_id, "id" => %id);
      self.server_ids.borrow_mut().insert(my_id, id);
      my_id
   }

   pub fn free_id(&self, id: EntityID) {
      if let Some(id) = self.server_ids.borrow_mut().remove_eid(id) {
         self.next_id.set(id);
      }
   }

   pub fn read_id(&self, id: u16, client_local: bool) -> EntityID {
      (if client_local { self.client_ids.borrow() }
         else { self.server_ids.borrow() }
      ).get_eid(id).unwrap_or_else(|| {
         // We need to continue to read the event even if the ID is invalid,
         // so that we can parse the rest of the events in the packet and only
         // discard this one. We set a flag for an invalid ID and return a
         // placeholder.
         self.invalid_id.set(true);
         warn!(self.log, "Read invalid ID: {} {}", id, client_local);
         INVALID_ID
      })
   }

   pub fn is_invalid(&self) -> bool {
      let value = self.invalid_id.get();
      self.invalid_id.set(false);
      value
   }

   pub fn write_id(&self, eid: EntityID) -> ::model::error::Result<(u16, bool)> {
      if let Some(id) = self.server_ids.borrow().get_id(eid) {
         Ok((id, false))
      } else {
         bail!("Unknown entity ID: {:?}", eid)
      }
   }

   pub fn new_id(&self, view: &View, id: u16) -> EntityID {
      let reserved = view.reserve();
      let my_id = self.link_entity(reserved);
      debug!(self.log, "mapping local {} -> global {}", id, my_id);
      self.map_evs.borrow_mut().push(EntityMap { local: id, global: my_id });
      debug!(self.log, "adding {} -> {:?} to client_ids", id, reserved);
      self.client_ids.borrow_mut().insert(id, reserved);
      reserved
   }

   fn next_id(&self) -> u16 {
      let start = self.next_id.get();
      let mut id = self.next_id.get();
      while self.server_ids.borrow().contains_id(id) {
         id += 1;
         if id >= 1024 { id = 0; }
         if id == start { panic!("no more free entity IDs :("); }
      }
      self.next_id.set(id + 1);
      id
   }
}

impl EntityIDMap {
   pub fn new() -> EntityIDMap {
      EntityIDMap {
         a_b: FnvHashMap::with_capacity_and_hasher(256, Default::default()),
         b_a: FnvHashMap::with_capacity_and_hasher(256, Default::default()),
      }
   }

   pub fn contains_id(&self, id: u16) -> bool { self.a_b.contains_key(&id) }
   pub fn get_id(&self, eid: EntityID) -> Option<u16> { self.b_a.get(&eid).cloned() }

   pub fn contains_eid(&self, eid: EntityID) -> bool { self.b_a.contains_key(&eid) }
   pub fn get_eid(&self, id: u16) -> Option<EntityID> { self.a_b.get(&id).cloned() }

   pub fn remove_eid(&mut self, eid: EntityID) -> Option<u16> {
      if let Some(id) = self.b_a.remove(&eid) { self.a_b.remove(&id); Some(id) }
         else { None }
   }

   pub fn insert(&mut self, id: u16, eid: EntityID) {
      self.a_b.insert(id, eid);
      self.b_a.insert(eid, id);
   }
}
