//! A simple file server for sending maps to clients.

use std::collections::{HashMap, VecDeque};
use std::net::SocketAddr;
use std::sync::Arc;
use std::cmp;
use reffers::rc::Strong;

use proto::message::*;
use proto::Sender;

type Cache = Strong<HashMap<String, DlResource>>;

/// A simple file server which implements altitude's protocol to share resources.
/// Files are not loaded from the file system, only from memory.
/// There is no download limit, the entire file is sent at once and each packet
/// written to the socket as soon as it's writable.
pub struct FileServer {
   log: ::slog::Logger,
   /// Currently active instances
   connections: HashMap<SocketAddr, Instance>,
   /// Cache of file data ready to be sent
   cache: Cache,
   sender: Sender,
}

/// The connection state for a single client.
struct Instance {
   id: u8,
   resource: String,
   cache: Cache,
   chunk_queue: VecDeque<u32>,
   /// An indicator of download progress?
   _unk: f32,
}

/// Some data split into chunks to be sent to clients.
struct DlResource {
   data: Vec<Arc<(Vec<u8>, u64)>>,
}

impl FileServer {
   /// Create a new FileServer.
   pub fn new(log: ::Log, sender: Sender) -> FileServer {
      FileServer {
         log: log,
         connections: HashMap::default(),
         cache: Strong::new(HashMap::default()),
         sender: sender,
      }
   }

   /// Add a new resource to the cache.
   pub fn add_to_cache(&mut self, resource: String, data: Vec<u8>) {
      self.cache.get_mut().insert(resource, DlResource::new(data));
   }

   /// Handle an incoming message
   pub fn process(&mut self, msg: &Message) {
      if let Data::Dl(ref message) = *msg.data() {
         let addr = msg.addr();

         let sender = &self.sender;
         let send = |dl: Download| {
            sender.send(Message::new_deliver(addr, Data::Dl(dl)));
         };

         match *message {
            Download::Request(ref req) => {
               if !self.cache.get().contains_key(&req.resource) {
                  warn!(self.log, "Got a request for an unknown resource";
                  "resource" => &req.resource);
                  return;
               }

               let cache = self.cache.clone();
               let c = self.connections.entry(addr)
                  .or_insert_with(|| Instance::new(cache));

               c.set_resource(req.id, req.resource.clone());
               send(Download::Accept(req.id));

               let to_send = c.remaining();
               c.send_chunks(send, to_send);
            }
            Download::Missing(ref missing) => {
               let mut remove = false;
               if let Some(c) = self.connections.get_mut(&addr) {
                  c.add_missing(missing.clone());
                  remove = c.remaining() == 0;
                  let to_send = c.remaining();
                  c.send_chunks(send, to_send);
               }
               if remove {
                  self.connections.remove(&addr);
               }
            }
            _ => (),
         }
      }
   }
}

impl Instance {
   /// Create a new instance.
   fn new(cache: Cache) -> Instance {
      Instance {
         id: 0,
         resource: "".to_string(),
         chunk_queue: VecDeque::new(),
         cache: cache,
         _unk: 0.,
      }
   }

   /// Get the number of chunks waiting to be sent.
   fn remaining(&self) -> usize {
      self.chunk_queue.len()
   }

   /// Set the next resource to upload, overwriting any current upload.
   fn set_resource(&mut self, id: u8, resource: String) {
      if let Some(res) = (*self.cache.get()).get(&resource) {
         let chunks = res.data.len() as u32;
         self.id = id;
         self.resource = resource;
         self.chunk_queue = (0..chunks).collect();
      }
   }

   /// Add chunks the client has reported missing back to the queue.
   fn add_missing(&mut self, msg: DlMissing) {
      self._unk = msg.unk;
      self.chunk_queue.append(&mut msg.chunks.into());
   }

   /// Send chunks to the client up to a given maximum.
   fn send_chunks<F: Fn(Download)>(&mut self, send: F, mut to_send: usize) {
      if to_send == 0 { return }
      while let Some(chunk) = self.next_chunk() {
         send(Download::Data(chunk));
         to_send -= 1;
         if to_send == 0 && self.remaining() != 0 { return }
      }
      send(Download::Complete(self.id));
   }

   /// Get the next chunk to send
   fn next_chunk(&mut self) -> Option<DlData> {
      self.chunk_queue.pop_front().and_then(|c| {
         (*self.cache.get()).get(&self.resource).map(|resource| {
            DlData {
               id: self.id,
               chunk: c,
               data: resource.data[c as usize].clone(),
            }
         })
      })
   }
}

impl DlResource {
   /// Split the given data into chunks, creating a new DlResource.
   fn new(data: Vec<u8>) -> DlResource {
      let chunks = ((data.len() as i64) - 1) / 512 + 1;

      let chunk_data = (0..chunks).map(|c| {
         let end = cmp::min((c as usize + 1) * 512, data.len());
         let len = 8 * (end as u64 - (c as u64 * 512));
         Arc::new((data[(c as usize * 512)..end].to_owned(), len))
      }).collect::<Vec<_>>();

      DlResource {
         //bytes: data.len(),
         data: chunk_data,
      }
   }
}

