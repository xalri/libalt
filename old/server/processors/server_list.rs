
use std::time::*;
use std::net::SocketAddr;
use proto::message::*;
use proto::Sender;

lazy_static!{
   /// The current address of the official vapor server.
   pub static ref VAPOR_ADDR: SocketAddr = ([34, 195, 1, 175], 31383).into();
}

/// The period in seconds at which to send server list keepalive messages
const KEEPALIVE_S: u64 = 240;

/// Connects to and registers with the master server list as a server.
pub struct ListJoiner {
   log: ::Log,
   sender: Sender,
   registered: bool,
   last_keepalive: Instant,
}

impl ListJoiner {
   /// Create a new `ListJoiner`
   pub fn new(log: ::Log, sender: Sender) -> ListJoiner {
      ListJoiner {
         log,
         sender,
         registered: false,
         last_keepalive: Instant::now(),
      }
   }

   /// Request to join the master server list.
   pub fn register(&self) {
      let m = Data::RegisterServer(RegisterServer::AuthReq(1));
      self.sender.send(Message::new_order(*VAPOR_ADDR, m));
   }

   /// Returns true if the server is registered with the master server.
   pub fn is_registered(&self) -> bool {
      self.registered
   }

   /// Run this every tick, sends keepalive messages at a regular interval.
   pub fn update(&mut self) {
      if self.last_keepalive.elapsed() > Duration::from_secs(KEEPALIVE_S) {
         self.keepalive()
      }
   }

   /// Send a keepalive message to the master server
   fn keepalive(&mut self) {
      self.last_keepalive = Instant::now();
      if !self.registered { return; }
      let keepalive = Data::RegisterServer(RegisterServer::Keepalive);
      self.sender.send(Message::new_order(*VAPOR_ADDR, keepalive));
   }

   /// Process an incoming message
   pub fn process(&mut self, msg: &Message) {
      if let Data::RegisterServer(ref data) = *msg.data() {
         if msg.addr() != *VAPOR_ADDR {
            warn!(self.log, "Got server list message from non-vapor server {}", msg.addr());
            return;
         }

         match *data {
            RegisterServer::AuthResp(ref resp) => {
               self.registered = false;
               if resp.is_listed {
                  info!(self.log, "We're already listed on the server list"; "remote_addr" => ?resp.addr);
                  self.registered = true;
               } else if !resp.is_ok {
                  let err = resp.message.clone().unwrap_or_else(|| "unknown".into());
                  error!(self.log, "Server list request error: {}", err);
               } else if let Some(uuid) = resp.uuid {
                  let hello = Message::new_deliver(*VAPOR_ADDR,
                                                   Data::RegisterServer(RegisterServer::Hello(uuid)));
                  self.sender.send(hello);
               } else {
                  error!(self.log, "Server list resp has no UUID, can't send hello.");
               }
            }
            RegisterServer::HelloResp(ref resp) => {
               if resp.a && resp.b {
                  let err = resp.message.clone().unwrap_or_else(|| "unknown".into());
                  error!(self.log, "Server list hello error: {}", err);
               } else {
                  info!(self.log, "Successful server list hello"; "remote_addr" => ?resp.addr);
                  self.last_keepalive = Instant::now();
                  self.registered = true;
               }
            }
            RegisterServer::KeepaliveResp(ref resp) => {
               if let Some(e) = resp.message.clone() {
                  warn!(self.log, "Server list keepalive error, attempting to re-register";
                  "error" => e);
                  self.register();
                  self.registered = false;
               }
            }
            _ => ()
         }
      }
   }
}

