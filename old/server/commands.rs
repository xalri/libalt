
use std::str::FromStr;
use std::net::IpAddr;
use std::time::Duration;
use model::types::UUID;
use error::*;

#[derive(Debug, Clone, PartialEq)]
pub enum Command {
   Vote(Box<Command>),
   CastBallot(u32),
   ChangeMap(String),
   Ban(BanID, Duration, String),
   Block(BanID, Duration, String),
   Kick(String),
   Drop(String),
   ListBans,
   ListBlocks,
   RemoveBan(BanID),
   RemoveBlock(BanID),

   //AssignTeam,
   //BalanceTeams,
   //WeaponMode,
   //GravityMode,
   //ListMaps,
   //ListPlayers,
   //LogPlanes,
   //LogStatus,
   //ModifyTournament,
   //NextMap,
   //SetBallScore,
   //SetSpawnPoint,
   //PlaneScale,
   //Rcon,
   //ChangeServer,
   //SelectMap,
   //ServerMsg,
   //ServerWhisper,
   //StartTournament,
   //StopTournament,
   //DisableSpawn,
   //SetEnergyMod,
   //SetHealthMod,
   //SetViewScale,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BanID {
   IP(IpAddr),
   Vapor(UUID),
   Nickname(String),
}

impl FromStr for BanID {
   type Err = Error;
   fn from_str(s: &str) -> Result<BanID> {
      match UUID::from_str(s) {
         Ok(id) => Ok(BanID::Vapor(id)),
         Err(_) => match IpAddr::from_str(s) {
            Ok(ip) => Ok(BanID::IP(ip)),
            Err(_) => bail!("Invalid ban ID '{}'", s),
         }
      }
   }
}

fn parse_ban(parts: &[String]) -> Result<(Duration, String)> {
   if parts.len() < 3 { bail!("Missing parameters"); }

   let value: u64 = parts[0].parse()?;
   let unit: DurationUnit = parts[1].parse()?;
   let duration = Duration::from_secs(value * unit.seconds());
   let reason = parts[2..].join(" ");

   Ok((duration, reason))
}

impl FromStr for Command {
   type Err = Error;
   fn from_str(s: &str) -> Result<Command> {
      let vote = s.starts_with("vote");
      let parts = split(if !vote { s } else { s[4..].trim() });
      if parts.is_empty() {
         bail!("Invalid command '{}'", s);
      }

      let params = match parts[0].as_str() {
         "castBallot" => 2,
         "changeMap" => 2,
         "ban" => 5,
         "addBan" => 5,
         "block" => 5,
         "addBlock" => 5,
         "kick" => 2,
         "drop" => 2,
         "listBans" => 1,
         "listBlocks" => 1,
         "removeBan" => 2,
         "removeBlock" => 2,
         x => bail!("unknown command {}", x),
      };
      if parts.len() < params { bail!("Missing parameters"); }

      let command = match parts[0].as_str() {
         "castBallot" => Command::CastBallot(parts[1].parse()?),
         "changeMap" => Command::ChangeMap(parts[1].clone()),
         "ban" => {
            let (duration, reason) = parse_ban(&parts[2..])?;
            Command::Ban(BanID::Nickname(parts[1].clone()), duration, reason)
         }
         "addBan" => {
            let (duration, reason) = parse_ban(&parts[2..])?;
            Command::Ban(parts[1].as_str().parse()?, duration, reason)
         }
         "block" => {
            let (duration, reason) = parse_ban(&parts[2..])?;
            Command::Block(BanID::Nickname(parts[1].clone()), duration, reason)
         }
         "addBlock" => {
            let (duration, reason) = parse_ban(&parts[2..])?;
            Command::Block(parts[1].as_str().parse()?, duration, reason)
         }
         "kick" => Command::Kick(parts[1].clone()),
         "drop" => Command::Drop(parts[1].clone()),
         "listBans" => Command::ListBans,
         "listBlocks" => Command::ListBlocks,
         "removeBan" => Command::RemoveBan(parts[1].parse()?),
         "removeBlock" => Command::RemoveBlock(parts[1].parse()?),
         x => bail!("unknown command {}", x),
      };

      Ok(if vote { Command::Vote(Box::new(command)) } else { command })
   }
}

pub fn split(s: &str) -> Vec<String> {
   let mut parts = Vec::new();
   let mut next = String::new();
   let mut escape = false;
   let mut quote = false;

   for c in s.chars() {
      if escape {
         next.push(c);
         escape = false;
      } else if c == '\\' {
         escape = true;
      } else if quote && c != '\"' {
         next.push(c);
      } else if c.is_whitespace() {
         if next.len() > 0 && !next.chars().all(char::is_whitespace) {
            parts.push(next.clone());
            next.clear();
         }
      } else if c == '\"' {
         quote = !quote;
      } else {
         next.push(c);
      }
   }

   if next.len() > 0 {
      parts.push(next);
   }
   parts
}

#[derive(Clone, Copy)]
enum DurationUnit {
   Minute, Hour, Day, Week, Forever
}

impl FromStr for DurationUnit {
   type Err = Error;
   fn from_str(s: &str) -> Result<Self> {
      Ok(match s {
         "minute" => DurationUnit::Minute,
         "hour" => DurationUnit::Hour,
         "day" => DurationUnit::Day,
         "week" => DurationUnit::Week,
         "forever" => DurationUnit::Forever,
         x => bail!("Invalid duration unit: {}", x),
      })
   }
}

impl DurationUnit {
   fn seconds(&self) -> u64 {
      match *self {
         DurationUnit::Minute => 60,
         DurationUnit::Hour => 3600,
         DurationUnit::Day => 86_400,
         DurationUnit::Week => 604_800,
         DurationUnit::Forever => 31_536_000, // Close enough, right? :3
      }
   }
}

#[cfg(test)]
mod test {
   use super::*;

   #[test]
   fn parse_commands() {
      assert_eq!(
         Command::Vote(Box::new(Command::Ban(
            BanID::Nickname("a b".to_string()),
            Duration::from_secs(20 * 60),
            "some reason".to_string()))),
         "vote ban \"a b\" 20 minute some reason".parse().unwrap());
   }
}
