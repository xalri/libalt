
use std::net::SocketAddr;
use std::collections::VecDeque;
use std::cmp;
#[allow(unused_imports)] use log::*;
use esc::geom::*;
use esc::net::socket::Sender;
use esc::net::id::*;

use altx::*;
use model::world::*;
use model::team::*;
use model::events::*;
use model::components::*;
use model::storage::*;
use proto::*;

use client::*;
use limbo::Destination;

/// Connects a bunch of clients in a game world.
pub struct ServerState {
   pub storage: *mut Storage,
   pub ids: ServerIDMap,
   pub map_size: Vec2,
   pub world: World,
   pub clients: Vec<Client<Playing>>,
   next_player_no: i16,
   pending_evs: VecDeque<WorldEv>,
   sender: Sender<Message>,
   current_client_idx: Option<usize>,
   local_to_global: Vec<(u16, u16)>,
}

// A ServerState is a place clients can end up after joining.
impl Destination for ServerState {
   /// The resource the client must load before joining.
   fn descriptor(&self) -> MapDescriptor {
      self.world.map_descriptor()
   }

   /// Add a new client.
   fn add_client(&mut self, client: Client<Loading>) {
      self.remove_client(client.addr, "disconnected");

      let remote = self.ids.new_remote();
      let snapshot = self.world.sync_state(unsafe { remote.ptr_rule() });

      let mut client = client.play(remote, self.map_size);
      client.player_no = self.next_player_no;
      self.next_player_no += 1;

      for ev in snapshot.events {
         client.send_ev(&ServerEv::World(ev));
      }
      let player_no = client.player_no;
      client.send_ev(&ServerEv::SyncComplete { player_no });
      let msg = client.tick().unwrap();
      self.sender.send(Message::new(client.addr, MessageData::Game(msg)));
      self.clients.push(client);
   }
}

impl ServerState {
   pub fn new(arena: Arena, sender: Sender<Message>) -> ServerState {
      let map_size = arena.map_size;
      let mut world = World::new(arena);
      let storage: *mut _ = &mut *world.storage;
      let mut state = ServerState {
         storage,
         world,
         map_size,
         ids: ServerIDMap::new(),
         sender,
         clients: vec![],
         pending_evs: VecDeque::default(),
         current_client_idx: None,
         next_player_no: 1,
         local_to_global: vec![],
      };
      // Maps network IDs for components in the arena.
      while let Some(ModelEv::NewComponent{ net_id, ptr }) = state.world.outgoing_events.pop() {
         state.ids.map(net_id, ptr);
         state.ids.next_id = cmp::max(state.ids.next_id, net_id.id + 1);
      }
      state
   }

   pub fn tick(&mut self, res: &AtlasSource) {

      let mut component_range = vec![];
      let mut components = vec![];

      for c in &mut self.clients {
         component_range.push((components.len(), 0));
         c.decode_state(&mut components);
         component_range.last_mut().unwrap().1 = components.len();
      }

      for c in &components { c.apply_pre() }
      self.world.tick();
      self.handle_world_evs(res);
      for c in &components { c.apply_post() }

      while let Some(e) = self.pending_evs.pop_front() {
         self.ev(e, res, -1)
      }

      let mut unresponsive = vec![];

      for i in 0..self.clients.len() {
         let c = &mut self.clients[i];
         let (begin, end) = component_range[i]; // The range of components to exclude
         for j in 0..components.len() {
            if j < begin || j >= end {
               c.send_state(components[j].clone())
            }
         }
         let msg = c.tick().unwrap();
         self.sender.send(Message::new(c.addr, MessageData::Game(msg)));
         if c.unresponsive() {
            warn!("Removing unresponsive client {}", c.id);
            unresponsive.push(c.addr);
         }
      }

      for addr in unresponsive { self.remove_client(addr, "unresponsive") }
   }

   pub fn remove_client(&mut self, addr: SocketAddr, _reason: &str) {
      let storage = unsafe{ &mut *self.storage };
      if let Some(idx) = self.client_from_addr(addr) {
         let id = self.clients[idx].player_no;

         for p in storage.planes.ref_iter() {
            if p.player == id {
               let pos = p.body.t.position;
               let vel = p.body.t.linear_vel * 0.5;
               self.pending_evs.push_back(WorldEv::DropItem { plane: p, pos, vel });
               self.pending_evs.push_back(WorldEv::RemoveComponent { id: RemovableId::Plane(p) });
               break;
            }
         }

         self.clients.remove(idx);
      }
   }

   /// Find a client from an address
   pub fn client_from_addr(&self, addr: SocketAddr) -> Option<usize> {
      for (i, c) in self.clients.iter().enumerate() {
         if c.addr == addr { return Some(i) }
      }
      None
   }

   /// Process an incoming message
   pub fn process(&mut self, msg: &Message, res: &AtlasSource) {
      if let MessageData::Game(data) = msg.data() {

         if let Some(idx) = self.client_from_addr(msg.addr()) {

            self.clients[idx].receive(data.clone());

            self.current_client_idx = Some(idx);
            while let Some(ev) = self.clients[idx].next_ev() {
               debug!("Got event: {:?}", ev);
               match ev {
                  ClientEv::World(ev) => {
                     self.world.handle_event(ev.clone(), res);
                     self.handle_world_evs(res);

                     let server_ev = ServerEv::World(ev);

                     // The event may contain new components, which include client-local IDs.
                     // handle_world_evs populates self.local_to_global such that the NetIDRule
                     // can encode using the global IDs.
                     for i in 0..self.clients.len() {
                        if i == idx { continue }
                        debug!("Sending with map: {:?}", self.local_to_global);
                        let c = &mut self.clients[i];
                        let mut rule = unsafe { c.ev_rule() };
                        rule.id_rule = NetIDRule { local_to_global: Some(&self.local_to_global[..]) };
                        c.send_ev_with_rule(&server_ev, rule);
                     }
                  }
                  ClientEv::PowerupCollide { plane, powerup, pos, vel } => {
                     let feasible = powerup.alive()
                        && powerup.alive
                        && (powerup.is_autouse || plane.item.is_none())
                        && (powerup.team == Team::Spectator || plane.team == powerup.team);

                     if feasible {
                        self.ev(WorldEv::PowerupPickup { plane, powerup, pos, vel, shatter_container: true }, res, -1);
                     }
                  }
               }
               self.handle_world_evs(res);
            }
            self.current_client_idx = None;
         } else {
            warn!("Got game message from unknown address {}", msg.addr());
         }
      }
   }

   /// Apply and forward a world event
   pub fn ev(&mut self, ev: WorldEv, res: &AtlasSource, source: i16) {
      self.world.handle_event(ev.clone(), res);
      self.handle_world_evs(res);

      let server_ev = ServerEv::World(ev);
      for c in &mut self.clients {
         if c.player_no != source { c.send_ev(&server_ev); }
      }
   }

   /// Handle new internal events from the simulation.
   fn handle_world_evs(&mut self, res: &AtlasSource) {
      self.local_to_global.clear();
      while let Some(ev) = self.world.outgoing_events.pop() {
         match ev {
            ModelEv::NewComponent { net_id, ptr } => {
               if let Some(idx) = self.current_client_idx {
                  // TODO: a broken client may send non-client local id, so we probably shouldn't panic for that.
                  let global_id = self.clients[idx].id_map().map(net_id, ptr);
                  self.local_to_global.push((net_id.id, global_id.id));
                  self.clients[idx].send_ev(&ServerEv::IDMap { global: global_id, local: net_id });
               } else {
                  self.ids.map(net_id, ptr)
               }
            }
            ModelEv::PowerupSpawnerTimeout { mut spawner, kind } => {
               let common = PowerupInit {
                  id: self.ids.reserve(),
                  position: spawner.container.transform.position,
                  linear_vel: Vec2::zero(),
                  team: spawner.team,
                  state: PowerupState::Spawned{ spawner },
               };

               let init = match kind {
                  PowerupKind::Wall => ComponentInit::WallPowerup(WallPowerupInit { id: self.ids.reserve(), common }),
                  PowerupKind::Missile => ComponentInit::MissilePowerup(MissilePowerupInit { id: self.ids.reserve(), common }),
                  PowerupKind::Shield => ComponentInit::ShieldPowerup(ShieldPowerupInit { id: self.ids.reserve(), common }),
                  PowerupKind::Health => ComponentInit::HealthPowerup(HealthPowerupInit { id: self.ids.reserve(), common, health: 100 }),
                  // TODO:
                  _ => ComponentInit::HealthPowerup(HealthPowerupInit { id: self.ids.reserve(), common, health: 100 }),
               };

               self.ev(WorldEv::CreateComponent { init }, res, -1);
            }
            ModelEv::TurretShoot { turret, orientation, lifetime } => {
               let init = ComponentInit::TurretBullet(TurretBulletInit {
                  id: self.ids.reserve(),
                  common: BulletInit {
                     id: self.ids.reserve(),
                     team: turret.team,
                     lifetime,
                     orientation,
                     position: turret.bullet_origin(orientation),
                     linear_vel: turret.bullet_velocity(orientation),
                  }
               });

               self.ev(WorldEv::CreateComponent { init }, res, -1);

               /*
               let mut bullet = Bullet::new(res, &self.storage);
               turret.init_bullet(&mut bullet, orientation, lifetime);
               self.bullets.push(bullet);
               */
            }
            _ => (),

            /*
            ModelEv::PowerupCollide { pos, vel } => {
               let t = self.storage.insert(ShatteredPowerupContainer::new(&self.storage, res, pos, vel * 0.8));
               self.shattered_powerup_containers.push(t);
            }
            */
         }
      }
   }
}