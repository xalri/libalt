use std::net::SocketAddr;

use esc::net::id::*;
use esc::encode::NestedData;
use esc::geom::*;
use esc::util::time::*;
use esc::net::sequencer::*;
use esc::error::*;
#[allow(unused_imports)] // TODO: rust bug? import is used for macros.
use log::*;

use proto::*;
use model::world::MapDescriptor;
use model::state::*;
use model::events::*;

/// Handles communications with a single client.
///
/// Clients can be in one of two states, represented by the type parameter.
/// On accepting a join request, we create a `Client<()>`, with no state. On
/// sending a load request it becomes `Client<Loading>`, then when the player
/// has joined a game it's `Client<Playing>`.
pub struct Client<S> {
   pub id: ClientID,
   pub counter: u8,
   /// A number identifying the player within a game.
   pub player_no: i16,
   /// The client's address
   pub addr: SocketAddr,
   /// The client's state (loading, playing, etc)
   s: S,
}

/// Client state for loading a map.
pub struct Loading {
   /// The player's currently loading resource
   resource: MapDescriptor,
}

/// Client state for participating in a game.
pub struct Playing {
   /// Sequences events.
   sequencer: Sequencer<ClientEv, ServerEv>,
   /// Incoming component updates which are queued to run on the next tick.
   /// Must only be decoded after all events have been processed.
   state_in: Option<NestedData>,
   /// Component updates to send in the next message
   state_out: Vec<ComponentState>,
   /// The number of game ticks for which the client has been connected.
   tick: u64,
   /// The number of updates written since we last received a message.
   unresponsive: u64,
   /// Manages client local IDS.
   id_map: RemoteIDs,
   /// The time we last sent a `Tick` event.
   tick_sync_timer: u64,
   map_size: Vec2,
}

impl<T> Client<T> {
   /// Create a new `Client`
   pub fn new(addr: SocketAddr, id: ClientID, player_no: i16) -> Client<()> {
      Client { addr, id, s: (), counter: 0, player_no }
   }

   /// Start loading a map, discards any current state.
   pub fn load(self, resource: MapDescriptor) -> Client<Loading> {
      let counter = if self.counter < 3 { self.counter + 1 } else { 0 };
      Client {
         s: Loading{ resource },
         counter,
         addr: self.addr,
         id: self.id,
         player_no: self.player_no,
      }
   }

   /// Get the address of the client.
   pub fn addr(&self) -> SocketAddr { self.addr }

   /// Get the client's player number.
   pub fn player_no(&self) -> i16 { self.player_no }
}

impl Client<Loading> {
   /// Join the game.
   pub fn play(self, id_map: RemoteIDs, map_size: Vec2) -> Client<Playing> {
      Client {
         s: Playing {
            sequencer: Sequencer::new(self.counter),
            tick: 0,
            unresponsive: 0,
            id_map,
            tick_sync_timer: precise_time_ns(),
            state_in: None,
            state_out: vec![],
            map_size,
         },
         counter: self.counter,
         addr: self.addr,
         id: self.id,
         player_no: self.player_no,
      }
   }

   /// Get the resource for the game when the load started.
   pub fn resource(&self) -> MapDescriptor { self.s.resource.clone() }
}

impl Client<Playing> {
   /// Handle an incoming message
   pub fn receive(&mut self, mut data: GameMessage) {
      self.s.unresponsive = 0;
      let up_to_date = self.s.sequencer.receive(&mut data.events);
      self.s.state_in = if up_to_date { Some(data.state) } else { None }
   }

   /// Parse and take the next incoming event from the sequencer, returns `None` if there are none.
   pub fn next_ev(&mut self) -> Option<ClientEv> {
      let rule = unsafe { self.ev_rule() };
      self.s.sequencer.next_ev(rule)
   }

   /// Send an event to this client.
   pub fn send_ev(&mut self, ev: &ServerEv) {
      let rule = unsafe { self.ev_rule() };
      self.send_ev_with_rule(ev, rule);
   }

   pub fn send_ev_with_rule(&mut self, ev: &ServerEv, rule: EventRule) {
      debug!("sending event {:?}", ev);
      let res = self.s.sequencer.add_event(rule, ev);
      if let Err(e) = res {
         error!("Failed to encode event: {} ({:?})", e, ev);
      }
   }

   pub fn id_map(&mut self) -> &mut RemoteIDs {
      &mut self.s.id_map
   }

   /// Create a rule for encoding/decoding events
   pub unsafe fn ev_rule(&self) -> EventRule {
      EventRule {
         ptr_rule: self.s.id_map.ptr_rule(),
         map_size: self.s.map_size,
         id_rule: NetIDRule { local_to_global: None },
      }
   }

   pub fn send_state(&mut self, s: ComponentState) {
      self.s.state_out.push(s);
   }

   /// Decode any incoming component state
   pub fn decode_state(&mut self, components: &mut Vec<ComponentState>) {
      let rule = unsafe{ self.ev_rule() };
      read_component_state(&mut self.s.state_in, components, rule).unwrap()
   }

   /// Create the next message to be sent to the client.
   pub fn tick(&mut self) -> Result<GameMessage> {
      self.s.unresponsive += 1;

      // Every second send a ServerEv::Tick
      if precise_time_ns() - self.s.tick_sync_timer > seconds_to_ns(1.0) {
         self.s.tick_sync_timer = precise_time_ns();
         let tick = self.s.tick as u32;
         self.send_ev(&ServerEv::Tick(tick));
      }

      // Create a new message to send to the client.
      let events = self.s.sequencer.make_msg()?;
      let state = write_component_state(&self.s.state_out, unsafe{ self.ev_rule() }).unwrap();
      self.s.state_out.clear();

      self.s.tick += 1;
      Ok(GameMessage{ events, state })
   }

   /// Check if the client is unresponsive.
   pub fn unresponsive(&self) -> bool {
      self.s.unresponsive > 150
   }

   /*
   /// Send the player an `Init` event, gives their player number and initial team.
   /// The given team is that which the player will first attempt to spawn with, not
   /// the team they're assigned to (which is probably the spectator team).
   pub fn init(&mut self, team: Team) {
      let player = self.s.player_no;
      self.add_event(&Event::AssignPlayerID(AssignPlayerID { player, team }));
   }

   /// Update the world from post-state data sent by the client. Runs between
   /// the arena's update and the sending of game messages.
   pub fn run_post_state(&mut self) {
      self.s.entities_in.clear();
      let post_state = self.s.post_state.drain(..).collect::<Vec<_>>();
      for x in post_state {
         let log = self.log.new(o!("msg_id" => x.msg_id));
         match x.process(&log, self.map_size(), |id| Ok(self.s.view.entity_mut(id)?)) {
            Ok(ids) => {
               for (id, apply) in ids {
                  if apply { self.s.entities_in.push(id); }
               }
            }
            Err(e) => error!(log, "Failed to parse post state: {}", e),
         }
      }
   }

   /// Add some entities to be updated in the next packet we send.
   pub fn update_entities(&mut self, ids: &[EntityID]) {
      self.s.entities_out.extend_from_slice(ids)
   }

   /// Forget about an entity, allowing the ID we assigned to be re-used.
   /// This should only be called some time _after_ an entity is queued to be
   /// removed, when the arena has actually deleted the entity.
   pub fn forget_entity(&mut self, id: EntityID) {
      self.s.id_map.free_id(id);
   }

   /// Remove an entity from the client's view -- note that this does not
   /// delete the entity itself.
   pub fn remove_entity(&mut self, id: EntityID) {
      self.s.view.forget(id);
      self.add_event(&Event::RemoveEntity(id));
   }

   /// Send the client an updated `ServerConf`.
   pub fn update_conf(&mut self, conf: &ServerConf) {
      if &self.conf != conf {
         self.add_event(&Event::ServerConfig(conf.clone()));
         self.conf = conf.clone();
      }
   }

   /// Send an existing entity to the client.
   pub fn link_entity(&mut self, id: EntityID) {
      self.s.view.link_entity(id);
      self.s.id_map.link_entity(id);
   }
   */
}
