//! A place for clients while they're loading a map.

use std::time::*;
use std::collections::HashMap;
use std::net::SocketAddr;
use std::vec::Drain;

#[allow(unused_imports)] use log::*;
use reffers::rc::*;
use esc::net::socket::Sender;

use proto::*;
use model::world::MapDescriptor;

use client::*;

/// An exit point from `Limbo`. Each destination must be a single
/// game -- it's map must be constant so that the resource we ask the
/// client to load is still valid when they join.
pub trait Destination {
   /// The resource the client must load before joining.
   fn descriptor(&self) -> MapDescriptor;

   /// Add a new client
   fn add_client(&mut self, client: Client<Loading>);
}

/// A place `Client`s go while loading a map.
pub struct Limbo<T> {
   loading: HashMap<SocketAddr, Soul<T, Loading>>,
   sender: Sender<Message>,
   lost: Vec<Lost>,
}

/// A `Client` awaiting transfer to their destination.
struct Soul<T, S> {
   client: Client<S>,
   keepalive: Instant,
   keepalive_sent: Instant,
   destination: Weak<T>,
}

/// An error which can occur with a client loading a map.
pub enum Lost {
   /// The client disconnected while loading a map.
   Disconnected(Client<Loading>),
   /// The client timed out before completing.
   Timeout(Client<Loading>),
   /// The client finished loading the map but the destination no longer exists.
   Lost(Client<Loading>),
}

impl<T: Destination + 'static> Limbo<T> {
   /// Create a new `Limbo`
   pub fn new(sender: Sender<Message>) -> Limbo<T> {
      Limbo {
         loading: HashMap::with_capacity(16),
         sender,
         lost: Vec::with_capacity(4),
      }
   }

   /// Check for unresponsive clients. Returns an iterator over the collection of
   /// clients who didn't reach their destination.
   pub fn lost(&mut self) -> Drain<Lost> {
      let mut timeout = Vec::new();
      let mut keepalive = Vec::new();
      for (addr, c) in &self.loading {
         if c.keepalive.elapsed() > Duration::from_secs(20) { timeout.push(*addr); }
         if c.keepalive_sent.elapsed() > Duration::from_secs(2) { keepalive.push(*addr); }
      }

      for addr in timeout {
         self.lost.push(Lost::Timeout(self.loading.remove(&addr).unwrap().client));
      }

      for addr in keepalive {
         self.sender.send(Message::new_order(addr, MessageData::Join(Join::KeepAlive)));
      }

      self.lost.drain(..)
   }

   /// Add a new client. Panics if the given destination has already been dropped.
   pub fn add_client<S>(&mut self, client: Client<S>, destination: Weak<T>) {
      self.load(Soul {
         client,
         keepalive: Instant::now(),
         keepalive_sent: Instant::now(),
         destination,
      });
   }

   /// Process an incoming message.
   pub fn process(&mut self, msg: &Message) {
      let addr = msg.addr();
      if !self.loading.contains_key(&addr) {
         return;
      }

      match *msg.data() {
         MessageData::Join(Join::Disconnect) => {
            self.lost.push(Lost::Disconnected(self.loading.remove(&addr).unwrap().client));
         }
         MessageData::Join(Join::KeepAlive) => {
            self.loading.get_mut(&addr).unwrap().keepalive = Instant::now();
         }
         MessageData::Join(Join::LoadResp{ ref resource }) => {
            let soul = self.loading.remove(&addr).unwrap();


            if let Ok(mut dest) = soul.destination.try_get_mut() {
               if dest.descriptor() == *resource {
                  dest.add_client(soul.client);
               } else {
                  warn!("Destination's resource has changed, resending load request");
                  self.load(soul);
               }
            } else {
               self.lost.push(Lost::Lost(soul.client));
            }
         }
         _ => (),
      }
   }

   fn load<S>(&mut self, soul: Soul<T, S>) {
      let addr = soul.client.addr();
      info!("Sending load request {}", addr);

      let resource = soul.destination.get().descriptor();

      let client = soul.client.load(resource.clone());
      let counter = client.counter;

      let soul = Soul {
         client,
         destination: soul.destination,
         keepalive: soul.keepalive,
         keepalive_sent: soul.keepalive_sent,
      };

      let data = Join::Load{ resource, counter };

      self.sender.send(Message::new_order(addr, MessageData::Join(data)));
      self.loading.insert(addr, soul);
   }
}
