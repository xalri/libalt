#![feature(rust_2018_preview)]

extern crate simple_logger;
extern crate log;
extern crate esc;
extern crate altx;
extern crate model;
extern crate proto;
extern crate server;
extern crate reffers;

use std::time::Duration;

use reffers::rc::*;
use log::*;
use esc::net::socket::select;
use esc::net::socket::*;
use esc::resources::*;

use model::world::*;
use altx::*;
use proto::*;
use server::state::*;
use server::limbo::*;
use server::client::*;

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();

   let mut socket = Socket::<MessageData, _>::new("0.0.0.0:27277", MessageDataRule).unwrap();

   let (out_s, out_r) = unbounded();
   let mut pings = Pings::new(out_s.clone());

   let root = Resources::new(ROOT_DIR);
   let res = AtlasSource::new(root.subdir(RESOURCE_DIR)).unwrap();
   let maps = root.subdir(MAP_DIR);

   let arena = Arena::load(&maps, &res, "tbd_cave.altx").unwrap();

   let mut limbo = Limbo::new(out_s.clone());
   let state = Strong::new(ServerState::new(arena, out_s.clone()));

   info!("Starting main loop");
   let tick = tick(Duration::new(0, 33_333_333));
   loop {
      select! {
         recv(out_r, msg) => {
            socket.connections.send(msg.unwrap());
         }
         recv(socket.raw_rx, msg) => {
            socket.connections.recv(msg.unwrap());
         }
         recv(socket.incoming_rx, msg) => {
            let msg = msg.expect("Socket closed unexpectedly");
            match msg.data.as_ref().unwrap().clone() {
               MessageData::Ping(data) => pings.process(data, msg.addr),
               MessageData::Join(Join::Request(JoinRequest { id, .. })) => {
                  limbo.add_client(Client::<()>::new(msg.addr, id, 0), state.get_weak());
               }
               MessageData::Join(Join::Disconnect) => {
                  state.get_mut().remove_client(msg.addr, "Disconnected");
               }
               _ => (),
            }
            limbo.process(&msg);
            state.get_mut().process(&msg, &res);
         }
         recv(tick) => {
            pings.update();
            socket.connections.update();
            state.get_mut().tick(&res);
         }
      }
   }
}
